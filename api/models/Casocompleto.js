"use strict";

const { Modelo } = require("./Modelo");
const Funcionario = require("../models/Funcionario");
const Usuario = require("../models/Usuario");
const Archivos_caso = require("../models/Archivos_caso");
const Postulado = require("../models/Postulado");

class Caso extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "caso";
    }

    $afterGet(queryContext) {
        return this.ag();
    }

    async funcionario() {
        if(this.id_funcionario){
            var fun = await Funcionario.query()
                .where("id", this.id_funcionario);
            if (fun[0]) var funcionario = fun[0];
            else var funcionario = { nom_fun: '' };
            this.funcionario = funcionario;
        }
    }
    async usuario() {
        if(this.id_usuario){
            var usu = await Usuario.query()
                .where("id", this.id_usuario)
                .select("id","nom_usu","ape_usu","imagen","cor_usu", "rol");
            if (usu[0]) var usuario = usu[0];
            else var usuario = { nom_usu: '' };
            this.usuario = usuario;
        }
    }
    async abogado() {
        if(this.id_abogado){
            var usu = await Usuario.query()
                .where("id", this.id_abogado)
                .select("id","nom_usu","ape_usu","imagen","cor_usu", "rol","num_doc_usu","imagen_firma","mat_usu");
            if (usu[0]) var abogado = usu[0];
            else var abogado = { nom_usu: '' };
            this.abogado = abogado;
        }
    }
    async comentarios() {
        if(this.id){
            var com = await Postulado.query()
                .where({ "id_caso": this.id, "tipo": 1, "est_pos": 2});
            if (com[0]) var comentario = com;
            else var comentario = {};
            this.comentarios = comentario;
        }
    }

    async montor() {
        if(this.id){
            var mon = await Postulado.raw(`
             select * , 
             (select COALESCE(SUM(t.monto),0) from transacciones t where t.id_caso=p.id_caso and t.tipo=1) as recaudo,
             (select COALESCE(SUM(tr.monto),0) from transacciones tr, caso c  where c.id=tr.id_caso and c.id_abogado=tr.id_usuario and tr.id_caso=p.id_caso and tr.tipo=2) as pagado,
             (select pp.pre_pos_2 from transacciones tr, caso c , postulado pp where pp.id_usuario=c.id_abogado and pp.id_caso=c.id and pp.tipo=2 and c.id=tr.id_caso and c.id_abogado=tr.id_usuario and tr.id_caso=p.id_caso and tr.tipo=2 group by p.id_caso) as a_pagar
             from postulado p
             where p.id_caso = ? and p.tipo = ? and p.est_pos = ?;`, [this.id, 1, 2]);

            if (mon[0][0]) var monto = mon[0][0].recaudo;
            else var monto = {};
            this.montor = monto;
        }
    }

    async unidos() {
        if(this.id){
            var uni = await Postulado.query().where({ "id_caso": this.id, "tipo": 1, "est_pos": 2});
            
            if (uni[0]) var unido = uni;
            else var unido = {};
            this.unidos = unido;
        }
    }

    

    async ag() {
        await this.funcionario();
        await this.usuario();
        await this.abogado();
        await this.comentarios();
        await this.unidos();
        await this.montor();
    }
}

module.exports = Caso;
