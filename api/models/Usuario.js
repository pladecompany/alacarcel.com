"use strict";

const { Modelo } = require("./Modelo");
const Paises = require("../models/Pais");
const Estado = require("../models/Estado");
const Ciudad = require("../models/Ciudad");

const QueryBuilder = require("objection").QueryBuilder;

class UserQueryBuilder extends QueryBuilder {
    doctores(model) {
        if (model.id) {
            return this.update(model).where("id", model.id);
        } else {
            return this.insert(model);
        }
    }
}

class Usuario extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "usuarios";
    }

    //Buscador de query personalizado
    static get QueryBuilder() {
        return UserQueryBuilder;
    }
}

module.exports = Usuario;
