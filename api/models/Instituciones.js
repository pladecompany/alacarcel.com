"use strict";

const { Modelo } = require("./Modelo");

class Instituciones extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "instituciones";
    }

}

module.exports = Instituciones;
