"use strict";

const { Modelo } = require("./Modelo");
const Usuario = require("../models/Usuario");
const Caso = require("../models/Caso");

class Transacciones extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "transacciones";
    }

    $afterGet(queryContext) {
        return this.ag();
    }

    async usuario() {
        if(this.id_usuario){
            var usu = await Usuario.query()
                .where("id", this.id_usuario)
                .select("id","nom_usu","ape_usu","imagen","cor_usu", "rol", "nic_usu");
            if (usu[0]) var usuario = usu[0];
            else var usuario = { nom_usu: '' };
            this.usuario = usuario;
        }
    }
    async caso() {
        if(this.id_caso){
            var cas = await Caso.query()
                .where("id", this.id_caso)
                .select("nom_caso");
            if (cas[0]) var caso = cas[0];
            else var caso = {};
            this.caso = caso;
        }
    }

    async ag() {
       	await this.usuario();
       	await this.caso();
    }

}

module.exports = Transacciones;
