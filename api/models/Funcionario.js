"use strict";

const { Modelo } = require("./Modelo");
const Instituciones = require("../models/Instituciones");

class Funcionarios extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "funcionario";
    }

    $afterGet(queryContext) {
        return this.ag();
    }

    async instituciones() {
        var ins = await Instituciones.query()
            .join('estado', 'estado.id', 'instituciones.id_estado')
            .join('ciudad', 'ciudad.id', 'instituciones.id_ciudad')
            .where("instituciones.id", this.id_institucion)
            .select("instituciones.nom_ins","estado.nombre as nombre_estado","ciudad.nombre");
        if (ins[0]) var institucion = ins[0];
        else var institucion = { nom_usu: '' };
        this.institucion = institucion;
    }

    async ag() {
        await this.instituciones();
    }
}

module.exports = Funcionarios;
