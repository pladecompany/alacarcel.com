"use strict";

const { Modelo } = require("./Modelo");

class Admin extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "admins";
    }

}

module.exports = Admin;
