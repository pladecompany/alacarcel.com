"use strict";

const { Modelo } = require("./Modelo");

class Notificacion extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "notificacion";
    }

}

module.exports = Notificacion;
