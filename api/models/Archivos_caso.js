"use strict";

const { Modelo } = require("./Modelo");

class ArchivosCaso extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "archivos_caso";
    }

}

module.exports = ArchivosCaso;
