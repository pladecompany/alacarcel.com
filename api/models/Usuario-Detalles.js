"use strict";

const { Modelo } = require("./Modelo");
const Paises = require("../models/Pais");
const Estado = require("../models/Estado");
const Ciudad = require("../models/Ciudad");

const QueryBuilder = require("objection").QueryBuilder;

class UserQueryBuilder extends QueryBuilder {
    doctores(model) {
        if (model.id) {
            return this.update(model).where("id", model.id);
        } else {
            return this.insert(model);
        }
    }
}

class Usuario extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "usuarios";
    }
    $afterGet(queryContext) {
        return this.ag();
    }

    async pais() {
        var dato = await Paises.query()
            .where("id", this.id_pais)
            .select("nombre");
        if (dato[0]) this.nom_pais = dato[0].nombre;
        else this.nom_pais = '';
    }
    async estado() {
        var dato = await Estado.query()
            .where("id", this.id_estados)
            .select("nombre");
        if (dato[0]) this.nom_estado = dato[0].nombre;
        else this.nom_estado = '';
    }
    async ciudad() {
        var dato = await Ciudad.query()
            .where("id", this.id_ciudad)
            .select("nombre");
        if (dato[0]) this.nom_ciudad = dato[0].nombre;
        else this.nom_ciudad = '';
    }

    async ag() {
        await this.pais();
        await this.estado();
        await this.ciudad();
    }

    //Buscador de query personalizado
    static get QueryBuilder() {
        return UserQueryBuilder;
    }
}

module.exports = Usuario;
