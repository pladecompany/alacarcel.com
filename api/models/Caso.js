"use strict";

const { Modelo } = require("./Modelo");
const Funcionario = require("../models/Funcionario");
const Usuario = require("../models/Usuario");
const Archivos_caso = require("../models/Archivos_caso");
const Postulado = require("../models/Postulado");

class Caso extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "caso";
    }

    $afterGet(queryContext) {
        return this.ag();
    }

    async funcionario() {
        if(this.id_funcionario){
            var fun = await Funcionario.query()
                .where("id", this.id_funcionario);
            if (fun[0]) var funcionario = fun[0];
            else var funcionario = { nom_fun: '' };
            this.funcionario = funcionario;
        }
    }
    async usuario() {
        if(this.id_usuario){
            var usu = await Usuario.query()
                .where("id", this.id_usuario)
                .select("id","nom_usu","ape_usu","imagen","cor_usu", "rol");
            if (usu[0]) var usuario = usu[0];
            else var usuario = { nom_usu: '' };
            this.usuario = usuario;
        }
    }
    async abogado() {
        if(this.id_abogado){
            var usu = await Usuario.query()
                .where("id", this.id_abogado)
                .select("id","nom_usu","ape_usu","imagen","cor_usu", "rol");
            if (usu[0]) var abogado = usu[0];
            else var abogado = { nom_usu: '' };
            this.abogado = abogado;
        }
    }
    async archivos() {
        if(this.id){
            var arc = await Archivos_caso.query()
                .where("id_caso", this.id).select("visible");
            if (arc[0]) var archivo = arc;
            else var archivo = [];
            this.archivos = archivo;
        }
    }
    async comentarios() {
        if(this.id){
            var com = await Postulado.query()
                .where({ "id_caso": this.id, "tipo": 1, "est_pos": 2});
            if (com[0]) var comentario = com.length;
            else var comentario = 0;
            this.comentarios = comentario;
        }
    }

    async ag() {
        await this.funcionario();
        await this.usuario();
        await this.abogado();
        await this.archivos();
        await this.comentarios();
    }
}

module.exports = Caso;
