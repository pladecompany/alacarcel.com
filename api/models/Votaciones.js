"use strict";

const { Modelo } = require("./Modelo");

class Votaciones extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "votaciones";
    }

}

module.exports = Votaciones;
