"use strict";

const { Modelo } = require("./Modelo");
const Usuario = require("../models/Usuario");

class Postulado extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "postulado";
    }

    $afterGet(queryContext) {
        return this.ag();
    }

    async usuario() {
        if(this.id_usuario){
            var usu = await Usuario.query()
                .where("id", this.id_usuario)
                .select("nom_usu","ape_usu","imagen","num_doc_usu","imagen_firma");
            if (usu[0]) var usuario = usu[0];
            else var usuario = { nom_usu: '' };
            this.usuario = usuario;
        }
    }

    async ag() {
        await this.usuario();
    }
}

module.exports = Postulado;