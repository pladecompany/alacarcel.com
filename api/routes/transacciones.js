//llamar al modelo de Usuarios
const Caso = require("../models/Caso");
const Usuario = require("../models/Usuario");
const Postulado = require("../models/Postulado");
//llamar al modelo de Usuarios
const Transacciones = require("../models/transacciones");
const Notificaciones = require("../models/Notificacion");

const funciones = require("../socketEvents/funciones");
//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const jwt = require("jsonwebtoken");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");

const mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

const moment = require("moment");
const jsonfile = require('jsonfile');
const paypal = require("paypal-rest-sdk");
const configknex = require("../../knexfile");

var file = __dirname+ '/../configuracion.json';
var files = jsonfile.readFileSync(file);
 
var router = express.Router();
formato = {
            separador: ',', // separador para los miles
            sepDecimal: '.', // separador para los decimales
            formatear:function (num){
                num +='';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                }
                return this.simbol + splitLeft  +splitRight;
            },
            precio:function(num, simbol){
                this.simbol = simbol ||'';
                
                var num = num.toString();
                num=num.replace(/,/g, '.');
                num=Math.round(num * 100) / 100;
                
                
                return this.formatear(num.toFixed(2));
    
            }
}
router.post(
    "/webhook", upload.none(), async(req, res) => { 
        console.log(req.body);
        return AR.enviarDatos(req.body, res);
    });
router.post(
    "/process_pay", upload.none(), async(req, res) => {
        //console.log(req.body);
        var data = req.body;
        var id_transaccion = data.item_number.split("ID PAGO: ")[1];
        var id_postulado = req.query.id;
  if (data.payment_status == "Completed" || data.payment_status == "Processed") {
    const buscar = await Transacciones.query()
      .where("id_pago", data.txn_id)
      .catch(err => {
        res.send(err);
        return;
      });
     

        if (!buscar[0]) {
            if(data.payment_fee<data.payment_gross){
                // data.payment_gross = parseFloat(parseFloat(data.payment_gross)-parseFloat(data.payment_fee));
            }
            const postulad = await Postulado.query()
              .where("id", id_postulado)
              .catch(err => {
                res.send(err);
                return;
              });

            if (postulad[0]) {
                postulado = postulad[0];
            }else {
                return AR.enviarDatos(data, res);
            }  
            const cas = await Caso.query()
                .where({ id: postulado.id_caso })
                .catch(err => {
                    console.log(err);
                }); 
                if (cas[0]) {
                    caso = cas[0];
                }else {
                   return AR.enviarDatos(data, res);
                }  

                  

          const transaccion = await Transacciones.query()
                    .updateAndFetchById(id_transaccion, 
                    {
                        id_usuario: postulado.id_usuario,
                        fec_reg_pago: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                        tipo: 1,
                        id_pago: data.txn_id,
                        estatus_pago: data.payment_status,
                        monto: data.payment_gross,
                        id_caso: caso.id,
                        retiro: 0
                    })
                    .catch(err => {
                        console.log(err);
                    });

                    if (transaccion.id) {
                          
            
                await Postulado.query()
                    .updateAndFetchById(id_postulado, { est_pos:2 })
                    .catch(err => {
                        console.log(err);
                    }); 

                        await Usuario.query()
                            .where({ id: postulado.id_usuario })
                            .then(async usuario => {
                                if (usuario[0]) {
                                    usuario = usuario[0];
                                    var cuerpo = `Hola, ${usuario.nom_usu} <br>
                                                         se ha confirmado un nuevo pago <b>${"$" + formato.precio(data.payment_gross)}</b>,
                                                         relacionado al caso <b>${caso.nom_caso}</b>.
                                                        `;
                                    mailer.enviarMail({
                                        direccion: usuario.cor_usu,
                                        titulo: "Nuevo pago confirmado",
                                        mensaje: mailp({
                                            cuerpo: cuerpo,
                                            titulo: "Nuevo pago confirmado"
                                        })
                                    });
                                    const not = await Notificaciones.query()
                                        .insert({
                                            descripcion: "Nuevo pago confirmado",
                                            contenido: cuerpo,
                                            estatus: 0,
                                            id_usuario: usuario.id,
                                            tipo_noti: 3,
                                            id_usable: transaccion.id,
                                            fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                        })
                                        .then(not => {
                                            not.id_caso = caso.id;
                                            var socketReceptor = funciones.encontrarSocket(usuario.id, usuario.rol);
                                        
                                            if (socketReceptor) {
                                                global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                            }
                                        })
                                        .catch(err => {
                                            console.log(err);
                                        });

                                    
                                        if (caso) {
                                             
                                            var cuerpo = `Hola, ${caso.usuario.nom_usu} <br>
                                                             ${usuario.nom_usu} ${usuario.ape_usu} se <b>unió</b>
                                                             a su caso <b>${caso.nom_caso}</b>.
                                                             <br><br>
                                                             Comentario: <b>${postulado.des_pos}</b>
                                                            `;
                                            mailer.enviarMail({
                                                direccion: caso.usuario.cor_usu,
                                                titulo: "Un nuevo usuario se ha unido",
                                                mensaje: mailp({
                                                    cuerpo: cuerpo,
                                                    titulo: "Un nuevo usuario se ha unido"
                                                })
                                            });
                                            const not = await Notificaciones.query()
                                                .insert({
                                                    descripcion: "Un nuevo usuario se ha unido",
                                                    contenido: cuerpo,
                                                    estatus: 0,
                                                    id_usuario: caso.usuario.id,
                                                    tipo_noti: 0,
                                                    id_usable: postulado.id_usuario,
                                                    fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                                })
                                                .then(not => {
                                                    var socketReceptor = funciones.encontrarSocket(caso.usuario.id, caso.usuario.rol);
                                                
                                                    if (socketReceptor) {
                                                        global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                                    }
                                                })
                                                .catch(err => {
                                                    console.log(err);
                                                });

                                                /*Notificaciones a todos los usuarios*/

                                                const postus = await Postulado.raw(`
                                                         select p.* , u.nom_usu, u.ape_usu, u.cor_usu, u.rol
                                                         from postulado p, usuarios u
                                                         where p.id_usuario=u.id and p.id_caso = ? and p.tipo = ? and p.est_pos = ? group by p.id_usuario`, [caso.id, 1, 2])
                                                    .catch(err => {
                                                        console.log(err);
                                                    });
                                                const postusa = await Postulado.raw(`
                                                         select p.* , u.nom_usu, u.ape_usu, u.cor_usu, u.rol , 
                                                         (select COALESCE(SUM(t.monto),0) from transacciones t where t.id_caso=p.id_caso and t.tipo=1) as recaudo
                                                         from postulado p, usuarios u
                                                         where p.id_usuario=u.id and p.id_caso = ? and p.tipo = ? group by p.id_usuario having recaudo >= p.pre_pos_2`, [caso.id, 2])
                                                    .catch(err => {
                                                        console.log(err);
                                                    });
                                                
                                                 
                                    if (postus) {
                                        if(postus[0]){
                                            var unidos = postus[0];
                                            var demandantes = files.demandantes;

                                            if(unidos.length>=demandantes) {
                                                if(postusa){
                                                    if(postusa[0]) {
                                                        if(postusa[0].length>0){
                                                                console.log("Preparando alertas...");
                                                                caso.usuario.id_usuario = caso.usuario.id;
                                                                unidos.push(caso.usuario);
                                                                for (var i = 0; i < unidos.length; i++) {
                                                                     var usuario = unidos[i];
                                                                        var cuerpo = `Hola, ${usuario.nom_usu} <br>
                                                                                         es tiempo de elegir a un abogado para el
                                                                                         caso <b>${caso.nom_caso}</b>.
                                                                                                            `;
                                                                        mailer.enviarMail({
                                                                            direccion: usuario.cor_usu,
                                                                            titulo: "Que comience la votación",
                                                                            mensaje: mailp({
                                                                                cuerpo: cuerpo,
                                                                                titulo: "Que comience la votacion"
                                                                            })
                                                                        });
                                                                        const not = await Notificaciones.query()
                                                                            .insert({
                                                                                descripcion: "Que comience la votación",
                                                                                contenido: cuerpo,
                                                                                estatus: 0,
                                                                                id_usuario: usuario.id_usuario,
                                                                                tipo_noti: 4,
                                                                                id_usable: caso.id,
                                                                                fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                                                            })
                                                                            .then(not => {
                                                                                var socketReceptor = funciones.encontrarSocket(usuario.id_usuario, usuario.rol);
                                                                            
                                                                                if (socketReceptor) {
                                                                                    global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                                                                }
                                                                            })
                                                                            .catch(err => {
                                                                                console.log(err);
                                                                            });
                                                                };
                                                                await Caso.query()
                                                                .updateAndFetchById(caso.id, {sts_caso:5})
                                                                .then(async casss => {
                                                                })
                                                                .catch(err => {
                                                                    console.log(err);
                                                                });
                                                            }else {
                                                                    //avisar q no hay abogados
                                                                    console.log("Preparando alertas falta abogado...");
                                                                    caso.usuario.id_usuario = caso.usuario.id;
                                                                    unidos.push(caso.usuario);
                                                                    for (var i = 0; i < unidos.length; i++) {
                                                                         var usuario = unidos[i];
                                                                            var cuerpo = `Hola, ${usuario.nom_usu} <br>
                                                                                             debes esperar a que un abogado se postule al
                                                                                             caso <b>${caso.nom_caso}</b>, solo asi se dára inicio a la votación,
                                                                                             pronto recibirá una notificación.
                                                                                                                `;
                                                                            mailer.enviarMail({
                                                                                direccion: usuario.cor_usu,
                                                                                titulo: "Falta poco para iniciar la votación",
                                                                                mensaje: mailp({
                                                                                    cuerpo: cuerpo,
                                                                                    titulo: "Falta poco para iniciar la votación"
                                                                                })
                                                                            });
                                                                            const not = await Notificaciones.query()
                                                                                .insert({
                                                                                    descripcion: "Falta poco para iniciar la votación",
                                                                                    contenido: cuerpo,
                                                                                    estatus: 0,
                                                                                    id_usuario: usuario.id_usuario,
                                                                                    tipo_noti: 2,
                                                                                    id_usable: caso.id,
                                                                                    fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                                                                })
                                                                                .then(not => {
                                                                                    var socketReceptor = funciones.encontrarSocket(usuario.id_usuario, usuario.rol);
                                                                                
                                                                                    if (socketReceptor) {
                                                                                        global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                                                                    }
                                                                                })
                                                                                .catch(err => {
                                                                                    console.log(err);
                                                                                });
                                                                    };
                                                                    await Caso.query()
                                                                    .updateAndFetchById(caso.id, {sts_caso:1})
                                                                    .then(async casss => {
                                                                    })
                                                                    .catch(err => {
                                                                        console.log(err);
                                                                    });
                                                                 }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                    }
                                     
                            }
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    }
                    return AR.enviarDatos(transaccion, res);
        }else {
            return AR.enviarDatos(data, res);
        }
    } else {
            return AR.enviarDatos(data, res);
    }
});

router.get("/transaccion",upload.none(), sesi.validar_admin, async (req, res) => {
    data = req.query;
    sqlext = '';
    if(data.fecd && data.fech){
        sqlext = ' fec_reg_pago>="'+data.fecd+' 00:00:00" AND fec_reg_pago<="'+data.fech+' 23:59:59"';
    }
    
    const transac = await Transacciones.query().where({tipo:1}).whereRaw(sqlext).orderBy("id","desc")
      .catch(err => {
        res.send(err);
      });
  
    AR.enviarDatos(transac, res);
});

router.get("/recibidos",upload.none(), sesi.validar_usu, async (req, res) => {
    console.log(1);
    await Transacciones.raw(`
         select t.*, c.nom_caso
         from transacciones t inner join usuarios u inner join caso c
         where t.id_usuario=u.id and t.tipo=2 and t.id_caso=c.id and t.id_usuario='${req.query.ida}' order by t.id desc;`)
        .then(trans => {
            AR.enviarDatos(trans[0], res);
        });
});
router.get("/realizados",upload.none(), sesi.validar_usu, async (req, res) => {
   
    await Transacciones.raw(`
         select t.*, c.nom_caso
         from transacciones t inner join usuarios u inner join caso c
         where t.id_usuario=u.id and t.id_caso=c.id and t.id_usuario='${req.query.ida}' and t.tipo=1 order by t.id desc;`)
        .then(trans => {
            AR.enviarDatos(trans[0], res);
        });
});
router.get("/pagos-pendientes",upload.none(), sesi.validar_admin, async (req, res) => {
    const transac = await Transacciones.query().where({tipo:2}).orderBy("id","desc")
      .catch(err => {
        res.send(err);
      });
  
    AR.enviarDatos(transac, res);
});

router.post("/pago-pendiente",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {
        data = req.body;
        data.id_usuario = data.id_abogado || data.id_usuario;

        valido = true;
        let err = false;
        var Receptor =  await Usuario.query()
        .findOne("id", data.id_usuario)
        .throwIfNotFound()
        .catch(err => {
            console.log(err);
        });
        try {
           
            if (!data.id_usuario) {
                err = 'Debes seleccionar un usuario.';
            }
            else if (!data.id_caso) {
                err = 'Debes seleccionar un caso.';
            }
            else if (!data.monto) {
                err = 'Debes ingresar un monto.';
            }
            //else if (!data.ref) {
            //    err = "Debe ingresar una referencia.";
            //}
            else if (!Receptor.correo_paypal) {
                err = 'El usuario no tiene correo Paypal.';
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                 paypal.configure({
                    mode: configknex.paypal.mode,
                    client_id: configknex.paypal.client_id,
                    client_secret: configknex.paypal.secret_client
                  });
                var create_payment_json = {
                    sender_batch_header: {
                      sender_batch_id: "batch_" + moment().format("YYYY_MM_DD_HH_mm_ss"),
                      email_subject: "Tu pago fue aprobado"
                    },
                    items: [
                      {
                        recipient_type: "EMAIL",
                        amount: {
                          value: "" + data.monto + "",
                          currency: "USD"
                        },
                        receiver: Receptor.correo_paypal,
                        note: "Tu pago fue aprobado",
                        sender_item_id: "item_" + moment().format("YYYY_MM_DD_HH_mm_ss_mm")
                      }
                    ]
                  };
                    await paypal.payout.create(create_payment_json, async function(err, payment) {
                        if (err) {
                            console.log(err.response.details)
                          return AR.enviarDatos({ r: false, msg: "Paypal no proceso el pago correctamente." }, res, 200);
                        } else {
                             var inser = {
                                    id_usuario: data.id_usuario,
                                    id_caso: data.id_caso,
                                    fec_reg_pago: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                                    tipo: 2,
                                    id_pago: payment.batch_header.payout_batch_id,
                                    estatus_pago: "Completed",
                                    monto: data.monto,
                                    retiro: 0
                            };
                            if(payment.batch_header.batch_status=="SUCCESS"){
                                const pago = await Transacciones.query()
                                .insert(inser)
                                .catch(err => {
                                    console.log(err);
                                });
                                pago.r = true;
                                pago.msg = "Se registro correctamente!";
                                return AR.enviarDatos(pago, res);
                            } else if(payment.batch_header.batch_status=="PENDING" || payment.batch_header.batch_status=="PROCESSING"){
                                    inser.estatus_pago = "Pendiente de procesar";
                                    const pago = await Transacciones.query()
                                    .insert(inser)
                                    .catch(err => {
                                        console.log(err);
                                    });
                                    pago.r = true;
                                    pago.msg = "Se registro correctamente! <br> ESTATUS: " + inser.estatus_pago;
                                     await Transacciones.query().where({tipo:2, estatus_pago : "Pendiente de procesar"}).groupBy('id')
                                      .then(async t => {
                                            global.PENDINGS = t;
                                      })
                                      .catch(err => {
                                        
                                      }); 
                                    return AR.enviarDatos(pago, res);
                            } else {
                                return AR.enviarDatos({ r: false, msg: "Paypal no proceso el pago correctamente. <br> ESTATUS: " + payment.batch_header.batch_status }, res, 200);
                            }
                            
                        }
                    });
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
});
router.post("/unirse/pagar",
    archivos.none(),
    sesi.validar_usu, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        
        try {
           
            if (!data.id_usuario) {
                err = 'Debes seleccionar un usuario.';
            }
            else if (!data.id_caso) {
                err = 'Debes seleccionar un caso.';
            }
            else if (!data.monto) {
                err = 'Debes ingresar un monto.';
            }
            
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
               const pago = await Transacciones.query()
                    .insert({
                        id_usuario: data.id_usuario,
                        id_caso: data.id_caso,
                        fec_reg_pago: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                        tipo: 0,
                        id_pago: null,
                        estatus_pago: "Pendiente de procesar",
                        monto: data.monto,
                        retiro: 0
                    })
                    .catch(err => {
                        console.log(err);
                    });
                    pago.r = true;
                    pago.msg = "Se registro correctamente!";
                    AR.enviarDatos(pago, res);
            }
             
        } catch (err) {
            console.log(err);
            next(err);
        }
});
 
router.get("/buscar-casos-completados-abogado/:id", async(req, res) => {
    data = req.query;
    
    await Caso.raw(`
        select p.pre_pos_2 , c.id, c.nom_caso,
        (select COALESCE(SUM(tr.monto),0) from transacciones tr where tr.id_caso=c.id and tr.tipo=1) as recaudo,
        (select COALESCE(SUM(tr.monto),0) from transacciones tr where tr.id_caso=c.id and tr.tipo=2) as pagado
        from postulado p, caso c where p.id_caso=c.id and p.id_usuario=c.id_abogado and p.tipo=2 and c.sts_caso=2 and c.id_abogado='${req.params.id}' group by c.id having recaudo >= p.pre_pos_2;`)
    .then(async casos => {
        AR.enviarDatos( casos[0], res);
    });
    
});

router.get("/cantidad-casos-en-curso", async(req, res) => {
    await Caso.query().where({sts_caso:2}).count()
    .then(async casos => {
        AR.enviarDatos( casos, res);
    });
    
});

router.get("/delete/:id",
    archivos.none(),
    sesi.validar_admin, async(req, res) => {
        const eliminado = await Transacciones.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado) AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
        else AR.enviarDatos("0", res);
    });
module.exports = router;