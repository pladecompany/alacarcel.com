//llamar al modelo de Usuarios
const Notificaciones = require("../models/Notificacion");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const jwt = require("jsonwebtoken");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");

const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

const moment = require("moment");

var router = express.Router();
 

//Obtener usuarios
router.post('/',upload.none(),async (req, res, next) => {
    var data = req.body
    if(data.limit!=null && data.limit!="")
            var limit = data.limit;
        else 
            var limit ="5";

    var string = " ORDER BY ID DESC LIMIT "+limit+" ";
    
    if(data.limit=="todas")
        string = "";
    var notificaciones = await Notificaciones.raw('select * from notificacion where id_usuario='+data.id+' and  estatus=0'+string+';') 
  AR.enviarDatos(notificaciones[0], res);
});

router.get('/admin',upload.none(),async (req, res, next) => {
    var data = req.body
    if(data.limit!=null && data.limit!="")
            var limit = data.limit;
        else 
            var limit ="5";

    var string = " ORDER BY ID DESC LIMIT "+limit+" ";
    
    if(data.limit=="todas")
        string = "";
    var notificaciones = await Notificaciones.raw('select * from notificacion where id_usuario is null and  estatus=0'+string+';') 
  AR.enviarDatos(notificaciones[0], res);
}); 
router.post('/vista/:id',upload.none(),async (req, res, next) => {
    var datos = {
        estatus:1
    }
    await Notificaciones.query()
    .updateAndFetchById(req.params.id, datos)
    .then(notificacion => {
    notificacion.msj = "Se editó correctamente";
    AR.enviarDatos(notificacion, res);
    })
    .catch(err => {
    console.log(err);
        res.send(err);
    });
});

router.post('/count',upload.none(),async (req, res, next) => {
    data = req.body;
    var notificaciones = await Notificaciones.raw('select count(*) as count from notificacion where id_usuario='+data.id+' and  estatus=0;')
    
    AR.enviarDatos(notificaciones[0], res);
});

router.get('/count/admin',upload.none(),async (req, res, next) => {
    data = req.body;
    var notificaciones = await Notificaciones.raw('select count(*) as count from notificacion where id_usuario is null and  estatus=0;')
    
    AR.enviarDatos(notificaciones[0], res);
});


module.exports = router;
