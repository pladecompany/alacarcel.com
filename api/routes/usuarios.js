//llamar al modelo de Usuarios
const Usuario = require("../models/Usuario");

const UsuarioD = require("../models/Usuario-Detalles");


//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");
const mailer = require("../mails/Mailer");
const mail = require("../mails/recuperarPass");
const mail2 = require("../mails/confirmacion");
const mailp = require("../mails/correo");
const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
const crypto = require("crypto");
const moment = require("moment");
const { NotFoundError } = require("objection");

var router = express.Router();
router.get(
    "/verificar", upload.none(), async(req, res) => {
        data = req.query;

        updatee = {
            est_usu: 1
        };
        //console.log(data);
        await Usuario.query()
            .where("token", data.token)
            .update(updatee)
            .then(async u => {
                var usuario = {};
                if (u == 1) {
                    usuario.r = true;
                    usuario.msg = "Cuenta verificada exitosamente, ya puedes ingresar.";
                } else {
                    usuario.r = false;
                    usuario.msg = "Token vencido.";
                    return AR.enviarAuth(res, true, null, usuario);
                }

                await Usuario.query()
                    .findOne("token", data.token)
                    .throwIfNotFound()
                    .then(usuario => {
                        usuario.r = true;
                        usuario.msg = "Cuenta verificada exitosamente, ya puedes ingresar.";
                        usuario.token = uuid.v1();
                        Usuario.raw(
                            "UPDATE usuarios SET token='" + usuario.token + "' WHERE cor_usu='" + usuario.cor_usu + "';"
                        ).then(usuario => {

                        });
                        return AR.enviarAuth(res, true, null, usuario);
                    })
                    .catch(err => {
                        res.send(err);
                    });
            })
            .catch(err => {
                res.send(err);
            });
    }
);

router.post("/reenviar-token", upload.none(), async(req, res, next) => {
    data = req.body;
    valido = false;
    let err = false;
    try {

        if (!data.correo) {
            err = "Debe ingresar un correo electrónico.";
        }
        if (!validarMail(data.correo)) {
            err = 'Por favor introduzca un correo válido';
        }


        if (err) {
            return AR.enviarDatos({ r: false, msg: err }, res, 200);
        }
        const correo = await Usuario.query()
            .select('cor_usu', 'id', 'nom_usu')
            .where({ cor_usu: data.correo })
            .then(async resp => {
                if (resp.length != 0) {
                    if (resp[0].cor_usu == data.correo) {
                        console.log(resp[0].id);
                        var too = uuid.v1();
                        await Usuario.query()
                            .updateAndFetchById(resp[0].id, { token: too })
                            .catch(err => {
                                console.log(err);
                            });
                        mailer.enviarMail({
                            direccion: data.correo,
                            titulo: "trabajoenbelleza.com | Confirmar Cuenta",
                            mensaje: mail2({
                                nombre: resp[0].nom_usu,
                                email: data.correo,
                                tipo: 'usuarios',
                                id: resp[0].id,
                                token: too,
                                clave: ""
                            })
                        });
                        return AR.enviarDatos({ r: true, msg: " Verifique su cuenta de correo" }, res, 200);
                    } else {
                        return AR.enviarDatos({ r: false, msg: "No existe un usuario con este correo" }, res, 200);
                    }
                } else {
                    return AR.enviarDatos({ r: false, msg: "No existe un usuario con este correo" }, res, 200);
                }
            })
            .catch(err => {
                console.log(err);
            });
    } catch (err) {
        console.log(err);
        next(err);
    }
});
// sesi.validar_usu O validar_admin validar sesion
//Registrar un usuario
router.post("/", upload.none(), async(req, res, next) => {
    data = req.body;
    console.log(data);
    valido = true;
    let err = false;
    try {
        if (!data.password_con) {
            err = 'Por favor introduzca el confirmar contraseña';
        }
        if (!data.password_neg) {
            err = 'Por favor introduzca una contraseña';
        }
        if (!data.cor_reg) {
            err = "Debe ingresar un correo electrónico.";
        }
        if (!validarMail(data.cor_reg)) {
            err = 'Por favor introduzca un correo válido';
        }
        if (!data.mat_reg && data.tip_reg=='Abogado') {
            err = 'Por favor introduzca la Matricula';
        }
        if (!data.nic_reg && data.tip_reg=='Usuario') {
            err = 'Por favor introduzca el Nickname';
        }

        if (err) {
            return AR.enviarDatos({ r: false, msg: err }, res, 200);
        }
        const correo = await Usuario.query()
            .select('cor_usu')
            .where({ cor_usu: data.cor_reg })
            .then(resp => {
                if (resp.length != 0) {
                    if (resp[0].cor_usu == data.cor_reg) {
                        valido = false;
                        AR.enviarError('Ya existe un usuario con este correo', res, 200);
                        return;
                    }
                }
            })
            .catch(err => {
                console.log(err);
            });
            if(data.tip_reg=='Abogado'){
                const matricula = await Usuario.query().select('mat_usu').where({ mat_usu: data.mat_reg })
                .then(resp => {
                    if (resp.length != 0) {
                        if (resp[0].mat_usu == data.mat_reg) {
                            valido = false;
                            AR.enviarError('Ya existe un abogado con esta Matricula', res, 200);
                            return;
                        }
                    }
                }).catch(err => {console.log(err);});
            }else if(data.tip_reg=='Usuario'){
                const nickname = await Usuario.query().select('nic_usu').where({ nic_usu: data.nic_reg })
                .then(resp => {
                    if (resp.length != 0) {
                        if (resp[0].nic_usu == data.nic_reg) {
                            valido = false;
                            AR.enviarError('Ya existe un usuario con este Nickname', res, 200);
                            return;
                        }
                    }
                }).catch(err => {console.log(err);});
            }
        if (valido) {
            if(data.tip_reg=='Abogado'){
                matri=data.mat_reg;
                nick=null;
            }else{
                matri=null;
                nick=data.nic_reg;
            }
            const usuario = await Usuario.query()
                .insert({
                    cor_usu: data.cor_reg,
                    dir_usu: "",
                    est_usu: 0,
                    nic_usu: nick,
                    mat_usu: matri,
                    rol: data.tip_reg,
                    pas_usu: bcrypt.hashSync(data.password_neg, 8),
                    fec_reg_usu: new Date(),
                    token: uuid.v1(),
                    id_pais: 2
                })
                .catch(err => {
                    console.log(err);
                })
            let token = jwt.sign({ id: usuario.id }, config.secret, {
                expiresIn: 86400
            });
            if (usuario) {
                mailer.enviarMail({
                    direccion: data.cor_reg,
                    titulo: "alacarcel.com | Confirmar Cuenta",
                    mensaje: mail2({
                        nombre: '',
                        email: data.cor_reg,
                        tipo: 'usuarios',
                        id: usuario.id,
                        token: usuario.token,
                        clave: data.password_neg
                    })
                });


            }
            return AR.enviarAuth(res, 200, token, usuario);
        }
    } catch (err) {
        console.log(err);
        next(err);
    }
});

router.get("/", /*sesi.validar_admin,*/ async(req, res) => {
    await UsuarioD.query().where("rol", "Usuario").then(usu => {
        AR.enviarDatos(usu, res);
    });
});
router.get("/abogado", /*sesi.validar_admin,*/ async(req, res) => {
    await UsuarioD.query().where("rol", "Abogado").then(usu => {
        AR.enviarDatos(usu, res);
    });
});

router.get('/admin', upload.none(), async(req, res, next) => {
    const usuario = await Usuario.raw("select u.*,p.nombre as pais from usuarios u inner join paises p on u.id_pais=p.id order by u.id DESC;")
        .catch(err => {
            res.send(err);
        });
    AR.enviarDatos(usuario[0], res);
});

router.post('/correo', upload.none(), async(req, res, next) => {
    var data = req.body;
    valido = true;
    const correo_usuario = await Usuario.query()
        .select('cor_usu')
        .where({ cor_usu: data.correo })
        .then(resp => {
            if (resp.length != 0) {
                if (resp[0].cor_usu == data.correo) {
                    valido = false;
                    AR.enviarError('Ya existe un usuario con este correo', res, 200);
                    return false;
                }
            }
        })
        .catch(err => {
            console.log(err);
        });
     
    if (valido) {
        AR.enviarDatos({ valid: true }, res);
    }
});


//Actualizar usuarios
router.post("/edit", archivos.fields([{ name: "imagen", maxCount: 1 } ,{ name: "imagen_1", maxCount: 1 },{ name: "imagen_2", maxCount: 1 } ]), async(req, res, next) => {
    data = req.body;
    valido = true;
    let err = false;
    try {
        /*if (!data.correo) {
            err = "Debe ingresar un correo electrónico.";
        }
        if (!validarMail(data.correo)) {
            err = 'Por favor introduzca un correo válido';
        }*/
        if (!data.nombre) {
            err = 'Por favor introduzca el nombre';
        }
        if (req.files) {
            if (req.files["imagen"]) {
                data.imagen = config.rutaArchivo(req.files["imagen"][0].filename);
            }
            if (req.files["imagen_1"]) {
                data.imagen_1 = config.rutaArchivo(req.files["imagen_1"][0].filename);
            }
            if (req.files["imagen_2"]) {
                data.imagen_2 = config.rutaArchivo(req.files["imagen_2"][0].filename);
            }
        }
        if (data.imagen_firma) {
                var file = "img_firma__" + crypto.randomBytes(18).toString("hex") + "__.jpg";

                await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.imagen_firma.split(",")[1], 'base64'), 'base64', function(err) {
                    // console.log(err);
                });
                data.img_firmada = config.rutaArchivo(file);
        }


        if (err) {
            return AR.enviarDatos({ r: false, msg: err }, res, 200);
        }
        
        if (valido) {
            const datos = {
                nom_usu: data.nombre,
                ape_usu: data.apellido,
                tlf_usu: data.telefono,
                tip_doc_usu: data.tipo_doc,
                num_doc_usu: data.num,
                id_pais: 2,
                id_estados: data.id_estado,
                id_ciudad: data.id_ciudad,
                sex_usu: data.sexo,
                imagen: data.imagen,
                imagen_1: data.imagen_1,
                imagen_2: data.imagen_2,
                correo_paypal: data.correo_paypal ? data.correo_paypal : null,
                imagen_firma: data.img_firmada
            }
            if(data.status_usu==1)
                datos.est_usu=2;

            await Usuario.query()
                .updateAndFetchById(data.id, datos)
                .then(clientes => {

                    clientes.msj = "Se editó correctamente";
                    AR.enviarDatos(clientes, res);
                })
                .catch(err => {
                    console.log(err);
                    res.send(err);
                });
        }
    } catch (err) {
        console.log(err);
        next(err);
    }
});
//Actualizar usuarios
router.post("/edit-listadoadmin", archivos.fields([{ name: "imagen", maxCount: 1 } /*,{ name: "cv", maxCount: 1 }*/ ]), async(req, res, next) => {
    data = req.body;
    valido = true;
    let err = false;
    
    try {
        if (!data.correo) {
            err = "Debe ingresar un correo electrónico.";
        }
        if (!validarMail(data.correo)) {
            err = 'Por favor introduzca un correo válido';
        }
        if (!data.nombre) {
            err = 'Por favor introduzca el nombre';
        }
        if (req.files) {
            if (req.files["imagen"]) {
                data.imagen = config.rutaArchivo(req.files["imagen"][0].filename);
            }
        }

        if (err) {
            return AR.enviarDatos({ r: false, msg: err }, res, 200);
        }
        const correo = await Usuario.query()
            .select('cor_usu', 'id')
            .where({ cor_usu: data.correo })
            .then(resp => {
                if (resp.length != 0) {
                    if (resp[0].cor_usu == data.correo && resp[0].id == data.id_usu) {
                        console.log('el correo pertenece al usuario');
                    } else {
                        if (resp[0].cor_usu == data.correo) {
                            valido = false;
                            return AR.enviarError('Ya existe un usuario con este correo', res, 200);
                        }
                    }
                }
            })
            .catch(err => {
                console.log(err);
            });
            if(data.rol_usu=='abogado'){
                const matricula = await Usuario.query().select('mat_usu', 'id').where({ mat_usu: data.mat_usu })
                .then(resp => {
                    if (resp.length != 0) {
                        if (resp[0].mat_usu == data.mat_usu && resp[0].id != data.id_usu) {
                            valido = false;
                            AR.enviarError('Ya existe un abogado con esta Matricula', res, 200);
                            return;
                        }
                    }
                }).catch(err => {console.log(err);});
            }else if(data.rol_usu=='usuario'){
                const nickname = await Usuario.query().select('nic_usu', 'id').where({ nic_usu: data.nic_usu })
                .then(resp => {
                    if (resp.length != 0) {
                        if (resp[0].nic_usu == data.nic_usu && resp[0].id != data.id_usu) {
                            valido = false;
                            AR.enviarError('Ya existe un usuario con este Nickname', res, 200);
                            return;
                        }
                    }
                }).catch(err => {console.log(err);});
            }
        if (valido) {
            const datos = {
                nom_usu: data.nombre,
                ape_usu: data.ape_usu,
                tlf_usu: data.tlf_usu,
                cor_usu: data.correo,
                tip_doc_usu: data.tipo_doc,
                num_doc_usu: data.num,
                id_pais: data.id_pais,
                id_estados: data.id_estado,
                id_ciudad: data.id_ciudad,
                sex_usu: data.sexo,
                est_usu: data.sts,
                imagen: data.imagen
            }
            if(data.rol_usu=='usuario')
                datos.nic_usu=data.nic_usu;
            else
                datos.mat_usu=data.mat_usu;
            await Usuario.query()
                .updateAndFetchById(data.id_usu, datos)
                .then(usu => {
                    usu.r = true;
                    usu.msj = "Se editó correctamente";
                    AR.enviarDatos(usu, res);
                })
                .catch(err => {
                    console.log(err);
                    res.send(err);
                });
        }
    } catch (err) {
        console.log(err);
        next(err);
    }
});

//Datos de un usuario
router.get("/:id", archivos.none(), async(req, res) => {
    const usuario = await Usuario.query()
        .findById(req.params.id)
        .catch(err => {
            res.send(err);
        });

    AR.enviarDatos(usuario, res);
});

//Eliminar usuario
router.get("/delete/:id", archivos.none(), async(req, res) => {
    const eliminado = await Usuario.query()
        .delete()
        .where({ id: req.params.id })
        .catch(err => {
            console.log(err);
            res.send(err);
        });
    if (eliminado) AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
    else AR.enviarDatos("0", res);
});

router.post("/recuperar-clave", archivos.none(), async(req, res, next) => {
    data = req.body;
    try {
        if (!validarMail(data.correo)) {
            AR.enviarError('Debe ingresar un correo electrónico.', res, 200);
            return;
        }

        await Usuario.query()
            .findOne("cor_usu", data.correo)
            .throwIfNotFound()
            .then(cliente => {
                //var nuevaPass = "";
                //var caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                //for (var i = 0; i < 8; i++)
                //    nuevaPass += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
                var tok = uuid.v1()
                mailer.enviarMail({
                    direccion: cliente.cor_usu,
                    titulo: 'alacarcel.com | Recuperar Contraseña',
                    mensaje: mail({
                        titulo: 'Recuperar Contraseña',
                        nombre: cliente.nom_usu,
                        token: tok
                            //contraseña: nuevaPass
                    })
                });
                Usuario.query()
                    .patchAndFetchById(cliente.id, { pas_usu: null, token: tok })
                    .then(usr => {
                        return AR.enviarDatos({ msg: "Se ha enviado un correo a " + usr.cor_usu, user: usr }, res);
                         
                    })
                    .catch(err => {
                        //console.log(err)
                        next(err)

                    });
            })
            .catch(err => {
                
                    //console.log(err)
                   return AR.enviarDatos({ msg: "El correo no existe." }, res);
            });
    } catch (err) {
        console.log(err)
        next(err);
    }
});
router.post("/recuperar-clave-token", archivos.none(), async(req, res, next) => {
    data = req.body;

    try {
        if (!data.token) {
            AR.enviarError('Debe ingresar un correo electrónico.', res, 200);
            return;
        }
        if (!data.pass_new) {
            AR.enviarError('Por favor introduzca la nueva contraseña', res, 200);
            return;
        }
        if (!data.pass_new_con) {
            AR.enviarError('Por favor introduzca el repetir contraseña', res, 200);
            return;
        }
        await Usuario.query()
            .findOne("token", data.token)
            .throwIfNotFound()
            .then(async usuario => {
                usuario.token = uuid.v1();
                await Usuario.query().patchAndFetchById(usuario.id, { pas_usu: bcrypt.hashSync(data.pass_new_con, 8), token: usuario.token });
                return AR.enviarAuth(res, true, null, usuario);
            })
            .catch(err => {
                
                    next(err);
             
            });
    } catch (err) {

        next(err);
    }
});
router.post("/cambiar-clave", archivos.none(), async(req, res, next) => {
    data = req.body;
    try {
        if (!data.pass) {
            AR.enviarError('Por favor introduzca la contraseña actual', res, 200);
            return;
        }
        if (!data.pass_new) {
            AR.enviarError('Por favor introduzca la nueva contraseña', res, 200);
            return;
        }
        if (!data.pass_new_con) {
            AR.enviarError('Por favor introduzca el repetir contraseña', res, 200);
            return;
        }
        if (data.pass_new != data.pass_new_con) {
            AR.enviarError('Las contraseñas no coinciden', res, 200);
            return;
        }
        
            const usuario = await Usuario.query()
                .findOne("id", data.id)
                .throwIfNotFound()
                .catch(err => console.log(err));

            let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pas_usu);

            if (!passwordIsValid) {
                AR.enviarError('Su contraseña actual es incorrecta.', res, 200);
                return;
            }
            var obj = {
                pas_usu: bcrypt.hashSync(req.body.pass_new, 8)
            }
            await Usuario.query()
                .patchAndFetchById(
                    data.id, obj)
                .then(usuario => {
                    usuario.msg = "Ha cambiado su contraseña satisfactoriamente.";
                    return AR.enviarDatos(usuario, res);
                })
                .catch(err => {
                    console.log(err);
                    res.send(err);
                });
        
    } catch (err) {
        console.log(err)
        next(err);
    }
});
module.exports = router;