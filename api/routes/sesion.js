const config = require("../config");
const Admin = require("../models/Admin");
const Usuario = require("../models/Usuario"); 

var Sesion = function(){

    this.validar_usu = async function(req, res, next){

        var usu = req.body.cor_usu || req.query.cor_usu || req.body.correo || req.query.correo;
        var pas = req.body.pas_usu || req.query.pas_usu || req.body.pass || req.query.pass;
        var tok = req.body.token   || req.query.token || req.params.token;
         
        await Usuario.raw(
            "SELECT * FROM usuarios WHERE token='"+tok+"';"
        ).then(usuario => {
            if(usuario[0].length>0)
                return next();
            else
                return res.sendStatus(401);   
        })
        .catch(err => {

            return res.sendStatus(401);
        });
        

    },
    this.validar_admin = async function(req, res, next){

        var usu = req.body.cor_usu || req.query.cor_usu || req.body.correo || req.query.correo;
        var pas = req.body.pas_usu || req.query.pas_usu || req.body.pass || req.query.pass;
        var tok = req.body.token   || req.query.token;

       
        await Admin.raw(
            "SELECT * FROM admins WHERE token='"+tok+"';"
        ).then(admins => {
               if(admins[0].length>0)
                return next();
            else
                return res.sendStatus(401);   
        })
        .catch(err => {
            return res.sendStatus(401);
        });
        

    }

    this.validar_admin_usu = async function(req, res, next){

        var usu = req.body.cor_usu || req.query.cor_usu || req.body.correo || req.query.correo;
        var pas = req.body.pas_usu || req.query.pas_usu || req.body.pass || req.query.pass;
        var tok = req.body.token   || req.query.token;

        
        await Admin.raw(
            "SELECT * FROM admins WHERE token='"+tok+"';"
        ).then(async admins => {
            if(admins[0].length>0)
                return next();
            else{
                await Usuario.raw("SELECT * FROM usuarios WHERE token='"+tok+"';")
                .then(usuario => {
                    if(usuario[0].length>0)
                        return next();
                    else
                        return res.sendStatus(401);   
                })
                .catch(err => {
                    return res.sendStatus(401);
                });
            }
        })
        .catch(err => {
            return res.sendStatus(401);
        });
        

    }
}

module.exports = Sesion;
