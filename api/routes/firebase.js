//llamar al modelo
const Firebase = require("../models/Firebase");
const express = require("express");
const config = require("../knexfile");


var FCM = require('fcm-node');
var serverKey = config.production.firebase_key_server;
var fcm = new FCM(serverKey);
 
var firebase = function(){


    this.enviarPushNotification = async function(id_usuario, title, body, obj, retorno){
      
                var sql = "SELECT * FROM firebases WHERE id_usuario=?;";
                await Firebase.raw(sql
                ).then(Usuarios => {
                    
                    if(data.nFilas == 0)
                            retorno({r: false, msj: 'No envio la Notificación'});
                    else{

                        var resul = Usuarios;
                        var arreglo = [];
                        for(i = 0 ; i < resul.length; i++){
                            var fila = resul[i];
                            arreglo[i] = fila.token;
                        }
                        var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera) 
                            registration_ids: arreglo, 

                            notification: {
                                title: title, 
                                body: body ,
                                sound : "default",
                                badge: "1"
                            },
                            data: obj,
                            priority: 'high'
                        };

                        fcm.send(message, function(err, response){
                            if (err) {
                                console.log("Err Fcm: " + err);
                            } else {
                                console.log("Envio push bien");
                            }
                        });
                        retorno({r: true});
                    }
                });
    }
}

module.exports = firebase;
