//llamar al modelo de Usuarios
const Admin = require("../models/Admin");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");

const jwt = require("jsonwebtoken");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");
var uuid = require("uuid");
const mailer = require("../mails/Mailer");
const mail = require("../mails/recuperarPassAd");
const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");


var router = express.Router();

// sesi.validar_usu O validar_admin validar sesion

router.post("/add", archivos.fields([{ name: "imagen", maxCount: 1 }]), async(req, res, next) => {
    data = req.body;
    valido = true;
    let err = false;
    try {

        if (!data.correo) {
            err = "Debe ingresar un correo electrónico.";
        }
        if (!validarMail(data.correo)) {
            err = 'Por favor introduzca un correo válido';
        }
        if (!data.nombre) {
            err = 'Introduzca un nombre.'
        }
        if (!data.apellido) {
            err = 'Introduzca un apellido.'
        }
        if (req.files) {
            if (req.files["imagen"])
                data.imagen = config.rutaArchivo(req.files["imagen"][0].filename);
        }
        if (err) {
            return AR.enviarDatos({ r: false, msg: err }, res, 200);
        }
        if (valido) {
            const datos = {
                nombre: data.nombre,
                apellido: data.apellido,
                sexo: data.sexo,
                correo: data.correo,
                telefono: data.telefono,
                imagen: data.imagen,
                pass: bcrypt.hashSync("12345", 8)
            }
            await Admin.query()
                .insert(datos)
                .then(admin => {
                    admin.msj = "Perfil agregado correctamente";
                    admin.rol = 'admin'
                    AR.enviarDatos(admin, res);
                })
                .catch(err => {
                    console.log(err);
                    res.send(err);
                });
        }
    } catch (err) {
        console.log(err);
        next(err);
    }
});
//Actualizar admin
router.post("/edit", archivos.fields([{ name: "imagen", maxCount: 1 }]), async(req, res, next) => {
    data = req.body;
    valido = true;
    let err = false;
    try {

        if (!data.correo) {
            err = "Debe ingresar un correo electrónico.";
        }
        if (!validarMail(data.correo)) {
            err = 'Por favor introduzca un correo válido';
        }
        if (!data.nombre) {
            err = 'Introduzca un nombre.'
        }
        if (!data.apellido) {
            err = 'Introduzca un apellido.'
        }
        if (req.files) {
            if (req.files["imagen"])
                data.imagen = config.rutaArchivo(req.files["imagen"][0].filename);
        }
        if (err) {
            return AR.enviarDatos({ r: false, msg: err }, res, 200);
        }
        if (valido) {
            const datos = {
                nombre: data.nombre,
                apellido: data.apellido,
                sexo: data.sexo,
                correo: data.correo,
                telefono: data.telefono,
                imagen: data.imagen
            }
            await Admin.query()
                .updateAndFetchById(data.id, datos)
                .then(admin => {
                    admin.msj = "Perfil actualizado correctamente";
                    admin.rol = 'admin'
                    AR.enviarDatos(admin, res);
                })
                .catch(err => {
                    console.log(err);
                    res.send(err);
                });
        }
    } catch (err) {
        console.log(err);
        next(err);
    }
});

router.post("/cambiar-clave", archivos.none(), async(req, res, next) => {
    data = req.body;
    try {
        if (!data.pass) {
            AR.enviarError('Por favor introduzca la contraseña actual', res, 200);
            return;
        }
        if (!data.pass_new) {
            AR.enviarError('Por favor introduzca la nueva contraseña', res, 200);
            return;
        }
        if (!data.pass_new_con) {
            AR.enviarError('Por favor introduzca el repetir contraseña', res, 200);
            return;
        }
        if (data.pass_new != data.pass_new_con) {
            AR.enviarError('Las contraseñas no coinciden', res, 200);
            return;
        }
        const admin = await Admin.query()
            .findOne("id", data.id)
            .throwIfNotFound()
            .catch(err => console.log(err));

        let passwordIsValid = bcrypt.compareSync(data.pass, admin.pass);

        if (!passwordIsValid) {
            AR.enviarError('Su contraseña actual es incorrecta.', res, 200);
            return;
        }
        var obj = {
            pass: bcrypt.hashSync(req.body.pass_new, 8)
        }
        await Admin.query()
            .patchAndFetchById(
                data.id, obj)
            .then(admin => {
                admin.msg = "Ha cambiado su contraseña satisfactoriamente.";
                AR.enviarDatos(admin, res);
            })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
    } catch (err) {
        console.log(err)
        next(err);
    }
});


router.post("/recuperar-clave", archivos.none(), async(req, res, next) => {
    data = req.body;

    try {
        if (!validarMail(data.correo)) {
            AR.enviarError('Debe ingresar un correo electrónico.', res, 200);
            return;
        }

        await Admin.query()
            .findOne("correo", data.correo)
            .throwIfNotFound()
            .then(cliente => {
                 
                //var nuevaPass = "";
                //var caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                //for (var i = 0; i < 8; i++)
                //    nuevaPass += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
                var tok = uuid.v1()
                mailer.enviarMail({
                    direccion: cliente.correo,
                    titulo: 'Denunciapp | Recuperar Contraseña',
                    mensaje: mail({
                        titulo: 'Recuperar Contraseña',
                        nombre: cliente.nombre,
                        token: tok
                            //contraseña: nuevaPass
                    })
                });
                Admin.query()
                    .patchAndFetchById(cliente.id, { pass: null, token: tok })
                    .then(usr => {
                        return AR.enviarDatos({ msg: "Se ha enviado un correo a " + usr.correo, user: usr }, res);
                        return;
                    })
                    .catch(err => {
                        console.log(err)
                        next(err)

                    });
            })
            .catch(err => {
                    //console.log(err)
                    next(err);
            });
    } catch (err) {
        console.log(err)
        next(err);
    }
});
router.post("/recuperar-clave-token", archivos.none(), async(req, res, next) => {
    data = req.body;

    try {
        if (!data.token) {
            AR.enviarError('Debe ingresar un correo electrónico.', res, 200);
            return;
        }
        if (!data.pass_new) {
            AR.enviarError('Por favor introduzca la nueva contraseña', res, 200);
            return;
        }
        if (!data.pass_new_con) {
            AR.enviarError('Por favor introduzca el repetir contraseña', res, 200);
            return;
        }
        await Admin.query()
            .findOne("token", data.token)
            .throwIfNotFound()
            .then(async usuario => {
                usuario.token = uuid.v1();
                usuario.rol = 'admin';
                await Admin.query().patchAndFetchById(usuario.id, { pass: bcrypt.hashSync(data.pass_new_con, 8), token: usuario.token });
                return AR.enviarAuth(res, true, null, usuario);
            })
            .catch(err => {
                
                    next(err);
             
            });
    } catch (err) {

        next(err);
    }
});
module.exports = router;