//llamar al modelo de Usuarios
const Paises = require("../models/Pais");
const Estado = require("../models/Estado");
const Ciudad = require("../models/Ciudad");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const jwt = require("jsonwebtoken");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");

const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

const moment = require("moment");

var router = express.Router();
 


router.get('/',upload.none(),async (req, res, next) => {
    const paises = await Paises.query()
    .catch(err => {
      res.send(err);
    });
  AR.enviarDatos(paises, res);
});

router.get('/activos',upload.none(),async (req, res, next) => {
  const paises = await Paises.raw('select p.* from paises p inner join estado e inner join ciudad c on p.id=e.id_pais and e.id=c.id_estado where p.estatus=1 group by p.id order by p.nombre asc;')
  .catch(err => {
    res.send(err);
  });
AR.enviarDatos(paises[0], res);
});

//Actualizar usuarios
router.post("/edit", archivos.fields([{ name: "imagen", maxCount: 1 }]),async (req, res, next) => {
    data = req.body;
    valido = true;
    let err = false;
    try{
      
      if(!data.estatus){
        err= 'Seleccione un estatus.'
      }
      console.log(data.id)
      const paises = await Paises.raw('select p.* from paises p inner join estado e inner join ciudad c on p.id=e.id_pais and e.id=c.id_estado where p.id='+data.id+' group by p.id;')
      .catch(err => {
        res.send(err);
      });
      if(data.estatus == 1){
        console.log(paises[0])
        if(paises[0].length==0){
          err = 'No tiene estados o ciudades registradas en este país.'
        }
      }
      if(err){
          return AR.enviarDatos({r: false, msg: err},res, 200);
      }
      if(valido){
        const datos = {
            estatus: data.estatus,
          }
        await Paises.query()
          .updateAndFetchById(data.id, datos)
          .then(paises => {
            paises.msj = "Estatus actualizado correctamente";
            AR.enviarDatos(paises, res);
          })
          .catch(err => {
            console.log(err);
              res.send(err);
          });
        }
      }
    catch (err) {
      console.log(err);
    next(err);
  }
});

//Datos de un usuario
router.get("/:id",archivos.none(), async (req, res) => {
    const pais = await Paises.query()
      .findById(req.params.id)
      .catch(err => {
        res.send(err);
      });
  
    AR.enviarDatos(pais, res);
  });
  

router.get("/estado/:id",archivos.none(), async (req, res) => {
  const pais = await Estado.query()
    .where({id_pais:req.params.id})
    .catch(err => {
      res.send(err);
    });

  AR.enviarDatos(pais, res);
});

router.get("/estado_ciudad/:id",archivos.none(), async (req, res) => {
  var pais = await Estado.raw(' select e.* from estado e inner join ciudad c where e.id=c.id_estado and e.id_pais='+req.params.id+' group by e.id;')
    .catch(err => {
      res.send(err);
    });
    if(pais[0]=="undefined"){
      pais = [];
    }else{
      pais = pais[0];
    }
  AR.enviarDatos(pais, res);
});

//ciudades
router.get('/ciudades/:id',archivos.none(),async (req, res, next) => {
  var query = req.query
  
  const ciudades = await Ciudad.raw('select c.*,e.nombre as estado,p.nombre as pais,p.id as id_pais from ciudad c inner join estado e inner join paises p where e.id=c.id_estado and p.id=e.id_pais;')
  .catch(err => {
    res.send(err);
  });
  const ciudades1 = await Ciudad.raw('select c.*,e.nombre as estado,p.nombre as pais,p.id as id_pais from ciudad c inner join estado e inner join paises p where e.id=c.id_estado and p.id=e.id_pais;')
  .catch(err => {
    res.send(err);
  });
  var data = {
    ciudades:ciudades[0],
    count:ciudades1[0].length

  }
AR.enviarDatos(data, res);

});
router.get("/ciudad/:id",archivos.none(), async (req, res) => {
  const pais = await Ciudad.query()
    .where({id_estado:req.params.id})
    .catch(err => {
      res.send(err);
    });

  AR.enviarDatos(pais, res);
});
//ciudades
router.get('/ciudades-casos/:id',archivos.none(),async (req, res, next) => {
  var data = req.query;
  sqlext='';
  if(data.tipo){
        if(data.tipo==1){
          sqlext=' AND d.id_usuario='+data.ida;
        }else if(data.tipo==4){
          sqlext=' AND d.id_abogado='+data.ida;
        }else if(data.tipo==2){ //unirse a casos
          sqlext=' AND f.tipo=1 AND f.id_usuario='+data.ida;
        }else if(data.tipo==3){ //postulaciones
          sqlext=' AND f.tipo=2 AND f.id_usuario='+data.ida;
        }     
    }
    if(data.buscar){
        if(data.buscar!=''){
            sqlext+=' AND (d.nom_caso LIKE "%'+data.buscar+'%" OR b.nom_ins LIKE "%'+data.buscar+'%" OR c.nom_fun LIKE "%'+data.buscar+'%" OR c.ape_fun LIKE "%'+data.buscar+'%" OR e.nom_usu LIKE "%'+data.buscar+'%" OR e.ape_usu LIKE "%'+data.buscar+'%")';
        }
            
    }
  if (data.tipo==2 || data.tipo==3){ 
    const ciudades = await Ciudad.raw("SELECT a.*, (SELECT count(*) FROM instituciones b, funcionario c, caso d, usuarios e, postulado f WHERE a.id=b.id_ciudad AND b.id=c.id_institucion AND c.id=d.id_funcionario AND e.id=d.id_usuario AND d.id=f.id_caso "+sqlext+") as total FROM ciudad a WHERE a.id_estado="+req.params.id).catch(err => {console.log(err);});
    AR.enviarDatos(ciudades[0], res);
  }else{ 
    const ciudades = await Ciudad.raw("SELECT a.*, (SELECT count(*) FROM instituciones b, funcionario c, caso d, usuarios e WHERE a.id=b.id_ciudad AND b.id=c.id_institucion AND c.id=d.id_funcionario AND e.id=d.id_usuario "+sqlext+") as total FROM ciudad a WHERE a.id_estado="+req.params.id).catch(err => {console.log(err);});
    AR.enviarDatos(ciudades[0], res);
  }
  
}); 
router.get('/ciudades-casos-2/:id',archivos.none(),async (req, res, next) => {
  var data = req.query;
  sqlext='';
  if(data.tipo){
        if(data.tipo==1){
          sqlext=' caso.id_usuario='+data.ida;
        }else if(data.tipo==2){ //unirse a casos
          sqlext=' postulado.tipo=1 AND postulado.id_usuario='+data.ida;
        }else if(data.tipo==3){ //postulaciones
          sqlext=' postulado.tipo=2 AND postulado.id_usuario='+data.ida;
        }     
    }
    if(data.buscar){
        if(data.buscar!=''){
            if(sqlext!='')
                sqlext+=' AND';
            
            sqlext+=' (caso.nom_caso LIKE "%'+data.buscar+'%" OR instituciones.nom_ins LIKE "%'+data.buscar+'%" OR funcionario.nom_fun LIKE "%'+data.buscar+'%" OR funcionario.ape_fun LIKE "%'+data.buscar+'%" OR usuarios.nom_usu LIKE "%'+data.buscar+'%" OR usuarios.ape_usu LIKE "%'+data.buscar+'%")';
        }
            
    }

  const ciudades = await Ciudad.query()
    .where({id_estado:req.params.id})
    .catch(err => {
      res.send(err);
    });
    for (var i = 0; i < ciudades.length; i++) {
      if (data.tipo==2 || data.tipo==3) {

        const ciudades1 = await Ciudad.query()
          .join('instituciones', 'instituciones.id_ciudad', 'ciudad.id')
          .join('funcionario', 'funcionario.id_institucion', 'instituciones.id')
          .join('caso', 'caso.id_funcionario', 'funcionario.id')
          .join('usuarios', 'usuarios.id', 'caso.id_usuario')
          .join('postulado', 'postulado.id_caso', 'caso.id')
          .where({'instituciones.id_ciudad':ciudades[i].id})
          .whereRaw(sqlext)
          .catch(err => {
            res.send(err);
          });
          if(ciudades1[0])
            ciudades[i].total=ciudades1.length;
          else
            ciudades[i].total=0;
      }else{
        const ciudades1 = await Ciudad.query()
          .join('instituciones', 'instituciones.id_ciudad', 'ciudad.id')
          .join('funcionario', 'funcionario.id_institucion', 'instituciones.id')
          .join('caso', 'caso.id_funcionario', 'funcionario.id')
          .join('usuarios', 'usuarios.id', 'caso.id_usuario')
          .where({'instituciones.id_ciudad':ciudades[i].id})
          .whereRaw(sqlext)
          .catch(err => {
            res.send(err);
          });
          if(ciudades1[0])
            ciudades[i].total=ciudades1.length;
          else
            ciudades[i].total=0;
      }
        
    }

  AR.enviarDatos(ciudades, res);

});
router.post("/ciudad", archivos.none(),async (req, res, next) => {
  data = req.body;
  valido = true;
  let err = false;
  try{
    
    if(!data.estado){
      err= 'Seleccione un Estado.'
    }
    if(!data.ciudades){
      err= 'Introduzca al menos una ciudad.'
    }
    if(err){
        return AR.enviarDatos({r: false, msg: err},res, 200);
    }
    if(valido){
      datos =  []
      for(var i = 0;i< data.ciudades.length;i++){
        datos.push({
          id_estado:data.estado,
          nombre:data.ciudades[i].ciudad
        })
      }
      console.log(datos)
      const ciudades = await Ciudad.query()
      .insertWithRelated(datos)
      .catch(err=>{
        console.log(err);
      })
      let token = jwt.sign({ id: ciudades.id }, config.secret, {
          expiresIn: 86400
        });

      AR.enviarAuth(res,200,token,ciudades);
      }
    }
  catch (err) {
    console.log(err);
  next(err);
}
});

router.post("/ciudad/edit", archivos.none(),async (req, res, next) => {
  data = req.body;
  valido = true;
  let err = false;
  try{
    
    if(!data.estado){
      err= 'Seleccione un Estado.'
    }
    if(!data.ciudades){
      err= 'Introduzca al menos una ciudad.'
    }
    if(err){
        return AR.enviarDatos({r: false, msg: err},res, 200);
    }
    if(valido){
      datos = {
        id_estado:data.estado,
        nombre:data.ciudades
      }
      
      const ciudades = await Ciudad.query()
      .updateAndFetchById(data.id, datos)
      .catch(err=>{
        console.log(err);
      })
      let token = jwt.sign({ id: ciudades.id }, config.secret, {
          expiresIn: 86400
        });

      AR.enviarAuth(res,200,token,ciudades);
      }
      }
    catch (err) {
      console.log(err);
    next(err);
  }
});
router.get("/ciudades/delete/:id",archivos.none(), async (req, res) => {
  const eliminado = await Ciudad.query()
      .delete()
      .where({ id: req.params.id })
      .catch(err => {
      console.log(err);
      res.send(err);
      });
  if (eliminado) AR.enviarDatos("Registro eliminado exitosamente", res);
  else AR.enviarDatos("0", res);
});
module.exports = router;
