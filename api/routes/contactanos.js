
const AR = require("../ApiResponser");
const express = require("express");

const mailer = require("../mails/Mailer");
const mail = require("../mails/correo");
 
const multer = require("multer");
const upload = multer();

const moment = require("moment");
const { NotFoundError } = require("objection");

var router = express.Router();
router.post("/",async (req, res, next) => { 
data = req.body;
 
			  var title_mail="¡Nuevo Contacto! " + data.asunto;
              var boby_mail= `
              	Dirección de contacto, <b>${data.correo}</b><br> 
              	Mensaje, <b>${data.mensaje}</b>
              `;
                  
              mailer.enviarMail({
                    direccion: 'contactanos@trabajoenbelleza.com',
                    titulo: "TEB | Trabajo en belleza",
                    mensaje: mail({
                        titulo: title_mail,
                        cuerpo: boby_mail
                    })
                });
    AR.enviarDatos({r:true}, res);
});
module.exports = router;