//llamar al modelo de Usuarios
const Caso = require("../models/Caso");
const Casocompleto = require("../models/Casocompleto");
const Usuario = require("../models/Usuario");
const Postulado = require("../models/Postulado");
const Archivos_caso = require("../models/Archivos_caso");
const Notificaciones = require("../models/Notificacion");
const Transacciones = require("../models/transacciones");
const Votaciones = require("../models/Votaciones");

const funciones = require("../socketEvents/funciones");
//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const jwt = require("jsonwebtoken");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");

const mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

const moment = require("moment");
const jsonfile = require('jsonfile');
var file = __dirname+ '/../configuracion.json';
var files = jsonfile.readFileSync(file);

var router = express.Router();
formato = {
            separador: ',', // separador para los miles
            sepDecimal: '.', // separador para los decimales
            formatear:function (num){
                num +='';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                }
                return this.simbol + splitLeft  +splitRight;
            },
            precio:function(num, simbol){
                this.simbol = simbol ||'';

                var num = num.toString();
                num=num.replace(/,/g, '.');
                num=Math.round(num * 100) / 100;


                return this.formatear(num.toFixed(2));

            }
}

router.post("/",
    archivos.fields([{ name: "imagen[]", maxCount: 99 }]), async(req, res, next) => {
        data = req.body;
        valido = true;
        let err = false;
        try {
            if (!data.des_caso) {
                err = 'Debes ingresar una descripción para el caso.';
            }
            if (!data.list_fun) {
                err = 'Debes seleccionar un funcionario.';
            }
            if (!data.nom_caso) {
                err = "Debe ingresar el nombre del caso.";
            }

            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                const caso = await Caso.query()
                    .insert({
                        id_usuario: data.ida,
                        id_funcionario: data.list_fun,
                        nom_caso: data.nom_caso,
                        des_caso: data.des_caso,
                        sts_caso: 1,
                        fec_reg_caso: new Date()
                    })
                    .catch(err => {
                        console.log(err);
                    })

                if (caso.id && req.files) {
                    if (req.files["imagen[]"]) {
                        imagenes = req.files["imagen[]"];
                        pimagenes = data.privaimagen;
                        for (var i = 0; i < imagenes.length; i++) {
                            indiceimagen=data.arrayimagen[i];//saber el indice del radio desde el indice del archivo
                            await Archivos_caso.query().insert({
                                id_caso: caso.id,
                                archivo: config.rutaArchivo(imagenes[i].filename),
                                nom_archivo: imagenes[i].originalname,
                                visible: pimagenes[indiceimagen],
                                tipo: imagenes[i].mimetype
                            }).catch(err => { console.log(err); });
                        }
                    }
                    //Alertas
                    await Caso.query()
                        .where({ id: caso.id })
                        .then(async caso => {
                            if (caso[0]) {
                                caso = caso[0];
                                var cuerpo = `Hola, ${caso.usuario.nom_usu} <br>
                                                     se ha publicado su caso <b>${caso.nom_caso}</b>.
                                                    `;
                                mailer.enviarMail({
                                    direccion: caso.usuario.cor_usu,
                                    titulo: "Nuevo caso publicado",
                                    mensaje: mailp({
                                        cuerpo: cuerpo,
                                        titulo: "Nuevo caso publicado"
                                    })
                                });
                                const not = await Notificaciones.query()
                                    .insert({
                                        descripcion: "Nuevo caso publicado",
                                        contenido: cuerpo,
                                        estatus: 0,
                                        id_usuario: caso.usuario.id,
                                        tipo_noti: 2,
                                        id_usable: caso.id,
                                        fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                    })
                                    .then(not => {
                                       var socketReceptor = funciones.encontrarSocket(caso.usuario.id, caso.usuario.rol);

                                        if (socketReceptor) {
                                            global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                        }
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });

                            }
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }


                caso.r = true;
                caso.msg = "Caso creado correctamente!";
                AR.enviarDatos(caso, res);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.get("/", async(req, res) => {
    data = req.query;
    if (data.op) {
        pag = data.op;
        pag--;
        pag = pag * 15;
    } else
        pag = 0;
    sqlext = '';

    if (data.id_caso) {
        sqlext = 'caso.id=' + data.id_caso;
    }

    if (data.id_ciudad && data.id_estado > 0 && !data.id_caso) {
        if (data.id_ciudad > 0)
            sqlext = 'instituciones.id_ciudad=' + data.id_ciudad;
        else if (data.id_ciudad == 0 && data.id_estado)
            sqlext = 'instituciones.id_estado=' + data.id_estado;
    }

    if (data.tipo) {
        if (data.tipo == 1) {
            if (sqlext != '')
                sqlext += ' AND caso.id_usuario=' + data.ida;
            else
                sqlext += ' caso.id_usuario=' + data.ida;
        } else if (data.tipo == 4) {
            if (sqlext != '')
                sqlext += ' AND caso.id_abogado=' + data.ida;
            else
                sqlext += ' caso.id_abogado=' + data.ida;
        } else if (data.tipo == 2) { //unirse a casos
            if (sqlext != '')
                sqlext += ' AND postulado.tipo=1';
            else
                sqlext += ' postulado.tipo=1';
        } else if (data.tipo == 3) { //postulaciones
            if (sqlext != '')
                sqlext += ' AND postulado.tipo=2';
            else
                sqlext += ' postulado.tipo=2';
        }

    }
    if (data.buscar) {
        if (data.buscar != '') {
            if (sqlext != '')
                sqlext += ' AND';

            sqlext += ' (caso.nom_caso LIKE "%' + data.buscar + '%" OR instituciones.nom_ins LIKE "%' + data.buscar + '%" OR funcionario.nom_fun LIKE "%' + data.buscar + '%" OR funcionario.ape_fun LIKE "%' + data.buscar + '%" OR usuarios.nom_usu LIKE "%' + data.buscar + '%" OR usuarios.ape_usu LIKE "%' + data.buscar + '%")';
        }

    }
    sqlext2 = sqlext;
    if (data.tipo == 2 || data.tipo == 3) {
        await Caso.query().limit(15).offset(pag)
            .join('usuarios', 'usuarios.id', 'caso.id_usuario')
            .join('postulado', 'postulado.id_caso', 'caso.id')
            .join('funcionario', 'funcionario.id', 'caso.id_funcionario')
            .join('instituciones', 'instituciones.id', 'funcionario.id_institucion')
            //.where({ 'caso.sts_caso': 0 })
            .where({ 'postulado.id_usuario': data.ida })
            .whereRaw(sqlext).orderBy("caso.id", "desc").then(async casos => {
                const total = await Caso.query()
                    .join('usuarios', 'usuarios.id', 'caso.id_usuario')
                    .join('postulado', 'postulado.id_caso', 'caso.id')
                    .join('funcionario', 'funcionario.id', 'caso.id_funcionario')
                    .join('instituciones', 'instituciones.id', 'funcionario.id_institucion')
                    //.where({ 'caso.sts_caso': 0 })
                    .where({ 'postulado.id_usuario': data.ida })
                    .whereRaw(sqlext2).count();
                AR.enviarDatos({ casos: casos, total: total }, res);
            });
    } else {
        await Caso.query().limit(15).offset(pag)
            .join('usuarios', 'usuarios.id', 'caso.id_usuario')
            .join('funcionario', 'funcionario.id', 'caso.id_funcionario')
            .join('instituciones', 'instituciones.id', 'funcionario.id_institucion')
            //.where({ 'caso.sts_caso': 0 })
            .whereRaw(sqlext).orderBy("caso.id", "desc").then(async casos => {
                const total = await Caso.query()
                    .join('usuarios', 'usuarios.id', 'caso.id_usuario')
                    .join('funcionario', 'funcionario.id', 'caso.id_funcionario')
                    .join('instituciones', 'instituciones.id', 'funcionario.id_institucion')
                    //.where({ 'caso.sts_caso': 0 })
                    .whereRaw(sqlext2).count();
                AR.enviarDatos({ casos: casos, total: total }, res);
            });
    }

});
router.get("/buscar-caso", async(req, res) => {
    await Caso.query()
        .where({ 'caso.id': req.query.idc})
        .select("id")
        .then(async casos => {
            return AR.enviarDatos(casos, res);
        });
});
router.get("/buscar-caso-id/:id", async(req, res) => {
    await Caso.query()
        .where({ 'caso.id': req.params.id})
        .select("id","nom_caso","des_caso")
        .then(async casos => {
            return AR.enviarDatos(casos, res);
        });
});
router.get("/buscar-caso-pdf/:id", async(req, res) => {
    await Casocompleto.query()
        .where({ 'id': req.params.id})
        .then(async casos => {
            return AR.enviarDatos(casos, res);
        });
});
router.get("/caso-obtener", async(req, res) => {
        await Caso.query()
            .join('usuarios', 'usuarios.id', 'caso.id_usuario')
            .join('funcionario', 'funcionario.id', 'caso.id_funcionario')
            .join('instituciones', 'instituciones.id', 'funcionario.id_institucion')
            .where({ 'caso.id': req.query.usable})
            .orderBy("caso.id", "desc").then(async casos => {
                return AR.enviarDatos(casos, res);
            });
});

router.get("/mis-unidos/usuario", async(req, res) => {
    data = req.query;
    if (data.op) {
        pag = data.op;
        pag--;
        pag = pag * 5;
    } else
        pag = 0;
    await Caso.query()
        .join('postulado', 'caso.id', 'postulado.id_caso')
        .join('usuarios', 'postulado.id_usuario', 'usuarios.id')
        .limit(5).offset(pag)
        //.where('sts_caso', 0)
        .where('usuarios.id', req.query.ida)
        .where('postulado.tipo', 1)
        .orderBy("caso.id", "desc").then(async casos => {
            const total = await Caso.query()
                //.where('sts_caso', 0)
                .select('caso.*')
                .join('postulado', 'caso.id', 'postulado.id_caso')
                .join('usuarios', 'postulado.id_usuario', 'usuarios.id')
                .where('usuarios.id', req.query.ida)
                .where('postulado.tipo', 1)
                .count();
            AR.enviarDatos({ casos: casos, total: total }, res);
        });
});
router.get("/archivos", async(req, res) => {
    data = req.query;
    usuario = await Caso.query().where('id', data.idc).select('id_usuario');
    sqlext='';
    if(usuario[0].id_usuario!=data.ida){
        if(data.tipo=='Abogado')
            sqlext=' visible >= 1';
        else
            sqlext=' visible = 1';
    }
    await Archivos_caso.query().where('id_caso', data.idc).whereRaw(sqlext).then(archivos => {
        AR.enviarDatos(archivos, res);
    });
});
router.post("/editar",
    archivos.fields([{ name: "imagen[]", maxCount: 99 }]), async(req, res, next) => {

        data = req.body;
        valido = true;
        let err = false;
        try {

            if (!data.id) { err = 'Debes enviar el id del caso'; }

            if (!data.des_caso) {
                err = 'Debes ingresar una descripción para el caso.';
            }
            if (!data.nom_caso) {
                err = "Debe ingresar el nombre del caso.";
            }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }
            var datos = {
                nom_caso: data.nom_caso,
                des_caso: data.des_caso
            };
            if (valido) {
                const caso = await Caso.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async caso => {
                        if (req.files) {
                            if (req.files["imagen[]"]) {
                                imagenes = req.files["imagen[]"];
                                pimagenes = data.privaimagen;
                                for (var i = 0; i < imagenes.length; i++) {
                                    indiceimagen=data.arrayimagen[i];//saber el indice del radio desde el indice del archivo
                                    await Archivos_caso.query().insert({
                                        id_caso: data.id,
                                        archivo: config.rutaArchivo(imagenes[i].filename),
                                        nom_archivo: imagenes[i].originalname,
                                        visible: pimagenes[indiceimagen],
                                        tipo: imagenes[i].mimetype
                                    }).catch(err => { console.log(err); });
                                }
                            }
                            imagenese = data.privaimagen_edit;
                            await imagenese.forEach( async function(valor, indice, array) {
                                await Archivos_caso.query()
                                .updateAndFetchById(indice, {visible:valor})
                                .catch(err => { console.log(err); });
                            });
                        }
                        caso.msg = "Caso actualizado con éxito";
                        caso.r = true;
                        AR.enviarDatos(caso, res);
                    })
                    .catch(err => {
                        //respuesta error
                        console.log(err);
                        return AR.enviarDatos({ msg: "Error al actualizar el caso", error: err }, res, 200);
                    })

            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.get("/delete/:id",
    archivos.none(), async(req, res) => {
        const eliminado = await Caso.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado) AR.enviarDatos({ msj: "Caso eliminado exitosamente" }, res);
        else AR.enviarDatos("0", res);
    });
router.get("/delete_archivo/:id",
    archivos.none(), async(req, res) => {
        const eliminado = await Archivos_caso.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado) AR.enviarDatos({ msj: "Archivo eliminado exitosamente" }, res);
        else AR.enviarDatos("0", res);
    });

//ACCIONES


router.post("/postularse/agregar",
    archivos.none(),
    sesi.validar_usu, async(req, res, next) => {
        data = req.body;

        valido = true;
        unido = false;
        let err = false;
        try {



            if (!data.pre_pos_3) {
                err = "Debes ingresar un precio si eres seleccionado.";
            }

            if (!data.pre_pos_2) {
                err = "Debes ingresar un precio mínimo.";
            }

            if (!data.pre_pos_1) {
                err = "Debes ingresar un precio.";
            }

            if (!data.des_pos) {
                err = 'Debes ingresar un comentario.';
            }
            if (!data.car_pos) {
                err = 'Debes ingresar los cargos.';
            }
            if (!data.id_usuario) {
                err = 'Debes ingresar un id de Abogado.';
            }
            if (!data.id_caso) {
                err = 'Debes ingresar un id de Caso.';
            }
            const existe = await Postulado.query()
                .select('id')
                .where({ id_usuario: data.id_usuario, tipo: 2, id_caso: data.id_caso })
                .then(async existe => {
                    if (existe.length != 0) {
                        err = 'Ya estas postulado a este Caso.';
                        unido = true;
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            const abogado = await Usuario.query()
                .findById(data.id_usuario)
                .catch(err => {
                    err = 'El id Abogado no existe.';
                });
            const nopueded = await Caso.query()
                .select('id')
                .where({ id: data.id_caso ,sts_caso :1 })
                .then(async nopueded => {
                    if (existe.length != 0) {
                        
                    } else {
                        err = 'Este caso no esta actualmente en Propuesta, no esta disponible para nuevas postulaciones.';
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            const nopuede = await Caso.query()
                .select('id')
                .where({ id: data.id_caso ,sts_caso :5 })
                .then(async nopuede => {
                    if (nopuede.length != 0) {
                        err = 'Este caso esta en actualmente en Votación, no esta disponible para nuevas postulaciones.';
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            

            if (err) {
                return AR.enviarDatos({ r: false, msg: err, unido: unido }, res, 200);
            }

            if (valido) {
                const postular = await Postulado.query()
                    .insert({
                        id_caso: data.id_caso,
                        id_usuario: data.id_usuario,
                        des_pos: data.des_pos,
                        car_pos: data.car_pos,
                        pre_pos_1: data.pre_pos_1,
                        pre_pos_2: data.pre_pos_2,
                        pre_pos_3: data.pre_pos_3,
                        tipo: 2,
                        fec_reg_pos: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                    })
                    .then(async postular => {
                        postular.r = true;
                        postular.msg = "Se postuló correctamente!";
                        //Alertas
                        await Caso.query()
                            .where({ id: data.id_caso })
                            .then(async caso => {
                                if (caso[0]) {
                                    caso = caso[0];
                                    var cuerpo = `Hola, ${caso.usuario.nom_usu} <br>
                                                     ${abogado.nom_usu} ${abogado.ape_usu} se <b>postuló</b>
                                                     a su caso <b>${caso.nom_caso}</b>.
                                                     <br><br>
                                                     Comentario: <b>${data.des_pos}</b><br>
                                                     Cargos: <b>${data.car_pos}</b>
                                                    `;
                                    mailer.enviarMail({
                                        direccion: caso.usuario.cor_usu,
                                        titulo: "Nuevo abogado postulado",
                                        mensaje: mailp({
                                            cuerpo: cuerpo,
                                            titulo: "Nuevo abogado postulado"
                                        })
                                    });
                                    const not = await Notificaciones.query()
                                        .insert({
                                            descripcion: "Nuevo abogado postulado",
                                            contenido: cuerpo,
                                            estatus: 0,
                                            id_usuario: caso.usuario.id,
                                            tipo_noti: 1,
                                            id_usable: data.id_usuario,
                                            fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                        })
                                        .then(not => {
                                            var socketReceptor = funciones.encontrarSocket(caso.usuario.id, caso.usuario.rol);

                                            if (socketReceptor) {
                                                global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                            }
                                        })
                                        .catch(err => {
                                            console.log(err);
                                        });
                                    await PrepararAlertas(caso.id, "3");
                                }
                            })
                            .catch(err => {
                                console.log(err);
                            });

                        return AR.enviarDatos(postular, res);
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.get("/postularse/eliminar/:id",
    archivos.none(), sesi.validar_usu, async(req, res) => {

        const eliminado = await Postulado.query()
            .delete()
            .where({ id_caso: req.params.id, tipo: 2, id_usuario: req.query.id_usuario })
            .catch(err => {
                console.log(err);
            });
        console.log(eliminado);
        if (eliminado) return AR.enviarDatos({ r: true, msg: "Se retiro exitosamente" }, res);
        else return AR.enviarDatos("0", res);
    });
router.get("/postulaciones-y-unidos/:id", /*sesi.validar_admin_usu,*/ async(req, res) => {

    const unidos = await Postulado.raw(`
             select * , count(*) as todos ,
             (select COALESCE(SUM(t.monto),0) from transacciones t where t.id_caso=p.id_caso and t.tipo=1) as recaudo,
             (select COALESCE(SUM(tr.monto),0) from transacciones tr, caso c  where c.id=tr.id_caso and c.id_abogado=tr.id_usuario and tr.id_caso=p.id_caso and tr.tipo=2) as pagado,
             (select COALESCE(pp.pre_pos_2,0) from caso c , postulado pp where pp.id_usuario=c.id_abogado and pp.id_caso=c.id and pp.tipo=2 and p.id_caso=c.id group by pp.id_caso ) as a_pagar
             from postulado p
             where p.id_caso IN (?) and p.tipo = ? and p.est_pos = ? group by p.id_caso;`, [req.query.casos, 1, 2])
        .catch(err => {
            console.log(err);
        });
    const postulados = await Postulado.raw(`
             select * , count(*) as todos
             from postulado
             where id_caso IN (?) and tipo = ? group by id_caso;`, [req.query.casos, 2])
        .catch(err => {

        });
    if (unidos && postulados) {
        return AR.enviarDatos([unidos[0], postulados[0]], res);
    } else if (unidos) {
        return AR.enviarDatos([unidos[0],
            []
        ], res);
    } else if (postulados) {
        return AR.enviarDatos([
            [], postulados[0]
        ], res);
    } else {
        return AR.enviarDatos([
            [],
            []
        ], res);
    }


});

router.get("/postulaciones-activas-abogado/:id", sesi.validar_admin_usu, async(req, res) => {

    await Postulado.raw(`
         select *
         from postulado
         where id_caso IN (?) and id_usuario = ? and tipo = ? group by id_caso;`, [req.query.casos, req.params.id, 2])
        .then(postulados => {
            return AR.enviarDatos(postulados[0], res);
        })
        .catch(err => {
            return AR.enviarDatos([], res);
        });

});
router.get("/postularse/listado/:id", sesi.validar_admin_usu, async(req, res) => {
    await Postulado.raw(`
         select p.*, u.nom_usu, u.ape_usu, u.imagen
         from postulado p inner join usuarios u inner join caso c
         where p.id_usuario=u.id and p.tipo=2 and p.id_caso=c.id and c.id='${req.params.id}' ;`)
        .then(postulados => {
            AR.enviarDatos(postulados[0], res);
        });
});

router.get("/postularse/listado-votacion/:id", sesi.validar_admin_usu, async(req, res) => {

    await Postulado.raw(`
         select p.*, u.nom_usu, u.ape_usu, u.imagen ,
         (select count(*) from votaciones v where v.id_usuario='${req.query.id_usuario}' and v.id_caso='${req.params.id}') as yavote,
         (select v.id_abogado from votaciones v where v.id_usuario='${req.query.id_usuario}' and v.id_caso='${req.params.id}') as mielegido,
         (select COALESCE(SUM(t.monto),0) from transacciones t where t.id_caso=p.id_caso and t.tipo=1) as recaudo
         from postulado p inner join usuarios u inner join caso c
         where p.id_usuario=u.id and p.tipo=2 and p.id_caso=c.id and c.id='${req.params.id}' group by p.id_usuario having recaudo >= p.pre_pos_2;`)
        .then(postulados => {

            AR.enviarDatos(postulados[0], res);
        });
});
router.post("/votar",
    archivos.none(),
    sesi.validar_usu, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        unido = false;

        try {

            if (!data.id_abogado) {
                err = 'Debes ingresar un id de Abogado.';
            }

            if (!data.id_usuario) {
                err = 'Debes ingresar un id de Usuario.';
            }
            if (!data.id_caso) {
                err = 'Debes ingresar un id de Caso.';
            }
            const existe = await Votaciones.query()
                .select('id')
                .where({ id_usuario: data.id_usuario, id_caso: data.id_caso })
                .then(async existe => {
                    if (existe.length != 0) {
                        err = 'Ya votaste para este caso.';
                        unido = true;
                    }
                })
                .catch(err => {
                    console.log(err);
                });


            const usuario = await Usuario.query()
                .findById(data.id_usuario)
                .catch(err => {
                    err = 'El id Usuario no existe.';
                });
            if (err) {
                return AR.enviarDatos({ r: false, msg: err, unido: unido }, res, 200);
            }

            if (valido) {

                const votar = await Votaciones.query()
                    .insert({
                        id_caso: data.id_caso,
                        id_usuario: data.id_usuario,
                        id_abogado: data.id_abogado,
                        fec_reg_vot: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                        valor_vot: 0
                    })
                    .then(async votar => {
                        votar.r = true;
                        votar.msg = "Votación enviada correctamente!";
                        await calculoGanador(votar);
                        return AR.enviarDatos(votar, res);
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
        } catch (err) {
            console.log(err);
            next(err);
        }

    });
async function calculoGanador(data) {
                var Pos = await Postulado.raw(`
                     select p.* , u.nom_usu, u.ape_usu, u.cor_usu, u.rol
                     from postulado p, usuarios u
                     where p.id_usuario=u.id and p.id_caso = ? and p.tipo = ? and p.est_pos = ? group by p.id_usuario`, [data.id_caso, 1, 2])
                .catch(err => {
                    console.log(err);
                });
                var Vot = await Votaciones.query()
                        .where({id_caso: data.id_caso })
                        .catch(err => {
                            console.log(err);
                        });
                var Abo  = await Postulado.raw(`
                     select p.*, u.nom_usu, u.ape_usu, u.imagen , u.cor_usu , u.rol ,
                     (select count(*) from votaciones v where v.id_abogado=p.id_usuario and v.id_caso=c.id) as misvotos
                     from postulado p inner join usuarios u inner join caso c
                     where p.id_usuario=u.id and p.tipo=2 and p.id_caso=c.id and c.id='${data.id_caso}' ;`)
                    .catch(err => {
                        console.log(err);
                    });
                if(!Pos[0] || !Vot[0] || !Abo[0]) {
                    return true;
                }
                Pos = Pos[0];
                Abo = Abo[0];

                 await Caso.query()
                        .join('usuarios', 'usuarios.id', 'caso.id_usuario')
                        .join('funcionario', 'funcionario.id', 'caso.id_funcionario')
                        .join('instituciones', 'instituciones.id', 'funcionario.id_institucion')
                        .where({ 'caso.id': data.id_caso})
                        .orderBy("caso.id", "desc").then(async casos => {
                            if(casos[0]) {
                                var caso = casos[0];
                                var fijo = parseFloat(files.voto);
                                if(fijo<=0){
                                    fijo = 0.1;
                                }
                                if(Vot.length>=(Pos.length+1)){
                                    var participantes = [];
                                    var ganadores = [];
                                    for (var i = 0; i < Abo.length; i++) {
                                        var abogado = Abo[i];
                                        var posibilidad = ((abogado.misvotos*(100-fijo))/Vot.length);

                                        for (var j = 0; j < Vot.length; j++) {
                                            var votos = Vot[j];
                                            if(votos.id_abogado==abogado.id_usuario){
                                                if(caso.id_usuario==votos.id_usuario) {
                                                    posibilidad = (posibilidad+fijo);
                                                }
                                            }
                                        };

                                        abogado.posibilidad = posibilidad;
                                        ganadores.push(abogado);
                                        participantes.push(posibilidad);
                                    };
                                     function comparar ( a, b ){ return b - a; }
                                     participantes = participantes.sort( comparar );
                                     var p = participantes[0];
                                     for (var i = 0; i < participantes.length; i++) {

                                         var g = ganadores[i];
                                         if(p==g.posibilidad) {
                                            //encontramos un ganador

                                            var cuerpo = `Hola, ${g.nom_usu} <br>
                                                             eres el mas votado para respresentar el caso <b>${caso.nom_caso}</b>. <br>
                                                             Queremos saber tu respuesta...
                                                            `;
                                            mailer.enviarMail({
                                                direccion: g.cor_usu,
                                                titulo: "Eres el mas votado",
                                                mensaje: mailp({
                                                    cuerpo: cuerpo,
                                                    titulo: "Eres el mas votado"
                                                })
                                            });
                                            const not = await Notificaciones.query()
                                                .insert({
                                                    descripcion: "Eres el mas votado",
                                                    contenido: cuerpo,
                                                    estatus: 0,
                                                    id_usuario: g.id_usuario,
                                                    tipo_noti: 5,
                                                    id_usable: caso.id,
                                                    fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                                })
                                                .then(not => {
                                                    var socketReceptor = funciones.encontrarSocket(g.id_usuario, g.rol);

                                                    if (socketReceptor) {
                                                        global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                                    }
                                                })
                                                .catch(err => {
                                                    console.log(err);
                                                });
                                            return true;
                                            // fin encontrado
                                         }
                                     };
                                }
                            }
                        })
                        .catch(err => {
                            console.log(err);
                        });
}
router.post("/unirse/agregar",
    archivos.none(),
    sesi.validar_usu, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        unido = false;

        try {

            if (!data.des_pos) {
                err = 'Debes ingresar una descripción.';
            }

            if (!data.id_usuario) {
                err = 'Debes ingresar un id de Usuario.';
            }
            if (!data.id_caso) {
                err = 'Debes ingresar un id de Caso.';
            }
            const existe = await Postulado.query()
                .select('id')
                .where({ id_usuario: data.id_usuario, tipo: 1, id_caso: data.id_caso })
                .then(async existe => {
                    if (existe.length != 0) {
                        err = 'Ya te uniste a este Caso.';
                        unido = true;
                    }
                })
                .catch(err => {
                    console.log(err);
                });


            const usuario = await Usuario.query()
                .findById(data.id_usuario)
                .catch(err => {
                    err = 'El id Usuario no existe.';
                });

            const nopueded = await Caso.query()
                .select('id')
                .where({ id: data.id_caso ,sts_caso :1 })
                .then(async nopueded => {
                    if (existe.length != 0) {
                        
                    } else {
                        err = 'Este caso no esta actualmente en Propuesta, no esta disponible para unirse.';
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            const nopuede = await Caso.query()
                .select('id')
                .where({ id: data.id_caso ,sts_caso :5 })
                .then(async nopuede => {
                    if (nopuede.length != 0) {
                        err = 'Este caso esta en actualmente en Votación, no esta disponible para unirse.';
                    }
                })
                .catch(err => {
                    console.log(err);
                });

            if (err) {
                return AR.enviarDatos({ r: false, msg: err, unido: unido }, res, 200);
            }

            if (valido) {
                const postular = await Postulado.query()
                    .insert({
                        id_caso: data.id_caso,
                        id_usuario: data.id_usuario,
                        des_pos: data.des_pos,
                        pre_pos_1: 0,
                        pre_pos_2: 0,
                        pre_pos_3: 0,
                        tipo: 1,
                        est_pos: 1,
                        fec_reg_pos: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                    })
                    .then(async postular => {
                        postular.r = true;
                        postular.msg = "Se unió correctamente!";
                        return AR.enviarDatos(postular, res);
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });
router.get("/unirse/eliminar/:id",
    archivos.none(), sesi.validar_usu, async(req, res) => {

        const eliminado = await Postulado.query()
            .delete()
            .where({ id_caso: req.params.id, tipo: 1, id_usuario: req.query.id_usuario })
            .catch(err => {
                console.log(err);
            });
        console.log(eliminado);
        if (eliminado) return AR.enviarDatos({ r: true, msg: "Se retiro exitosamente" }, res);
        else return AR.enviarDatos("0", res);
    });

router.get("/postulaciones-activas-usuario/:id", sesi.validar_admin_usu, async(req, res) => {

    await Postulado.raw(`
         select *
         from postulado
         where id_caso IN (?) and id_usuario = ? and tipo = ? group by id_caso;`, [req.query.casos, req.params.id, 1])
        .then(postulados => {
            return AR.enviarDatos(postulados[0], res);
        })
        .catch(err => {
            return AR.enviarDatos([], res);
        });
});
router.get("/unirse/listado/:id", sesi.validar_admin_usu, async(req, res) => {
    await Postulado.raw(`
         select p.*, u.nom_usu, u.ape_usu, u.imagen, u.correo_paypal,
         (select COALESCE(SUM(tr.monto),0) from transacciones tr  where c.id=tr.id_caso and c.id_abogado=tr.id_usuario and tr.id_caso=p.id_caso and tr.tipo=2) as pagado_abogado, 
         (select COALESCE(SUM(tr.monto),0) from transacciones tr where tr.id_caso=p.id_caso and tr.tipo=1) as recaudo,
         (select COALESCE(tr.monto,0) from transacciones tr where tr.id_caso=c.id and tr.tipo=2 and tr.id_usuario=p.id_usuario and tr.estatus_pago!='Denied') as pagado,
         (select estatus_pago from transacciones tr where tr.id_caso=c.id and tr.tipo=2 and tr.id_usuario=p.id_usuario and tr.estatus_pago!='Denied') as estatus_pago,
         (select COALESCE(tr.monto,0) from transacciones tr where tr.id_caso=c.id and tr.tipo=1 and tr.id_usuario=p.id_usuario and tr.estatus_pago='Completed') as aporte,
         (select COALESCE(pp.pre_pos_2,0) from caso c , postulado pp where pp.id_usuario=c.id_abogado and pp.id_caso=c.id and pp.tipo=2 and p.id_caso=c.id group by pp.id_caso ) as a_pagar
         from postulado p inner join usuarios u inner join caso c
         where p.est_pos=2 and p.id_usuario=u.id and p.tipo=1 and p.id_caso=c.id and c.id='${req.params.id}' ;`)
        .then(postulados => {
            AR.enviarDatos(postulados[0], res);
        });
});
router.get("/comentarios/:id", async(req, res) => {

    await Postulado.query().where({ "id_caso": req.params.id, "tipo": 1, "est_pos": 2}).select("des_pos","id_usuario").then(comentario => {
        AR.enviarDatos(comentario, res);
    });
});
router.get("/actualizar-sts/:id", sesi.validar_admin, async(req, res) => {

    const caso = await Caso.query()
        .updateAndFetchById(req.params.id, {sts_caso:req.query.sts})
        .then(async caso => {
            caso.msg = "Estatus actualizado con éxito";
            caso.r = true;
            AR.enviarDatos(caso, res);
        })
        .catch(err => {
            //respuesta error
            console.log(err);
            return AR.enviarDatos({ msg: "Error al actualizar el caso", error: err }, res, 200);
        })
});

//ABogado procesos
router.post("/abogado-aceptar-caso", archivos.none(), sesi.validar_usu, async(req, res) => {

    const caso = await Caso.query()
        .updateAndFetchById(req.body.id_caso, {sts_caso:2, id_abogado:req.body.id_usuario})
        .then(async caso => {
            caso.msg = "Gracias por tu respuesta, ya eres el representante del caso.";
            caso.r = true;
            await PrepararAlertas(req.body.id_caso, "2");
            AR.enviarDatos(caso, res);
        })
        .catch(err => {
            //respuesta error
            console.log(err);
            return AR.enviarDatos({ msg: "Error al actualizar el caso", error: err }, res, 200);
        })
});
router.post("/abogado-rechazar-caso", archivos.none(), sesi.validar_usu, async(req, res) => {

    const ccc = await Caso.query()
        .updateAndFetchById(req.body.id_caso, {sts_caso:1, id_abogado:null})
        .then(async cc => {
            cc.msg = "Gracias por tu respuesta, tu postulación será ignorada...";
            cc.r = true;

            await Postulado.query()
            .delete()
            .where({ id_caso: req.body.id_caso, tipo: 2, id_usuario: req.body.id_usuario })
            .catch(err => {
                console.log(err);
            });

            await Votaciones.query()
            .delete()
            .where({ id_caso: req.body.id_caso})
            .catch(err => {
                console.log(err);
            });
            await PrepararAlertas(req.body.id_caso, "1");
            AR.enviarDatos(cc, res);
        })
        .catch(err => {
            //respuesta error
            console.log(err);
            return AR.enviarDatos({ msg: "Error al actualizar el caso", error: err }, res, 200);
        })
});
async function PrepararAlertas(ID, OP) {
const cas = await Caso.query()
                .where({ id: ID})
                .catch(err => {
                    console.log(err);
                });
                if (cas[0]) {
                    caso = cas[0];
                     if (caso) {
                            const postus = await Postulado.raw(`
                                                         select p.* , u.nom_usu, u.ape_usu, u.cor_usu, u.rol
                                                         from postulado p, usuarios u
                                                         where p.id_usuario=u.id and p.id_caso = ? and p.tipo = ? and p.est_pos = ? group by p.id_usuario`, [caso.id, 1, 2])
                                                    .catch(err => {
                                                        console.log(err);
                                                    });

                            const postusa = await Postulado.raw(`
                                                         select p.* , u.nom_usu, u.ape_usu, u.cor_usu, u.rol ,
                                                         (select COALESCE(SUM(t.monto),0) from transacciones t where t.id_caso=p.id_caso and t.tipo=1) as recaudo
                                                         from postulado p, usuarios u
                                                         where p.id_usuario=u.id and p.id_caso = ? and p.tipo = ? group by p.id_usuario having recaudo >= p.pre_pos_2`, [caso.id, 2])
                                                    .catch(err => {
                                                        console.log(err);
                                                    });

                                                if (postus) {
                                                    if(postus[0]){
                                                        var unidos = postus[0];
                                                        var demandantes = files.demandantes;

                                                        if(unidos.length>=demandantes) {
                                                             if(postusa){
                                                                if(postusa[0]) {
                                                                    if(postusa[0].length>0){
                                                                            console.log("Preparando alertas...");
                                                                            caso.usuario.id_usuario = caso.usuario.id;
                                                                            unidos.push(caso.usuario);
                                                                            for (var i = 0; i < unidos.length; i++) {
                                                                                 var usuario = unidos[i];
                                                                                     if(OP=="1") {


                                                                                         var cuerpo = `Hola, ${usuario.nom_usu} <br>
                                                                                                         El abogado ganador rechazó la invitación para representar el
                                                                                                         caso <b>${caso.nom_caso}</b>, es necesario votar de nuevamente
                                                                                                                            `;
                                                                                        mailer.enviarMail({
                                                                                            direccion: usuario.cor_usu,
                                                                                            titulo: "Que comience la votación",
                                                                                            mensaje: mailp({
                                                                                                cuerpo: cuerpo,
                                                                                                titulo: "Que comience la votacion"
                                                                                            })
                                                                                        });
                                                                                        var not = await Notificaciones.query()
                                                                                            .insert({
                                                                                                descripcion: "Que comience la votación",
                                                                                                contenido: cuerpo,
                                                                                                estatus: 0,
                                                                                                id_usuario: usuario.id_usuario,
                                                                                                tipo_noti: 4,
                                                                                                id_usable: caso.id,
                                                                                                fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                                                                            })
                                                                                            .then(not => {
                                                                                                var socketReceptor = funciones.encontrarSocket(usuario.id_usuario, usuario.rol);

                                                                                                if (socketReceptor) {
                                                                                                    global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                                                                                }
                                                                                            })
                                                                                            .catch(err => {
                                                                                                console.log(err);
                                                                                            });
                                                                                     }else if(OP=="2") {
                                                                                        var cuerpo = `Hola, ${usuario.nom_usu} <br>
                                                                                                             Ya tenemos un representante para el
                                                                                                             caso <b>${caso.nom_caso}</b>.
                                                                                                                                `;
                                                                                            mailer.enviarMail({
                                                                                                direccion: usuario.cor_usu,
                                                                                                titulo: "Ya contamos con un representante",
                                                                                                mensaje: mailp({
                                                                                                    cuerpo: cuerpo,
                                                                                                    titulo: "Ya contamos con un representante"
                                                                                                })
                                                                                            });
                                                                                            var not = await Notificaciones.query()
                                                                                                .insert({
                                                                                                    descripcion: "Ya contamos con un representante",
                                                                                                    contenido: cuerpo,
                                                                                                    estatus: 0,
                                                                                                    id_usuario: usuario.id_usuario,
                                                                                                    tipo_noti: 2,
                                                                                                    id_usable: caso.id,
                                                                                                    fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                                                                                })
                                                                                                .then(not => {
                                                                                                    var socketReceptor = funciones.encontrarSocket(usuario.id_usuario, usuario.rol);

                                                                                                    if (socketReceptor) {
                                                                                                        global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                                                                                    }
                                                                                                })
                                                                                                .catch(err => {
                                                                                                    console.log(err);
                                                                                                });
                                                                                        }else if(OP=="3") {
                                                                                            var cuerpo = `Hola, ${usuario.nom_usu} <br>
                                                                                             es tiempo de elegir a un abogado para el
                                                                                             caso <b>${caso.nom_caso}</b>.
                                                                                                                `;
                                                                                                mailer.enviarMail({
                                                                                                    direccion: usuario.cor_usu,
                                                                                                    titulo: "Que comience la votación",
                                                                                                    mensaje: mailp({
                                                                                                        cuerpo: cuerpo,
                                                                                                        titulo: "Que comience la votacion"
                                                                                                    })
                                                                                                });
                                                                                            var not = await Notificaciones.query()
                                                                                                .insert({
                                                                                                    descripcion: "Que comience la votación",
                                                                                                    contenido: cuerpo,
                                                                                                    estatus: 0,
                                                                                                    id_usuario: usuario.id_usuario,
                                                                                                    tipo_noti: 4,
                                                                                                    id_usable: caso.id,
                                                                                                    fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                                                                                })
                                                                                                .then(not => {
                                                                                                    var socketReceptor = funciones.encontrarSocket(usuario.id_usuario, usuario.rol);

                                                                                                    if (socketReceptor) {
                                                                                                        global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                                                                                    }
                                                                                                })
                                                                                                .catch(err => {
                                                                                                    console.log(err);
                                                                                                });
                                                                                        }
                                                                            };
                                                                                if(OP=="1"){
                                                                                     await Caso.query()
                                                                                    .updateAndFetchById(caso.id, {sts_caso:5})
                                                                                    .then(async casss => {
                                                                                    })
                                                                                    .catch(err => {
                                                                                        console.log(err);
                                                                                    });
                                                                                } else if(OP=="2"){
                                                                                    // asi quedaria el caso
                                                                                } else if(OP=="3") {
                                                                                     await Caso.query()
                                                                                    .updateAndFetchById(caso.id, {sts_caso:5})
                                                                                    .then(async casss => {
                                                                                    })
                                                                                    .catch(err => {
                                                                                        console.log(err);
                                                                                    });
                                                                                }
                                                                            }else {
                                                                                //avisar q no hay abogados
                                                                                console.log("Preparando alertas falta abogado...");
                                                                                caso.usuario.id_usuario = caso.usuario.id;
                                                                                unidos.push(caso.usuario);
                                                                                for (var i = 0; i < unidos.length; i++) {
                                                                                     var usuario = unidos[i];
                                                                                        var cuerpo = `Hola, ${usuario.nom_usu} <br>
                                                                                                         debes esperar a que un abogado se postule al
                                                                                                         caso <b>${caso.nom_caso}</b>, solo asi se dára inicio a la votación,
                                                                                                         pronto recibirá una notificación.
                                                                                                                            `;
                                                                                        mailer.enviarMail({
                                                                                            direccion: usuario.cor_usu,
                                                                                            titulo: "Falta poco para iniciar la votación",
                                                                                            mensaje: mailp({
                                                                                                cuerpo: cuerpo,
                                                                                                titulo: "Falta poco para iniciar la votación"
                                                                                            })
                                                                                        });
                                                                                        const not = await Notificaciones.query()
                                                                                            .insert({
                                                                                                descripcion: "Falta poco para iniciar la votación",
                                                                                                contenido: cuerpo,
                                                                                                estatus: 0,
                                                                                                id_usuario: usuario.id_usuario,
                                                                                                tipo_noti: 2,
                                                                                                id_usable: caso.id,
                                                                                                fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                                                                            })
                                                                                            .then(not => {
                                                                                                var socketReceptor = funciones.encontrarSocket(usuario.id_usuario, usuario.rol);

                                                                                                if (socketReceptor) {
                                                                                                    global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", not);
                                                                                                }
                                                                                            })
                                                                                            .catch(err => {
                                                                                                console.log(err);
                                                                                            });
                                                                                };
                                                                                await Caso.query()
                                                                                .updateAndFetchById(caso.id, {sts_caso:1})
                                                                                .then(async casss => {
                                                                                })
                                                                                .catch(err => {
                                                                                    console.log(err);
                                                                                });
                                                                             }
                                                                        }
                                                                    }

                                                        }
                                                    }
                                                }
                                }

                }
}
module.exports = router;