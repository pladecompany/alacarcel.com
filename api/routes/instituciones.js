//llamar al modelo de Usuarios
const Instituciones = require("../models/Instituciones");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const jwt = require("jsonwebtoken");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

const moment = require("moment");

var router = express.Router();
 

router.post("/",
    archivos.none(),
    sesi.validar_admin_usu, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
           
            if (!data.id_ciudad) {
                err = 'Debes seleccionar una ciudad.';
            }
             if (!data.id_estado) {
                err = 'Debes seleccionar un estado.';
            }
             if (!data.id_pais) {
                err = "Debes seleccionar un país.";
            }
            if (!data.des_ins) {
                err = 'Debes ingresar una descripción.';
            }
            if (!data.nom_ins) {
                err = "Debe ingresar el nombre.";
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                const instituciones = await Instituciones.query()
                    .insert({
                        nom_ins: data.nom_ins,
                        des_ins: data.des_ins,
                        id_pais: data.id_pais,
                        id_estado: data.id_estado,
                        id_ciudad: data.id_ciudad,
                        fec_reg_ins: new Date()
                    })
                    .catch(err => {
                        console.log(err);
                    })



                instituciones.r = true;
                instituciones.msg = "Se registro correctamente!";
                AR.enviarDatos(instituciones, res);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.get("/", sesi.validar_admin_usu, async(req, res) => {
    await Instituciones.raw(`
        select i.*, p.nombre as pais,e.nombre as estado,c.nombre as ciudad
         from instituciones i inner join paises p inner join estado e inner join ciudad c 
         where p.id=i.id_pais and e.id=i.id_estado and c.id=i.id_ciudad ;`)
        .then(instituciones => {
        AR.enviarDatos(instituciones[0], res);
    });
});
router.post("/editar",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        valido = true;
        let err = false;
        try {

           
            if (!data.id_ciudad) {
                err = 'Debes seleccionar una ciudad.';
            }
             if (!data.id_estado) {
                err = 'Debes seleccionar un estado.';
            }
             if (!data.id_pais) {
                err = "Debes seleccionar un país.";
            }
            if (!data.des_ins) {
                err = 'Debes ingresar una descripción.';
            }
            if (!data.nom_ins) {
                err = "Debe ingresar el nombre.";
            }
             if (!data.id) { err = 'Debes enviar el id de la Insitución'; }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }
            var datos = {
                nom_ins: data.nom_ins,
                des_ins: data.des_ins,
                id_pais: data.id_pais,
                id_estado: data.id_estado,
                id_ciudad: data.id_ciudad
            };
            if (valido) {
                const instituciones = await Instituciones.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async instituciones => {
                        instituciones.msg = "Registro actualizado con éxito";
                        instituciones.r = true;
                        AR.enviarDatos(instituciones, res);
                    })
                    .catch(err => {
                        //respuesta error
                        console.log(err);
                        return AR.enviarDatos({ msg: "Error al actualizar la Institución", error: err }, res, 200);
                    })

            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.get("/delete/:id",
    archivos.none(),
    sesi.validar_admin, async(req, res) => {
        const eliminado = await Instituciones.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado) AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
        else AR.enviarDatos("0", res);
    });
module.exports = router;
