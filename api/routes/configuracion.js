const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var router = express.Router();
var archivos = require("../Archivos");
var config = require("../config");
//Llamar al helper de errores
const { ValidationError } = require("../Errores");
const { NotFoundError } = require("objection");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
const jsonfile = require('jsonfile');
var fs = require('fs');

var file = __dirname+ '/../configuracion.json';

router.get("/",archivos.none(), async (req, res,next) => {
  var files = jsonfile.readFileSync(file);
  AR.enviarDatos(files, res);
});

router.post("/",archivos.none(),sesi.validar_admin, async (req, res,next) => {
    err = false;
    data = req.body;
    if (data.monto.length <1) {
      err = 'Introduzca el monto mínimo para unirse a un caso'
    }
    if (data.cant.length <1) {
      err = 'Introduzca la cantidad de demandantes'
    }
    if (data.voto.length <1) {
      err = 'Introduzca el porcentaje de voto del denunciante'
    }
    if(err){return AR.enviarDatos({r: false, msg: err},res, 200);}
    jsonfile.readFile(file, function(err, obj) {
      var fileObj = obj;

      fileObj.monto=parseFloat(req.body.monto);
      fileObj.demandantes=parseFloat(req.body.cant);
      fileObj.voto=parseFloat(req.body.voto);

      jsonfile.writeFile(file, fileObj, function(err,obj) {
        if (err) throw err;
      fileObj.msg = "Configuración actualizada con éxito"
      AR.enviarDatos(fileObj, res);
      });
    });

});


module.exports = router;
