//llamar al modelo de Usuarios
const Usuario = require("../models/Usuario");
const Admin = require("../models/Admin");
//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();


//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();
 
// sesi.validar_usu O validar_admin validar sesion
//Logear un Admin
router.post("/", upload.none(), async (req, res, next) => {
  data = req.body;
  
  try {
    if (!data.correo)
      throw new ValidationError("Debe ingresar un correo electrónico.");
    if (!data.pass) throw new ValidationError("Debe ingresar una contraseña.");
    
      await Usuario.query()
        .findOne("cor_usu", data.correo)
        .throwIfNotFound()
        .then(usuario => {
          let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pas_usu);
          if (!passwordIsValid)
            throw new AuthError("Su contraseña es incorrecta.");
          
            if (usuario.est_usu == 0)
              return AR.enviarError(
                "No puede iniciar sesión porque su usuario está inactivo.",
                res,
                403
              );
            else { 
              usuario.token  = uuid.v1();
              Usuario.raw(
              "UPDATE usuarios SET token='"+usuario.token+"' WHERE cor_usu='"+usuario.cor_usu+"';"
              ).then(usuario => {
                      
              });
               
              return AR.enviarAuth(res, true, null, usuario); 
            }
        })
        .catch(err => {
          //console.log(err);
          if (err instanceof AuthError) next(err);
          //else console.log(err);
          else
            AR.enviarError(
              "No se encontró un usuario con este correo.",
              res,
              404
            );
        });
    
      
    
  } catch (err) {
    next(err);
  }
});

router.post("/admin", upload.none(), async (req, res, next) => {
  data = req.body;

  try {
    if (!data.correo)
      throw new ValidationError("Debe ingresar un correo electrónico.");
    if (!data.pass) throw new ValidationError("Debe ingresar una contraseña.");

    await Admin.query()
      .findOne("correo", data.correo)
      .throwIfNotFound()
      .then(usuario => {
        let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pass);
        if (!passwordIsValid)
          throw new AuthError("Su contraseña es incorrecta.");
        
          if (usuario.est_usu == 0)
            return AR.enviarError(
              "No puede iniciar sesión porque su usuario está bloqueado.",
              res,
              403
            );
          else { 
            usuario.token  = uuid.v1();
            Usuario.raw(
            "UPDATE admins SET token='"+usuario.token+"' WHERE correo='"+usuario.correo+"';"
            ).then(usuario => {
                    
            });
            usuario.rol = 'admin'
            return AR.enviarAuth(res, true, null, usuario); 
          }
      }) 
      .catch(err => {
        if (err instanceof AuthError) next(err);
        //else console.log(err);
        else
          AR.enviarError(
            "No se encontró un admin con este correo.",
            res,
            404
          );
      });
      
  } catch (err) {
    next(err);
  }
});


module.exports = router;
