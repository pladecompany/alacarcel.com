//llamar al modelo de Usuarios
const Funcionario = require("../models/Funcionario");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const jwt = require("jsonwebtoken");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

const moment = require("moment");

var router = express.Router();
 

router.post("/",
    archivos.none(),
    sesi.validar_admin_usu, async(req, res, next) => {
        data = req.body;

        valido = true;
        let err = false;
        try {
            if (!data.nom) {
                err = "Debe ingresar el nombre.";
            }
            if (!data.ape) {
                err = 'Debes ingresar el apellido.';
            }
            if (!data.ced) {
                err = "Debes ingresar la cédula.";
            }
            if (!data.ins) {
                err = 'Debes seleccionar una institución.';
            }
            if (!data.car) {
                err = 'Debes ingresar un cargo.';
            }
            if (err) {
                return AR.enviarDatos({ r: false, msg: err }, res, 200);
            }

            if (valido) {
                const funcionario = await Funcionario.query()
                    .insert({
                        id_institucion: data.ins,
                        nom_fun: data.nom,
                        ape_fun: data.ape,
                        ced_fun: data.ced,
                        car_fun: data.car,
                        fec_reg_fun: new Date()
                    })
                    .catch(err => {
                        console.log(err);
                    })



                funcionario.r = true;
                funcionario.msg = "Se registro correctamente!";
                AR.enviarDatos(funcionario, res);
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.get("/", sesi.validar_admin_usu, async(req, res) => {
    await Funcionario.query().then(funcionario => {
        AR.enviarDatos(funcionario, res);
    });
});
router.post("/editar",
    archivos.none(),
    sesi.validar_admin, async(req, res, next) => {

        data = req.body;
        valido = true;
        let err = false;
        try {

            if (!data.id) { err = 'Debes enviar el id del funcionario'; }

            if (!data.nom) {
                err = "Debe ingresar el nombre.";
            }
            if (!data.ape) {
                err = 'Debes ingresar el apellido.';
            }
            if (!data.ced) {
                err = "Debes ingresar la cédula.";
            }
            if (!data.ins) {
                err = 'Debes seleccionar una institución.';
            }
            if (!data.car) {
                err = 'Debes ingresar un cargo.';
            }

            if (err) { return AR.enviarDatos({ r: false, msg: err }, res, 200); }
            var datos = {
                id_institucion: data.ins,
                nom_fun: data.nom,
                ape_fun: data.ape,
                ced_fun: data.ced,
                car_fun: data.car
            };
            if (valido) {
                const funcionario = await Funcionario.query()
                    .updateAndFetchById(data.id, datos)
                    .then(async funcionario => {
                        funcionario.msg = "Registro actualizado con éxito";
                        funcionario.r = true;
                        AR.enviarDatos(funcionario, res);
                    })
                    .catch(err => {
                        //respuesta error
                        console.log(err);
                        return AR.enviarDatos({ msg: "Error al actualizar el funcionario", error: err }, res, 200);
                    })

            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    });

router.get("/delete/:id",
    archivos.none(),
    sesi.validar_admin, async(req, res) => {
        const eliminado = await Funcionario.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado) AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
        else AR.enviarDatos("0", res);
    });
module.exports = router;
