//Importar librerias
const Knex = require("knex");
const morgan = require("morgan");
const express = require("express");
const promiseRouter = require("express-promise-router");
const bodyParser = require("body-parser");
const os = require("os");
const Server = os.hostname();
const knexConfig = require("../knexfile");
const paypal = require("paypal-rest-sdk");
const Transacciones = require("./models/transacciones");
var fs = require("fs");
//cron
var cron = require('node-cron');
const Notificaciones = require("./models/Notificacion");
const moment = require("moment");
const mailer = require("./mails/Mailer");
const mail = require("./mails/correo");



if (Server == knexConfig.Hosname) {
    var https = require("https");
    var privateKey  = fs.readFileSync('/etc/ssl/private/inthecompanies.key', 'utf8');
    var certificate = fs.readFileSync('/etc/ssl/inthecompanies_com.crt', 'utf8');
    var ca = fs.readFileSync('/etc/ssl/inthecompanies_com.ca-bundle', 'utf8');
    var credentials = { ca, key: privateKey, cert: certificate };
}


//Importar Config
const { Model } = require("objection");

//Importar Validador de errores
const ValidadorDeErrores = require("./ValidarErrores");
var knex;

if (Server == knexConfig.Hosname) {
    knex = Knex(knexConfig.production);
} else {
    //knex = Knex(knexConfig.production);
    knex = Knex(knexConfig.development);
}

Model.knex(knex);

const router = promiseRouter();

//CORS middleware
const allowCrossDomain = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header("Access-Control-Allow-Headers", "*");
    next();
};

//Iniciar Express
const app = express()
    .use(
        bodyParser.urlencoded({
            extended: true
        })
    )
    .use(bodyParser.json())
    .use(morgan("dev"))
    .use(router)
    //.use(allowCrossDomain)
    .set("json spaces", 2);
app.all('*', function(req, res, next) {
    var responseSettings = {
        "AccessControlAllowOrigin": req.headers.origin,
        "AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
        "AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
        "AccessControlAllowCredentials": true
    };
    res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
    res.header("Access-Control-Allow-Origin", responseSettings.AccessControlAllowOrigin);
    res.header("Access-Control-Allow-Headers", (req.headers['access-control-request-headers']) ? req.headers['access-control-request-headers'] : "x-requested-with");
    res.header("Access-Control-Allow-Methods", (req.headers['access-control-request-method']) ? req.headers['access-control-request-method'] : responseSettings.AccessControlAllowMethods);

    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    } else {
        next();
    }


});

//Definir las rutas en la carpeta "routes"
definirRutas([
    "admin",
    "login",
    "usuarios",
    "paises",
    "notificaciones",
    "contactanos",
    "funcionario",
    "instituciones",
    "casos",
    "configuracion",
    "transacciones"
]);
app.use(ValidadorDeErrores);

//Definir rutas de archivos
app.get("/public/uploads/:filename", function(req, res) {
    console.log(req.params.filename);
    res.sendFile(__dirname + `/public/uploads/${req.params.filename}`);
});
app.get("/assets/:filename", function(req, res) {
    console.log(req.params.filename);
    res.sendFile(__dirname + `/assets/${req.params.filename}`);
});

//Iniciar el servidor en un puerto
if (Server == knexConfig.Hosname) {
    var httpsServer = https.createServer(credentials, app);
    httpsServer.listen(knexConfig.PORT);
    httpsServer.keepAliveTimeout = 60000 * 2;
} else {
    var server = app.listen(knexConfig.PORT, () => {
        console.log(
            "API Corriendo en puerto: %s",
            server.address().port
        );
    });
    server.keepAliveTimeout = 60000 * 2;
}

//--------------------------------------------INICIO SOCKET-------------------
if (Server == knexConfig.Hosname) {
    var io = require("socket.io")(httpsServer);
} else {
    var io = require("socket.io")(server);
}

const USUARIOSC = []; // Usuarios conectados en la app
global.USUARIOSC = USUARIOSC; // para usar el arreglo de usuarios conectados en cualquier parte del api...(donde sea)
global.IO = io; // para hacer emits desde cualquier parte del api (de donde sea)
global.PENDINGS = [];
io.on("connection", function(socket) {
    /**
     * Le dice al client que está conectado.
     * De momento no se usa, pero probablemente la necesite en un futuro
     */
    socket.emit("CONNECT");

    socket.on("conectar", require("./socketEvents/conectar")(socket));
    socket.on("ENVIARNOTIFICACION", require("./socketEvents/notificaciones")(socket));
    socket.on("desconectar", require("./socketEvents/desconectar")(socket));


});

//-----------------------------------------------------------------FIN SOCKET

function definirRutas(rutas) {
    rutas.forEach(ruta => {
        app.use(`/${ruta}`, require(`./routes/${ruta}`));
    });
}

//cron
    async function RunPending() {
       await Transacciones.query().where({tipo:2, estatus_pago : "Pendiente de procesar"}).groupBy('id')
      .then(async t => {
            global.PENDINGS = t;
      })
      .catch(err => {
        
      }); 
    }
      

cron.schedule("* * * * *", async function() {
    console.log('ejecutando...');
    await RunPending();
    paypal.configure({
        mode: knexConfig.paypal.mode,
        client_id: knexConfig.paypal.client_id,
        client_secret: knexConfig.paypal.secret_client
    });
     
    for (var i = 0; i < global.PENDINGS.length; i++) {
        var P = global.PENDINGS[i];
        await paypal.payout.get(P.id_pago, async function(errr, payment) {
            
            
            for (var j = 0; j < global.PENDINGS.length; j++) {
                if(global.PENDINGS[j].id_pago==payment.batch_header.payout_batch_id) {
                    var P = global.PENDINGS[j];
                    var del = j;
                }
            };
            
            if (errr) {
                console.log(errr);
                
            }else {
                 
                 if(payment.batch_header.batch_status=="SUCCESS"){
                    await Transacciones.query()
                    .updateAndFetchById(P.id, {id_pago:payment.items[0]["activity_id"], estatus_pago: "Completed", fec_reg_pago: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')})
                    .then(async casss => {

                    })
                    .catch(err => {
                        //console.log(err);
                    });
                    
                } else if(payment.batch_header.batch_status=="PENDING" || payment.batch_header.batch_status=="PROCESSING"){
                                    
                } else { 
                        await Transacciones.query()
                        .updateAndFetchById(P.id, {id_pago:payment.items[0]["activity_id"], estatus_pago: "Denied"})
                        .then(async casss => {
                            delete global.PENDINGS[del];
                            global.PENDINGS.splice(del, 1);
                        })
                        .catch(err => {
                            // console.log(err);
                           
                        });
                        
                }

            }
        });
    };
    
});