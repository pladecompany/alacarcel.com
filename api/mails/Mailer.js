"use strict";

const nodemailer = require("nodemailer");
const config = require("../config");

module.exports = {
    enviarMail(mail) {

        console.log("Recibiendo mensaje...")
        var mensaje = {
            from: "alacarcel.com noreply@alacarcel.com",
            to: mail.direccion,
            subject: mail.titulo,
            text: mail.texto ? mail.texto : mail.mensaje,
            html: mail.mensaje
        };
        console.log("Mensaje obtenido.")

        console.log("Creando Transportador.")
        let transporter = nodemailer.createTransport(config.mailConfig);

        /*        transporter.verify(function(error, success) {
            if (error) {
                console.log(error);
            } else {
                console.log("Servidor listo para enviar el correo");
            }
        });*/

        console.log("Intentando enviar correo.")
        transporter.sendMail(mensaje, function(err, info) {
            if (err) console.log(err);
            console.log(info);
        });
    },

    enviarMailPrueba(mail) {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        nodemailer.createTestAccount((err, account) => {
            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                host: "smtp.ethereal.email",
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: account.user, // generated ethereal user
                    pass: account.pass // generated ethereal password
                }
            });

            // setup email data with unicode symbols
            var mensaje = {
                from: "denunciapp20@gmail.com",
                to: mail.direccion,
                subject: mail.titulo,
                text: mail.texto ? mail.texto : mail.mensaje,
                html: mail.mensaje
            };

            // send mail with defined transport object
            transporter.sendMail(mensaje, (error, info) => {
                if (error) {
                    return console.log(error);
                }
                console.log("Message sent: %s", info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

                // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
            });
        });
    }
};