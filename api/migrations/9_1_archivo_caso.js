exports.up = knex => {
    return knex.schema.createTable("archivos_caso", table => {
        table.increments("id").primary();
        table.string("archivo");
        table.string("nom_archivo");
        table.string("tipo");
        table.integer("visible");//1-todo publico, 2-solo abogado
        table.integer('id_caso').unsigned().references('id').inTable('caso').onDelete('CASCADE');
    });
  };
  
exports.down = knex => {
    return knex.schema.dropTableIfExists("archivos_caso");

};
  