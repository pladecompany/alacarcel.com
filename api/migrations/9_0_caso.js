exports.up = knex => {
    return knex.schema.createTable("caso", table => {
        table.increments("id").primary();
        table.string("nom_caso");
        table.text("des_caso");
        table.integer('id_usuario').unsigned().references('id').inTable('usuarios').onDelete('CASCADE');
        table.integer('id_abogado').unsigned().references('id').inTable('usuarios').onDelete('CASCADE');
        table.integer('id_funcionario').unsigned().references('id').inTable('funcionario').onDelete('CASCADE');
        table.datetime("fec_reg_caso");
        table.date("fec_ini_caso");
        table.date("fec_fin_caso");
        table.integer("sts_caso");
    });
  };
  
exports.down = knex => {
    return knex.schema.dropTableIfExists("caso");

};
  