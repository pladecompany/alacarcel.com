exports.up = knex => {
  return knex.schema.createTable("instituciones", table => {
    table.increments("id").primary();
    table.string("nom_ins");
    table.string("des_ins");
    table.integer("id_pais");
    table.integer("id_estado");
    table.integer("id_ciudad");
    table.date("fec_reg_ins");
  });
};

exports.down = knex => {
  return knex.schema.dropTableIfExists("instituciones");
};
