exports.up = knex => {
    return knex.schema.createTable("admins", table => {
        table.increments("id").primary();
        table.string("nombre");
        table.string("apellido");
        table.string("correo", 150).unique();
        table.string("pass");
        table.string("telefono");
        table.string("imagen");
        table.string("sexo");
        table.string("token", 150).unique();
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("admins");
};
