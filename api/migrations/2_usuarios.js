exports.up = knex => {
  return knex.schema.createTable("usuarios", table => {
    table.increments("id").primary();
    table.string("nom_usu");
    table.string("ape_usu");
    table.string("cor_usu", 150).unique();
    table.string("correo_paypal", 150);
    table.string("pas_usu");
    table.string("sex_usu");
    table.string("dir_usu");
    table.integer("est_usu").defaultTo(0);
    table.string("imagen");
    table.string("imagen_1");
    table.string("imagen_2");
    table.string("imagen_firma");
    table.string("tlf_usu", 50);
    table.integer("id_pais");
    table.integer("id_estados");
    table.integer("id_ciudad");
    table.date("fec_reg_usu");
    table.date("fec_nac_usu");
    table.string("rol"); // nivel de acceso , Usuario | Abogado
    table.string("nic_usu", 150).unique();
    table.string("mat_usu", 150).unique();
    table.string("tip_doc_usu");
    table.string("num_doc_usu");
    table.string("token", 150).unique();
  });
};

exports.down = knex => {
  return knex.schema.dropTableIfExists("usuarios");
};
