exports.up = knex => {
  return knex.schema.createTable("funcionario", table => {
    table.increments("id").primary();
    table.string("nom_fun");
    table.string("ape_fun");
    table.string("ced_fun", 50).unique();
    table.string("car_fun");
    table.integer("id_institucion").unsigned().references('id').inTable('instituciones');
    table.date("fec_reg_fun");
  });
};

exports.down = knex => {
  return knex.schema.dropTableIfExists("funcionario");
};
