exports.up = knex => {
    return knex.schema.createTable("paises", table => {
      table.increments("id").primary();
      table.string("nombre");
      table.string("imagen");
      table.string("estatus"); //0 inactivo // 1 activo 

    });
  };
  
exports.down = knex => {
    return knex.schema.dropTableIfExists("paises");
};
  