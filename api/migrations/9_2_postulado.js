exports.up = knex => {
    return knex.schema.createTable("postulado", table => {
        table.increments("id").primary();
        table.integer('id_usuario').unsigned().references('id').inTable('usuarios').onDelete('CASCADE');
        table.integer('id_caso').unsigned().references('id').inTable('caso').onDelete('CASCADE');
        table.datetime("fec_reg_pos");
        table.integer("tipo");// 1-Unirse 2-postularse
        table.integer("est_pos"); //1-pendiente 2-pagada
        table.text("des_pos");
        table.text("car_pos");
        table.float("pre_pos_1", 50, 2);
        table.float("pre_pos_2", 50, 2);
        table.float("pre_pos_3", 50, 2);
    });
  };
  
exports.down = knex => {
    return knex.schema.dropTableIfExists("postulado");

};
  