exports.up = knex => {
    return knex.schema.createTable("transacciones", table => {
        table.increments("id").primary();
        table.integer('id_usuario').unsigned().references('id').inTable('usuarios').onDelete('CASCADE');
        table.integer('id_caso');
        table.datetime("fec_reg_pago");
        table.integer("tipo");// 1-suma 0-resta
        table.string("id_pago", 100).unique();
        table.string("estatus_pago", 100);
        table.float("monto", 50, 2);
        table.integer("retiro");// 0
    });
  };
  
exports.down = knex => {
    return knex.schema.dropTableIfExists("transacciones");

};
  