exports.up = knex => {
    return knex.schema.createTable("notificacion", table => {
        table.increments("id").primary();
        table.string("descripcion");
        table.text("contenido");
        table.datetime("fecha");
        table.integer("estatus").defaultTo(0); //1 activo // o inactivo
        table.string("tabla_usuario");
        table.integer("tipo_noti"); // 0 usuario unido, 1 abogado postulado, 2 nuevo caso, 3 nuevo fondo, 4 hora de votar, 5 abogado ganador
        table.integer("id_usable");
        table
            .integer('id_usuario')
            .unsigned()
            
            table
            .foreign('id_usuario')
            .references('id')
            .inTable('usuarios')
            .onDelete('CASCADE');
    });
  };
  
exports.down = knex => {
    return knex.schema.dropTableIfExists("notificacion");

};
  