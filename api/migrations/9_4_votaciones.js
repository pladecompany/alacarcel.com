exports.up = knex => {
    return knex.schema.createTable("votaciones", table => {
        table.increments("id").primary();
        table.integer('id_usuario').unsigned().references('id').inTable('usuarios').onDelete('CASCADE');
        table.integer('id_caso').unsigned().references('id').inTable('caso').onDelete('CASCADE');
        table.integer('id_abogado').unsigned().references('id').inTable('usuarios').onDelete('CASCADE');
        table.datetime("fec_reg_vot");
        table.float("valor_vot", 50, 2);
    });
  };
  
exports.down = knex => {
    return knex.schema.dropTableIfExists("votaciones");

};
  