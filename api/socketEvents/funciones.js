const _ = require("lodash");
module.exports = {
  enviarData(idReceptor, commit, data) {
    let socketReceptor = this.encontrarSocket(idReceptor);

    if (socketReceptor) {
      console.log("Enviando data al socket: " + socketReceptor);
      global.IO.to(socketReceptor).emit(commit, data);
    } else {
      console.log("No se encontró socket en línea para el ID: " + idReceptor);
    }
  },
  encontrarID(socket) {
    console.log("Encontrando emisor del socket: " + socket);
    let userData = _.find(global.USUARIOSC, o => {
      return o.socket == socket;
    });

    if (userData) return userData.user.id;
  },
  encontrarSocket(id,tabla) {
    let user = _.find(global.USUARIOSC, o => {
      if(o.user.id == id && o.user.rol == tabla){
        return o.user;
      }
    });

    return user ? user.socket : null;
  }
};
