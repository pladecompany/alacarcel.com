const funciones = require("./funciones");
 

const Notificacion = require("../models/Notificacion");

module.exports = function(socket) {
  return async function(data) {

    const _notificacion = await Notificacion.query()
      .insert({
        descripcion:data.descripcion,
        contenido:data.contenido,
        estatus:data.estatus,
        tabla_usuario:data.tabla_usuario,
        id_usuario:data.id_usuario,
        fecha: new Date(),
        
      })
      .catch(err => console.log(err));

      const _notificacion_consulta = await Notificacion.raw("select n.* from notificacion n where n.id="+_notificacion.id+";")
        .catch(err=>console.log(err));
        if(_notificacion){
          console.log(data.tabla_usuario)
           
                var socketReceptor = funciones.encontrarSocket(data.id_usuario, 'usuarios');
            
            console.log(socketReceptor)
            if (socketReceptor) {
              global.IO.to(socketReceptor).emit("RECIBIRNOTIFICACION", _notificacion_consulta[0]);
              //socket.broadcast.to(socketReceptor).emit("RECIBIRMENSAJE", _mensaje);
            
            }
    }
  }
}
