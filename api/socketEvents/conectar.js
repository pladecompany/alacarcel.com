const buscarConectado = require("../functions/buscarConectado");
const desconectar = require("../functions/desconectar");
const _ = require("lodash");
module.exports = function(socket) {
    return function(data) {
        let dash =
            "----------------------------------------------------------------------------------";
        console.log(
            "Se conectó el usuario " + data.id + ", " +
            ((data.rol == "admin") ? data.nombre : data.nom_usu) +
            " con el socket: " +
            socket.id
        );

        /* 
          Por hacer: Trabajar los sockets como arreglo en caso de que un usuario se conecte
          desde dos dispositivos
        */

        _.remove(USUARIOSC, o => {
            if (o.user) {
                return o.user.id == data.id;
            } else return false;
        });

        USUARIOSC.push({
            socket: socket.id,
            user: data
        });
        global.USUARIOSC = USUARIOSC;



        socket.emit("conectadosOnline", USUARIOSC);
    };
};