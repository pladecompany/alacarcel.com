const os = require("os");
const Server = os.hostname();
const knexConfig = require("../knexfile");
if (Server == knexConfig.Hosname) {
    var dominio = "https://inthecompanies.com:" + knexConfig.PORT;
    var dominioWeb = "https://pladecompany.com/demos/denunciapp/";
} else {
    var dominio = "http://localhost:" + knexConfig.PORT;
    var dominioWeb = "https://localhost/denunciapp/";

}
module.exports = {
    secret: "Pl@d3C0mp4nY",
    dominio: dominio,
    dominioWeb: dominioWeb,
    rutaArchivo: function(archivo) {
        return this.dominio + "\\public\\uploads\\" + archivo;
    },
    rutaWeb: function(ruta) {
        return this.dominioWeb + ruta;
    },

    mailConfig: {
        pool: true,
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            user: "denunciapp20@gmail.com",
            pass: "denunciapp2019"
        },
        tls: {
            rejectUnauthorized: false
        }
    },

    mailmaster: {
        nombre: "Name & ",
        apellido: "Last Name"
    }
};