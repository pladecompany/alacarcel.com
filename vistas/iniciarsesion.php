<div class="row m-0" style="background: #ebf4f9;">
	<div class="col s12 m12 l6 clr_white flexCenter vh100 flexColumn hide-on-med-only hide-on-small-only p-0">
		<img src="static/img/fondo.jpg" class="boxHome">
	</div>

	<div class="col s12 m12 l6 vh100">
		<form method="post" action="#" class="login-form" id="login_entrar" onsubmit="return false;">
			<div class="boxAccount__body">
				<div class="row">
					<div class="col s12 text-center">
						<h5 class="text-bold">INICIAR SESIÓN</h5>
					</div>
				</div>
				
				<div class="row mb-0">
					<div class="col s12">
						<div class="input-field">
							<p>Correo electrónico</p>
							<input id="correo" name="correo" type="text" class="form-app" placeholder="Correo electrónico">
						</div>
					</div>
					
					<div class="col s12">
						<div class="input-field">
							<p>Contraseña</p>
							<input id="pass" name="pass" type="password" class="form-app" placeholder="Contraseña">
						</div>
					</div>
				</div>

				<div class="row mb-0">
					<div class="col s12">
						<p>
							<label>
								<input type="checkbox" name="recuerdame" />
								<span>Recuérdame</span>
							</label>
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col s12 text-center">
						<button type="submit" id="btn_entrar" class="btn btn-blue waves-effect w50">Ingresar <i class="fa" id="login1"></i></button>
					</div>
				</div>

				<div class="row">
					<div class="col s12 text-center ">
						<b>¿Olvidaste tu contraseña? <a href="?op=recuperar" class="clr_blue">Recuperar</a></b>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script src="static/js/jquery.min.js"></script>
<script src="static/js/backend-panel/login.js"></script>

<?php include_once("registro_postulante.php"); ?>