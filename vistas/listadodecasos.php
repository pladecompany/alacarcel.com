<div class="row hide info-usu-casos">
	<div class="col s12">
		<div style="background-image: url('static/img/portada.jpg');background-size: 100% 100%;background-repeat: no-repeat;">
			<div class="boxBasePerfil">
				<img src="static/img/user.png" class="info-usu-img boxPerfil--img" style="height: 100px;width: 100px;">
				<h6 class="text-center text-bold clr_blue info-usu-name"></h6>
			</div>
		</div>
	</div>
</div>

<form action="vistas/pdf/?op=caso" target="_blanck" class="hide" method="POST" id="form_pdf" enctype="multipart/form-data">
	<input type="hidden" name="casopdf" id="casopdf" value="" required>
</form>
<div id="modalCompartir" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Compartir caso</span>
		<a href="#!" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<div class="row">
			<div class="col s12 m6 offset-m3">
				<a href="#!" class="btn btn-blue w100 mb-3 " id="compartir-social-f"><i class="fab fa-facebook"></i> Facebook</a>
				<a href="#!" class="btn btn-blue w100 mb-3 " id="compartir-social-t"><i class="fab fa-twitter"></i> Twitter</a>
				<a href="#!" class="btn btn-blue w100 mb-3 " id="compartir-social-l"><i class="fab fa-linkedin"></i> linkedin</a>
				<div class="row">
					<div class="col s10">
						<div class="input-field">
							<input type="text" id="copyurl" name="copyurl" readonly class="form-app text" placeholder="http://denunciapp/?op=listadodecasos">
						</div>
					</div>
					<div class="col s2"><br>
						<a href="#!" class="btn btn-blue copy-text"><i class="fa fa-copy"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- btn compartir
<a href="#modalCompartircaso" class="modal-trigger btn btn-blue" id="btn-ver-caso-compartido"><i class="fa fa-share"></i></a>
-->
<div id="modalCompartircaso" class="modal" data-backdrop="static" data-keyboard="false">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Caso</span>
		
	</div>

	<div class="modal-content modal-sm" id="publicados-sinloguearse">
		<!--<ul class="collection" style="background: transparent;">
			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 center">
						<a href="?op=listadodecasos&amp;list=publicados&amp;usuario=1"><img src="static/img/flag-icons/Argentina.png" class="img-user boxPerfil--img img-cover imgcasos"></a>
						<h6 style="font-size:14px;"><b>Usuario Apellido Usuario</b></h6>
					</div>
					<div class="col s8">
						<h5><b>#43</b> Caso: caso 29 de febrero</h5>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4"><b>Descripción</b></div>
					<div class="col s8 border-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua.</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4"><b>Estatus</b></div>
					<div class="col s8 border-left">Estatus</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4"><b>Monto recaudado</b></div>
					<div class="col s8 border-left">$3</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4"><b>Fecha de publicación</b></div>
					<div class="col s8 border-left">04-03-20</div>
				</div>
			</li>

			<li class="collection-item text-center">
				<a href="#" class="btn btn-blue clr_white">Unirse</a>
			</li>
		</ul>-->
	</div>
</div>

<br class="hide info-usu-casos"><br class="hide info-usu-casos">
<div style="background: #f7f7f7;min-height: 90vh;" class="cont">
	<div class="row mb-0">
		<div class="col s12 m12 text-center">
			<h4 class="text-center clr_blue text-bold" id="tit_casos"></h4>
		</div>
		<div class="col s12 m3">
			<div class="row">
				<div class="col s12 hide">
					<ul class="collection">
						<li class="collection-header">
							<h6 class="text-left pl-1">Saldo disponible <a href="#modalfondos" class="modal-trigger right"><i class="fa fa-plus-circle clr_white pr-2"></i></a></h6>
						</li>	
						<li class="collection-item text-center">
							<h1 style="color: #3f51b5;"><span>0.00</span> $</h1>
						</li>
					</ul>
				</div>

				<div class="col s12">
					<ul class="collection">
						<li class="collection-header">
							<h6 class="text-center">Buscar caso</h6>
						</li>	
						<li class="collection-item text-center">
							<input type="search" name="search" id="search" class="form-app">
							<a href="#" class="btn btn-blue clr_white btn_buscar_casos"> Buscar</a>
						</li>
					</ul>
				</div>

				<div class="col s12">
					<ul class="collection">
						<li class="collection-header">
							<h6 class="text-center">Estado</h6>
						</li>	
						<li class="collection-item">
							<select class="mt-0 browser-default input-field" id="estado" name="estado">
								<option selected>Selecciona el Estado</option>
							</select>
						</li>
					</ul>
				</div>

				<div class="col s12">
					<ul class="collection li-ciudades">
						
					</ul>
				</div>
			</div>
		</div>

					
		<div class="col s12 m9 pr-4" id="publicados">
		</div>
	</div>

	<div class="row mb-0 text-center mb-3"> 
		<ul class="pagination">
		</ul>
	</div>

	<div class="row m-0 hab_niv_usu hide">
		<div class="col s12 l12">
			<div class="fixed-action-btn">
				<a class="btn-floating btn-large btn-blue crea-caso" href="#">
					<i class="large fa fa-plus"></i>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="progress progresRequest" id="Progress__P">
	<div class="determinate" style="width: 0%" id="Progress__C">
	</div>
</div>
<style>
	.progresRequest {
		position: fixed !important;
		z-index: 100;
		top: -0.5rem !important;
		background-color: transparent !important;
	}
	
	.progresRequest .determinate {
		background-color: #f5821f !important;
	}
</style>
<!--<script src="https://www.paypal.com/sdk/js?client-id=AZXH-Ml4j97UQPXa759m2ohqTvoUXUhdlxt2Q7D-1N08YGy0FuWQ3iweUfR8PykIN0u5ehOMOy4UBoMq&currency=USD&debug=true"></script>
 -->
<?php
 include_once('modalcasos.php');
?>
<script type="text/javascript">
	$(document).ready(function(){

		$(document).on('click', ".btnOpc", function(){
			var idv = this.id;
			$("#divopciones"+idv).show();
		});

		$(document).on('click', ".btnx", function(){
			var idv = this.id;
			$("#divopciones"+idv).hide();
		});

		$(document).on('click', ".eliminarArchivo", function(){
			var idv = this.id;
			$("#divArchivo"+idv).remove();
		});

		$("#addInstitucion").click(function(){
			$("#nom_ins , #des_ins").val('');
            $("#est_i").val('').trigger("change");
			$("#formInstituto").show();
		});

		$("#cerrarInstituto").click(function(){
			$("#formInstituto").hide();
		});

		$("#btnComentarios").click(function(){
			$("#divComentarios").show();
		});


	});
</script>