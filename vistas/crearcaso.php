<div class="cont">
	<section class="my-4 container">
		<div class="row flexCenter">
			<div class="card boxPerfil">
				<div class="boxHead mt-0">
					<h6>CREAR CASO</h6>
				</div>

				<div class="boxPerfil">
					<form action="" method="">
						<div class="row mb-0">
							<div class="col s12 m6">
								<div class="input-field">
									<p for="des_caso">Descripción del caso</p>
									<textarea class="textarea" id="des_caso"></textarea>
								</div>
							</div>

							<div class="col s12 m6">
								<div class="input-field">
									<p for="fun_caso">Funcionario</p>
									<input type="text" id="fun_caso" name="fun_caso" class="form-app" required>
								</div>
							</div>

							<div class="col s12 m6">
								<div class="input-field">
									<p for="ini_caso">Fecha de inicio</p>
									<input type="date" id="ini_caso" name="ini_caso" class="form-app" required>
								</div>
							</div>

							<div class="col s12 m6">
								<div class="input-field">
									<p for="fin_caso">Fecha fin</p>
									<input type="date" id="fin_caso" name="fin_caso" class="form-app" required>
								</div>
							</div>

							<div class="col s12 m6">
								<div class="input-field">
									<p for="est_caso">Estatus</p>
									<input type="text" id="est_caso" name="est_caso" class="form-app" required>
								</div>
							</div>

							<div class="col s12 m6">
								<div class="input-field">
									<p for="url_caso">Url archivo</p>
									<input type="url" id="url_caso" name="url_caso" class="form-app" required>
								</div>
							</div>

							<div class="col s12 m6">
								<div class="input-field">
									<p for="tip_caso">Tipo archivo</p>
									<input type="text" id="tip_caso" name="tip_caso" class="form-app" required>
								</div>
							</div>

							<div class="col s12 m6">
								<div class="input-field">
									<p for="reg_caso">Fecha registro</p>
									<input type="text" id="reg_caso" name="reg_caso" class="form-app" required>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col s12 text-center pb-2">
								<button class="btn waves-effect btn-blue" id="bt_save">Guardar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="static/js/jquery.min.js"></script>
		<script type="text/javascript" src="static/js/backend-panel/cambiarclave.js"></script>
	</section>
</div>