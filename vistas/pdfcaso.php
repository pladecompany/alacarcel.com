<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>

<body>
	<table style="border:1px solid #ddd;">
		<tr>
			<td colspan="2" align="center" style="border-bottom: 1px solid #ddd;"><img src="../static/img/logo.png" width="300px"></td>
		</tr>

		<tr>
			<td colspan="2" style="border-bottom: 1px solid #ddd;font-size: 22px;"><b>#43</b> Caso: caso 29 de febrero</td>
		</tr>

		<tr>
			<td colspan="2" style="border-bottom: 1px solid #ddd;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat.</td>
		</tr>

		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Funcionario:</b></td>
			<td style="border-bottom: 1px solid #ddd;">Daniela Manzanilla</td>
		</tr>

		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Institución</b></td>
			<td style="border-bottom: 1px solid #ddd;">UPTT</td>
		</tr>

		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Ubicación</b></td>
			<td style="border-bottom: 1px solid #ddd;" colspan="3">Av. Unda</td>
		</tr>

		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Comentarios:</b></td>
			<td colspan="3">
				<div style="border-bottom: 1px solid #ddd;">
					<img src="../static/img/user.png" class="img-comen" style="width: 25px;height: 25px;">
					<b>Daniela Manzanilla</b><br> Lorem ipsum dolor sit amet
				</div><br>
				<div style="border-bottom: 1px solid #ddd;">
					<img src="../static/img/user.png" class="img-comen" style="width: 25px;height: 25px;">
					<b>Daniela Manzanilla</b><br> Lorem ipsum dolor sit amet
				</div><br>
				<div style="border-bottom: 1px solid #ddd;">
					<img src="../static/img/user.png" class="img-comen" style="width: 25px;height: 25px;">
					<b>Daniela Manzanilla</b><br> Lorem ipsum dolor sit amet
				</div><br>
			</td>
		</tr>

		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Estatus: </b></td>
			<td style="border-bottom: 1px solid #ddd;">Estatus</td>
		</tr>

		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Monto recaudado:</b></td>
			<td style="border-bottom: 1px solid #ddd;">$4</td>
		</tr>

		<tr>
			<td style="border-bottom: 1px solid #ddd;" colspan="4" align="center"><b>Usuarios unidos al caso</b></td>
		</tr>
		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Nombre y apellido:</b></td>
			<td style="border-bottom: 1px solid #ddd;" colspan="3">Daniela Manzanilla</td>
		</tr>
		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Indentificación:</b></td>
			<td style="border-bottom: 1px solid #ddd;" colspan="3">V-21160654</td>
		</tr>
		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Firma</b></td>
			<td style="border-bottom: 1px solid #ddd;" colspan="3"></td>
		</tr>

		<tr>
			<td style="border-bottom: 1px solid #ddd;" colspan="4" align="center"><b>Abogado representante</b></td>
		</tr>
		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Nombre y apellido:</b></td>
			<td style="border-bottom: 1px solid #ddd;" colspan="3">Gabriel Manzanilla</td>
		</tr>
		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Indentificación:</b></td>
			<td style="border-bottom: 1px solid #ddd;" colspan="3">V-21360654</td>
		</tr>
		<tr>
			<td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;"><b>Matrícula</b></td>
			<td style="border-bottom: 1px solid #ddd;" colspan="3"></td>
		</tr>
		<tr>
			<td style="border-right: 1px solid #ddd;"><b>Firma</b></td>
			<td colspan="3"></td>
		</tr>
	</table>
</body>

</html>