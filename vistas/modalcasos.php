<style type="text/css">
	body{
		background: #f7f7f7;
	}
		 /* Paypal */
	 	@media screen and (max-width: 400px) {
            #paypal-button-container {
                width: 100%;
            }
        }
        
        /* Paypal Media query for desktop viewport */
        @media screen and (min-width: 400px) {
            #paypal-button-container {
                width: 250px;
            }
        }

	.cspan{
		background: #a7a4a47a;
    	position: relative;
    	display: block;
	}
	.cspan1{
		width: 80px;
		height: 80px;
		border-radius: 50%;
    	margin: .5rem;
    	display: inline-block;
    }
    .cspan1-c{
		width: 40px;
		max-width: 100%;
		height: 40px;
		border-radius: 50%;
    	margin: .5rem;
    	display: inline-block;
    }
	.cspan2{
		width: 70%;
		height: 10px;
		display: inline-block;
	}
	.cspan2-2{
		width: 40%;
		height: 10px;
		margin-top: 5px;
    	display: inline-block;
	}
	.cspan3{
		width: 70%;
		height: 10px;
		margin: 2.5933333333rem 0 0.656rem 0;
    }
    .cspan4{
		width: 50%;
		height: 10px;
		margin: 0.5933333333rem 0 0.656rem 0;
    }
    .cspan5{
		width: 35%;
		height: 10px;
		margin: 0.5933333333rem 0 0.656rem 0;
    }
    .cspan {
		animation: animarfondo 2s infinite;
	}
	@keyframes animarfondo{
		0% {
		    background-color: #a7a4a47a;
		}
		50% {
		    background-color:#a7a4a42b;
		}
		100% {
		    background-color:#a7a4a47a;
		}

	}
	.span-arc{
		font-size: 10px !important;
    	padding-left: 18px !important;
    	height: 5px !important;
    	line-height: 22px !important;
	}
	[type="radio"]+span:before, [type="radio"]+span:after {
		width: 10px;
    	height: 10px;
    	border: 2px solid #00b0f0 !important;
	}
	[type="radio"]:checked+span:after, [type="radio"].with-gap:checked+span:after {
    	background-color: #00b0f0;
	}
	.opc_sts {
	    background: #2196f3;
	    color: #fff;
	    padding: .1rem;
	    border-radius: 5px;
	    font-size: 10px;
	    margin-right: 3px;
	}
	
</style>
<!--  modal fondos -->
<div id="modalfondos" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Confirmar postulación | <b class="nom_caso"></b></span>
		<a href="#!" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<form action="" id="crear_fondo">
			<div class="row">
				<div class="col s12">
					<div class="row">
						<div class="col s12">
							<p class="m-0">Monto mínimo para unirse</p>	
							<input type="text"  id="monto_fon" data-decimal="." data-thousands="," class="newInput indent Amount">
						</div>
					</div>
				</div>
				<div class="col s12 text-center hide">
					<div id="paypal-button-container"></div>
				</div>
				<div class="col s12 center my-2">
					<button type="button"   class="btn btn-blue save_fondo">Procesar</button>
					</br>
					<b class="m-0" style="font-size:10px;">Su transacción se confirma de forma automática, pero puede tardar varios minutos.</b>	
				</div>
			</div>
		</form>
	</div>
</div>

<!-- modal crear caso -->
<div id="modalCrearcaso" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Crear un caso</span>
		<a href="#!" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<form action="" id="crear_caso">
			<div class="row">
				<div class="col s12">
					<div class="row">
						<div class="col s12">
							<p class="m-0">Nombre del caso</p>	
							<input type="text" class="newInput indent" id="nom_caso" name="nom_caso">
						</div>

						<div class="col s12">
							<p class="m-0">Descripción</p>	
							<textarea class="textarea" id="des_caso" name="des_caso"></textarea>
						</div>

						<div class="col s10 m4">
							<p class="m-0">Funcionario</p>
							<select class="mt-0 browser-default input-field limpiar_form_2" id="list_fun" name="list_fun">
								<option value="" selected>Selecciona el funcionario</option>
							</select>
						</div>
						<div class="col s2 m2">
							<a href="#modalCrearFuncionario" class="btn btn-blue modal-trigger mt-4"><i class="fa fa-plus"></i></a>
						</div>

						<div class="col s12 m6 mt-1">
							<p class="m-0">Instituto</p>	
							<input type="text" disabled class="newInput indent" id="ver_inst">
						</div>

						<div class="col s12">
							<p class="m-0">Adjuntar archivos</p>
							<div class="file-field input-field m-0">
								<div class="btn btn-blue crear_archivo" id="lista_archivos">
									<span class="fa fa-upload "></span>
								</div>

								<div class="file-path-wrapper">
								</div>
							</div>

							<section class="col s12 lista_archivos">
								<h5 class="text-center">Archivos adjuntos</h5>
							</section>
						</div>
					</div>
				</div>

				<div class="col s12 center my-2">
					<a href="#!" class="btn btn-blue save_caso">Publicar caso</a>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- modal editar caso -->
<div id="modalEditarcaso" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Editar caso</span>
		<a href="#!" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<form action="" id="edit_caso">
			<div class="row">
				<div class="col s12">
					<div class="row">
						<div class="col s12">
							<input type="hidden" class="hide" name="id" id="id_caso">
							<p class="m-0">Nombre</p>	
							<input type="text" class="newInput indent" id="nom_caso_edit" name="nom_caso">
						</div>

						<div class="col s12">
							<p class="m-0">Descripción</p>	
							<textarea class="textarea" id="des_caso_edit" name="des_caso"></textarea>
						</div>

						<div class="col s12">
							<div class="file-field input-field m-0 mb-2">
								<div class="btn btn-blue crear_archivo" id="lista_archivos_e">
									<span class="fa fa-upload "></span>
								</div>

								<div class="file-path-wrapper">
								</div>
							</div>

							<section class="col s12 lista_archivos_e mt-2">
								
							</section>
						</div>
					</div>
				</div>

				<div class="col s12 center my-2">
					<a href="#!" class="btn btn-blue save_edit_caso">Guardar</a>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- modal ver usuarios unidos al caso -->
<div id="modalUsuariosunidos" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Usuarios unidos al caso</span>
		<a href="#!" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<div class="row">
			<div class="col s12">
				<ul class="collection UnidosList">
					
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- modal ver postulados al caso -->
<div id="modalPostuladosunidos" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Postulados al caso</span>
		<a href="#!" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<div class="row">
			<div class="col s12">
				 
				<ul class="collection PostuladosList">

				</ul>
			</div>
		</div>
	</div>
</div>

<!-- modal ver archivos adjuntados -->
<div id="modalArchivosadjuntados" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Archivos adjuntados</span>
		<a href="#!" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<div class="row">
			<div class="col s12">
				<ul class="collection" id="ul_arc">
					<!--<li class="collection-item collection-casos avatar clr_black">
						<i class="fa fa-file circle"></i>
						<p>Nombre-del-archivo</p>
						<a href="#!" class="secondary-content"><i class="fa fa-download"></i></a>
					</li>

					<li class="collection-item collection-casos avatar clr_black">
						<i class="fa fa-image circle green"></i>
						<p>Nombre-del-archivo</p>
						<a href="#!" class="secondary-content"><i class="fa fa-download"></i></a>
					</li>

					<li class="collection-item collection-casos avatar clr_black">
						<i class="fa fa-file-pdf circle red"></i>
						<p>Nombre-del-archivo</p>
						<a href="#!" class="secondary-content"><i class="fa fa-download"></i></a>
					</li>-->
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- modal postularse al caso -->
<div id="modalPostularcaso" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Postularse al caso</span>
		<a href="#!" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
			
		<form action="" id="form_postular">
			<div class="row DIVPostular div_p_n">
				<p class="m-0">Desea postularse a <b class="nom_caso"></b></p>
				<div class="col s12">
					<div class="row">
						<div class="col s6">
							<p class="m-0">Honorario profesional</p></br>
							<input type="text"  id="pre_pos_1" data-decimal="." data-thousands="," class="newInput indent Amount">
						</div>
						<div class="col s6">
							<p class="m-0">Honorario mínimo</p></br>	
							<input type="text"  id="pre_pos_2" data-decimal="." data-thousands="," class="newInput indent Amount">
						</div>
						<div class="col s4 hide">
							<p class="m-0">Precio si es seleccionado como representante</p>	
							<input type="text"  id="pre_pos_3" data-decimal="." data-thousands="," class="newInput indent Amount">
						</div>
						<div class="col s12 m6">
							<p class="m-0">Cargos que propone contra el funcionario</p>	
							<textarea class="textarea" id="car_pos"></textarea>
						</div>
						<div class="col s12 m6">
							<p class="m-0">Comentarios</p>	
							<textarea class="textarea" id="des_pos_2"></textarea>
						</div>
					</div>
				</div>

				<div class="col s12 center my-2">
					<button type="button" href="#!" class="btn btn-blue BTPOSTULARSE">Postularse</button>
				</div>
			</div>
			<div class="row hide DivnoPostular div_p_n">
				<div class="col s12">
					<div class="row">
						<div class="col s12 text-center">
							<h5 class="m-0">Desea no postularse a <b class="nom_caso"></b></h5>	
								<a href="#!" class="btn-blue btn-floating btn-flat waves-effect waves-light BTRETIRARMECASO">
								   <i class="fa fa-check-circle tooltipped" data-position="bottom" data-tooltip="Si"></i>
								</a>
								<a href="#!" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-close">
								   <i class="fa fa-times tooltipped" data-position="bottom" data-tooltip="No"></i>
								</a>
						</div>
					</div>
				</div>
				<div class="col s12 center my-2 hide divRetiro div_p_n">
					<button type="button" class="btn btn-blue BTRETIRARSECASO">Retirarse</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- modal unirse al caso -->
<div id="modalUnirsecaso" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Unirse al caso</span>
		<a href="#!" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<form action="">
			<div class="row DivR_U hide DivUn">
				<div class="col s12">
					<div class="row">
						<div class="col s12 text-center">
							<h5 class="m-0">¿Desea unirse a <b class="nom_caso"></b>?</h5><br>
							<a href="#!" class="btn btn-blue btn-flat waves-effect waves-light BTUNIRME">
								Sí <i class="fa fa-check-circle"></i>
							</a>
							<a href="#!" class="btn btn-blue btn-flat waves-effect waves-light modal-close">
								No <i class="fa fa-times-circle"></i>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row hide DivRetirarse DivR_U">
				<div class="col s12">
					<div class="row">
						<div class="col s12 text-center">
							<h5 class="m-0">¿Desea retirarse del caso: <b class="nom_caso"></b>?</h5><br>	
							<a href="#!" class="btn btn-blue btn-flat waves-effect waves-light BTRETIRARME">
								Sí <i class="fa fa-check-circle"></i>
							</a>
							<a href="#!" class="btn btn-blue btn-flat waves-effect waves-light modal-close">
								No <i class="fa fa-times-circle"></i>
							</a>
						</div>
					</div>
				</div>

				<div class="col s12 center my-2 hide DivRetirarme DivR_U">
					<button type="button" class="btn btn-blue BTRETIRARSE">Retirarse</button>
				</div>
			</div>
			<div class="row hide DivUnirse DivR_U">
				<div class="col s12">
					<div class="row">
						<div class="col s12">
							<p class="m-0">Comentarios acerca del caso</p>	
							<textarea class="textarea" id="des_pos"></textarea>
						</div>
					</div>
				</div>

				<div class="col s12 center my-2">
					<button type="button" class="btn btn-blue BTUNIRSE">Unirse</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- modal crear funcionario -->
<div id="modalCrearFuncionario" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Crear funcionario</span>
		<a href="#!" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<form action="">
			<div class="row">
				<div class="col s12 m8">
					<p class="m-0">Instituciones</p>
					<select class="mt-0 browser-default input-field" id="institucion" name="institucion">
						<option value="" selected>Selecciona la institución</option>
					</select>
				</div>

				<div class="col s4">
					<a href="#" class="btn btn-blue mt-4" id="addInstitucion"><i class="fa fa-plus"></i></a>
				</div>

				<div class="col s12 mt-2">
					<div class="row divBuscadores" id="formInstituto" style="display: none;">
						<a href="#" id="cerrarInstituto"><i class="fa fa-times right p-2"></i></a>
						<h6 class="text-center text-bold">Agregar institución</h6>
						<div class="col s12">
							<p class="m-0">Nombre de la institución</p>	
							<input type="text" class="newInput" id="nom_ins">
						</div>

						<div class="col s12">
							<p class="m-0">Descripción</p>	
							<textarea class="textarea" id="des_ins"></textarea>
						</div>

						<div class="col s12">
							<p class="mb-1">Estado</p>
							<select class="mt-0 browser-default input-field" id="est_i" name="est_i">
								<option value="" selected>Selecciona el estado</option>
							</select>
						</div>

						<div class="col s12">
							<p class="mb-1">Ciudad</p>
							<select class="mt-0 browser-default input-field" id="ciu_i" name="ciu_i">
								<option value="" selected>Selecciona la ciudad</option>
							</select>
						</div>

						<div class="col s12 text-center mt-2 mb-2">
							<a href="#!" class="btn btn-blue save-inst">Agregar</a>
						</div>
					</div>
				</div>


				<div class="col s12 m6">
					<p class="m-0">Cédula</p>	
					<input type="text" class="newInput number" name="ced_fun" id="ced_fun">
				</div>

				<div class="col s12 m6">
					<p class="m-0">Nombre</p>	
					<input type="text" class="newInput" name="nom_fun" id="nom_fun">
				</div>

				<div class="col s12 m6">
					<p class="m-0">Apellido</p>	
					<input type="text" class="newInput" name="ape_fun" id="ape_fun">
				</div>

				<div class="col s12 m6">
					<p class="m-0">Cargo dentro de la Institución</p>	
					<input type="text" class="newInput" name="car_fun" id="car_fun">
				</div>

				<div class="col s12 text-center">
					<a href="#!" class="btn btn-blue save-fun">Guardar</a>
				</div>
			</div>
		</form>
	</div>
</div>
