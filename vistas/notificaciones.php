<style type="text/css">
	body{
		background: #f7f7f7;
	}
		
	.cspan{
		background: #a7a4a47a;
    	position: relative;
    	display: block;
	}
	.cspan1{
		width: 120px;
		height: 120px;
		border-radius: 50%;
    	margin: .5rem;
    }
	.cspan2{
		width: 70%;
		height: 10px;
		margin-left: 10%;
	}
	.cspan2-2{
		width: 40%;
		height: 10px;
		margin-top: 5px;
    	margin-left: 25%;
	}
	.cspan3{
		width: 70%;
		height: 10px;
		margin: 2.5933333333rem 0 0.656rem 0;
    }
    .cspan4{
		width: 50%;
		height: 10px;
		margin: 0.5933333333rem 0 0.656rem 0;
    }
    .cspan5{
		width: 35%;
		height: 10px;
		margin: 0.5933333333rem 0 0.656rem 0;
    }
    .cspan {
		animation: animarfondo 2s infinite;
	}
	@keyframes animarfondo{
		0% {
		    background-color: #a7a4a47a;
		}
		50% {
		    background-color:#a7a4a42b;
		}
		100% {
		    background-color:#a7a4a47a;
		}

	}
	#modalCaso{
		width:85% !important;
	}
	.modalCasoP{
		width:100% !important;
		max-height: 100% !important;
    	height: 100% !important;
    	top: 0% !important;
	}
	.opc_sts {
	    background: #2196f3;
	    color: #fff;
	    padding: .1rem;
	    border-radius: 5px;
	    font-size: 10px;
	    margin-right: 3px;
	    margin-top: 3px;
	}
 
</style>
<div class="cont container">
	<div class="row">
		<div class="boxHead col s12 my-2">
			<h6 class="">NOTIFICACIONES</h6>
		</div>
	</div>

	<div class="row col 12 view_more" style="margin-top: 2rem;">
		<div onclick="$(this).html('<center>Cargando...</center>'); obtener_mis_notis(); ">
			<center>Cargar más...</center>
		</div>
	</div>

	<div class="card-content text-bold misNotificaciones" style="margin-top: 2.5rem;">   
	</div>
	
	<div id="modalCaso" class="modal">
		<div class="modal-header bg-blue py-3">
			<span class="ml-3">Vista previa del caso</span>
			<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
		</div>

		<div class="modal-content DivCuerpo">
			<div class="row mt-3" style="background: #fff;border: 1px solid #ccc;border-radius: .5rem;">
	         <div class="col s12">
	         <div class="row">
	         <div class="col s2 center p-2">
	         <span class="cspan cspan1"></span>
	         <span class="cspan cspan2"></span>
	         <span class="cspan cspan2-2"></span>
	         </div>
	         <div class="col s10">
	         <span class="cspan cspan3"></span>
	         <span class="cspan cspan4"></span>
	         <br>
	         <span class="cspan cspan4"></span>
	         <span class="cspan cspan5"></span>
	         </div>
	         </div>
	         </div>
	         </div>
			</div>
			<ul class="PostuladosList collection hide" style="padding: 1rem;background-color: transparent !important;margin: 1rem;">

			</ul>
	</div>
<!-- modal ver archivos adjuntados -->
<div id="modalArchivosadjuntados" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Archivos adjuntados</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<div class="row">
			<div class="col s12">
				<ul class="collection" id="ul_arc">
					 
				</ul>
			</div>
		</div>
	</div>
</div>



 

</div>
