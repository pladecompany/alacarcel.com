<div class="cont">
	<section class="my-4 container">
		<div class="row" style="margin-bottom: -2rem;">
			<div class="card boxPerfil">
				<div class="boxHead mt-0">
					<h6>MI PERFIL <span class="info-tipo"></span></h6>
				</div>

				<form action="" method="post" id="update_perfil" class="p-3">
					<div class="row">
						<!-- BOTONES PARA USUARIO-->
						<div class="col s12 text-right hab_niv_usu hide">
							<a href="?op=listadodecasos&list=unidos" class="btn waves-effect btn-blue">
								<i class="fa fa-user-plus tooltipped" data-position="bottom" data-tooltip="Casos que me he unido"></i>
							</a>
							<a href="?op=listadodecasos&list=publicados" class="btn waves-effect btn-blue">
								<i class="fa fa-paper-plane tooltipped" data-position="bottom" data-tooltip="Casos que he publicado"></i>
							</a>
						</div>

						<!-- BOTONES PARA ABOGADO-->
						<div class="col s12 text-right hab_niv_abo hide">
							<a href="?op=listadodecasos&list=postulados" class="btn waves-effect btn-blue">
								<i class="fa fa-user-plus tooltipped" data-position="bottom" data-tooltip="Casos en los que me he postulado"></i>
							</a>
							<a href="?op=listadodecasos&list=representados" class="btn waves-effect btn-blue">
								<i class="fa fa-paper-plane tooltipped" data-position="bottom" data-tooltip="Casos que he representado"></i>
							</a>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m4">
							<div class="flexCenter">
								<img class="boxPerfil--img" src="static/img/user.png" onerror="this.src='static/img/user.png'" id="imagen">
							</div>
							
							<div class="text-center">
								<div class="p-image btn btn-floating btn-blue">
									<span class="" onclick=" img_usu.click(); ">
										<i class="fa fa-camera"></i>
									</span>

									<input class="file-upload" type="file" accept="image/*" name="imagen" id="img_usu" />
								</div>
							</div>
						</div>

						<div class="col s6 m4">
							<div class="flexCenter">
								<img class="boxDNI--img" src="static/img/card.png" onerror="this.src='static/img/user.png'" id="imagen_1">
							</div>

							<div class="flexCenter">
								<small>DNI - Frontal</small>
							</div>
							
							<div class="text-center">
								<div class="p-image btn btn-floating btn-blue">
									<span class="" onclick=" img_usu_1.click(); ">
										<i class="fa fa-camera"></i>
									</span>

									<input class="file-upload" type="file" accept="image/*" name="imagen_1" id="img_usu_1" />
								</div>
							</div>
						</div>

						<div class="col s6 m4">
							<div class="flexCenter">
								<img class="boxDNI--img" src="static/img/card.png" onerror="this.src='static/img/user.png'" id="imagen_2">
							</div>

							<div class="flexCenter">
								<small>DNI - Trasera</small>
							</div>
							
							<div class="text-center">
								<div class="p-image btn btn-floating btn-blue">
									<span class=""  onclick=" img_usu_2.click(); ">
										<i class="fa fa-camera"></i>
									</span>

									<input class="file-upload" type="file" accept="image/*" name="imagen_2" id="img_usu_2" />
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m4">
							<div class="input-field">
								<p class="text-bold" for="nom_usu">Nombre</p>
								<input type="text" id="nombree" name="nombree" class="form-app text">
							</div>
						</div>
						
						<div class="col s12 m4">
							<div class="input-field">
								<p class="text-bold" for="ape_usu">Apellido</p>
								<input type="text" id="apellido" name="apellido" class="form-app text">
							</div>
						</div>
						
						<div class="col s12 m4">
							<div class="input-field">
								<p class="text-bold" for="tel_usu">Teléfono</p>
								<input type="text" id="telefono" name="telefono" class="form-app telefono" maxlength="18">
							</div>
						</div>

						<div class="col s12 m4">
							<div class="input-field">
								<p class="text-bold" for="">Tipo de documento</p>
								<select name="tipo_doc" class="browser-default" id="tipo_doc">
									<option value="" selected disabled>Seleccione tipo de documento</option>
									<option>Cédula</option>
									<option>Pasaporte</option>
								</select>
							</div>
						</div>
						
						<div class="col s12 m4">
							<div class="input-field">
								<p class="text-bold" for="num">Número de documento</p>
								<input type="text" id="num" name="num" class="form-app num" maxlength="18">
							</div>
						</div>

						<div class="col s12 m4 mb-3">
							<p class="text-bold mb-0">Sexo</p>	
							<select class="mt-0 browser-default input-field" id="sexo" name="sexo">
								<option value="Femenino" selected>Femenino</option>
								<option value="Masculino">Masculino</option>
							</select>
						</div>

						<!--<div class="col s12 m4">
							<div class="input-field">
								<p class="text-bold" for="pais">País</p>
								<select name="id_pais" class="browser-default" id="pais">
									<option value="" selected disabled>Seleccione un país</option>
								</select>
							</div>
						</div>-->

						<div class="col s12 m4">
							<div class="input-field">
								<p  class="text-bold" for="estado">Estado/Provincia</p>
								<select class="browser-default" id="estado" name="id_estado">
									<option value="" selected disabled>Seleccione un estado...</option>
								</select>
							</div>
						</div>	

						<div class="col s12 m4">
							<div class="input-field">
								<p  class="text-bold" for="ciudad">Ciudad</p>
								<select class="browser-default" id="ciudad" name="id_ciudad">
									<option value="" selected disabled>primero seleccione un estado...</option>
								</select>
							</div>
						</div>	

						<div class="col s12 m4">
							<div class="input-field">
								<p class="text-bold" for="">Correo Paypal para recibir los pagos</p>
								<input type="email" id="correo_paypal" name="correo_paypal" class="form-app">
							</div>
						</div>


						<!--FIRMA -->
						<div class="row">
							<div class="col s12 m12 text-center">
								<h5 class="text-bold">Firma Digital</h5>
							</div>	
							<div class="col s12 m6">
								 
								<div class="bodyFirma" class="col s12" height="100">
									<canvas id="canvas">...</canvas>
								</div>
								<div class="col s12">
									<button style="margin:1rem !important;" id="limpiar" class="btn waves-effect btn-blue left hide" type="button">Limpiar</button>
									<button style="margin:1rem !important;" id="guardarFirma" class="btn waves-effect btn-blue hide right" type="button">Aceptar</button>
								</div>
								
								<input class="hide" type="text"  name="imagen_firma" id="imagen_firma" />
							</div>

							<div class="col s12 m6">
								<div class="flexCenter" style="margin-top: 2.5rem;border: solid 1px #ddd;background-color: white;">
									<img class="" style="max-width:100% !important;" with="100%" src="static/img/logo.png" onerror="this.src='static/img/logo.png'" id="imagen_3">
								</div>
							</div>
						</div>

						<div class="col s12 text-center mt-4">
							<button class="btn waves-effect btn-blue saveperfil" type="button">Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>



<style type="text/css">
	.dropdown-content {
		z-index: 1003;
	}
	
	.bodyFirma{
	  background-color: #eee;
	  font-family: Arial, sans-serif;
	}

	canvas {
	  display: block;
	  margin: 0 auto;
	  margin: calc(30vh - 150px) auto 0;
	  background: #fff;
	  border-radius: 3px;
	  box-shadow: 0px 0px 15px 3px #ccc;
	  cursor:pointer;
	}
	#limpiar,#guardarFirma{
	  text-align:center;
	  cursor:pointer;
	}
</style>