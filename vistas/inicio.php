<div class="row m-0" style="background: #ebf4f9;">
	<div class="col s12 m12 l6 clr_white flexCenter vh100 flexColumn hide-on-med-only hide-on-small-only p-0">
		<img src="static/img/fondo.jpg" class="boxHome">
	</div>

	<div class="col s12 m12 l6 vh100">
		<form method="post" action="#" id="login_entrar" onsubmit="return false;">
			<div class="boxAccount__body">
				<div class="row">
					<div class="col s12 text-center">
						<h5 class="text-bold">CREAR CUENTA</h5>
						<small class="text-center">Una vez que completes el formulario, te enviaremos un correo de confirmación</small>
					</div>
				</div>

				<div class="row m-0">
					<div class="col s12 m12 l6">
						<div class="input-field">
							<p>Rol</p>
							<select class="browser-default input-field mt-0" onchange="tipousuario();" id="tip_reg" name="tip_reg">
								<option value="" selected>Selecciona el rol</option>
								<option value="Abogado">Abogado</option>
								<option value="Usuario">Usuario</option>
							</select>
						</div>
					</div>

					<div class="col s12 m12 l6 ver_abo">
						<div class="input-field">
							<p>Matricula</p>
							<input type="text" name="mat_reg" id="mat_reg"  class="form-control">
						</div>
					</div>

					<div class="col s12 m12 l6 ver_usu" style="display:none;">
						<div class="input-field">
							<p>Nickname</p>
							<input type="text" name="nic_reg" id="nic_reg"  class="form-control">
						</div>
					</div>

					<div class="col s12 m12 l6">
						<div class="input-field">
							<p>Correo</p>
							<input type="email" name="cor_reg" id="cor_reg"  class="form-control">
						</div>
					</div>

					<div class="col s12 m12 l6">
						<div class="input-field">
							<p>Contraseña <i class="icon_ast">*</i></p>
							<input id="password_neg" name="password_neg" type="password" class="form-app" placeholder="Contraseña">
							<button class="btn btn-password" type="button" onclick="mostrarContrasena()">Mostrar</button>
						</div>
					</div>

					<div class="col s12 m12">
						<div class="input-field">
							<p>Confirmar contraseña <i class="icon_ast">*</i></p>
							<input id="password_con" name="password_con" type="password" class="form-app" placeholder="Contraseña">
							<button class="btn btn-password" type="button" onclick="mostrarContrasena('password_con')">Mostrar</button>
						</div>
					</div>
				</div>

				<div class="row mt-2">
					<div class="col s12 text-center">
						<a href="#!" class="btn btn-blue waves-effect save_reg">Crear cuenta</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	function tipousuario(){
		tipo=$("#tip_reg").val();
		if(tipo=='Abogado'){
			$(".ver_abo").show();
			$(".ver_usu").hide();
		}else if(tipo=='Usuario'){
			$(".ver_usu").show();
			$(".ver_abo").hide();
		}else{
			$(".ver_abo").show();
			$(".ver_usu").hide();
		}

	}
</script>
