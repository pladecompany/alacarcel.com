<?php 
  $data = json_decode($_POST['casopdf'], true);
  //include_once("funciones.php");
?>
<style type="text/css">
    *{
        font-family: Times, serif;
    } 
</style>

<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
 
   <table style="border:1px solid #ddd;">
        <tr>
            <td colspan="2" align="center" style="border-bottom: 1px solid #ddd;"><img src="../../static/img/logo.png" width="500"></td>
        </tr>

        <tr>
            <td colspan="2" style="border-bottom: 1px solid #ddd;font-size: 22px;width: 670px;"><b>#<?php echo $data[0]["id"]; ?></b> Caso: <?php echo $data[0]["nom_caso"]; ?></td>
        </tr>

        <tr>
            <td colspan="2" style="border-bottom: 1px solid #ddd;width: 670px;"><?php echo $data[0]["des_caso"]; ?></td>
        </tr>

        <tr>
            <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Funcionario:</b></td>
            <td style="border-bottom: 1px solid #ddd;width: 400px;"><?php echo $data[0]["funcionario"]["nom_fun"]." ".$data[0]["funcionario"]["ape_fun"]; ?></td>
        </tr>

        <tr>
            <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Institución</b></td>
            <td style="border-bottom: 1px solid #ddd;width: 400px;"><?php echo $data[0]["funcionario"]["institucion"]["nom_ins"]; ?></td>
        </tr>

        <tr>
            <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Ubicación</b></td>
            <td style="border-bottom: 1px solid #ddd;width: 400px;" colspan="3"><?php echo $data[0]["funcionario"]["institucion"]["nombre_estado"]." - ".$data[0]["funcionario"]["institucion"]["nombre"]; ?></td>
        </tr>

        <tr>
            <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Comentarios:</b></td>
            <td colspan="3" style="width: 400px;">
                <?php 
                  for($i = 0; $i < count($data[0]["comentarios"]); $i++){
                    $imgc='../../static/img/user.png';
                    if($data[0]["comentarios"][$i]["usuario"]["imagen"]){
                        $imgc=$data[0]["comentarios"][$i]["usuario"]["imagen"];
                        $imgc=explode("\public\uploads\\",$imgc);
                        $imgc="../../api/public/uploads/".$imgc[1];
                    }
                ?>
                    <div style="border-bottom: 1px solid #ddd;">
                        <img src="<?php echo $imgc; ?>" style="border-radius:50%;" class="img-comen" height="25" width="25">
                        <b><?php echo $data[0]["comentarios"][$i]["usuario"]["nom_usu"]." ".$data[0]["comentarios"][$i]["usuario"]["ape_usu"]; ?></b><br> <?php echo $data[0]["comentarios"][$i]["des_pos"]; ?>
                    </div><br>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Estatus: </b></td>
            <?php 
                $txtsts='';
                if($data[0]["sts_caso"]==1)
                    $txtsts='Caso en propuesta';
                else if($data[0]["sts_caso"]==2)
                    $txtsts='Caso en curso';
                else if($data[0]["sts_caso"]==3)
                    $txtsts='Caso terminado';
                else if($data[0]["sts_caso"]==4)
                    $txtsts='Caso cancelado';
                else if($data[0]["sts_caso"]==5)
                    $txtsts='En votación';
            ?>
            <td style="border-bottom: 1px solid #ddd;width: 400px;"><?php echo $txtsts;?></td>
        </tr>
        
        <tr>
            <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Monto recaudado:</b></td>
            <td style="border-bottom: 1px solid #ddd;width: 400px;"><?php echo number_format($data[0]["montor"],2,'.', ','); ?> $</td>
        </tr>

        <tr>
            <td style="border-bottom: 1px solid #ddd;" colspan="4" align="center"><b>Usuarios unidos al caso</b></td>
        </tr>
        <?php 
            for($i = 0; $i < count($data[0]["unidos"]); $i++){
        ?>
            <tr>
                <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Nombre y apellido:</b></td>
                <td style="border-bottom: 1px solid #ddd;width: 400px;" colspan="3"><?php echo $data[0]["unidos"][$i]["usuario"]["nom_usu"]." ".$data[0]["unidos"][$i]["usuario"]["ape_usu"]; ?></td>
            </tr>

            <tr>
                <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Indentificación:</b></td>
                <td style="border-bottom: 1px solid #ddd;width: 400px;" colspan="3"><?php echo $data[0]["unidos"][$i]["usuario"]["num_doc_usu"]; ?></td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Firma</b></td>
                <td style="border-bottom: 1px solid #ddd;width: 400px;" colspan="3">
                    <?php 
                        if($data[0]["unidos"][$i]["usuario"]["imagen_firma"]){
                            $imgc=$data[0]["unidos"][$i]["usuario"]["imagen_firma"];
                            $imgc=explode("\public\uploads\\",$imgc);
                            $imgc="../../api/public/uploads/".$imgc[1];
                            echo '<img src="'.$imgc.'" height="70" width="70">';
                        }   
                    ?>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td style="border-bottom: 1px solid #ddd;" colspan="4" align="center"><b>Abogado representante</b></td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Nombre y apellido:</b></td>
            <td style="border-bottom: 1px solid #ddd;width: 400px;" colspan="3"><?php echo $data[0]["abogado"]["nom_usu"]." ".$data[0]["abogado"]["nom_usu"]; ?></td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Indentificación:</b></td>
            <td style="border-bottom: 1px solid #ddd;width: 400px;" colspan="3"><?php echo $data[0]["abogado"]["num_doc_usu"]; ?></td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;width: 270px;"><b>Matrícula</b></td>
            <td style="border-bottom: 1px solid #ddd;width: 400px;" colspan="3"><?php echo $data[0]["abogado"]["mat_usu"]; ?></td>
        </tr>
        <tr>
            <td style="border-right: 1px solid #ddd;width: 270px;"><b>Firma</b></td>
            <td colspan="3" style="width: 400px;">
                <?php 
                    if($data[0]["abogado"]["imagen_firma"]){
                        $imgc=$data[0]["abogado"]["imagen_firma"];
                        $imgc=explode("\public\uploads\\",$imgc);
                        $imgc="../../api/public/uploads/".$imgc[1];
                        echo '<img src="'.$imgc.'" height="70" width="70">';
                    }   
                ?>
            </td>
        </tr>
    </table>
 
</page>
