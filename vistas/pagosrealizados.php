<center>
<section class="my-3 Section">
	<style type="text/css" media="screen">
		.Section {
			width:80% !important;
		}
	</style>
	<h4 class="text-center clr_blue text-bold">PAGOS REALIZADOS</h4>
	 

	<table align="center" class="table pagos display dt-responsive funt" id="table" cellspacing="0" width="70%">
		<thead class="">
			<tr>
				<th>#</th>
				<th>ID del pago</th>
				<th>Codigo de paypal</th>
				<th>ID del caso</th>
				<th>Caso</th>
				<th>Monto aportado</th>
				<th>Fecha</th>
				<th>Estatus</th>
				<th>Opciones</th>
			</tr>

		</thead>

		<tbody>
			
		</tbody>
	</table>
</section>
</center>

<div id="modalVer" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Ver registro</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<ul class="collection with-header">
			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>ID:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 ID"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Caso:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 CASO"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Monto:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 MONTO"></p>
					</div>
				</div>
			</li>
			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Fecha:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 FECHA"></p>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>

</div>



