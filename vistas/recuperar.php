<div class="flexCenter cont bg_full">
	<div class="boxAccount__form">
		<form method="post" action="#" class="login-form" id="recuperar" onsubmit="return false;">
			
			<h5 class="center boxAccount--title">Recuperar contraseña</h5>
			
			<div class="boxAccount__body">
				<div class="row">
					<div class="col s12">
						<div class="input-field">
							<p>Correo electrónico</p>
							<input id="correo" name="correo" type="text" class="form-app" placeholder="Correo electrónico">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col s12 text-center">
						<button type="submit" id="btn_entrar" class="btn btn-blue waves-effect">Enviar<i class="fa" id="login1"></i></button>
					</div>
				</div>

				<div class="row">
					<div class="col s12 text-center">
						<b>¿Primera vez? <a href="?op=inicio" class="clr_blue">Regístrate</a></b><br>
						<b>¿Ya tienes cuenta? <a href="?op=iniciarsesion" class="clr_blue">Iniciar sesión</a></b>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

