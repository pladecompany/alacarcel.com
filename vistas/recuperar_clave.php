<div class="flexCenter cont bg_full">
	<div class="boxAccount__form">
		<form method="post" action="#" class="login-form" id="recuperar_clave" onsubmit="return false;">
			
			<h5 class="center boxAccount--title">Recuperar contraseña</h5>
			
			<div class="boxAccount__body">
				<div class="row hide">
					<div class="col s12">
						<div class="input-field">
							<p>TOKEN</p>
							<input id="token" name="token" value="<?php echo $_GET['token']; ?>">
						</div>
					</div>
				</div>
				<div class="col s12 m12">
					<div class="input-field">
						<p for="nue_cla">Nueva contraseña</p>
						<input type="password" id="nue_cla" name="pass_new" class="form-app" required>
					</div>
				</div>

				<div class="col s12 m12">
					<div class="input-field">
						<p for="rep_cla">Repetir contraseña</p>
						<input type="password" id="rep_cla" name="pass_new_con" class="form-app" required>
					</div>
				</div>
				<div class="row">
					<div class="col s12 text-center">
						<button type="submit" id="btn_REC" class="btn btn-blue waves-effect">Enviar <i class="fa" id="RES1"></i></button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

