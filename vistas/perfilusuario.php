<div class="row">
	<div class="col s12">
		<div style="background-image: url('static/img/portada.jpg');background-size: 100% 100%;background-repeat: no-repeat;">
			<div style="background: #fff;border: 2px solid #ccc; left: 50px;top: 50px;padding: 3rem;position: relative;width: 300px;text-align:center;">
				<img src="static/img/user.png" width="70%">
				<h6 class="text-center text-bold clr_blue">Daniela Manzanilla</h6>
			</div>
		</div>
	</div>
</div>

<br><br>

<div class="container">
	<h4 class="text-center clr_blue text-bold">Casos que he publicado</h4>
	<div class="row mt-3" style="background: #fff;border: 1px solid #ccc;border-radius: .5rem;">
		<div class="col s12">
			<div class="row">
				<div class="col s2 center p-2">
					<img src="static/img/user.png" onerror="this.src='static/img/favicon.png'" class="img-user">
					<h6>Daniela Manzanilla</h6>
				</div>

				<div class="col s10 p-1">
					<i class="fa fa-ellipsis-v right btnOpc" id="'+datos.id+'" style="cursor: pointer;"></i>
					<div id="divopciones'+datos.id+'" style="display: none;">
						<i class="fa fa-times right btnx" style="color: red;" id="'+datos.id+'"></i>
						<textarea id='dat_c_"+datos.id+"' style='display:none;'></textarea><a href="#" class="clr_black edit-caso" id="'+datos.id+'"><i class="fa fa-edit right" id="btnEditar"></i></a>
					<a href="#" class="clr_black"><i class="fa fa-trash right delete_c" id="'+datos.id+'"></i></a>
					</div>

					<h5>Caso: Edificio el SISAL</h5>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<h6><i class="fa fa-user"></i> Funcionario &nbsp;&nbsp;&nbsp;<i class="fa fa-building"></i> Instituto &nbsp;&nbsp;&nbsp;<a href="#modalArchivosadjuntados" class="clr_black"><i class="fa fa-folder-open"></i> Archivos adjuntos</a></h6>

					<a href="#modalUsuariosunidos" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-eye tooltipped modal-trigger" data-position="bottom" data-tooltip="Unidos al caso"></i>
					</a>

					<a href="#modalPostuladosunidos" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-users tooltipped" data-position="bottom" data-tooltip="Postulados"></i>
					</a>

					<a href="#modalUnirsecaso" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-paper-plane tooltipped" data-position="bottom" data-tooltip="Unirse al caso"></i>
					</a>

					<a href="#modalPostularcaso" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-user-plus tooltipped" data-position="bottom" data-tooltip="Postularse al caso"></i>
					</a>
			
					<br><small class="right"><i class="fa fa-clock"></i> hace 3 horas</small>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row mt-3" style="background: #fff;border: 1px solid #ccc;border-radius: .5rem;">
		<div class="col s12">
			<div class="row">
				<div class="col s2 center p-2">
					<img src="static/img/user.png" onerror="this.src='static/img/favicon.png'" class="img-user">
					<h6>Daniela Manzanilla</h6>
				</div>

				<div class="col s10 p-1">
					<i class="fa fa-ellipsis-v right btnOpc" id="'+datos.id+'" style="cursor: pointer;"></i>
					<div id="divopciones'+datos.id+'" style="display: none;">
						<i class="fa fa-times right btnx" style="color: red;" id="'+datos.id+'"></i>
						<textarea id='dat_c_"+datos.id+"' style='display:none;'></textarea><a href="#" class="clr_black edit-caso" id="'+datos.id+'"><i class="fa fa-edit right" id="btnEditar"></i></a>
					<a href="#" class="clr_black"><i class="fa fa-trash right delete_c" id="'+datos.id+'"></i></a>
					</div>

					<h5>Caso: Edificio el SISAL</h5>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<h6><i class="fa fa-user"></i> Funcionario &nbsp;&nbsp;&nbsp;<i class="fa fa-building"></i> Instituto &nbsp;&nbsp;&nbsp;<a href="#modalArchivosadjuntados" class="clr_black"><i class="fa fa-folder-open"></i> Archivos adjuntos</a></h6>

					<a href="#modalUsuariosunidos" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-eye tooltipped modal-trigger" data-position="bottom" data-tooltip="Unidos al caso"></i>
					</a>

					<a href="#modalPostuladosunidos" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-users tooltipped" data-position="bottom" data-tooltip="Postulados"></i>
					</a>

					<a href="#modalUnirsecaso" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-paper-plane tooltipped" data-position="bottom" data-tooltip="Unirse al caso"></i>
					</a>

					<a href="#modalPostularcaso" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-user-plus tooltipped" data-position="bottom" data-tooltip="Postularse al caso"></i>
					</a>
			
					<br><small class="right"><i class="fa fa-clock"></i> hace 3 horas</small>
				</div>
			</div>
		</div>
	</div>

	<div class="row mt-3" style="background: #fff;border: 1px solid #ccc;border-radius: .5rem;">
		<div class="col s12">
			<div class="row">
				<div class="col s2 center p-2">
					<img src="static/img/user.png" onerror="this.src='static/img/favicon.png'" class="img-user">
					<h6>Daniela Manzanilla</h6>
				</div>

				<div class="col s10 p-1">
					<i class="fa fa-ellipsis-v right btnOpc" id="'+datos.id+'" style="cursor: pointer;"></i>
					<div id="divopciones'+datos.id+'" style="display: none;">
						<i class="fa fa-times right btnx" style="color: red;" id="'+datos.id+'"></i>
						<textarea id='dat_c_"+datos.id+"' style='display:none;'></textarea><a href="#" class="clr_black edit-caso" id="'+datos.id+'"><i class="fa fa-edit right" id="btnEditar"></i></a>
					<a href="#" class="clr_black"><i class="fa fa-trash right delete_c" id="'+datos.id+'"></i></a>
					</div>

					<h5>Caso: Edificio el SISAL</h5>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<h6><i class="fa fa-user"></i> Funcionario &nbsp;&nbsp;&nbsp;<i class="fa fa-building"></i> Instituto &nbsp;&nbsp;&nbsp;<a href="#modalArchivosadjuntados" class="clr_black"><i class="fa fa-folder-open"></i> Archivos adjuntos</a></h6>

					<a href="#modalUsuariosunidos" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-eye tooltipped modal-trigger" data-position="bottom" data-tooltip="Unidos al caso"></i>
					</a>

					<a href="#modalPostuladosunidos" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-users tooltipped" data-position="bottom" data-tooltip="Postulados"></i>
					</a>

					<a href="#modalUnirsecaso" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-paper-plane tooltipped" data-position="bottom" data-tooltip="Unirse al caso"></i>
					</a>

					<a href="#modalPostularcaso" class="btn-blue btn-floating btn-flat waves-effect waves-light modal-trigger">
						<i class="fa fa-user-plus tooltipped" data-position="bottom" data-tooltip="Postularse al caso"></i>
					</a>
			
					<br><small class="right"><i class="fa fa-clock"></i> hace 3 horas</small>
				</div>
			</div>
		</div>
	</div>

	<div class="row mb-0 text-center mb-3"> 
		<ul class="pagination">
			<li class="disabled"><a href="#!"><i class="mdi-navigation-chevron-left"></i></a></li>
			<li class="active"><a href="#!">1</a></li>
			<li class="waves-effect"><a href="#!">2</a></li>
			<li class="waves-effect"><a href="#!">3</a></li>
			<li class="waves-effect"><a href="#!">4</a></li>
			<li class="waves-effect"><a href="#!">5</a></li>
			<li class="waves-effect"><a href="#!"><i class="mdi-navigation-chevron-right"></i></a></li>
		</ul>
	</div>
</div>