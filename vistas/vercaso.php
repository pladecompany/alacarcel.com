<div style="background: #f7f7f7;min-height: 90vh;" class="cont">
	<div class="row m-0">
		<div class="col s12 m3">
			<div class="row">
				<div class="col s12 hide">
					<ul class="collection">
						<li class="collection-header">
							<h6 class="text-left pl-1">Saldo disponible <a href="#modalfondos" class="modal-trigger right"><i class="fa fa-plus-circle clr_white pr-2"></i></a></h6>
						</li>	
						<li class="collection-item text-center">
							<h1 style="color: #3f51b5;"><span>0.00</span> $</h1>
						</li>
					</ul>
				</div>

				<div class="col s12">
					<ul class="collection">
						<li class="collection-header">
							<h6 class="text-center">Buscar caso</h6>
						</li>	
						<li class="collection-item text-center">
							<input type="search" name="search" id="search" class="form-app">
							<a href="#" class="btn btn-blue clr_white btn_buscar_casos"> Buscar</a>
						</li>
					</ul>
				</div>

				<div class="col s12">
					<ul class="collection">
						<li class="collection-header">
							<h6 class="text-center">Estado</h6>
						</li>	
						<li class="collection-item">
							<select class="mt-0 browser-default input-field" id="estado" name="estado">
								<option selected>Selecciona el Estado</option>
							</select>
						</li>
					</ul>
				</div>

				<div class="col s12">
					<ul class="collection li-ciudades">
						
					</ul>
				</div>
			</div>
		</div>

		<div class="col s12 m9">
			<div class="row mt-3 divCaso">
				<div class="col s12">
					<div class="row mb-0">
						<div class="col s2 center p-2">
							<a href="?op=listadodecasos&amp;list=publicados&amp;usuario=1">
								<img src="static/img/flag-icons/Argentina.png" class="img-user boxPerfil--img img-cover imgcasos">
							</a>
							<h6 style="font-size:14px;"><b>Usuario Apellido Usuario</b></h6>
						</div>

						<div class="col s10 p-1">
							<span><i class="ml-1 fa fa-ellipsis-v right btnOpc" id="6" style="cursor: pointer;"></i></span>
							<div id="divopciones6" style="display: none;">
								<a class="btn-blue btn-floating btn-opciones m-1 right"><i class="fa fa-times right btnx" id="6"></i></a>
								<textarea id="dat_c_6" style="display:none;"></textarea>

								<a href="#" class="btn-blue btn-floating btn-opciones m-1 right edit-caso" id="6">
									<i class="fa fa-edit" id="btnEditar"></i>
								</a>

								<a href="#" class="btn-blue btn-floating btn-opciones m-1 right">
									<i class="fa fa-trash delete_c" id="6"></i>
								</a>
							</div>
							<span class="right opc_ciu pl-2 pr-2 text-uppercase">
								<i class="fa fa-flag"></i> Buenos Aires - CRUCESITA
							</span>

							<h5><b class="tooltipped" data-position="bottom" data-tooltip="ID del Caso">#6 </b>Caso: Caso prueba</h5>

							<h6 class="casoMonto m-0">Monto recaudado: <b class="clr_blue">200$</b></h6>

							<p class="m-0">prueba prueba</p>

							<h6><i class="fa fa-user icon"></i> Juan  Tramposo &nbsp;&nbsp;&nbsp;<i class="fa fa-building icon"></i> Alcaldia de la Cuidad Crucesita &nbsp;&nbsp;&nbsp;

							<a style="cursor:pointer;" id="6" class="clr_black ver_archivos ver_archivo_6">
								<i class="fa fa-folder-open icon"></i> Archivos adjuntos&nbsp;&nbsp;
							</a>

							<a style="cursor:pointer;" id="6" class="clr_black btnComentarios">
								<i class="fa fa-comments icon"></i> 0 comentarios &nbsp;&nbsp;
							</a>
							</h6>

							<button type="button" id="6" href="#!" class="btn-blue btn-flat btn-floating  waves-effect waves-light BTUNIDOS">
								<i class="fa fa-eye tooltipped total_unidos" style="font-size:16px;" id="6" data-position="bottom" data-tooltip="Unidos al caso <b>(0)</b>"></i>
							</button> &nbsp;

							<button type="button" id="6" class="btn-blue btn-flat btn-floating  waves-effect waves-light BTPOSTULADOS">
								<i class="fa fa-users tooltipped total_postulados" style="font-size:16px;" id="6" data-position="bottom" data-tooltip="Postulados al caso <b>(0)</b>"></i>
							</button> &nbsp;
							<br>
							<small class="right"><i class="fa fa-clock"></i> hace unos segundos</small>

							<div id="divComentarios" class="mt-3 divComentarios" style="margin:0;">
								<div class="row m-0">
									<div class="col s1">
										<img src="static/img/user.png" class="img-user img-cover" style="width: 100%;">
									</div>

									<div class="col s11">
										<h6 class="mt-1"><b>Pedro Escamoso</b></h6>
									</div>

									<div class="col s12">
										<h6 class="m-0" style="word-break: break-word;width: 95% !important;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua.</h6>
									</div>
								</div>
								<hr>

								<div class="row m-0">
									<div class="col s1">
										<img src="static/img/user.png" class="img-user img-cover" style="width: 100%;">
									</div>

									<div class="col s11">
										<h6 class="mt-1"><b>Pedro Escamoso</b></h6>
									</div>

									<div class="col s12">
										<h6 class="m-0" style="word-break: break-word;width: 95% !important;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua.</h6>
									</div>
								</div>
							</div>


							<div class="mt-4">
								<h6 class="clr_blue text-center text-bold">Votación para abogados</h6>
								<ul class="collection">
									<li class="collection-item ">
										<div class="row mb-0">
											<div class="col s1">
												<p class="clr_black p-0">
													<label>
														<input name="group1" type="radio" />
														<span></span>
													</label>
												</p>
											</div>

											<div class="col s1 mt-2">
												<img src="static/img/user.png" alt="" class="circle w100">
											</div>

											<div class="col s9">
												<p class="clr_black p-0">
													Abogado
												</p>
											</div>
										</div>
									</li>
									<li class="collection-item ">
										<div class="row mb-0">
											<div class="col s1">
												<p class="clr_black p-0">
													<label>
														<input name="group1" type="radio" />
														<span></span>
													</label>
												</p>
											</div>

											<div class="col s1 mt-2">
												<img src="static/img/user.png" alt="" class="circle w100">
											</div>

											<div class="col s9">
												<p class="clr_black p-0">
													Abogado
												</p>
											</div>
										</div>
									</li>
									<li class="collection-item ">
										<div class="row mb-0">
											<div class="col s1">
												<p class="clr_black p-0">
													<label>
														<input name="group1" type="radio" />
														<span></span>
													</label>
												</p>
											</div>

											<div class="col s1 mt-2">
												<img src="static/img/user.png" alt="" class="circle w100">
											</div>

											<div class="col s9">
												<p class="clr_black p-0">
													Abogado
												</p>
											</div>
										</div>
									</li>
								</ul>

								<div class="text-center">
									<a href="" class="btn btn-blue">Votar</a>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>