<script type="text/javascript" src="static/js/backend-panel/general.js"></script>
<script>
	if(!user || user.rol == 'admin'){
		location.href = '?op=iniciarsesion'
	}
</script>

<div class="" style="min-height: 40rem;">
	<div class="container row inicio mt-5 mb-5">
		<div class="col s12 m4 mb-3">
			<a href="?op=home">
				<div class="boxInicio text-center">
					<i class="fa fa-home clr_blue fa-2x"></i>
					<p class="clr_black">Inicio</p>
				</div>
			</a>
		</div>

	<!-- OPCIONES COMO POSTULADO -->

		<div class="col s12 m4 mb-3">
			<a href="?op=perfil" id="link_plan">
				<div class="boxInicio text-center">
					<i class="fa fa-user clr_blue fa-2x"></i>
					<p class="clr_black">Mi perfil</p>
				</div>
			</a>
		</div>

		<div class="col s12 m4 mb-3 hab_niv_usu hide">
			<a href="?op=crearcaso" id="link_plan">
				<div class="boxInicio text-center">
					<i class="fa fa-suitcase clr_blue fa-2x"></i>
					<p class="clr_black">Crear Caso</p>
				</div>
			</a>
		</div>
	</div>
</div>