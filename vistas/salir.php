<script>
  var pais =  window.sessionStorage.getItem("pais");
  var redirigir = false;
  if(window.sessionStorage.getItem('casocompartirview') && window.sessionStorage.getItem('casocompartirview')!='null')
  	  redirigir = window.sessionStorage.getItem('casocompartirview');
  window.sessionStorage.clear();
  if(redirigir)
  	window.sessionStorage.setItem("casocompartirview", redirigir);	
  window.sessionStorage.setItem("pais", pais);
  window.location ="../?op=iniciarsesion";
</script>
