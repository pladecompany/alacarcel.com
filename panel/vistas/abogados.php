<section class="my-4">
	<h4 class="text-center clr_blue text-bold">ABOGADOS</h4>
	<table class="table display dt-responsive clientes usu" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Teléfono</th>
				<th>Matrícula</th>
				<th>Correo</th>
				<th>Opciones</th>
			</tr>

		</thead>

		<tbody>
			<!--<tr>
				<td>1</td>
				<td>Nombre</td>
				<td>Apellido</td>
				<td>Teléfono</td>
				<td>Matrícula</td>
				<td>Correo</td>
				<td>
					<a href="#modalVer" class="modal-trigger btn btn-blue"><i class="fa fa-eye"></i></a>
					<a href="#modalEditar" class="modal-trigger btn btn-blue"><i class="fa fa-edit"></i></a>
					<a href="#modalEliminar" class="modal-trigger btn btn-blue"><i class="fa fa-trash"></i></a>
				</td>
			</tr>-->
		</tbody>
	</table>
</section>

<!-- Modal -->

<div id="modalEditar" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Editar abogado</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<form action="" id="update_usu" class="">
			<input type="hidden" id="id_usu" name="id_usu">
			<div class="row">
				<div class="col s12 m2 offset-m5">
					<div class="flexCenter">
						<img class="boxPerfil--img img-cover" src="../static/img/user.png" onerror="this.src='../static/img/user.png'" id="imagen">
					</div>
					
					<div class="text-center">
						<div class="p-image btn-floating btn-blue">
							<i class="fa fa-upload upload-button"></i>
							<input class="file-upload" type="file" accept="image/*" name="imagen" id="img_usu" />
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<div class="row">
						<div class="col s6">
							<h6>Nombre</h6>	
							<input type="text" class="newInput indent" id="nom_usu" name="nom_usu">
						</div>

						<div class="col s6">
							<h6>Apellido</h6>	
							<input type="text" class="newInput indent" id="ape_usu" name="ape_usu">
						</div>

						<div class="col s6">
							<h6>Teléfono</h6>	
							<input type="text" class="newInput indent" id="tlf_usu" name="tlf_usu">
						</div>

						<div class="col s6">
							<h6 for="num">Matrícula</h6>
							<input type="text" id="mat_usu" name="mat_usu" class="form-app num" maxlength="18">
						</div>

						<div class="col s6">
							<h6>Correo</h6>	
							<input type="text" class="newInput indent" id="cor_usu" name="correo">
						</div>

						<div class="col s12 m6 mb-3">
							<div class="input-field">
								<p for="">Tipo de documento</p>
								<select name="tipo_doc" class="browser-default" id="tipo_doc">
									<option value="" selected disabled>Seleccione tipo de documento</option>
									<option>Cédula</option>
									<option>Pasaporte</option>
								</select>
							</div>
						</div>
						
						<div class="col s12 m6">
							<div class="input-field">
								<p for="num">Número de documento</p>
								<input type="text" id="num" name="num" class="form-app num" maxlength="18">
							</div>
						</div>

						<div class="col s12 m6 mb-3">
							<p class="text-bold mb-0">Sexo</p>	
							<select class="mt-0 browser-default input-field" id="sexo" name="sexo">
								<option value="Femenino" selected>Femenino</option>
								<option value="Masculino">Masculino</option>
							</select>
						</div>

						<!--<div class="col s12 m6 mb-3">
							<div class="input-field">
								<p for="pais">País</p>
								<select name="id_pais" class="browser-default" id="pais">
									<option value="" selected disabled>Seleccione un país</option>
								</select>
							</div>
						</div>-->

						<div class="col s12 m6">
							<div class="input-field">
								<p  for="estado">Estado/Provincia</p>
								<select class="browser-default" id="estado" name="id_estado">
									<option value="" selected disabled>primero seleccione un país...</option>
								</select>
							</div>
						</div>	

						<div class="col s12 m6">
							<div class="input-field">
								<p  for="ciudad">Ciudad</p>
								<select class="browser-default" id="ciudad" name="id_ciudad">
									<option value="" selected disabled>primero seleccione un estado...</option>
								</select>
							</div>
						</div>	
						<div class="col s12 m6 mb-3">
							<p class="text-bold mb-0">Estatus</p>	
							<select class="mt-0 browser-default input-field" id="sts" name="sts">
								<option value="0">Inactivo</option>
								<option value="1">Completar perfil</option>
								<option value="2">Activo</option>
								<option value="-1">Bloqueado</option>
							</select>
						</div>
					</div>
				</div>

				<div class="col s12 center my-2">
					<a href="#" class="btn btn-blue saveusu">Editar</a>
				</div>
			</div>
		</form>
	</div>
</div>


<div id="modalEliminar" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Eliminar abogado</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content text-center">
		<h5>¿Desea eliminar éste abogado?</h5>
		<a href="#" class="btn btn-blue">Sí</a>
		<a href="#" class="btn btn-blue">No</a>
	</div>
</div>

<div id="modalVer" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Ver abogado</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<ul class="collection with-header">
			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s12 flexCenter">
						<img src="../static/img/user.png" alt="" class="boxPerfil--img" id="perfil">
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Nombre:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 nom_usu"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Apellido:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 ape_usu"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Teléfono:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 tel_usu"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Matrícula</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 mat_usu"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Correo</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 cor_usu"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Documento:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 doc_usu"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Sexo</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 gen_usu"></p>
					</div>
				</div>
			</li>

			<!--<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>País:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 pai_usu"></p>
					</div>
				</div>
			</li>-->

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Estado:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 est_usu"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Ciudad:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 ciu_usu"></p>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>
<script type="text/javascript">
	tipousuario = 'abogado';
</script>


