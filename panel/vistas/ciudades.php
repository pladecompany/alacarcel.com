<section class="my-4">
	<h4 class="text-center clr_blue text-bold">CIUDADES</h4>
	<div class="text-right mb-2">
		<a href="#modalCiudades" class="modal-trigger btn btn-blue open">
			<i class="fa fa-plus-circle"></i> AGREGAR
		</a>
	</div>

	<table class="table display dt-responsive ciudades" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>N°</th>
				<th>País</th>
				<th>Estado/Provincia</th>
				<th>Ciudad</th>
				<th>Acciones</th>
			</tr>
		 </thead>

		<tbody>
	
			
		</tbody>
	</table>
</section>


<!-- Modal -->
<div id="modalCiudades" class="modal modalFullmv">
	<div class="modal-header">
		<div class="boxHead mt-0">
			<h6 id="title">Nueva ciudad</h6>
			<a href="#" class="right modal-close white-text mr-4"><img src="../static/img/icons/times.png" class="btnCerrar"></a>
		</div>
	</div>

	<div class="modal-content">
		<form action="" method="" id="form" onsubmit="return false;">
			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Selecciona el país <i class="icon_ast">*</i></label>
						<select id="pais" class="browser-default">
							
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Selecciona Estado/Provincia <i class="icon_ast">*</i></label>
						<select id="estado" class="browser-default">
							
						</select>
					</div>
				</div>
			</div>

			<div class="row no_edit">
				<div class="form-group col s10">
					<label >Nombre de la ciudad <i class="icon_ast">*</i></label>
					<textarea class="textarea" name="nom_ciu" id="ciudades" class="form-control"></textarea>
				</div>
				<div class="input-field col s2 pt-2">
					<button class="btn btn btn-blue add pt-1"><i class="fa fa-plus"></i></button>
				</div>

				<div class="col s12 ciudadess">
					
				</div>
			</div>

			<div class="row edite">
				<div class="form-group col s12">
					<label >Nombre de la ciudad</label>
					<textarea class="textarea"  id="ciudades_edit" class="form-control"></textarea>
				</div>
			</div>

			<div class="row">
				<div class="col s12 center">
					<a href="#" onclick="return false" class="btn btn-blue save">Guardar</a>
				</div>
			</div>
		</form>
	</div>
</div>


