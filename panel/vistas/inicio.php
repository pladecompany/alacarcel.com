<div class="">
	<div class="row mt-3">
		<div class="col s10 m5 offset-m3 text-center">
			<input type="search" class="number" id="id_del_caso" name="id_del_caso" placeholder="Tipee el ID del caso">
		</div>	
		<div class="col s2 m2 mt-1">
			<a href="#!" class="btn btn-blue buscar-caso"><i class="fa fa-search"></i></a>
			<a href="#!" class="btn btn-blue generar-pdf-admin"><i class="fa fa-file-pdf"></i></a>
		</div>
	</div>

	<div class="row mt-4">
		<div class="col s6 m3 mb-3">
			<a href="?op=instituciones">
				<div class="boxInicio text-center">
					<i class="fa fa-building fa-3x clr_blue"></i>
					<p class="clr_black">Instituciones</p>
				</div>
			</a>
		</div>

		<div class="col s6 m3 mb-3">
			<a href="?op=funcionarios">
				<div class="boxInicio text-center">
					<i class="fa fa-users fa-3x clr_blue"></i>
					<p class="clr_black">Funcionarios</p>
				</div>
			</a>
		</div>

		<div class="col s6 m3 mb-3">
			<a href="?op=usuarios">
				<div class="boxInicio text-center">
					<i class="fa fa-users fa-3x clr_blue"></i>
					<p class="clr_black">Usuarios</p>
				</div>
			</a>
		</div>

		<div class="col s6 m3 mb-3">
			<a href="?op=abogados">
				<div class="boxInicio text-center">
					<i class="fa fa-suitcase fa-3x clr_blue"></i>
					<p class="clr_black">Abogados</p>
				</div>
			</a>
		</div>

		<div class="col s6 m3 mb-3">
			<a href="?op=transacciones">
				<div class="boxInicio text-center">
					<i class="fa fa-stream fa-3x clr_blue"></i>
					<p class="clr_black">Transacciones</p>
				</div>
			</a>
		</div>

		<div class="col s6 m3 mb-3">
			<a href="?op=pagos_pendientes">
				<div class="boxInicio text-center">
					<i class="fa fa-tasks fa-3x clr_blue"></i>
					<p class="clr_black">Pagos pendientes</p>
				</div>
			</a>
		</div>

		<div class="col s6 m3 mb-3">
			<a href="?op=configuracion">
				<div class="boxInicio text-center">
					<i class="fa fa-cogs fa-3x clr_blue"></i>
					<p class="clr_black">Configuración</p>
				</div>
			</a>
		</div>

	</div>
</div>