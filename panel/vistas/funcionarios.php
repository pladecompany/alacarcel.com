<section class="my-4">
	<h4 class="text-center clr_blue text-bold">FUNCIONARIOS</h4>
	<div class="hide-on-med-and-down text-right mb-2">
		<a href="#" class="modal-trigger btn btn-blue add_funcionario">
			<i class="fa fa-plus-circle"></i> AGREGAR
		</a>
	</div>

	<table class="table display dt-responsive clientes funt" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>Cédula</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Institución</th>
				<th>Cargo</th>
				<th>Opciones</th>
			</tr>

		</thead>

		<tbody>
		</tbody>
	</table>
</section>

<!-- Modal -->
<div id="instituto" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Registrar funcionario</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<form action="" class="">
			<input type="hidden" id="id_fun" class="limpiar_form" value="">
			<div class="row">
				<div class="col s12">
					<div class="row">
						<div class="col s12 m12">
							<h6 class="text-bold">Cédula</h6>	
							<input type="text" class="newInput indent limpiar_form number" id="ced_fun">
						</div>

						<div class="col s12 m6">
							<h6 class="text-bold">Nombre</h6>	
							<input type="text" class="newInput indent limpiar_form" id="nom_fun">
						</div>

						<div class="col s12 m6">
							<h6 class="text-bold">Apellido</h6>	
							<input type="text" class="newInput indent limpiar_form" id="ape_fun">
						</div>

						<div class="col s12 m6">
							<h6 class="text-bold">Institución</h6>
							<select class="browser-default limpiar_form_2" id="institucion">
								<option value="" selected>Selecciona el institucion</option>
							</select>
						</div>

						<div class="col s12 m6 mt-1">
							<h6 class="text-bold">Cargo</h6>
							<input type="text" class="newInput indent limpiar_form" id="cargo">
						</div>
					</div>
				</div>

				<div class="col s12 center my-2">
					<a href="#" class="btn btn-blue save">Guardar</a>
				</div>
			</div>
		</form>
	</div>
</div>


<div id="modalEliminar" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Eliminar registro</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content text-center">
		<h5>¿Desea eliminar éste registro?</h5>
		<a href="#" class="btn btn-blue">Sí</a>
		<a href="#" class="btn btn-blue">No</a>
	</div>
</div>

<div id="modalVer" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Ver registro</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<ul class="collection with-header">
			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Cédula:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 ced_fun"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Nombre:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 nom_fun"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Apellido:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 ape_fun"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Institución:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 ins_fun"></p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Cargo</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 car_fun"></p>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>

</div>



