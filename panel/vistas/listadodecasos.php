<div class="row mt-3">
	<h4 class="text-center">Casos</h4>
	<div class="col s12 m3 offset-m1">
		<select class="mt-0 browser-default input-field" id="sts_caso_b" name="sts_caso_b">
			<option value="" disabled selected>Selecciona el estatus del caso</option>
			<option value="1">Caso en propuesta</option>
			<option value="2">Caso en curso</option>
			<option value="3">Caso terminado</option>
			<option value="4">Caso cancelado</option>
			<option value="5">En votación</option>
		</select>
	</div>
	<div class="col s12 m2 mt-1">
		<a href="#!" class="btn btn-blue" id="save_sts_caso"><i class="fa fa-check"></i></a>
	</div>
	<div class="col s12 m4 text-center">
		<input type="search" class="number" id="id_del_caso" name="id_del_caso" placeholder="Tipee el ID del caso">
	</div>	
	<div class="col s12 m2 mt-1">
		<a href="#!" class="btn btn-blue buscar-caso"><i class="fa fa-search"></i></a>
	</div>
</div>

<div class="row mb-0 mt-3" id="publicados">
	<div class="row mb-0 divCaso hide">
		<div class="col s2 center p-2">
			<a href="?op=perfil-usuario">
				<img src="https://inthecompanies.com:8200\public\uploads\cbbc51407d6b67a82347159d2b59788d8347.jpg" class="img-user boxPerfil--img img-cover imgcasos">
			</a>
			<h6><b>Aura Castillo</b></h6>
		</div>

		<div class="col s10 p-1">
			<span class="right opc_ciu"><i class="fa fa-flag"></i> Catamarca - C.A.B.A. </span>
			<h5>Caso: Todo es un fraude</h5>
			<p>Todo es un fraudeTodo es un fraude Todo es un fraude</p>
			<h6>
				<i class="fa fa-user"></i> Pedro Escamoso &nbsp;&nbsp;&nbsp;<i class="fa fa-building"></i> Administración de Parques Nacionales &nbsp;&nbsp;&nbsp;
				<a href="#" id="4" class="clr_black ver_archivos ver_archivo_4"><i class="fa fa-folder-open"></i> Archivos adjuntos</a>
				</h6><a href="#" id="btnComentarios" class="clr_black"><h6><i class="fa fa-comments"></i> ver comentarios &nbsp;&nbsp;</a></h6>

				<button type="button" id="4" href="#!" class="btn-blue btn-floating  waves-effect waves-light BTUNIDOS">
					<i class="fa fa-eye tooltipped total_unidos" id="4" data-position="bottom" data-tooltip="Unidos al caso <b>(0)</b>"></i></button> &nbsp;
				<button type="button" id="4" class="btn-blue btn-floating  waves-effect waves-light BTPOSTULADOS">
					<i class="fa fa-users tooltipped total_postulados" id="4" data-position="bottom" data-tooltip="Postulados al caso <b>(0)</b>"></i></button> &nbsp;
				<button id="4" type="button" class="btn-blue btn-floating  waves-effect waves-light BTNOPOST">
					<i class="fa tooltipped fa-user-plus" data-position="bottom" data-tooltip="Postularse al caso"></i></button> &nbsp;<br>
				<small class="right"><i class="fa fa-clock"></i> hace 22 minutos</small>
			</a>

			<div id="divComentarios" class="mt-3 divComentarios" style="">
				<div class="row m-0">
					<div class="col s1">
						<img src="../static/img/user.png" class="img-user img-cover" style="width: 100%;">
					</div>

					<div class="col s11"><h6 class="mt-1"><b>Pedro Escamoso</b></h6></div>

					<div class="col s12 offset-s1">
						<h6 class="m-0" style="word-break: break-word;width: 95% !important;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua.</h6>
					</div>
				</div>
				<hr>

				<div class="row m-0">
					<div class="col s1">
						<img src="../static/img/user.png" class="img-user img-cover" style="width: 100%;">
					</div>

					<div class="col s11"><h6 class="mt-1"><b>Pedro Escamoso</b></h6></div>

					<div class="col s12 offset-s1">
						<h6 class="m-0" style="word-break: break-word;width: 95% !important;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua.</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	include_once('../vistas/modalcasos.php');
?>
<!--<div class="row mb-0 text-center mb-3"> 
	<ul class="pagination">
	</ul>
</div>

<div class="row m-0 hab_niv_usu hide">
	<div class="col s12 l12">
		<div class="fixed-action-btn">
			<a class="btn-floating btn-large btn-blue crea-caso" href="#">
				<i class="large fa fa-plus"></i>
			</a>
		</div>
	</div>
</div>-->
