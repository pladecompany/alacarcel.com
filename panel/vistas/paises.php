<section class="my-4">
	<h4 class="text-center clr_blue text-bold">PAISES</h4>

	<table class="table paisess display dt-responsive" id="table1" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>N°</th>
				<th>País</th>
				<th>Imagen</th>
				<th>Estatus</th>
			</tr>
		</thead>

		<tbody>
		</tbody>
	</table>
</section>


<!-- Modal -->
<div id="modalPais" class="modal">
	<div class="modal-header">
		<div class="boxHead mt-0">
			<h6 id="title"></h6>
		<a href="#" class="right modal-close white-text mr-4"><img src="../static/img/icons/plus.png" class="btnCerrar"></a>
		</div>

	</div>

	<div class="modal-content">
		<form action="" method="" id="form" onsubmit="return false;">
			<div class="row">
				<div class="col s12">
					<div class="form-group">
						<label>Nombre del país</label>
						<input type="text" name="nom_pais" id="nom_pais" class="form-control">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s1">
					<div class="p-image btn-floating btn-red">
						<img src="../static/img/icons/subir.png" class="upload-button">
						<input class="file-upload" type="file" accept="image/*" name="img_pai" id="img_pais">
					</div>
				</div>

				<div class="col s11">
					<div class="form-group">
						<label>Seleccionar una imagen</label>
					</div>
				</div>
				<div class="col s12 center">
					<img src="" id="imagen" width="480" alt="">
				</div>	
			</div>

			<div class="row">
				<div class="col s12 center">
					<a href="#" onclick="return false" class="btn btn-red save">Guardar</a>
				</div>
			</div>
		</form>
	</div>
</div>




