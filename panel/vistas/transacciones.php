<section class="my-4">
	<h4 class="text-center clr_blue text-bold">TRANSACCIONES</h4>
	<div class="row">
		<div class="col s12">
			<div class="row">
				<div class="col s5">
					<?php 
						$fecha1 = new DateTime();
					    $fecha1->modify('first day of this month');
					    $diad= $fecha1->format('Y-m-d');
					    $fecha2 = new DateTime();
					    $diah= $fecha2->format('Y-m-d');
					?>
					<h6 class="text-bold">Desde</h6>	
					<input type="date" value="<?php echo $diad;?>" class="newInput indent" id="fec_des" name="fec_des">
				</div>

				<div class="col s5">
					<h6 class="text-bold">Hasta</h6>	
					<input type="date" value="<?php echo $diah;?>" class="newInput indent" id="fec_has" name="fec_has">
				</div>
				<div class="col s2 center">
					<br><br>
					<a href="#!" class="btn btn-blue listartrans" disabled>Buscando...</a>
				</div>
			</div>
		</div>
	</div>

	<table class="table display dt-responsive clientes funt" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>Correo</th>
				<th>Foto</th>
				<th>Usuario</th>
				<th>Caso</th>
				<th>Detalles pago</th>

				<th>Opción</th>
			</tr>

		</thead>

		<tbody>
			<!--<tr>
				<td>No.</td>
				<td>Usuario</td>
				<td>Caso</td>
				<td>Monto</td>
				<td>
					<a class="btn btn-blue modal-trigger" href="#modalVer"><i class="fa fa-eye"></i></a>
					<a class="btn btn-blue modal-trigger" href="#modalTransaccion"><i class="fa fa-edit"></i></a>
					<a class="btn btn-blue modal-trigger" href="#modalEliminar"><i class="fa fa-trash"></i></a>
				</td>
			</tr>-->
		</tbody>
	</table>
</section>

</div>



