<section class="my-4">
	<h4 class="text-center clr_blue text-bold">INSTITUCIONES</h4>
	<div class="text-right mb-2">
		<a href="#instituto" class="modal-trigger btn btn-blue open">
			<i class="fa fa-plus-circle"></i> AGREGAR
		</a>
	</div>

	<table class="table display dt-responsive instituciones" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Estado</th>
				<th>Ciudad</th>
				<th>Opciones</th>
			</tr>

		</thead>

		<tbody>
			 
		</tbody>
	</table>
</section>

<!-- Modal -->
<div id="instituto" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Registrar institución</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<form action="" class="formIns">
			<div class="row">
				<div class="col s12">
					<div class="row">
						<div class="col s12 m12">
							<h6>Nombre</h6>	
							<input type="text" class="newInput indent" id="nom_ins">
						</div>

						<div class="col s12 m12">
							<h6>Descripción</h6>	
							<textarea class="textarea" id="des_ins"></textarea>
						</div>

						<!--<div class="col s12 m4">
							<h6>País</h6>
							<select class="browser-default" id="id_pais">
								<option value="" selected>Selecciona el país</option>
							
							</select>
						</div>-->

						<div class="col s12 m6">
							<h6>Estado</h6>
							<select class="browser-default" id="id_estado">
								<option value="" selected>Selecciona el estado</option>
								 
							</select>
						</div>

						<div class="col s12 m6">
							<h6>Ciudad</h6>
							<select class="browser-default" id="id_ciudad">
								<option value="" selected>Selecciona la ciudad</option>
								 
							</select>
						</div>
					</div>
				</div>

				<div class="col s12 center my-2">
					<a href="#!" class="btn btn-blue save">Guardar</a>
				</div>
			</div>
		</form>
	</div>
</div>


<div id="modalEliminar" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Eliminar registro</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content text-center">
		<h5>¿Desea eliminar éste registro?</h5>
		<a href="#" class="btn btn-blue">Sí</a>
		<a href="#" class="btn btn-blue">No</a>
	</div>
</div>

<div id="modalVer" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Ver registro</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<ul class="collection with-header">
			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Nombre:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 nom_ins">Nombre</p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Descripción:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 des_ins">Descripción</p>
					</div>
				</div>
			</li>

			<!--<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>País:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 pais">País</p>
					</div>
				</div>
			</li>-->

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Estado:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 estado">Estado</p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Ciudad:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0 ciudad">Ciudad</p>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>



