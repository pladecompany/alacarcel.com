<div class="row">
	<div class="card boxPerfil">
		<div class="boxHead">
			<h6>MI PERFIL</h6>
		</div>

		<form action="" method="post" id="update_perfil">
			<div class="row">
				<div class="col s12 m2 offset-m5">
					<div class="flexCenter">
						<img class="boxPerfil--img img-cover" src="../static/img/user.png" onerror="this.src='../static/img/user.png'" id="imagen">
					</div>
					
					<div class="text-center">
						<div class="p-image btn-floating btn-blue">
							<i class="fa fa-upload upload-button"></i>
							<input class="file-upload" type="file" accept="image/*" name="imagen" id="img_usu" />
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12 m6">
					<div class="input-field">
						<p class="text-bold" for="nom_usu">Nombre</p>
						<input type="text" id="nombre" name="nombree" class="form-app">
					</div>
				</div>
				
				<div class="col s12 m6">
					<div class="input-field">
						<p class="text-bold" for="ape_usu">Apellido</p>
						<input type="text" id="apellido" name="apellido" class="form-app">
					</div>
				</div>

				<div class="col s12">
					<div class="input-field">
						<p class="text-bold" for="ape_usu">Correo</p>
						<input type="email" id="correo" name="correo" class="form-app">
					</div>
				</div>
				
				<div class="col s12 m6">
					<div class="input-field">
						<p class="text-bold" for="tel_usu">Teléfono, ejemplo: (+57 6097570359)</p>
						<input type="text" id="telefono" name="telefono" class="form-app telefono" maxlength="18">
					</div>
				</div>

				<div class="col s12 m6">
					<p class="text-bold mb-0">Género</p>	
					<select class="mt-0 browser-default input-field" id="sexo" name="sexo">
						<option value="Femenino" selected>Femenino</option>
						<option value="Masculino">Masculino</option>
						<option value="Otros">Otros</option>
					</select>
				</div>
				
				<div class="col s12 text-center pb-2">
					<button class="btn waves-effect btn-blue saveB">Guardar</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="../static/js/jquery.min.js"></script>
<script type="text/javascript" src="../static/js/backend-panel/perfil_admin.js"></script>
