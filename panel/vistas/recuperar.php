<div class="flexCenter cont">
	<div class="boxAccount__form">
		<form method="post" action="#" class="login-form" id="recuperar" onsubmit="return false;">
			
			<h4 class="center boxAccount--title">RECUPERAR CONTRASEÑA</h4>
			
			<div class="boxAccount__body">
				<div class="row">
					<div class="col s12">
						<div class="input-field">
							<p>Correo electrónico</p>
							<input id="correo" name="correo" type="text" class="form-app" placeholder="Correo electrónico">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col s12 text-center">
						<button type="submit" id="btn_entrar" class="btn btn-blue waves-effect">Enviar<i class="fa" id="login1"></i></button>
					</div>
				</div>

				<div class="row">
					<div class="col s12 text-center">
						 
						<b>¿Ya tienes cuenta? <a href="?op=login" class="clr_blue">Iniciar sesión</a></b>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

