<section class="my-4">
	<div class="right-align hide-on-large-only mr-2 my-4">
		<a href="#producto" class="btn btn-blue modal-trigger">
			<img src="../static/img/icons/plus.png"> Nuevo</i>
		</a>
	</div>
	<table class="table display dt-responsive clientes" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr class="hide-on-med-and-down">
				<th colspan="4">CLIENTES</th>
				<th colspan="2" class="text-right">
					<a href="#producto" class="modal-trigger white-text">
						<p><img src="../static/img/icons/plus.png"> Nuevo</p>
					</a>
				</th>
			</tr>

			<tr>
				<th>No.</th>
				<th>Fecha Alta</th>
				<th>Cliente</th>
				<th>Tipo de entrega</th>
				<th>Acciones</th>
			</tr>

		</thead>

		<tbody>
			
		</tbody>
	</table>
</section>

<!-- Modal Structure -->
<div id="producto" class="modal modalFull">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Cliente Nuevo</span>
		<a href="#" class="right modal-close white-text mr-4" id="cerrar_modal"><img src="../static/img/icons/times.png"></a>
	</div>
	<div class="modal-content">
		<form action="" class="">
			<div class="row">
				<div class="col s12">
					<div class="row">
						<div class="col s12 m2">
							<h6 class="text-bold">Nombre Comercial</h6>	
							<input type="text" class="newInput indent" id="nom_com">
						</div>
						<div class="col s12 m4">
							<h6 class="text-bold">Nombre Fiscal</h6>	
							<input type="text" class="newInput indent" id="nom_fis">
						</div>
						<div class="col s12 m4">
							<h6 class="text-bold">RFC</h6>	
							<input type="text" class="newInput indent" id="rfc" maxlength="14">
						</div>
						<div class="col s12 m2">
							<h6 class="text-bold">Uso del CFDI</h6>	
							<select class="browser-default" id="cfdi">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m3">
							<h6 class="text-bold">Vendedor</h6>
							<select class="browser-default" id="vendedores">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m2">
							<h6 class="text-bold">Tipo de entrega</h6>
							<select id="tipo" class="browser-default">
								<option value="" selected>Seleccionar</option>
								<option value="embarque">Embarque</option>
								<option value="entrega">Entrega Directa</option>
								<option value="recoleccion" >Recolección</option>
							</select>
						</div>
						<div class="col s12 m2 ocultar">
							<h6 class="text-bold">Calle y número</h6>	
							<input type="text" class="newInput indent" id="calle">
						</div>
						<div class="col s12 m2 ocultar">
							<h6 class="text-bold">Colonia</h6>	
							<input type="text" class="newInput indent" id="colonia">
						</div>
						<div class="col s12 m2 ocultar">
							<h6 class="text-bold">Estado</h6>
							<select class="browser-default" id="estados">
							</select>
						</div>
						<div class="col s12 m2 ocultar">
							<h6 class="text-bold">Municipio</h6>	
							<select class="browser-default" id="municipios">
								
							</select>
						</div>
						<div class="col s12 m2 ocultar">
							<h6 class="text-bold">Código postal</h6>	
							<input type="text" class="newInput indent" id="codigo">
						</div>
					</div>

					<div class="row requisitosEmbarque">
					    <h5 class="text-bold mb-3 requisitosEmbarque center">Requisitos de embarque consolidado</h5>
						<div class="col s12 m3 offset-m3">
							<h6 class="text-bold">Forma de pago </h6>
							<select class="browser-default" id="forma_pago">
								<option value="Por cobrar" selected>Por cobrar</option>
								<option value="Pagado">Pagado</option>
								<option value="Cobrar al regreso">Cobrar al regreso</option>
							</select>
						</div>
						<div class="col s12 m3">
							<h6 class="text-bold">Tipo de servicio </h6>
							<select class="browser-default" id="tipo_servicio">
								<option value="Entrega a domicilio" selected>Entrega a domicilio</option>
								<option value="Servicio ocurre">Servicio ocurre</option>
								<option value="Otra fletera">Otra fletera</option>
							</select>
						</div>
					</div>
					<div class="row requisitosMateriales">
						<div class="col s12 m3 offset-m3 relative">
							<h6 class="text-bold">Horario de recepción</h6>
							<span class="iconInside"><img src="../static/img/icons/reloj.png"></span>
							<input type="text" class=" timepicker newInput indent" id="hora">
						</div>
						<div class="col s12 m3">
							<h6 class="text-bold">Documentación requerida</h6>	
							<input type="text" class="newInput indent" id="docu">
						</div>
					</div>

					<div class="row requisitosRecoleccion">
					    <h5 class="text-bold mb-3 requisitosRecoleccion center">Requisitos para la recolección</h5>
						<div class="col s12 m4 offset-m4 relative">
							<h6 class="text-bold">Persona autorizada</h6>
							<span class="iconInside"><img src="../static/img/icons/user.png" width="30px"></span>
							<input type="text" class=" newInput indent" id="persona_autorizada">
						</div>
					</div>

					<div class="row">
						<div class="col s12 m4 ocultar_boton offset-m5">
							<a href="#" class="btn btn-green borderRadius pt-1" id="add_t">Guardar tipo de entrega</a>
						</div>
					</div>

					<div class="container " id="entrega">
						<h6 class="bg-blue py-3 center">Detalles envíos</h6>
						<table class="centered striped responsive-table " id="detalle_entrega">
							<thead class="bg-blue">
								<tr>
									<th>No.</th>
									<th>Tipo de entrega</th>
									<th>Estado</th>
									<th>Municipio</th>
									<th>Acciones</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								
							</tbody>
						</table>
					</div>
					<br>
				</div>
				<div class="col s12">
					<h5 class="text-bold mb-3">Contacto</h5>
					<div class="row">
						<div class="col s12 m4">
							<h6 class="text-bold">Contacto compras</h6>	
							<input type="text" class="newInput indent" id="com_con">
						</div>
						<div class="col s12 m4">
							<h6 class="text-bold">Teléfono</h6>	
							<input type="text" class="newInput indent number" id="com_tel" maxlength="11">

						</div>
						<div class="col s12 m4">
							<h6 class="text-bold">Correo</h6>	
							<input type="email" class="newInput indent" id="com_cor">
						</div>
					</div>
					
					<hr>
					<div class="row">
						<div class="col s12 m4">
							<h6 class="text-bold">Contacto almacén</h6>	
							<input type="text" class="newInput indent" id="alm_con">
						</div>
						<div class="col s12 m4">
							<h6 class="text-bold">Teléfono</h6>	
							<input type="text" class="newInput indent number" id="alm_tel" maxlength="11">
						</div>
						<div class="col s12 m4">
							<h6 class="text-bold">Correo</h6>	
							<input type="email" class="newInput indent" id="alm_cor">
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col s12 m4">
							<h6 class="text-bold">Contacto pagos</h6>	
							<input type="text" class="newInput indent" id="pag_con">
						</div>
						<div class="col s12 m4">
							<h6 class="text-bold">Teléfono</h6>	
							<input type="text" class="newInput indent number" id="pag_tel" maxlength="11">

						</div>
						<div class="col s12 m4">
							<h6 class="text-bold">Correo</h6>	
							<input type="email" class="newInput indent" id="pag_cor">
						</div>
					</div>
					<hr>
				</div>
				<div class="row">
					<div class="col s12 m3">
						<h6 class="text-bold">Tipo</h6>
						<select class="browser-default" id="tipo_req">
							<option value="Crédito" selected>Crédito</option>
							<option value="Contado">Contado</option>
						</select>
					</div>
					<div class="col s12 m2 ocultar_tipo">
						<h6 class="text-bold">Límite de crédito</h6>	
						<input type="text" class="newInput indent" id="limite">
					</div>
					<div class="col s12 m2 ocultar_tipo">
						<h6 class="text-bold">Días de crédito</h6>	
						<input type="text" class="newInput indent" id="dia">
					</div>
				</div>
					<div class="row">
						<div class="col s12 m4">
							<h6 class="text-bold">Producto</h6>	
							<input type="text" class="newInput indent" id="producto_s" readonly="">
						</div>
						<div class="col s12 m1">
							<h6 class="text-bold ">&nbsp;</h6>
							<a href="#lis_productos" class="btn btn-green borderRadius pt-1 modal-trigger" id="buscar" ></a>
						</div>
						<div class="col s12 m2">
							<h6 class="text-bold">Precio</h6>	
							<input type="text" class="newInput indent" id="precio">
						</div>
						<div class="col s12 m2">
							<h6 class="text-bold ">&nbsp;</h6>
							<a href="#" class="btn btn-green borderRadius pt-1 add"><img src="../static/img/icons/plus.png"></i></a>
							<a href="#" class="btn btn-green borderRadius pt-1 clear hide"><img src="../static/img/icons/trash.png"></a>
						</div>
					</div>
					<div class="container hide" id="detalles">
						<h6 class="bg-blue py-3 center">Productos seleccionados para el cliente.</h6>
						<table class="table1" id="detalles_productos">
							<thead class="bg-blue">
								<tr>
									<th>No.</th>
									<th>Producto</th>
									<th>Precio</th>
									<th>Acciones</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<br>
				</div>
				<div class="col s12 center my-2">
					<a href="#" class="btn btn-green save">Enviar</a>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Modal Structure -->
<div id="lis_productos" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Productos</span>
		<a href="#" class="right modal-close white-text mr-4"><img src="../static/img/icons/times.png"></a>
	</div>
	<div class="modal-content">
		<table class="table display dt-responsive" id="tabla_productos" cellspacing="0" width="100%">
			<thead class="">
				<tr class="hide-on-med-and-down">
					<th colspan="4">Productos</th>
				</tr>
				<tr>
					<th>No.</th>
					<th>Código</th>
					<th>Nombre</th>
					<th>Acciones</th>
				</tr>
			</thead>

			<tbody>
			<tr>
				<td class="center"></td>
				<td class="center"></td>
				<td class="center"></td>
				<td class="center"></td>
			</tr>
		</tbody>
	</table>
	</div>
</div>

<div id="ver_entrega" class="modal">
	<div class="modal-header bg-blue py-3">
		<span class="ml-3">Detalles envios.</span>
		<a href="#" class="right modal-close white-text mr-4"><img src="../static/img/icons/times.png"></a>
	</div>
	<div class="modal-content">
		<div class="row">
			<div class="col s12 m4">
				<h6 class=" tituloCategoriaModal">Tipo</h6>
				<span id="tip">- </span>
			</div>
			<div class="col s12 m4 ">
				<h6 class=" tituloCategoriaModal">Calle y número.</h6>
				<span id="cal">- </span>
			</div>
			<div class="col s12 m4 ">
				<h6 class=" tituloCategoriaModal">Colonia</h6>
				<span id="col">- </span>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m4 ">
				<h6 class=" tituloCategoriaModal">Estado</h6>
				<span id="est">- </span>
			</div>
			<div class="col s12 m4 ">
				<h6 class=" tituloCategoriaModal">Municipio</h6>
				<span id="mun">- </span>
			</div>
			<div class="col s12 m4 ">
				<h6 class=" tituloCategoriaModal">Código Postal.</h6>
				<span id="cod">- </span>
				<br>
				<br>
			</div>
		</div>
			
			<div id="tipos">
			
				
			</div>
		</div>
	</div>
</div>
</div>



