<div class="container">
	<div class="row flexCenter">
		<div class="card boxPerfil">
			<div class="boxHead mt-0">
				<h6>CONFIGURACIÓN</h6>
			</div>

			<div class="boxPerfil">
				<form action="" method="" id="form_con" onsubmit="return false;">
					<div class="row mb-0">
						<div class="col s12 m12">
							<div class="input-field">
								<p for="mon_con">Monto mínimo para unirse a un caso</p>
								<input type="text" id="mon_con" name="monto" class="form-app number" required>
							</div>
						</div>

						<div class="col s12 m12">
							<div class="input-field">
								<p for="can_con">Cantidad de demandantes (Usuarios unidos)</p>
								<input type="text" id="can_con" name="cant" class="form-app number" required>
							</div>
						</div>

						<div class="col s12 m12">
							<div class="input-field">
								<p for="por_con">Porcentaje de voto del denunciante: (1%-100%)</p>
								<p class="range-field active">
									<input type="range" id="votos" name="voto" min="1" max="100"><span class="thumb active" style="left: 683.656px; height: 30px; width: 30px; top: -20px; margin-left: -15px;"><span class="value"></span></span>
								</p>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 text-center pb-2">
							<button class="btn waves-effect btn-blue" id="bt_save">Aceptar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>