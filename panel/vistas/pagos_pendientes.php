<section class="my-4">
	<h4 class="text-center clr_blue text-bold">PAGOS PENDIENTES</h4>
	<div class="hide-on-med-and-down text-right mb-2">
		<a href="#!" class="btn btn-blue add_pago">
			<i class="fa fa-check"></i> REALIZAR PAGO
		</a>
	</div>

	<table class="table display dt-responsive clientes funt" id="table" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>No.</th>
				<th>Foto</th>
				<th>Abogado</th>
				<th>Caso</th>
				<th>Monto</th>
				<th>Fecha</th>
				<th>Estatus Paypal</th>
				<th>Transacción Paypal</th>
				<th>Opciones</th>
			</tr>

		</thead>

		<tbody>
			<!--<tr>
				<td>No.</td>
				<td>Usuario</td>
				<td>Caso</td>
				<td>Monto</td>
				<td>
					<a class="btn btn-blue modal-trigger" href="#modalVer"><i class="fa fa-eye"></i></a>
					<a class="btn btn-blue modal-trigger" href="#modalTransaccion"><i class="fa fa-edit"></i></a>
					<a class="btn btn-blue modal-trigger" href="#modalEliminar"><i class="fa fa-trash"></i></a>
				</td>
			</tr>-->
		</tbody>
	</table>
</section>

<!-- Modal -->
<div id="modalTransaccion" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Registrar transacción</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<form action="" class="">
			<input type="hidden" id="id_fun" class="limpiar_form" value="">
			<div class="row">
				<div class="col s12">
					<div class="row">
						<div class="col s12 m12">
							<h6 class="text-bold">Abogado</h6>
							<select class="browser-default limpiar_form_2" id="abo_pago">
								<option value="" selected>Selecciona el Abogado</option>
							</select>
						</div>

						<div class="col s12 m12 mt-1">
							<h6 class="text-bold">Casos en los que ha sido seleccionado</h6>
							<select class="browser-default limpiar_form_2" id="cas_pago">
								<option value="" selected>Selecciona el caso</option>
							</select>
						</div>

						<div class="col s12 m12">
							<h6 class="text-bold">Monto<span class="limite-pago" style="font-size: 14px;font-weight: 700;color: #00b0f0;"></span></h6>	
							<input type="text" class="newInput indent limpiar_form Amount" id="mon_pago" data-decimal="." data-thousands=",">
						</div>

						<div class="col s12 m12 hide">
							<h6 class="text-bold">Referencia</h6>	
							<input type="text" class="newInput indent limpiar_form" id="ref_pago">
						</div>
					</div>
				</div>

				<div class="col s12 center my-2">
					<button type="button" class="btn btn-blue save">Transferir monto</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="modalEliminar" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Eliminar registro</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content text-center">
		<h5>¿Desea eliminar éste registro?</h5>
		<a href="#" class="btn btn-blue">Sí</a>
		<a href="#" class="btn btn-blue">No</a>
	</div>
</div>

<div id="modalVer" class="modal">
	<div class="modal-header py-3">
		<span class="ml-3">Ver registro</span>
		<a href="#" class="right modal-close clr_black mr-4"><i class="fa fa-times"></i></a>
	</div>

	<div class="modal-content">
		<ul class="collection with-header">
			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Abogado:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0">Abogado</p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Cargos:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0">Cargos</p>
					</div>
				</div>
			</li>

			<li class="collection-item clr_black">
				<div class="row mb-0">
					<div class="col s4 m4 border-table">
						<b>Monto:</b>
					</div>
					<div class="col s8 m8">
						<p class="m-0">Monto</p>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>

</div>



