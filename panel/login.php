<!DOCTYPE html>
<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
include_once('ruta.php');
?>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Alacarcel">
	<meta name="keywords" content="Alacarcel">
	<meta name="author" content="Alacarcel">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- For iPhone -->
	<meta name="msapplication-TileColor" content="#00bcd4">
	<meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
	<title>alacarcel</title>
	<link rel="shortcut icon" href="../static/img/logo.png" />
	<link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">

	<!-- CORE CSS   --> 
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/css/materialize.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/css/style.css">
</head>

<body class="w100">
	<div class="flexCenter bg_full vh100">
		<div class="boxAccount__form">
			<form method="post" action="#" class="login-form" id="login_entrar" onsubmit="return false;">
				
				<h5 class="center text-bold boxAccount--title">Iniciar sesión</h5>

				<div class="boxAccount__body">
					<div class="row mb-0">
						<div class="col s12">
							<div class="input-field">
								<p>Correo electrónico</p>
								<input id="correo" name="correo" type="text" class="form-app" placeholder="Correo electrónico">
							</div>
						</div>
						
						<div class="col s12">
							<div class="input-field">
								<p>Contraseña</p>
								<input id="pass" name="pass" type="password" class="form-app" placeholder="Contraseña">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 text-center">
							<button type="submit" id="btn_entrar" class="btn btn-blue waves-effect save">Ingresar <i class="fa" id="login1"></i></button>
						</div>
					</div>
					
				</div>
			</form>
		</div>
	</div>

	<footer class="footer">
		<div class="row mb-0">
			<div class="col s12 text-center">
				<p class="text-bold m-2">© 2020 alacarcel.com</p>
			</div>
		</div>
	</footer>

	<script type="text/javascript" src="../static/js/jquery.min.js"></script>
	<script src="../static/js/materialize.min.js"></script>
	<script src="../static/js/backend-panel/general.js"></script>
	<script src="../static/js/backend-panel/login_admin.js"></script>
</body>
</html>
