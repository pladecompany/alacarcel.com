<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	

	$opcion = $_GET['op'];
	switch ($opcion) {
		case "inicio":
			$ruta="vistas/inicio.php";
		break;
		case "login":
			header('Location: login.php');
		break;
		case "recuperar":
			$ruta="vistas/recuperar.php";
		break;

		case "recuperar-clave":
			$ruta="vistas/recuperar_clave.php";
		break;

		case "instituciones":
			$ruta="vistas/instituciones.php";
		break;

		case "funcionarios":
			$ruta="vistas/funcionarios.php";
		break;

		case "usuarios":
			$ruta="vistas/usuarios.php";
		break;

		case "abogados":
			$ruta="vistas/abogados.php";
		break;

		case "transacciones":
			$ruta="vistas/transacciones.php";
		break;

		case "paises":
			$ruta="vistas/paises.php";
		break;
		
		case "ciudades":
			$ruta="vistas/ciudades.php";
		break;
		
		case "vercasos":
			$ruta="vistas/listadodecasos.php";
		break;

		case "configuracion":
			$ruta="vistas/configuracion.php";
		break;

		case "perfil":
			$ruta="vistas/perfil.php";
		break;

		case "cambiarclave":
			$ruta="vistas/cambiarclave.php";
		break;
		
		case "notificaciones":
			$ruta="vistas/notificaciones.php";
		break;

		case "salir":
			$ruta="vistas/salir.php";
		break;

		case "pagos_pendientes":
			$ruta="vistas/pagos_pendientes.php";
		break;
		
		default:
			$ruta="vistas/inicio.php";
		break;
	}
?>
