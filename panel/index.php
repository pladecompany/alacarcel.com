<!DOCTYPE html>
<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
include_once('ruta.php');
?>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- For iPhone -->
	<meta name="msapplication-TileColor" content="#00bcd4">
	<meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
	<title>alacarcel</title>
	<link rel="shortcut icon" href="../static/img/logo.png" />
	<link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">

	<!-- CORE CSS   --> 
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/css/materialize.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/css/scroll.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/css/style.css">

	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/font-awesome/css/all.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/JTables/css/jquery.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/JTables/css/dataTables.bootstrap.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/JTables/css/responsive.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/select2/select2.min.css"/>
	<link href="../static/js/quill/quill.css" rel="stylesheet">
	<script type="text/javascript">
		function getParameterByName(name) {
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
			return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		}
		op = getParameterByName('op');
		paneluser = 'admin';
		var user = window.sessionStorage.getItem("user") 
		if( user== null)
		{

			if(op !="login"){
				if(op !="login" && op!="recuperar" && op!="recuperar-clave") {
					window.location = "../panel/?op=login";	
				}
			}
		}else{
			user = JSON.parse(user)
			if(user.rol != "admin" && op !="login"){
				window.location = "../panel/?op=login";	
			}
		}
		
	</script>
</head>

<style type="text/css">
	.side-nav{
		width: 250px !important;
	}
</style>

<body>
	<header>
		<nav class="red lighten-2" role="navigation">
			<div class="nav-wrapper">
				<a href="?op=inicio">
					<img src="../static/img/logo.png" class="brand-logo">
				</a>
				
				<a class="button-collapse" data-activates="nav_mobile" href="javascript:;">
					<!--<i class="fa fa-bars"></i>-->
				</a>

				<ul class="right" id="log">
					<li>
						<a class="dropdown-trigger" data-target="menu"  href="#">
							<i class="fa fa-bars"></i>
						</a>
						<ul id='menu' class='dropdown-content dp2 mCustomScrollbar' data-mcs-theme="dark">
							<li class="" id="menu-filter">
								<div class="row">
									<div class="col s4 p-2 center">
										<a class="opc_nav" href="?op=inicio">
											<i class="fa fa-home clr_blue"></i>
											<div class="center m-0 inDiv clr_black">Inicio</div>
										</a>
									</div>

									<!--<div class="col s4 p-2 center">
										<a class="opc_nav" href="?op=ciudades">
											<i class="fa fa-flag clr_blue"></i>
											<div class="center m-0 inDiv clr_black">Ciudades</div>
										</a>
									</div>

									<div class="col s4 p-2 center">
										<a class="opc_nav" href="?op=paises">
											<i class="fa fa-flag clr_blue"></i>
											<div class="center m-0 inDiv clr_black">Paises</div>
										</a>
									</div>
									-->
									<div class="col s4 p-2 center">
										<a class="opc_nav" href="?op=instituciones">
											<i class="fa fa-building clr_blue"></i>
											<div class="center m-0 inDiv clr_black">Instituciones</div>
										</a>
									</div>

									<div class="col s4 p-2 center">
										<a class="opc_nav" href="?op=funcionarios">
											<i class="fa fa-users clr_blue"></i>
											<div class="center m-0 inDiv clr_black">Funcionarios</div>
										</a>
									</div>

									<div class="col s4 p-2 center">
										<a class="opc_nav" href="?op=usuarios">
											<i class="fa fa-users clr_blue"></i>
											<div class="center m-0 inDiv clr_black">Usuarios</div>
										</a>
									</div>

									<div class="col s4 p-2 center">
										<a class="opc_nav" href="?op=abogados">
											<i class="fa fa-suitcase clr_blue"></i>
											<div class="center m-0 inDiv clr_black">Abogados</div>
										</a>
									</div>

									<div class="col s4 p-2 center">
										<a class="opc_nav" href="?op=transacciones">
											<i class="fa fa-stream clr_blue"></i>
											<div class="center m-0 inDiv clr_black">Transacciones</div>
										</a>
									</div>

									<div class="col s4 p-2 center">
										<a class="opc_nav" href="?op=pagos_pendientes">
											<i class="fa fa-tasks clr_blue"></i>
											<div class="center m-0 inDiv clr_black">Pagos pendientes</div>
										</a>
									</div>

									<div class="col s4 p-2 center">
										<a class="opc_nav" href="?op=configuracion">
											<i class="fa fa-cogs clr_blue"></i>
											<div class="center m-0 inDiv clr_black">Configuración</div>
										</a>
									</div>
								</div>
							</li>
						</ul>
					</li>

					<li>
						<a href="?op=notificaciones" id="noti">
							<i class="fa fa-bell" style="color:#888;"></i>
						</a>
					</li>

					<li>
						<a class="dropdown-trigger" data-target="configuracion">
							<img src="../static/img/user.png"  width="50px" alt="" class="circle" id="img-perfil" style="vertical-align: middle; width:50px;height:50px;border:2px solid var(--blue);object-fit: cover;object-position: top center;" >
						</a>
						
						<ul id="configuracion" class="dropdown-content dp3">
							<li><a href="?op=configuracion">Configuración</a></li>
							<li><a href="?op=perfil">Perfil</a></li>
							<li><a href="?op=cambiarclave">Cambiar contraseña</a></li>
							<li><a href="#modal-salir" class="waves-effect waves-block waves-light modal-trigger">Salir</a></li>
						</ul>
					</li>
				</ul>

				<ul class="right">
                </ul>
			</div>
		</nav>
	</header>

	<div id="modal-salir" class="modal modal-salir">
		<div class="modal-content">
			<center><img src="../static/img/key.png"></center>
			<h5 class="text-center"><i class="mdi-hardware-security"></i> ¿Desea cerrar sesión?</h5>
			<div class="text-center">
				<a href="vistas/salir.php" id="" class="btn btn-blue modal-action bt_salir">Sí <i class="fa fa-check-circle"></i></a>
				<button class="btn btn-blue modal-action modal-close">No <i class="fa fa-times-circle"></i></button>
			</div>
		</div>
	</div>
	<form action="../vistas/pdf/?op=caso" target="_blanck" class="hide" method="POST" id="form_pdf" enctype="multipart/form-data">
		<input type="hidden" name="casopdf" id="casopdf" value="" required>
	</form>

	<div class="container main " style="min-height:600px;margin-bottom: 5rem;">
		<?php
		include_once($ruta);
		?>
	</div>

	<footer class="footer pb-1">
		<div class="container">
			<div class="row m-0 text-center">
				<div class="col s12 pt-4">
					<a href="" target="_blank" class="clr_black"><i class="fab fa-facebook fa-2x"></i></a>
					<a href="" target="_blank" class="clr_black"><i class="fab fa-instagram fa-2x"></i></a>
				</div>
			</div>

			<div class="row mb-0">
				<div class="col s12 text-center">
					<p class="text-bold m-2">© 2020 alacarcel.com</p>
				</div>
			</div>
		</div>
	</footer>

	<script type="text/javascript" src="../static/js/jquery.min.js"></script>
	<script type="text/javascript" src="../static/js/mask.js"></script>
	<script src="../static/js/quill/highlight.min.js"></script>
	<script src="../static/js/quill/quill.js"></script>
	<script src="../static/js/quill/image-resize.min.js"></script>
	<script type="text/javascript" src="../static/js/materialize.min.js"></script>
	<script src="../static/js/jquery.masknumber.min.js"></script>
	<script type="text/javascript" src="../static/js/moment/jquery.livequery.min.js"></script>
	<script type="text/javascript" src="../static/js/moment/jquery.timeago.js"></script>
	<script type="text/javascript" src="../static/js/funciones.js"></script>
	<script type="text/javascript" src="../static/js/backend-panel/general.js"></script>
	<script type="text/javascript" src="../static/js/moment.min.js"></script>	
	<script type="text/javascript" src='../static/js/moment.es.js'></script>
	<script type="text/javascript" src="../static/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script type="text/javascript" src="../static/js/plugins.js"></script>
	<script type="text/javascript" src='../static/js/axios.min.js'></script>

	<script type="text/javascript" src="../static/js/menu.js"></script>
	<script type="text/javascript" src="../static/js/main.js"></script>
	<script type="text/javascript" src="../static/js/scroll.min.js"></script>
	<script type="text/javascript" src="../static/js/socket/socket.js"></script>
	<script type="text/javascript" src="../static/js/moment/jquery.livequery.min.js"></script>
	<script type="text/javascript" src="../static/js/moment/jquery.timeago.js"></script>
	<script type="text/javascript" src="../static/lib/select2/select2.min.js"></script>
	<script type="text/javascript" src="../static/js/backend-panel/chat.js"></script>
	<script type="text/javascript" src="../static/js/backend-panel/socket_events.js"></script>
	<script type="text/javascript" src="../static/js/backend-panel/validarCampo.js"></script>

	<script type="text/javascript" src="../static/lib/JTables/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../static/lib/JTables/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="../static/lib/JTables/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" src="../static/lib/select2/select2.min.js"></script>
	<script>
	$(document).ready(function(){
			$('.Amount').maskNumber();
			$(document).on('blur', '.Amount', function(){
		        if(this.value.trim().length<=3){
		        	if(this.value.trim().length==3)
		        		this.value = formato.precio(this.value.split(",")[1]);
		        	else
		        		this.value = formato.precio(this.value);
		        } 
		    });
			$('.tooltipped').tooltip();
			$('.tooltipped').tooltip({delay: 50}).each(function () {
				var background = $(this).data('background-color');
				if (background) {
					$("#" + $(this).data('tooltip-id')).find(".backdrop").addClass(background);
				};
				var text = $(this).data('text-color');
				if (text) {
					$("#" + $(this).data('tooltip-id')).find("span").addClass(text);
				};
			});
			if($('.quill-edit').length>0){
						var container = $('.quill-edit').get(0);
						var toolbar_quill = ` 
						<!--<span class="ql-formats">
					      <select class="ql-font"></select>
					      <select class="ql-size"></select>
					    </span>-->
					    <span class="ql-formats">
					      <button class="ql-bold"></button>
					      <button class="ql-italic"></button>
					      <button class="ql-underline"></button>
					      <!--<button class="ql-strike"></button>-->
					    </span>
					    <!--<span class="ql-formats">
					      <select class="ql-color"></select>
					      <select class="ql-background"></select>
					    </span>
					    <span class="ql-formats">
					      <button class="ql-script" value="sub"></button>
					      <button class="ql-script" value="super"></button>
					    </span>
					    <span class="ql-formats">
					      <button class="ql-header" value="1"></button>
					      <button class="ql-header" value="2"></button>
					      <button class="ql-blockquote"></button>
					      <button class="ql-code-block"></button>
					    </span>
					    <span class="ql-formats">
					      <button class="ql-list" value="ordered"></button>
					      <button class="ql-list" value="bullet"></button>
					      <button class="ql-indent" value="-1"></button>
					      <button class="ql-indent" value="+1"></button>
					    </span>-->
					    <span class="ql-formats">
						<!--<button class="ql-direction" value="rtl"></button>-->
					      <select class="ql-align"></select>
					    </span>
					    <!--<span class="ql-formats">
					      <button class="ql-link"></button>
					      <button class="ql-image"></button>
					      <button class="ql-video"></button>
					      <button class="ql-formula"></button>
					      
					    </span>
					    <span class="ql-formats">
					      <button class="ql-clean"></button>
					      <button class="ql-showHtml"></button>
					    </span>-->`; 
						 
					    $(".toolbar-quill-edit").html(toolbar_quill);
						  quill = new Quill(container, {
						    modules: {
						      syntax: true,
						      toolbar: '.toolbar-quill-edit',
						      imageResize: {
						      		displaySize: true,
						            toolbarStyles: {
						            	
						                backgroundColor: 'black',
						                border: 'none',
						                color: 'white'
						            },
						            toolbarButtonStyles: {
						            },
						            toolbarButtonSvgStyles: {
						            },
					        	},
						    },
						    placeholder: "Ingrese su descripción aquí",
						    theme: 'snow'
						  });
						  var txtArea = document.createElement('textarea');
							txtArea.style.cssText = "min-height: 200px;z-index:100;width: 100%;margin-left:-0.1rem;background: rgb(29, 29, 29);box-sizing: border-box;color: rgb(204, 204, 204);font-size: 15px;outline: none;padding: 20px;line-height: 24px;font-family: Consolas, Menlo, Monaco, &quot;Courier New&quot;, monospace;position: absolute;top: 0;bottom: 0;border: none;display:none"

							var htmlEditor = quill.addContainer('ql-custom')
							htmlEditor.appendChild(txtArea)

							var myEditor = document.querySelector('.quill-edit');
							const characterLimit = 3501; 
							quill.on('text-change', (delta, oldDelta, source) => {
							  var html = myEditor.children[0].innerHTML;
							  txtArea.value = html;
								$('.character-count').text((characterLimit - quill.getLength()));
								if (quill.getLength() <= characterLimit) {
									return;
								}   
								
								const { ops } = delta;
								let updatedOps;
								if (ops.length === 1) {
									// text is inserted at the beginning
									updatedOps = [{ delete: ops[0].insert.length }]; 
								} else {
									// ops[0] == {retain: numberOfCharsBeforeInsertedText}
									// ops[1] == {insert: "example"}
									updatedOps = [ops[0], { delete: ops[1].insert.length }]; 
								}   
								quill.updateContents({ ops: updatedOps });
								
							})

							var customButton = document.querySelector('.ql-showHtml');
							if(customButton){
								customButton.addEventListener('click', function() {
									if (txtArea.style.display === '') {
										var html = txtArea.value
										quill.pasteHTML(html);
										
									}
									txtArea.style.display = txtArea.style.display === 'none' ? '' : 'none'
								});
							}
					}
		});
		 if(user){
			$('#img-perfil').on('error',function(){
				$(this).attr('src','../static/img/user.png')
			});
			 $('#log').removeClass('hide')
			 perfil()
			 function perfil(){
				 var user = JSON.parse(window.sessionStorage.getItem('user'));
				 if(user.imagen != null){
					 $('#img-perfil').attr('src',user.imagen)
				 }
				 $('#imagen').on('error',function(){
					 $(this).attr('src','../static/img/user.png')
				 })
			 }
			 var html = `
						<li>
							<a href="?op=notificaciones" class="dropdown-button" id="icon_noti" href="javascript:;">
								<i class="fa fa-bell" style="color:#888;"></i>
							</a>
						</li>` 
					$('#noti').html(html)
					$.ajax({
						url: dominio + "notificaciones/count/admin",
						type: "get",
						dataType: "json",
						data:{id:user.id},
						success: function(data){
							if(data[0].count >0){
								var html = `
								<li>
									<a href="?op=notificaciones" class="dropdown-button" id="icon_noti" href="javascript:;">
										<i class="fa fa-bell" style="color:#2196F3;"></i><span style="border-radius:50%;color:#2196F3;padding:4px;">${data[0].count}</span>
									</a>
								</li>` 
								$('#noti').html(html)
							}
							
						},
						error: function(xhr, status, errorThrown){
						console.log("Errr : "+status);
						}
					});
		 } 
		 
		 $(document).on('ready', function(){ 
			
			$('.timeago').livequery(function(){ $(this).timeago(); });
		});
		$(document).ready(function() {
			$('.select2').select2();
		});
	</script>

	<script>
		(function($){
			$(window).on("load",function(){
				$(".content").mCustomScrollbar();
			});
		})(jQuery);
	</script>

	<script>
		$(document).ready(function(){
			$('.table').DataTable( {
				"language": {
					"url": "../static/lib/JTables/Spanish.json"
				}
			} );
			$('.table').DataTable( {
				responsive: true
			} );
		} );
	</script>

	<script>
		var acentos=function(cadena)
			{
				var chars={
					"á":"a", "é":"e", "í":"i", "ó":"o", "ú":"u",
					"à":"a", "è":"e", "ì":"i", "ò":"o", "ù":"u", "ñ":"n",
					"Á":"A", "É":"E", "Í":"I", "Ó":"O", "Ú":"U",
					"À":"A", "È":"E", "Ì":"I", "Ò":"O", "Ù":"U", "Ñ":"N"}
				var expr=/[áàéèíìóòúùñ]/ig;
				var res=cadena.replace(expr,function(e){return chars[e]});
				return res;
			}
	</script>
</body>
</html>
