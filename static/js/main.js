$(document).ready(function(){
	M.AutoInit();
	$('.modal').modal();
	//$('.datepicker').datepicker();
	$('.tabs').tabs();
	$('.dropdown-trigger').dropdown();
    $(".datepicker_caducidad").datepicker({
		format: 'dd-mm-yyyy',
        selectMonths: true,
        selectYears: 20,
        max: new Date(),
        i18n: {
			months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
			monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
			weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
			weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
			weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"]
		}
    });
	$('.datepicker').datepicker({ 
		firstDay: true, 
		format: 'dd-mm-yyyy',
		max: new Date(),
		selectYears: 40,
		i18n: {
			months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
			monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
			weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
			weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
			weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"]
		}
	});
	$(".datepickeR").mask("00-00-0000", {placeholder: "__-__-____"});
	$('#file_1').change(function(){
		$('#inputval').text( $(this).val());
	});
});

$(document).ready(function() {
	var readURL = function(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('.profile-pic').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$(".datepickeR").on('blur', function(){
		  	var fecha = this.value;
	        fecha = fecha.split("-");
	        fecha = fecha[2]+"-"+fecha[1]+"-"+fecha[0];
	        if(new Date(fecha)=="Invalid Date"){
	        	this.value = "";
	        }

	});

	$(".file-upload").on('change', function(){
		readURL(this);
	});

	$(".upload-button").on('click', function() {
		$(".file-upload").click();
	});
});



$(document).ready(function(){
	$('.mostrarMaterial').hide();
    $(":radio.noaplica").prop("checked", true);

	$(':radio.noaplica').change(function() {
		if( $(this).is(':checked') ) {
			$('.ocultarRecoleccion').slideUp('slow');
			$('.mostrarMaterial').slideUp('slow');
		}else {

		}
	});

	$(':radio.solicitud').change(function() {
		if( $(this).is(':checked') ) {
			$('.ocultarRecoleccion').slideDown('slow');
			$('.mostrarMaterial').slideUp('slow');
		}
	});

	$(':radio.material').change(function() {

		if( $(this).is(':checked') ) {
			$('.mostrarMaterial').slideDown('slow');
			$('.ocultarRecoleccion').slideDown('slow');
		}else {

		}
	});
});


