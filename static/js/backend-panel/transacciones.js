$(document).ready(function() {
    listtransac();


    
    $('.listartrans').on('click', function() {
        var fec_d = new Date($("#fec_des").val()).getTime();
        var fec_h = new Date($("#fec_has").val()).getTime();
        if($("#fec_des").val()==''){
            M.toast({ html: "Ingresa desde que fecha" }, 5000);
            return;
        }else if($("#fec_has").val()==''){
            M.toast({ html: "Ingresa hasta que fecha" }, 5000);
            return;
        }else if(fec_d>fec_h){
            M.toast({ html: "La fecha desde no debe ser mayor de hasta" }, 5000);
            return;
        }else{
            listtransac();
        }
    });

    function listtransac() {
        data = {
            ida: user.id,
            token: user.token,
            fecd: $("#fec_des").val(),
            fech: $("#fec_has").val()
        }
        $(".listartrans").html("Buscando...");
        $(".listartrans").attr("disabled",true);
        $.ajax({
            url: dominio + 'transacciones/transaccion',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_transac(data);
            }

        })
    }

    function actualizar_tabla_transac(data) {
        var table = $('.funt').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            if(datos.usuario.imagen!=null)
                imgusuariot=datos.usuario.imagen;
             else
                imgusuariot='../static/img/user.png';
            var fr = moment(datos.fec_reg_pago).format('YYYY-MM-DD, HH:mm:ss');
            var option = '<a class="btn btn-blue ver-funcionario" href="?op=vercasos&idc=' + datos.id_caso + '"><i class="fa fa-eye"></i></a> ';
            var row = [count += 1, datos.usuario.cor_usu, '<img src="' + imgusuariot + '" onerror="this.src =\'../static/img/user.png\'" class="img-user boxPerfil--img img-cover" style="width:35px;height:35px;"> ', datos.usuario.nic_usu, "#"+datos.id_caso+" "+datos.caso.nom_caso, "<b>Monto:</b> "+formato.precio(datos.monto) + " $ <br><b>Id:</b>"+ datos.id_pago+"<br><b>Fecha: </b>"+ fr, option]
            table.row.add(row).draw().node();
        }
        
        $(".listartrans").html("Buscar");
        $(".listartrans").attr("disabled",false);
    }

});