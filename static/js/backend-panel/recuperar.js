$(document).ready(function() {
    $("#recuperar").on('submit', function(e) {
        $('#btn_entrar').prop('disabled', true);
        $('#login1').html(" <img src='static/img/cargando.gif' style='vertical-align:middle;' width='15px'>");
        var data = {
            correo: $("#correo").val(),
        };


        $.ajax({
            url: dominio + 'usuarios/recuperar-clave',
            type: 'POST',
            data: data,
            success: function(data) {
                $("#correo").val("");
                M.toast({ html: data.msg }, 10000);
                $('#btn_entrar').prop('disabled', false);

                $('#login1').html("");
            },
            error: function(err) {
                //M.toast({ html: err.responseJSON.msg }, 10000);
                $('#login1').html("");
                $('#btn_entrar').prop('disabled', false);
            }
        })
        return false;
    });
});