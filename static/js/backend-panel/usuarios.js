$(document).ready(function(){
  var estadoselec = ''; 
  var ciudadselec = '';
  //ajaxPaises();
  ajaxEstados(2);
  function ajaxPaises(){
         
        $.ajax({
            url : dominio+'paises/activos',
            type: 'GET',
            success:function(data){
                  
                  html = '';
                  TiposVeh = data;
                  for(var i = 0 ; i < data.length; i++){
                      html += '<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                  }

                  $("#pais").html(html);
                  $("#pais").formSelect();
            }

        })
  }
  $('#pais').on('change', function() {
        if (this.value) {
          ajaxEstados(this.value);
        } else {
          $('#estado').html('');
          $('#ciudad').html('');
        }
    });
    function ajaxEstados(id, id_estado = 0) {
        $('#estado').html('<option value="" selected disabled>Cargando...</option>');
        $.ajax({
            url: dominio + 'paises/estado_ciudad/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected disabled>Seleccione un estado</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#estado").html(html);
                if (estadoselec) {
                    $("#estado").val(estadoselec);
                }
                $("#estado").formSelect();
            }

        })
    }
    $("#estado").on('change', function() {

        ajaxCiudades(this.value);

    });

    function ajaxCiudades(id, id_ciudad = 0) {
        if (!id) {
            return;
        }
        $.ajax({
            url: dominio + 'paises/ciudad/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected disabled>Selecciona una ciudad</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#ciudad").html(html);
                if (ciudadselec != 0) {
                    $("#ciudad").val(ciudadselec);
                }
                $("#ciudad").formSelect();
            }

        })
    }

  obtenerUsuarios();
  function obtenerUsuarios(){

        var data = {
          ida: user.id,
          token: user.token
        }
        if(tipousuario=='usuario')
          url = dominio + "usuarios";
        else
          url = dominio + "usuarios/abogado";
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            dataType: "json",

            success: function(data){
              //console.log(data);
              actualizar_tabla_usuarios(data)
            },
            error: function(xhr, status, errorThrown){
              console.log("Errr : ");
            }
        });
  }

  function actualizar_tabla_usuarios(data){
        var table = $('.usu').DataTable({
        "language": {
            "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for(var i=0;i<data.length;i++){
            datos = data[i];
            if(tipousuario=='usuario')
              valor = datos.nic_usu;
            else
              valor = datos.mat_usu;
            if(datos.nom_usu)
              nombret=datos.nom_usu;
            else
              nombret='';
            var option = '<a class="btn btn-blue ver-usu" id="'+datos.id+'"><i class="fa fa-eye"></i></a> <a class="btn btn-blue edit-usu" id="'+datos.id+'"><i class="fa fa-edit"></i></a> <a class="btn btn-blue delete_u" id="'+datos.id+'"><i class="fa fa-trash"></i></a>';
            var row = [count+=1,"<textarea id='fun_u_"+datos.id+"' style='display:none;'>" + JSON.stringify(datos) + "</textarea>"+nombret,datos.ape_usu,datos.tlf_usu,valor,datos.cor_usu,option]
            table.row.add(row).draw().node();
        }
  }

  $(document).on('click', ".ver-usu", function(){
        idu = $(this).attr("id");
        var obj = JSON.parse($("#fun_u_"+idu).val());
        $(".nom_usu").html(obj.nom_usu);
        $(".ape_usu").html(obj.ape_usu);
        $(".tel_usu").html(obj.tlf_usu);
        if(tipousuario=='usuario')
          $(".nic_usu").html(obj.nic_usu);
        else
          $(".mat_usu").html(obj.mat_usu);
        $(".cor_usu").html(obj.cor_usu);
        if(obj.tip_doc_usu)
          $(".doc_usu").html(obj.tip_doc_usu+" - "+obj.num_doc_usu);
        $(".gen_usu").html(obj.sex_usu);
        //$(".pai_usu").html(obj.nom_pais);
        $(".est_usu").html(obj.nom_estado);
        $(".ciu_usu").html(obj.nom_ciudad);
        if (obj.imagen != null) {
          $('#perfil').attr('src', obj.imagen);
        }
        $('#perfil').on('error', function() {
          $(this).attr('src', '../static/img/user.png')
        })
        $("#modalVer").modal("open");
    });

  $(document).on('click', ".edit-usu", function(){
        idu = $(this).attr("id");
        var obj = JSON.parse($("#fun_u_"+idu).val());
        $("#id_usu").val(obj.id);
        $("#nom_usu").val(obj.nom_usu);
        $("#ape_usu").val(obj.ape_usu);
        $("#tlf_usu").val(obj.tlf_usu);
        if(tipousuario=='usuario')
          $("#nic_usu").val(obj.nic_usu);
        else
          $("#mat_usu").val(obj.mat_usu);
        $("#cor_usu").val(obj.cor_usu);
        $("#tipo_doc").val(obj.tip_doc_usu).trigger("change");
        $("#num").val(obj.num_doc_usu);
        $("#sexo").val(obj.sex_usu);
        //$("#pais").val(obj.id_pais).trigger("change");
        estadoselec = obj.id_estados;
        ciudadselec = obj.id_ciudad;
        $('#estado').html('<option value="" selected disabled>primero seleccione un país...</option>');
        $('#ciudad').html('<option value="" selected disabled>primero seleccione un estado...</option>');
        if (obj.id_pais) {
          ajaxEstados(obj.id_pais);
        }
        if (obj.id_estados) {
          ajaxCiudades(obj.id_estados);
        }

        $("#sts").val(obj.est_usu).trigger("change");

        if (obj.imagen != null) {
          $('#imagen').attr('src', obj.imagen);
        }else
          $('#imagen').attr('src', '../static/img/user.png');
        $('#imagen').on('error', function() {
          $(this).attr('src', '../static/img/user.png')
        })
        $("#modalEditar").modal("open");
    });
    $('#img_usu').on('change',function(event){
        var output = document.getElementById('imagen');
        output.src = URL.createObjectURL(event.target.files[0]);
    })
    $(document).on('click', '.saveusu', function() {
        nombre = $('#nom_usu').val()
        apellido = $('#ape_usu').val()
        telefono = $('#tlf_usu').val()
        if(tipousuario=='usuario'){
          nic = $("#nic_usu").val();
          mat = '';
        }else{
          nic = '';
          mat = $("#mat_usu").val();
        }
        correo = $('#cor_usu').val()
        tipo = $('#tipo_doc').val()
        num = $('#num').val()
        sexo = $('#sexo').val()
        
        //var pais = $("#pais").val()
        var estado = $("#estado").val()
        var ciudad = $("#ciudad").val()
        if (nombre.length < 3) {
            M.toast({ html: 'Por favor ingresa un nombre.' })
            return false;
        } else if (apellido.length < 3) {
            M.toast({ html: 'Por favor ingresa un apellido.' })
            return false;
        } else if (telefono.replace(/ /g, "").length < 8 /*|| telefono.replace(/ /g, "").length >15*/ ) {
            M.toast({ html: 'Télefono: Fórmato inválido.' })
            return false;
        } else if (!tipo) {
            M.toast({ html: 'Debe Seleccionar un tipo de documento.' })
            return false;
        } else if (num.length < 3) {
            M.toast({ html: 'Por favor ingresa el número de documento.' })
            return false;
        } else if (nic.length < 2 && tipousuario=='usuario') {
            M.toast({ html: 'Por favor ingresa el Nickname.' })
            return false;
        } else if (mat.length < 2 && tipousuario=='abogado') {
            M.toast({ html: 'Por favor ingresa la Matrícula.' })
            return false;
        } else if (!validar_mail(correo)) {
            err = "Por favor ingresa un correo válido.";
            $("#cor_reg").focus();
        } else if (!sexo) {
            M.toast({ html: 'Debe Seleccionar un sexo.' })
            return false;
        } else if (!estado) {
            M.toast({ html: 'Debe Seleccionar un estado.' })
            return false;
        } else if (!ciudad) {
            M.toast({ html: 'Debe Seleccionar un ciudad.' })
            return false;
        }
        url = dominio + "usuarios/edit-listadoadmin/";

        var array = nombre.split("");
        nombre = array[0].toLocaleUpperCase() + nombre.substr(1).toLowerCase();
        var data = new FormData(document.getElementById("update_usu"));
        data.append("id", user.id);
        data.append("token", user.token);
        data.append("nombre", nombre);
        data.append("rol_usu", tipousuario);
        $('.saveusu').attr("disabled", true);
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.saveusu').attr("disabled", false);
                if(data.r){
                    obtenerUsuarios();
                    $("#modalEditar").modal("close");
                    M.toast({html:data.msj}, 5000);
                }else if(data.msj){
                    M.toast({html:data.msj}, 5000);
                }else{
                    M.toast({html:data.msg}, 5000);
                }
            },
            error: function(xhr, status, err) {
                console.log("Errr : " + err);
                $('.saveusu').attr("disabled", false);

            }
        });
    })

  //-----------------ELIMINAR DATOS----------------------------//
    $(document).on('click','.delete_u',function(){
      var idv = this.id;
      var toastContent = '<span>¿Desea eliminar éste registro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

      M.toast({html:toastContent}, 4000);
    });
    $(document).on('click','.conf_si',function(){
      id = this.id
      var data = {
        ida: user.id,
        token: user.token
      };
      M.Toast.dismissAll();
      $.ajax({
        url:dominio+'usuarios/delete/'+id,
        type:'GET',
        data:data,
        success:function(data){
            if(data.msj){
                M.toast({html:data.msj}, 10000);
                obtenerUsuarios();
            }else{
                M.toast({html:"Error al eliminar."}, 10000);
              
            }
        }
      })

    })
 
});
