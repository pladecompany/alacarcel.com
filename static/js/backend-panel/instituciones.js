$(document).ready(function() {
    listinstitu();


    institu = [];
    id_institucion = null;


    function listinstitu() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'instituciones',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                institu = data;
                actualizar_tabla_institu(data)
            }

        })
    }

    function actualizar_tabla_institu(data) {
        var table = $('.instituciones').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            datos = data[i];

            var option = '<a class="btn btn-blue ver-Ins" i="' + i + '"><i class="fa fa-eye"></i></a> <a class="btn btn-blue edit-insti" i="' + i + '"><i class="fa fa-edit"></i></a> <a class="btn btn-blue delete_f" id="' + datos.id + '"><i class="fa fa-trash"></i></a>';
            var row = [count += 1, datos.nom_ins, datos.des_ins, datos.estado, datos.ciudad, option]
            table.row.add(row).draw().node();
        }
    }


    $(document).on('click', ".edit-insti", function() {

        var obj = institu[$(this).attr("i")];
        id_institucion = obj.id;
        $("#nom_ins").val(obj.nom_ins);
        $("#des_ins").val(obj.des_ins);
        $("#id_pais").val(obj.id_pais);

        id_estado = obj.id_estado;
        id_ciudad = obj.id_ciudad;
        ajaxEstados(obj.id_pais);
        ajaxCiudades(obj.id_estado);
        $("#instituto").modal("open");
    });

    $(document).on('click', ".addIns", function() {
        $(".formIns")[0].reset();
        $("#id_pais").val("").trigger("change");
        id_institucion = null;
        $("#instituto").modal("open");
    });
    $(document).on('click', ".ver-Ins", function() {

        var obj = institu[$(this).attr("i")];
        $(".nom_ins").html(obj.nom_ins);
        $(".des_ins").html(obj.des_ins);
        //$(".pais").html(obj.pais);
        $(".estado").html(obj.estado);
        $(".ciudad").html(obj.ciudad);
        $("#modalVer").modal("open");
    });

    $(document).on('click', ".save", function() {
        idf = $("#id_fun").val();
        var formData = new FormData();
        formData.append("nom_ins", $("#nom_ins").val());
        formData.append("des_ins", $("#des_ins").val());
        formData.append("id_pais", 2);
        formData.append("id_estado", $("#id_estado").val());
        formData.append("id_ciudad", $("#id_ciudad").val());
        formData.append("id", id_institucion);
        formData.append("ida", user.id);
        formData.append("token", user.token);
        if (id_institucion)
            url = "instituciones/editar";
        else
            url = "instituciones";
        $.ajax({
            url: dominio + "" + url,
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                if (data.r) {
                    listinstitu();
                    $("#instituto").modal('close');
                    $(".formIns")[0].reset();
                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                console.log(xhr);
            }
        });
    });


    //-----------------ELIMINAR DATOS----------------------------//
    $(document).on('click', '.delete_f', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste registro?</span><br><button class="btn-flat toast-action conf_si" id="' + idv + '">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

        M.toast({ html: toastContent }, 4000);
    });
    $(document).on('click', '.conf_si', function() {
        id = this.id
        var data = {
            ida: user.id,
            token: user.token
        };
        M.Toast.dismissAll();
        $.ajax({
            url: dominio + 'instituciones/delete/' + id,
            type: 'GET',
            data: data,
            success: function(data) {
                if (data.msj) {
                    M.toast({ html: data.msj }, 10000);
                    listinstitu();
                } else {
                    M.toast({ html: "Error al eliminar." }, 10000);

                }
            }
        })

    });
    //ajaxPaises();

    function ajaxPaises() {

        $.ajax({
            url: dominio + 'paises/activos',
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected>Selecciona el país</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#id_pais").html(html);
                $("#id_pais").formSelect();
            }

        })
    }
    ajaxEstados(2);
    function ajaxEstados(id) {

        $.ajax({
            url: dominio + 'paises/estado/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected>Selecciona el estado</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option ' + ((id_estado == data[i].id) ? "selected" : "") + ' value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }
                id_estado = null;
                $("#id_estado").html(html);
                $("#id_estado").formSelect();
            }

        })
    }

    function ajaxCiudades(id) {

        $.ajax({
            url: dominio + 'paises/ciudad/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected>Selecciona la ciudad</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option ' + ((id_ciudad == data[i].id) ? "selected" : "") + ' value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }
                id_ciudad = null;
                $("#id_ciudad").html(html);
                $("#id_ciudad").formSelect();
            }

        })
    }

    $("#id_pais").on('change', function() {

        ajaxEstados(this.value);
        ajaxCiudades("");

    });

    $("#id_estado").on('change', function() {

        ajaxCiudades(this.value);

    });


});