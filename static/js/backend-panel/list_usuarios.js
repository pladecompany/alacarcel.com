
$(document).ready(function(){
    var usuarios = []
    console.log("Modulo Listado de reclutadores");
    ajaxUsuarios();
    var table = $('.usuarios').DataTable({
        "language": {
            "url": "../static/lib/JTables/Spanish.json"
            }
        });

    function ajaxUsuarios(){
        /*data = {
            ida:user.id,
            token:user.token
        }*/
        $.ajax({
            url : dominio+'usuarios/admin',
            type: 'GET',
            success:function(data){
                actualizar_tabla_usuarios(data)
            }

        })
    }

    function actualizar_tabla_usuarios(data){
        
        count = 0
        table.clear().draw();
        usuarios = data;
        for(var i=0;i<data.length;i++){
            datos = data[i]
            var option = `
            <a href="#" onclick="return false;" class="btn btn-red ver" id="${i}"><img src="../static/img/icons/eye.png"></a>
            <a href="#" onclick="return false;" class="btn btn-red delete" id="${datos.id}"><img src="../static/img/icons/trash.png"></a>
            `
            var row = [count+=1,datos.nom_usu+' '+datos.ape_usu,datos.tlf_usu,datos.cor_usu,datos.pais,option]
            table.row.add(row).draw().node();
        }
    }
    $(document).on('click','.ver',function(){
        data = usuarios[this.id]
        $('#modalVerPostulante').modal('open');
        $('#nombre').html(data.nom_usu+' '+data.ape_usu)
        $('#telefono').html(data.tlf_usu)
        $('#correo').html(data.cor_usu)
        $('#pais').html(data.pais)
        $('#sexo').html(data.sex_usu)
        $('#direccion').html(data.dir_usu)
        $('#cv').html(data.cv ? '<a href="'+data.cv+'" id="cv_link" target="__blank" style="font-weight:bold;">Click para ver el CV</a>':'No posee cv registrado.')
        $('#fecha').html(moment(data.fec_reg_usu).format('DD-MM-YYYY'))
        $('#fecha_nac').html(moment(data.fec_nac_usu).format('DD-MM-YYYY'))
        $('#img-usu').attr('src',data.imagen)
    })  
    $('#img-usu').on('error',function(){
        $(this).attr('src','../static/img/user.png')
    })
    $(document).on('click','.delete',function(){
        
        var idv = this.id;
        var toastContent = '<span>¿Está seguro? Se borrará toda la información relacionada con este postulante.</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';
  
        M.toast({html:toastContent}, 6000);
    });
    $(document).on('click','.conf_si',function(){
        id = this.id
        M.Toast.dismissAll();
        
        $.ajax({
            url:dominio+'usuarios/delete/'+id,
            type:'GET',
            success:function(data){
                M.toast({html:data}, 2000);
                ajaxUsuarios();
            }
        })
    })
});

