 $(document).ready(function() {
     function getParameterByName(name) {
         name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
         var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
             results = regex.exec(location.search);
         return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
     }
     if(paneluser=='usuario')
        imguserperfil='static/img/user.png';
     else
        imguserperfil='../static/img/user.png';
     var tipoBU = getParameterByName("miscasos") || getParameterByName("unido");

     if(paneluser=='usuario'){
        if(!getParameterByName("viewcs")){
            listfuncionarios(0);
            listintituciones();
            ajaxEstados(2);
        }else{ 
            if(user){
                listfuncionarios(0);
                listintituciones();
                ajaxEstados(2);
            }else{
                $("#estado").attr("disabled",true);
                $("#search").attr("disabled",true);
                $(".btn_buscar_casos").attr("disabled",true);
                listcasos_sinloguearse(1);
            }
                
        }
        
     }
     

     institu = [];
     usercaso = [];
     Casos = [];
     IDCASOS = [];
     IDCASOSDOS = [];
     MontoMin = 1;
     var pagina = 1;
     var idciudad = 0;
     if(user)
        idciudad = user.id_ciudad;
     var tipolista = 0;
     tlist = getParameterByName('list');
     usuarioget = getParameterByName('usuario');
     if (tlist == 'publicados')
         $("#tit_casos").html("CASOS QUE HE PUBLICADO");
     else if (tlist == 'unidos')
         $("#tit_casos").html("CASOS QUE ME HE UNIDO");
     else if (tlist == 'postulados')
         $("#tit_casos").html("CASOS EN LOS QUE ME HE POSTULADO");
     else if (tlist == 'representados')
         $("#tit_casos").html("CASOS QUE HE REPRESENTADO");

     if (tlist) {
         if(usuarioget) {
            if(usuarioget!=user.id) {
                     if (tlist == 'publicados')
                     $("#tit_casos").html("CASOS PUBLICADOS DE <span class='info-usu-name'></span>");
                     else if (tlist == 'unidos')
                     $("#tit_casos").html("CASOS QUE APOYA <span class='info-usu-name'></span>");
                     else if (tlist == 'postulados')
                     $("#tit_casos").html("CASOS DONDE SE POSTULA <span class='info-usu-name'></span>");
                     else if (tlist == 'representados')
                     $("#tit_casos").html("CASOS REPRESENTADOS DE <span class='info-usu-name'></span>");
                     
                     $(".info-usu-casos").removeClass("hide");
                     $('.info-usu-name').html("<i class='fa fa-spinner fa-spin fa-fw'></i>");
                     $.ajax({
                         url: dominio + 'usuarios/' + usuarioget,
                         type: 'GET',
                         success: function(data) {
                                if(data.id == usuarioget) {
                                     $('.info-usu-name').html(((data.nom_usu)? data.nom_usu:'') + " " + ((data.ape_usu)?data.ape_usu:''));
                                     if (data.imagen != null) {
                                         $('.info-usu-img').attr('src', data.imagen)
                                     }
                                     $('.info-usu-img').on('error', function() {
                                         $(this).attr('src', imguserperfil)
                                     });
                                }
                         },
                         error: function(xhr, status, errorThrown) {
                             $('.info-usu-name').html("...");
                             console.log(xhr);
                         }
                     });  
            }else {
                 $(".info-usu-casos").removeClass("hide");
                 $('.info-usu-name').html(((user.nom_usu)?user.nom_usu:'') + " " + ((user.ape_usu)?user.ape_usu:''));
                 if (user.imagen != null) {
                     $('.info-usu-img').attr('src', user.imagen)
                 }
                 $('.info-usu-img').on('error', function() {
                     $(this).attr('src', imguserperfil)
                 });  
            }
         } else if(usuarioget==user.id || !usuarioget) {
             $(".info-usu-casos").removeClass("hide");
             $('.info-usu-name').html(((user.nom_usu)?user.nom_usu:'') + " " + ((user.ape_usu)?user.ape_usu:''));
             if (user.imagen != null) {
                 $('.info-usu-img').attr('src', user.imagen)
             }
             $('.info-usu-img').on('error', function() {
                 $(this).attr('src', imguserperfil)
             });
         }
         
     }
    if(user){
         $.ajax({
                url : dominio+'configuracion',
                type: 'GET',
                data:{ ida:user.id, token:user.token},
                success:function(data){
                    if(data){
                        if(data.monto){
                            MontoMin = parseFloat(data.monto);
                        }
                    }
                    
                },
                error: function(xhr, status, errorThrown){
                    console.log(xhr);
                }
          });
    }
     //listcasos(1);

     function progress__event(bt) {
         if (bt) {

             bt.attr('disabled', true);
         }
         var configprogress = {
             onUploadProgress: progressEvent => {
                 EstatusProgress(Math.floor((progressEvent.loaded * 100) / progressEvent.total), bt);
             }
         };
         return configprogress;
     }

     function EstatusProgress(value, bt) {
         var ProgressP = document.querySelector("#Progress__P");
         var ProgressC = document.querySelector("#Progress__C");
         if (ProgressP && ProgressC) {
             ProgressP.style.display = "block";
             if (value < 100) {
                 ProgressC.style.width = value + '%';

             } else {
                 if (bt) {
                     bt.attr('disabled', false);
                 }
                 ProgressC.style.width = value + '%';
                 setTimeout(function() {
                     ProgressP.style.display = "none";
                     ProgressC.style.width = '0%';
                 }, 500);
             }
         } else {
             if (bt) {
                 bt.attr('disabled', false);
             }
         }
     }

     function divcargando() {
         htmlcar = '';
         htmlcar += '<div class="row mt-3 divCaso">';
         htmlcar += '<div class="col s12">';
         htmlcar += '<div class="row">';
         htmlcar += '<div class="col s2 center p-2">';
         htmlcar += '<span class="cspan cspan1"></span>';
         htmlcar += '<span class="cspan cspan2"></span>';
         htmlcar += '<span class="cspan cspan2-2"></span>';
         htmlcar += '</div>';
         htmlcar += '<div class="col s10">';
         htmlcar += '<span class="cspan cspan3"></span>';
         htmlcar += '<span class="cspan cspan4"></span>';
         htmlcar += '<br>';
         htmlcar += '<span class="cspan cspan4"></span>';
         htmlcar += '<span class="cspan cspan5"></span>';
         htmlcar += '</div>';
         htmlcar += '</div>';
         htmlcar += '</div>';
         htmlcar += '</div>';
         return htmlcar;
     }
     function comencargando() {
        htmlcar = '';
        htmlcar += '<div class="row m-0">';
            htmlcar += '<div class="col s1">';
                htmlcar += '<span class="cspan cspan1-c"></span>';
            htmlcar += '</div>';
            htmlcar += '<div class="col s11"><span class="cspan cspan4"></span><span class="cspan cspan5"></span></div>';
        htmlcar += '</div>';
        return htmlcar;
    }
     //funcion para quitar tabindex del modal y funcione el select2
     //document.getElementById("instituto").removeAttribute("tabindex"); 

     function ajaxEstados(id, id_estado = 0) {
         $('#estado').html('<option value="" selected disabled>Cargando...</option>');
         $.ajax({
             url: dominio + 'paises/estado/' + id,
             type: 'GET',
             success: function(data) {

                 html = '<option value="" selected disabled>Seleccione un estado</option><option value="0">Todos los Casos</option>';
                 TiposVeh = data;
                 for (var i = 0; i < data.length; i++) {
                     html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                 }

                 $("#estado").html(html);
                 if(getParameterByName('viewcs'))
                    listcasos(1);
                 else
                    $("#estado").val(0).trigger("change");
                 /*if (user.id_estados) {
                    if(tlist){
                      $("#estado").val(0).trigger("change");
                    }else{
                      $("#estado").val(user.id_estados);
                        ajaxCiudades(user.id_estados);  
                    }
                    
                 }else if (user.id_estados==null) {
                    $("#estado").val(0).trigger("change");
                 }*/
                 $("#estado").formSelect();
             }

         })
     }

     $("#estado").on('change', function() {
         $(".pagination").html('');
         valor = $(this).val();
         if (valor == 0) {
             $(".li-ciudades").html('');
             listcasos(1);
             return;
         }
         lista = '<div class="row mt-2 text-center msj-casos">';
         lista += '<div class="col s12">';
         lista += '<div class="row m-0">';
         lista += '<div class="col s12 p-2">';
         lista += '<i class="fa fa-flag fa-3x clr_blue"></i>';
         lista += '<h5>Seleccione una ciudad</h5>';
         lista += '</div>';
         lista += '</div>';
         lista += '</div>';
         lista += '</div>';
         $("#publicados").html(lista);
         ajaxCiudades(this.value);

     });

     function ajaxCiudades(id, id_ciudad = 0) {
         if (!id) {
             return;
         }
         $(".li-ciudades").html('<li class="collection-header"><h6 class="text-center">Ciudades</h6></li><li class="collection-item"><a href="#"><i class="fa fa-spinner fa-spin fa-fw " style="color:#00b0f0;"></i> Cargando. . .</a></li>');
         data = {
             ida: ((usuarioget)?usuarioget:user.id),
             token: user.token,
             buscar: $("#search").val()
         }
         if (tlist == 'publicados')
             data.tipo = 1;
         else if (tlist == 'unidos')
             data.tipo = 2;
         else if (tlist == 'postulados')
             data.tipo = 3;
         else if (tlist == 'representados')
             data.tipo = 4;

         $.ajax({
             url: dominio + 'paises/ciudades-casos/' + id,
             type: 'GET',
             data: data,
             success: function(data) {
                 html = '<li class="collection-header"><h6 class="text-center">Ciudades</h6></li>';
                 for (var i = 0; i < data.length; i++) {
                     html += '<li class="collection-item"><a href="#" class="select-ciudad" id="' + data[i].id + '">' + data[i].nombre + ' (' + data[i].total + ')</a> <span class="icon-ciudad-selec" id="ciudad-selec-' + data[i].id + '"></span></li>';
                 }
                 if (data.length > 0)
                     html += '<li class="collection-item"><a href="#" class="text-bold select-ciudad" id="0">Ver todas</a> <span class="icon-ciudad-selec" id="ciudad-selec-0"></span></li>';
                 $(".li-ciudades").html(html);
                 if (idciudad) {
                     $("#ciudad-selec-" + idciudad).html('<i class="fa fa-check text-bold right" style="color:#00b0f0;"></i>');
                 }

             }

         })
     }

     $(document).on('click', ".select-ciudad", function() {
         idciudad = this.id;
         $(".icon-ciudad-selec").html('');
         $("#ciudad-selec-" + idciudad).html('<i class="fa fa-check text-bold right" style="color:#00b0f0;"></i>');
         listcasos(1);
     });

     function listfuncionarios(idfnew) {
         data = {
             ida: user.id,
             token: user.token
         }
         $.ajax({
             url: dominio + 'funcionario',
             type: 'GET',
             data: data,
             success: function(data) {
                 //console.log(data);
                 actualizar_select_funcionario(data,idfnew);

             }

         });
     }

     function obtenerCaso(id) {
         for (var i = 0; i < Casos.length; i++) {
             if (Casos[i].id == id) {
                 return Casos[i];
             }
         };
         return false;
     }

     function actualizar_select_funcionario(data,idfnew) {
         veri = 0;
         $("#list_fun").html('');
         for (var i = 0; i < data.length; i++) {
             veri++;
             datos = data[i];
             institu[datos.id] = datos.institucion.nom_ins;
             if(idfnew==datos.id)
                $("#ver_inst").val(datos.institucion.nom_ins);
             
                
             $("#list_fun").append('<option ' + ((idfnew==datos.id) ? "selected" : "") + ' value="' + datos.id + '">' + datos.nom_fun + " " + datos.ape_fun + '</option>');
             if (veri == data.length) {
                 $('#list_fun').select2({
                     language: {
                         noResults: function() {
                             return "No hay resultado";
                         },
                         searching: function() {
                             return "Buscando..";
                         }
                     },
                     dropdownParent: $('#modalCrearcaso') 
                 });
                 $('.select2').css("width", "100%");
             }
         }

     }
     var vistaindividual = true; 
     function listcasos(op) {
         $("#publicados").html(divcargando() + divcargando() + divcargando());
         $(".pagination").html('');
         idestado = $("#estado").val();
         if (!idestado)
             idestado = user.id_estados;
         data = {
             ida: ((usuarioget)?usuarioget:user.id),
             token: user.token,
             op: op,
             id_ciudad: idciudad,
             buscar: $("#search").val(),
             id_estado: idestado
         }

         if(getParameterByName('viewcs') && vistaindividual){
            data.id_caso=getParameterByName('viewcs');
            vistaindividual = false; 
         }

         url = "casos";
        
         if (tlist == 'publicados')
             data.tipo = 1;
         else if (tlist == 'unidos')
             data.tipo = 2;
         else if (tlist == 'postulados')
             data.tipo = 3;
         else if (tlist == 'representados')
             data.tipo = 4;
         $.ajax({
             url: dominio + url,
             type: 'GET',
             data: data,
             success: function(data) {
                 //console.log(data);
                 actualizar_lista_casos(data, op);
                 Casos = data.casos;
             }

         });
     }

     function actualizar_lista_casos(data, op) {
         //console.log(data.casos)
         lista = '';
         
          IDCASOS = [];
          IDCASOSDOS = [];
         datac = data.casos;
         if (datac.length == 0) {
             lista += '<div class="row mt-2 text-center msj-casos">';
             lista += '<div class="col s12">';
             lista += '<div class="row m-0">';
             lista += '<div class="col s12 p-2">';
             lista += '<i class="fa fa-times-circle fa-4x clr_blue"></i>';
             lista += '<h5>No se ha encontrado ningún caso</h5>';
             //lista+='<a href="?op=listadodecasos">Volver</a>';
             lista += '</div>';
             lista += '</div>';
             lista += '</div>';
             lista += '</div>';
         }

         for (var i = 0; i < datac.length; i++) {

             datos = datac[i];
             IDCASOSDOS.push(datos.id);
             lista += '<div class="row mt-3 divCaso">';
             lista += '<div class="col s12">';
             lista += '<div class="row mb-0">';
             lista += '<div class="col s3 m2 center p-2">';
             if(datos.usuario.imagen!=null)
                imgcaso=datos.usuario.imagen;
             else
                imgcaso=imguserperfil;
             lista += '<a href="?op=listadodecasos&list=publicados&usuario='+datos.id_usuario+'"><img src="' + imgcaso + '" class="img-user boxPerfil--img img-cover imgcasos"></a>';
             lista += '<h6 style="font-size:14px;"><b>' + datos.usuario.nom_usu + ' ' + datos.usuario.ape_usu + '</b></h6>';
             lista += '</div>';

             lista += '<div class="col s9 m10 p-1">';
             if (user.id == datos.id_usuario) {
                 usercaso[datos.id] = datos.id_usuario;
                 lista += '<span><i class="ml-1 fa fa-ellipsis-v right btnOpc" id="' + datos.id + '" style="cursor: pointer;"></i></span>';
                 lista += '<div id="divopciones' + datos.id + '" style="display: none;">';
                 lista += '<a class="btn-blue btn-floating btn-opciones m-1 right"><i class="fa fa-times right btnx" id="' + datos.id + '"></i></a>';
                 lista += "<textarea id='dat_c_" + datos.id + "' style='display:none;'>" + JSON.stringify(datos) + "</textarea>";
                 lista += '<a href="#" class="btn-blue btn-floating btn-opciones m-1 right edit-caso" id="' + datos.id + '"><i class="fa fa-edit" id="btnEditar"></i></a>';
                 lista += '<a href="#" class="btn-blue btn-floating btn-opciones m-1 right"><i class="fa fa-trash delete_c" id="' + datos.id + '"></i></a>';
                 lista += '';
                 lista += '</div>';
             }

             lista += '<span class="right opc_ciu pl-2 pr-2 text-uppercase"><i class="fa fa-flag"></i> ' + datos.funcionario.institucion.nombre_estado + ' - ' + datos.funcionario.institucion.nombre + ' </span>';
             txtsts='';
             if(datos.sts_caso==1)
                txtsts='Caso en propuesta';
             else if(datos.sts_caso==2)
                txtsts='Caso en curso';
             else if(datos.sts_caso==3)
                txtsts='Caso terminado';
             else if(datos.sts_caso==4)
                txtsts='Caso cancelado';
            else if(datos.sts_caso==5)
                txtsts='En votación';
             if(txtsts!='')
                lista += '<span class="right opc_sts pl-2 pr-2 text-uppercase"><i class="fa fa-certificate"></i> '+txtsts+' </span>';
             lista += '<div class="row mb-0"><div class="col s12 m9"><h5 class="m-1"><b class="tooltipped" data-position="bottom" data-tooltip="ID del Caso">#' + datos.id + ' </b>Caso: ' + datos.nom_caso + '</h5></div><div class="col s12 m3"><h6>Monto recaudado: <b id="' + datos.id + '" class="clr_blue MontoRe">0.00$</b></h6></div></div>';
             lista += '<p class="m-0 text-justify pr-2">' + datos.des_caso + '</p>';
             lista += '<h6> '+((datos.abogado)?' <i class="fa fa-user icon"></i> Representante: ' + datos.abogado.nom_usu + ' ' + datos.abogado.ape_usu + ' ':'')+' <i class="fa fa-user icon"></i> ' + datos.funcionario.nom_fun + ' ' + datos.funcionario.ape_fun + ' &nbsp;&nbsp;&nbsp;<i class="fa fa-building icon"></i> ' + datos.funcionario.institucion.nom_ins + ' &nbsp;&nbsp;&nbsp;';
             listarchivo='';
             if (datos.archivos.length > 0) {
                listarchivo = '<a style="cursor:pointer;" id="' + datos.id + '" class="clr_black ver_archivos ver_archivo_' + datos.id + '"><i class="fa fa-folder-open icon"></i> Archivos adjuntos&nbsp;&nbsp;</a>';
             } 

             //CONDICIONES PARA SABER SI HAY ARCHIVOS ADJUNTOS
             if(listarchivo !='' && user.id == datos.id_usuario)
                lista +=listarchivo;
             else if(listarchivo!='' && user.id != datos.id_usuario){
                can1=0;
                can2=0;
                for (var j =  0; j < datos.archivos.length; j++) {
                    if(datos.archivos[j].visible==1)
                        can1++;
                    else
                        can2++;
                };
                
                if(can1>0)
                    lista +=listarchivo;
                else if (can2>0 && user.rol=='Abogado')
                    lista +=listarchivo;
             }
                
             lista += '<a style="cursor:pointer;" id="' + datos.id + '" class="clr_black btnComentarios"><i class="fa fa-comments icon"></i> '+datos.comentarios+' comentarios &nbsp;&nbsp;</a></h6>';   
             lista += '<button type="button" id="' + datos.id + '"  href="#!" class="btn-blue btn-flat btn-floating  waves-effect waves-light BTUNIDOS">';
             lista += '<i class="fa fa-eye tooltipped total_unidos" style="font-size:16px;" id="' + datos.id + '" data-position="bottom" data-tooltip="Unidos al caso <b>(0)</b>"></i>';
             lista += '</button> &nbsp;';

             lista += '<button type="button" id="' + datos.id + '" class="btn-blue btn-flat btn-floating  waves-effect waves-light BTPOSTULADOS">';
             lista += '<i class="fa fa-users tooltipped total_postulados" style="font-size:16px;" id="' + datos.id + '" data-position="bottom" data-tooltip="Postulados al caso <b>(0)</b>"></i>';
             lista += '</button> &nbsp;';

             if (user.rol == "Usuario" && user.id != datos.id_usuario && datos.sts_caso==1) {
                 lista += '<button type="button" id="' + datos.id + '" class="btn-blue btn-flat btn-floating waves-effect waves-light BTUNIRSEMODAL BTUNIDOSOCKET" disabled>';
                 lista += '<i class="fa fa-user-plus tooltipped" style="font-size:16px;" data-position="bottom" data-tooltip="Unirse al caso"></i>';
                 lista += '</button> &nbsp;';
                 lista += '<button type="button" id="' + datos.id + '" class="btn-blue btn-flat btn-floating waves-effect waves-light hide BTPAGAR BTUNIDOSOCKET">';
                 lista += '<i class="fab fa-paypal tooltipped" style="font-size:16px;" data-position="bottom" data-tooltip="Ya estás unido solo falta un paso, presiona este botón para continuar, si ya pagaste debes esperar la confirmación automática."></i>';
                 lista += '</button> &nbsp;';
                 IDCASOS.push(datos.id);
            }
            if (user.rol == "Usuario" && user.id != datos.id_usuario) {
                 lista += '<button type="button" id="' + datos.id + '" tit="' + datos.nom_caso + '" class="btn-blue btn-flat btn-floating waves-effect waves-light compart-link">';
                 lista += '<i class="fa fa-share-alt tooltipped" style="font-size:16px;" data-position="bottom" data-tooltip="Compartir en redes sociales"></i>';
                 lista += '</button> &nbsp;';
             }

             if (user.rol == "Abogado" && user.id != datos.id_usuario && datos.id_abogado != user.id && datos.sts_caso==1) {
                 lista += '<button id="' + datos.id + '" type="button" class="btn-blue btn-flat btn-floating  waves-effect waves-light BTNOPOST" disabled>';
                 lista += '<i class="fa fa-user-plus tooltipped" style="font-size:16px;"  data-position="bottom" data-tooltip="Postularse al caso"></i>';
                 lista += '</button> &nbsp;';
                 //PARA VALIDACIONES DE POSTULACION
                 IDCASOS.push(datos.id);
             }
             if(datos.id_abogado == user.id && datos.sts_caso>1){
                lista += '<button type="button" id="' + datos.id + '" class="btn-blue btn-flat btn-floating waves-effect waves-light abrir-pdf abrir-pdf-' + datos.id + '">';
                lista += '<i class="fa fa-file-pdf tooltipped" style="font-size:16px;" data-position="bottom" data-tooltip="PDF con información del caso"></i>';
                lista += '</button> &nbsp;';
             }

             lista += '<br><small class="right"><i class="fa fa-clock"></i> ' + moment(datos.fec_reg_caso).toNow() + '</small>';
             lista += '<div id="divComentarios-' + datos.id + '" class="mt-3 divComentarios hide"></div>';
             lista += '</div>';
             lista += '</div>';
             lista += '</div>';
             lista += '</div>';
         }

         pagina = op;
         paginacion(op, data.total[0]);
         $("#publicados").html('');
         $("#publicados").append(lista);
         $('.tooltipped').tooltip();
         $('.imgcasos').on('error', function() {
             $(this).attr('src', imguserperfil);
         });
         if (user.rol == "Abogado") {
             // FUNCTION BUSCAR
             buscarCasosPropios();
         } else {
             buscarCasosPropiosU();
         }
         buscarUni_Pos();
     }

     function paginacion(pag, tot) {
         tot = tot['count(*)'];
         if (tot == 0)
             $(".pagination").html('');
         else {
             pagtot = parseFloat(tot) / 15;
             pagtot = Math.ceil(pagtot);
             htmlpag = '';
             pen_pag = parseInt(pag) + 1;
             if (pag <= 2) {
                 ne = 1;
                 nf = 5;
             } else if (pen_pag == pagtot && pag > 3) {
                 ne = parseInt(pag) - 3;
                 nf = pagtot;
             } else if (pag == pagtot && pag > 4) {
                 ne = parseInt(pag) - 4;
                 nf = pagtot;
             } else {
                 ne = parseInt(pag) - 2;
                 nf = parseInt(pag) + 2;
             }
             if (nf > pagtot)
                 nf = pagtot;
             for (var i = ne; i <= nf; i++) {
                 if (pag == i)
                     htmlpag += '<li class="active"><a href="#!">' + pag + '</a></li>';
                 else
                     htmlpag += '<li class="waves-effect click-pag" id="' + i + '"><a href="#!">' + i + '</a></li>';
             };
             $(".pagination").html(htmlpag);
         }

     }

     $(document).on('click', ".click-pag", function() {
         pag = this.id;
         listcasos(pag);
     });
     $(document).on('click', ".btn_buscar_casos", function() {
         if(!$("#estado").val())
            M.toast({ html: "Selecciona un estado" }, 5000);
         else{
            listcasos(1);
            ajaxCiudades($("#estado").val());
         } 
     });

     function buscarUni_Pos() {

         $(".total_unidos").attr('data-tooltip', 'Unidos al caso <b>(cargando...)</b>');
         $(".total_postulados").attr('data-tooltip', 'Postulados al caso <b>(cargando...)</b>');
         $('.tooltipped').tooltip();
         data = {
             id_usuario: user.id,
             token: user.token,
             casos: IDCASOSDOS
         }
         $.ajax({
             url: dominio + 'casos/postulaciones-y-unidos/' + data.id_usuario,
             type: 'GET',
             data: data,
             success: function(data) {
                
                 $(".total_unidos").attr('data-tooltip', 'Unidos al caso <b>(0)</b>');
                 $(".total_postulados").attr('data-tooltip', 'Postulados al caso <b>(0)</b>');
                 var MontoRe = $(".MontoRe");
                 var buttons = $(".total_unidos");
                 var buttonsp = $(".total_postulados");

                 for (var i = 0; i < buttons.length; i++) {
                     var b = buttons[i];
                     var mon = MontoRe[i];

                     for (var j = 0; j < data[0].length; j++) {
                         var caso = data[0][j];
                         
                         if(mon) {
                            if (caso.id_caso == mon.id) {
                             mon.innerText = formato.precio(caso.recaudo) + " $";
                            }
                         }
                         
                         if (caso.id_caso == b.id) {
                            
                            b.setAttribute("data-tooltip", "Unidos al caso <b>(" + caso.todos + ")</b>");

                         }
                     };
                 };
                 for (var i = 0; i < buttonsp.length; i++) {
                     var b = buttonsp[i];
                     for (var j = 0; j < data[1].length; j++) {
                         var caso = data[1][j];
                         if (caso.id_caso == b.id) {
                             b.setAttribute("data-tooltip", "Postulados al caso <b>(" + caso.todos + ")</b>");
                         }
                     };
                 };
             },
             error: function(xhr, status, errorThrown) {
                 $(".total_unidos").attr('data-tooltip', 'Unidos al caso <b>(0)</b>');
                 $(".total_postulados").attr('data-tooltip', 'Postulados al caso <b>(0)</b>');
                 $('.tooltipped').tooltip();
             }

         });
     }
     QuitarBotones = function QuitarBotones(data) {
            
             var buttons = $(".BTUNIDOSOCKET");
              for (var i = 0; i < buttons.length; i++) { 
                var b = buttons[i];
                if (data.id_caso == b.id) {
                    b.classList.add("hide");
                }
              }
              buscarUni_Pos();
     }
      buscarCasosPropiosU = function buscarCasosPropiosU() {

         $(".BTUNIRSEMODAL").find("i").removeClass('fa-user-plus');
         $(".BTUNIRSEMODAL").find("i").addClass('fa-spinner fa-spin fa-fw');
         $(".BTUNIRSEMODAL, .BTUSIPOST").attr("disabled", true);
         data = {
             id_usuario: user.id,
             token: user.token,
             casos: IDCASOS
         }
         $.ajax({
             url: dominio + 'casos/postulaciones-activas-usuario/' + data.id_usuario,
             type: 'GET',
             data: data,
             success: function(data) {

                 var buttons = $(".BTUNIRSEMODAL");

                 $(".BTUNIRSEMODAL, .BTUSIPOST").find("i").addClass('fa-user-plus');

                 for (var i = 0; i < buttons.length; i++) {
                     var b = buttons[i];
                     for (var j = 0; j < data.length; j++) {
                         var caso = data[j];
                         if (caso.id_caso == b.id) {
                             b.classList.remove("BTUNIRSEMODAL");
                             b.classList.add("BTUSIPOST");
                             b.children[0].setAttribute("data-tooltip","Retirarse del caso");
                             b.children[0].classList.remove("fa-user-plus");
                             b.children[0].classList.add("fa-user-minus");
                             if(caso.est_pos == 1) {
                               b.nextElementSibling.setAttribute("postulado", caso.id);
                               b.nextElementSibling.classList.remove("hide"); 
                             } else if(caso.est_pos == 2) {
                                b.classList.add("hide");
                             }
                             
                         }
                     };
                        if(getParameterByName('viewcs') == b.id && b.classList.contains("BTUNIRSEMODAL")) {
                                var butt = b;
                                butt.children[0].classList.remove('fa-user-plus');
                                butt.children[0].classList.add('fa-spinner','fa-spin','fa-fw');
                                butt.setAttribute("disabled", false);
                                setTimeout(function() {
                                    butt.children[0].classList.remove('fa-spinner','fa-spin','fa-fw');
                                    butt.children[0].classList.add('fa-user-plus');
                                    butt.click();
                                }, 2000);
                                
                        }

                 };

                 $(".BTUNIRSEMODAL, .BTUSIPOST").attr("disabled", false);
                 if(!getParameterByName('viewcs')) {
                    $(".BTUNIRSEMODAL, .BTUSIPOST").find("i").removeClass('fa-spinner fa-spin fa-fw');
                 } 
                 
             },
             error: function(xhr, status, errorThrown) {
                 $(".BTUNIRSEMODAL").attr("disabled", false);
                 $(".BTUNIRSEMODAL").find("i").removeClass('fa-spinner fa-spin fa-fw');
                 $(".BTUNIRSEMODAL").find("i").addClass('fa-user-plus');
             }

         });
     }

     function buscarCasosPropios() {
         $(".BTNOPOST").find("i").removeClass('fa-user-plus');
         $(".BTNOPOST").find("i").addClass('fa-spinner fa-spin fa-fw');
         data = {
             id_usuario: user.id,
             token: user.token,
             casos: IDCASOS
         }
         $.ajax({
             url: dominio + 'casos/postulaciones-activas-abogado/' + data.id_usuario,
             type: 'GET',
             data: data,
             success: function(data) {

                 var buttons = $(".BTNOPOST");
                 $(".BTNOPOST, .BTSIPOST").find("i").addClass('fa-user-plus');
                 for (var i = 0; i < buttons.length; i++) {
                     var b = buttons[i];
                     for (var j = 0; j < data.length; j++) {
                         var caso = data[j];
                         if (caso.id_caso == b.id) {
                             b.classList.remove("BTNOPOST");
                             b.classList.add("BTSIPOST");
                             b.children[0].setAttribute("data-tooltip","Retirarse del caso");
                             b.children[0].classList.add("fa-user-minus");
                             b.children[0].classList.remove("fa-user-plus");
                         }
                     };
                 };

                 $(".BTNOPOST, .BTSIPOST").attr("disabled", false);
                 $(".BTNOPOST, .BTSIPOST").find("i").removeClass('fa-spinner fa-spin fa-fw');
                 
             },
             error: function(xhr, status, errorThrown) {
                 $(".BTNOPOST").attr("disabled", false);
                 $(".BTNOPOST").find("i").removeClass('fa-spinner fa-spin fa-fw');
                 $(".BTNOPOST").find("i").addClass('fa-user-plus');
             }

         })
     }
     $(document).on('change', "#list_fun", function() {
         val = $(this).val();
         if (val != '')
             $("#ver_inst").val(institu[val]);
         else
             $("#ver_inst").val('');
     });

     $(document).on('click', ".crea-caso", function() {
         if(user.est_usu == 1){
            window.location = "?op=perfil";
            return;
         }
         $('.lista_archivos').html('');
         $("#crear_caso").trigger("reset");
         $(".limpiar_form_2").val('').trigger("change");
         $("#modalCrearcaso").modal("open");
     });
     $(document).on('click', ".edit-caso", function() {
         $("#edit_caso").trigger("reset");
         idc = $(this).attr("id");
         var obj = JSON.parse($("#dat_c_" + idc).val());
         $("#id_caso").val(obj.id);
         $("#nom_caso_edit").val(obj.nom_caso);
         $("#des_caso_edit").val(obj.des_caso);
         $('.lista_archivos_e').html('');
         data = {
             ida: user.id,
             token: user.token,
             idc: obj.id
         }
         $.ajax({
             url: dominio + 'casos/archivos/',
             type: 'GET',
             data: data,
             success: function(data) {

                 htmlarc = '';
                 for (var i = 0; i < data.length; i++) {
                     check_a='';
                     check_b='';
                     if(data[i].visible==1)
                        check_a='checked';
                     else if(data[i].visible==2)
                        check_b='checked';
                     htmlarc += '<div class="row mb-1 divArchivo li_arc_' + data[i].id + '" style="border:1px solid #ddd;">';
                     htmlarc += '<div class="col s7" style="overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">' + data[i].nom_archivo + '</div>';
                     htmlarc += '<div class="col s4" style="overflow: hidden;"><b style="font-size:10px;">Visible:</b><label><input type="radio" name="privaimagen_edit[' + data[i].id + ']" value="1" '+check_a+'> <span class="span-arc">Todo Publico</span></label> <label><input type="radio" name="privaimagen_edit[' + data[i].id + ']" value="2" '+check_b+'> <span class="span-arc">Abogados</span></label></div>';
                     htmlarc += '<div class="col s1"><a href="#" style="line-height: 1.5;" class="fa fa-times-circle delete_a right" id="' + data[i].id + '"></a></div>';
                     htmlarc += '</div>';
                 }
                 $(".lista_archivos_e").html(htmlarc);
             }

         })
         $("#modalEditarcaso").modal("open");
     });


     $(document).on('click', ".ver_archivos", function() {
         idc = $(this).attr("id");
         data = {
             ida: user.id,
             token: user.token,
             tipo: user.rol,
             idc: idc
         }
         $.ajax({
             url: dominio + 'casos/archivos/',
             type: 'GET',
             data: data,
             success: function(data) {
                 //console.log(data);
                 $("#ul_arc").html('');
                 lihtml = '';
                 for (var i = 0; i < data.length; i++) {
                     lihtml += '<li class="collection-item collection-casos avatar clr_black li_arc_' + data[i].id + '">';
                     if (data[i].tipo == 'image/png' || data[i].tipo == 'image/gif' || data[i].tipo == 'image/jpeg' || data[i].tipo == 'image/jpg')
                         lihtml += '<i class="fa fa-image circle green"></i>';
                     else if (data[i].tipo == 'application/pdf')
                         lihtml += '<i class="fa fa-file-pdf circle red"></i>';
                     else
                         lihtml += '<i class="fa fa-file circle"></i>';
                     lihtml += '<p>' + data[i].nom_archivo + '</p>';
                     idusu = usercaso[data[i].id_caso];
                     if (idusu && idusu == user.id)
                         lihtml += '<a href="#" class="secondary-content" style="right: 38px;top: 19px;"><i class="fa fa-trash right delete_a" id="' + data[i].id + '" name="' + data[i].id_caso + '"></i></a>';
                     lihtml += '<a href="#!" class="secondary-content descargar-arc" ruta="' + data[i].archivo + '" name="' + data[i].nom_archivo + '"><i class="fa fa-download"></i></a>';
                     lihtml += '</li>';
                 }
                 $("#ul_arc").html(lihtml);
                 $("#modalArchivosadjuntados").modal("open");
             }

         })

     });

     $(document).on('click', ".descargar-arc", function() {
         ruta = $(this).attr("ruta").split("\\");
         name = $(this).attr("name");
         window.open('descargar.php?url=' + ruta[3] + "&name=" + name, '_blank');
     });

     $(document).on('click', ".save_caso", function() {
         var nomc = $("#nom_caso").val();
         var desc = $("#des_caso").val();
         var func = $("#list_fun").val();

         if (nomc.length < 5) {
             M.toast({ html: "Nombre del Caso: Mínimo 5 caracteres." }, 5000);
         } else if (func == '') {
             M.toast({ html: "Funcionario: Debes seleccionar a un funcionario." }, 5000);
         } else if (desc.length < 10) {
             M.toast({ html: "Descripción del Caso: Mínimo 10 caracteres." }, 5000);
         } else {
             $(".save_caso").attr("disabled", true);
             $(".save_caso").html('Guardando <i class="fa fa-spinner fa-spin fa-fw"></i>');
             var formData = new FormData(document.getElementById("crear_caso"));
             formData.append("ida", user.id);
             formData.append("token", user.token);
             url = "casos";

             axios.post(dominio + "" + url, formData, progress__event($(".save_caso")))
                 .then(function(response) {
                     //console.log(response.data);
                     $(".save_caso").attr("disabled", false);
                     $(".save_caso").html('Publicar caso');
                     if (response.data.r) {
                         $("#estado").val(0).trigger("change");
                         //listcasos(pagina);
                         //ajaxCiudades($("#estado").val());
                         $('.lista_archivos').html('');
                         $("#modalCrearcaso").modal('close');
                         $("#crear_caso").trigger("reset");
                         $(".limpiar_form_2").val('').trigger("change");
                         M.toast({ html: response.data.msg }, 5000);
                     } else {
                         M.toast({ html: response.data.msg }, 5000);
                     }

                 })
                 .catch(function(error) {
                     $(".save_caso").attr("disabled", false);
                     $(".save_caso").html('Publicar caso');
                     console.log(error);

                 });
         }
     });

     $(document).on('click', ".save_edit_caso", function() {
         var nomc = $("#nom_caso_edit").val();
         var desc = $("#des_caso_edit").val();

         if (nomc.length < 5) {
             M.toast({ html: "Nombre del Caso: Mínimo 5 caracteres." }, 5000);
         } else if (desc.length < 10) {
             M.toast({ html: "Descripción del Caso: Mínimo 10 caracteres." }, 5000);
         } else {
             $(".save_edit_caso").attr("disabled", true);
             $(".save_edit_caso").html('Guardando <i class="fa fa-spinner fa-spin fa-fw"></i>');
             var formData = new FormData(document.getElementById("edit_caso"));
             formData.append("ida", user.id);
             formData.append("token", user.token);
             url = "casos/editar/";
             axios.post(dominio + "" + url, formData, progress__event($(".save_edit_caso")))
                 .then(function(response) {
                     $(".save_edit_caso").attr("disabled", false);
                     $(".save_edit_caso").html('Guardar');
                     if (response.data.r) {
                         listcasos(pagina);
                         $("#modalEditarcaso").modal('close');
                         M.toast({ html: response.data.msg }, 5000);
                     } else {
                         M.toast({ html: response.data.msg }, 5000);
                     }
                 })
                 .catch(function(error) {
                     $(".save_edit_caso").attr("disabled", false);
                     $(".save_edit_caso").html('Guardar');
                     console.log(error);

                 });
         }
     });

     var arc = 0;
     $(document).on('click', '.crear_archivo', function() {
         imprimir = this.id;
         $(".archivo_selec").each(function() {
             veri = $(this).val();
             idve = this.id;
             idve = idve.split("_")
             if (veri == '')
                 $("#divArchivo" + idve[1]).remove();
         });
         htmlarc = '<div class="row mb-1 divArchivo" id="divArchivo' + arc + '"><input type="hidden" name="arrayimagen[]" value="'+arc+'"><input type="file" name="imagen[]" id="arcnew_' + arc + '" class="hide archivo_selec">';
         htmlarc += '<div class="col s7 txt_arc_' + arc + '" style="overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"></div>';
         //htmlarc += '<div class="col s3 pri_arc_' + arc + '" style="display:none;overflow: hidden;"><select style="display:block" name="privaimagen[]" class="select-archivos"><option value="1" selected>Todo publico</option><option value="2">Solo abogados</option></select></div>';
         htmlarc += '<div class="col s4 pri_arc_' + arc + '" style="display:none;overflow: hidden;"><b style="font-size:10px;">Visible:</b><label><input type="radio" name="privaimagen[' + arc + ']" value="1" checked> <span class="span-arc">Todo Publico</span></label> <label><input type="radio" name="privaimagen[' + arc + ']" value="2"> <span class="span-arc">Abogados</span></label></div>';
         htmlarc += '<div class="col s1 btn_ele_arc_' + arc + '" style="display:none;"><a href="#" style="line-height: 1.5;" class="fa fa-times-circle eliminarArchivo right" id="' + arc + '"></a></div>';

         htmlarc += '</div>';
         $('.' + imprimir).append(htmlarc);
         
         $("#arcnew_" + arc).trigger("click");
         arc++;
     });

     $(document).on('change', '.archivo_selec', function() {
         valor = this.id;
         valor = valor.split("_");
         name = document.getElementById('arcnew_' + valor[1]).files[0].name;
         $(".txt_arc_" + valor[1]).html(name);
         $(".btn_ele_arc_" + valor[1]).show();
         $(".pri_arc_" + valor[1]).show();
     });

     //-----------------ELIMINAR DATOS----------------------------//
     $(document).on('click', '.delete_c', function() {
         var idv = this.id;
         var toastContent = '<span>¿Desea eliminar éste caso?</span><br><button class="btn-flat toast-action conf_si" id="' + idv + '">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

         M.toast({ html: toastContent }, 4000);
     });
     $(document).on('click', '.conf_si', function() {
         id = this.id
         var data = {
             ida: user.id,
             token: user.token
         };
         M.Toast.dismissAll();
         $.ajax({
             url: dominio + 'casos/delete/' + id,
             type: 'GET',
             data: data,
             success: function(data) {
                 if (data.msj) {
                     M.toast({ html: data.msj }, 10000);
                     listcasos(pagina);
                 } else {
                     M.toast({ html: "Error al eliminar." }, 10000);

                 }
             }
         })

     })

     $(document).on('click', '.delete_a', function() {
         var idv = this.id;
         var idc = $(this).attr('name');
         var toastContent = '<span>¿Desea eliminar éste archivo?</span><br><button class="btn-flat toast-action conf_si_a" id="' + idv + '" name="' + idc + '">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

         M.toast({ html: toastContent }, 4000);
     });
     $(document).on('click', '.conf_si_a', function() {
         id = this.id;
         idc = $(this).attr('name');
         var data = {
             ida: user.id,
             token: user.token
         };
         M.Toast.dismissAll();
         $.ajax({
             url: dominio + 'casos/delete_archivo/' + id,
             type: 'GET',
             data: data,
             success: function(data) {
                 if (data.msj) {
                     M.toast({ html: data.msj }, 10000);
                     $(".li_arc_" + id).remove();
                     ver_arc = $("#ul_arc").html();

                     if (ver_arc.trim() == '') {
                         $(".ver_archivo_" + idc).remove();
                         $("#modalArchivosadjuntados").modal("close");
                     }

                 } else {
                     M.toast({ html: "Error al eliminar." }, 10000);

                 }
             }
         })

     });

     //ACCIONES
     var Id_caso = null;
     var ultimoBT = null;
     $(document).on('click', ".BTUNIRSEMODAL", function() {
         if(user.est_usu == 1){
            window.location = "?op=perfil" + ((getParameterByName("viewcs"))?"&viewcs=" + getParameterByName("viewcs"):"");
            return;
         }
         if(user.correo_paypal == '' || user.correo_paypal == null){
            M.toast({ html: "Debes tener un correo paypal para poder unirte a un caso" }, 5000);
            return;
         }
         //modalUnirsecaso
         ultimoBT = this;
         Id_caso = this.id;

         $(".nom_caso").text(obtenerCaso(this.id).nom_caso);
         $(".DivR_U").addClass("hide");
         $(".DivUn").removeClass("hide");
         $("#des_pos").val("");

         $("#modalUnirsecaso").modal('open');
     });
     $(document).on('click', ".BTUSIPOST", function() {

         ultimoBT = this;
         Id_caso = this.id;

         $(".DivR_U").addClass("hide");
         $(".DivRetirarse").removeClass("hide");

         $(".nom_caso").text(obtenerCaso(this.id).nom_caso);

         $("#modalUnirsecaso").modal('open');
     });

     $(document).on('click', ".BTNOPOST", function() {
         if(user.est_usu == 1){
            window.location = "?op=perfil";
            return;
         }
         if(user.correo_paypal == '' || user.correo_paypal == null){
            M.toast({ html: "Debes tener un correo paypal para poder postularte a un caso" }, 5000);
            return;
         }
         ultimoBT = this;
         Id_caso = this.id;

         $(".div_p_n").addClass("hide");
         $(".DIVPostular").removeClass("hide");

         $(".nom_caso").text(obtenerCaso(this.id).nom_caso);
         $("#pre_pos").val("");
         $("#des_pos_2").val("");

         $("#modalPostularcaso").modal('open');
     });
     $(document).on('click', ".BTSIPOST", function() {

         ultimoBT = this;
         Id_caso = this.id;

         $(".div_p_n").addClass("hide");
         $(".DivnoPostular").removeClass("hide");

         $(".nom_caso").text(obtenerCaso(this.id).nom_caso);

         $("#modalPostularcaso").modal('open');
     });
     $(document).on('click', ".BTUNIRME", function() {
         $(".DivUnirse").removeClass("hide");
     });
     $(document).on('click', ".BTRETIRARMECASO", function() {
         $(".divRetiro").removeClass("hide");
     });
     $(document).on('click', ".BTRETIRARME", function() {
         $(".DivRetirarme").removeClass("hide");
     });

     function Quitarcoma(value, op) {
         if (value == "") {
             value = "0";
         }
         if (value) {
             for (var i = 0; i < value.split(",").length + 10; i++) {
                 value = value.replace(",", "");
             }
             //value = value.replace(",", ".");
         }
         if (value.length == "3" || value.length == "2") {
             if (value.split(".").length == 0)
                 value = "0." + "" + value;
             else
                 value = "0" + "" + value;
         }
         return parseFloat(value);
     }
     $(document).on('click', ".BTPOSTULARSE", function() {
         //modalPostularcaso
         var bt = this;
         var formData = new FormData();
         formData.append("id_usuario", user.id);
         formData.append("token", user.token);
         formData.append("car_pos", $("#car_pos").val().trim());
         formData.append("des_pos", $("#des_pos_2").val().trim());
         formData.append("pre_pos_1", Quitarcoma($("#pre_pos_1").val().trim()));
         formData.append("pre_pos_2", Quitarcoma($("#pre_pos_2").val().trim()));
         formData.append("pre_pos_3", Quitarcoma($("#pre_pos_3").val().trim()));
         formData.append("id_caso", Id_caso);

         url = "casos/postularse/agregar";
         $(bt).attr("disabled", true);
         $(bt).html('Enviando <i class="fa fa-spinner fa-spin fa-fw"></i>');
         $.ajax({
             url: dominio + "" + url,
             type: 'POST',
             cache: false,
             dataType: 'json',
             contentType: false,
             processData: false,
             data: formData,
             success: function(data) {

                 $(bt).attr("disabled", false);
                 $(bt).html('Postularse');
                 if (data.r) {
                     ultimoBT.classList.remove("BTNOPOST");
                     ultimoBT.classList.add("BTSIPOST");
                     ultimoBT.children[0].setAttribute("data-tooltip","Retirarse del caso");
                     ultimoBT.children[0].classList.add("fa-user-minus");
                     ultimoBT.children[0].classList.remove("fa-user-plus");
                     $(".div_p_n").addClass("hide");
                     $("#modalPostularcaso").modal('close');
                     M.toast({ html: data.msg }, 5000);
                     buscarUni_Pos();
                     $("#form_postular")[0].reset();
                     
                 } else {
                   
                     if (data.unido) {
                         $(".div_p_n").addClass("hide");
                         $(".DivnoPostular").removeClass("hide");
                     }
                     
                     M.toast({ html: data.msg }, 5000);
                 }

             },
             error: function(xhr, status, errorThrown) {
                 $(bt).attr("disabled", false);
                 $(bt).html('Postularse');
                 console.log(xhr);
             }
         });
     });
     $(document).on('click', ".BTUNIRSE", function() {
         //modalUnirsecaso
         var bt = this;
         var formData = new FormData();
         formData.append("id_usuario", user.id);
         formData.append("token", user.token);
         formData.append("des_pos", $("#des_pos").val().trim());
         formData.append("id_caso", Id_caso);

         url = "casos/unirse/agregar";
         $(bt).attr("disabled", true);
         $(bt).html('Enviando <i class="fa fa-spinner fa-spin fa-fw"></i>');
         $.ajax({
             url: dominio + "" + url,
             type: 'POST',
             cache: false,
             dataType: 'json',
             contentType: false,
             processData: false,
             data: formData,
             success: function(data) {

                 $(bt).attr("disabled", false);
                 $(bt).html('Unirse');
                 if (data.r) {
                     ultimoBT.classList.remove("BTUNIRSEMODAL");
                     ultimoBT.classList.add("BTUSIPOST");
                     ultimoBT.children[0].setAttribute("data-tooltip","Retirarse del caso");
                     ultimoBT.children[0].classList.add("fa-user-minus");
                     ultimoBT.children[0].classList.remove("fa-user-plus");
                     ultimoBT.nextElementSibling.setAttribute("postulado", data.id);
                     ultimoBT.nextElementSibling.classList.remove("hide");
                     $(".DivR_U").addClass("hide");
                     $("#modalUnirsecaso").modal('close');
                     M.toast({ html: data.msg }, 5000);
                     buscarUni_Pos(); 
                     setTimeout(function() {
                         abrirPago(data.id,Id_caso);
                     },  300);
                 } else {
                     
                      
                     if (data.unido) {
                         $(".DivR_U").addClass("hide");
                         $(".DivRetirarse").removeClass("hide");
                     }
                      
                     M.toast({ html: data.msg }, 5000);
                 }

             },
             error: function(xhr, status, errorThrown) {
                 $(bt).attr("disabled", false);
                 $(bt).html('Unirse');
                 console.log(xhr);
             }
         });
     });
    var id_postulado = false;
    var id_caso_pago = false;
    $(document).on('click', ".BTPAGAR", function() { 
            abrirPago($(this).attr("postulado"),this.id);
    });
    function abrirPago(id,caso){
        id_postulado = id;
        id_caso_pago = caso;
        $("#modalfondos").modal('open');
        $("#monto_fon").val(formato.precio(MontoMin));
        $(".nom_caso").text(obtenerCaso(caso).nom_caso);

    }
     $(document).on('click', ".BTUNIDOS", function() {
         //modalUnirsecaso
         var ul = $(".UnidosList");
         var bt = this;
         var Data = {};
         Data.id_usuario = user.id;
         Data.token = user.token;
         Data.id_caso = this.id;

         url = "casos/unirse/listado/" + Data.id_caso;
         $(bt).attr("disabled", true);
         $(bt).find("i").removeClass('fa-eye');
         $(bt).find("i").addClass('fa-spinner fa-spin fa-fw');
         ul.html("");
         $.ajax({
             url: dominio + "" + url,
             type: 'GET',
             data: Data,
             success: function(data) {

                 $(bt).attr("disabled", false);
                 $(bt).find("i").addClass('fa-eye');
                 $(bt).find("i").removeClass('fa-spinner fa-spin fa-fw');
                 var UL = "";
                 for (var i = 0; i < data.length; i++) {
                     var datos = data[i];

                     UL += `<li class="collection-item collection-casos avatar clr_black">
                                <a href="?op=listadodecasos&list=publicados&usuario=${datos.id_usuario}">
                                    <img src="${datos.imagen}" alt="" class="circle Imgunidos" width="80%">
                                    <h6 class="title clr_blue text-bold">${datos.nom_usu} ${datos.ape_usu}</h6>
                                </a>
                            </li>`;
                 };

                 if (data.length >= 1) {
                     ul.html(UL);
                     $('.Imgunidos').on('error', function() {
                         $(this).attr('src', imguserperfil);
                     });
                     $("#modalUsuariosunidos").modal('open');

                 } else {
                     M.toast({ html: "Ningún usuario se ha unido a este caso." }, 5000);
                 }

             },
             error: function(xhr, status, errorThrown) {
                 $(bt).attr("disabled", false);
                 $(bt).find("i").addClass('fa-eye');
                 $(bt).find("i").removeClass('fa-spinner fa-spin fa-fw');
                 console.log(xhr);
             }
         });
     });
     $(document).on('click', ".BTPOSTULADOS", function() {
         //modalUnirsecaso
         var ul = $(".PostuladosList");
         var bt = this;
         var Data = {};
         Data.id_usuario = user.id;
         Data.token = user.token;
         Data.id_caso = this.id;

         url = "casos/postularse/listado/" + Data.id_caso;
         $(bt).attr("disabled", true);
         $(bt).find("i").removeClass('fa-users');
         $(bt).find("i").addClass('fa-spinner fa-spin fa-fw');
         ul.html("");
         $.ajax({
             url: dominio + "" + url,
             type: 'GET',
             data: Data,
             success: function(data) {

                 $(bt).attr("disabled", false);
                 $(bt).find("i").addClass('fa-users');
                 $(bt).find("i").removeClass('fa-spinner fa-spin fa-fw');
                 var UL = "";
                 for (var i = 0; i < data.length; i++) {
                     var datos = data[i];

                     UL += `<li class="collection-item collection-casos avatar clr_black">
                                <a href="?op=listadodecasos&list=postulados&usuario=${datos.id_usuario}">
                                    <img src="${datos.imagen}" alt="" class="circle ImgunidosP" width="80%">
                                    <h6 class="title clr_blue text-bold">${datos.nom_usu} ${datos.ape_usu}</h6>
                                </a>
                            </li>`;
                 };

                 if (data.length >= 1) {
                     ul.html(UL);
                     $('.ImgunidosP').on('error', function() {
                         $(this).attr('src', imguserperfil);
                     });
                     $("#modalPostuladosunidos").modal('open');

                 } else {
                     M.toast({ html: "Ningún abogado se ha postulado a este caso." }, 5000);
                 }

             },
             error: function(xhr, status, errorThrown) {
                 $(bt).attr("disabled", false);
                 $(bt).find("i").addClass('fa-users');
                 $(bt).find("i").removeClass('fa-spinner fa-spin fa-fw');
                 console.log(xhr);
             }
         });
     });
     $(document).on('click', ".BTRETIRARSE", function() {
         //modalUnirsecaso
         var bt = this;
         var Data = {};
         Data.id_usuario = user.id;
         Data.token = user.token;
         Data.id_caso = Id_caso;

         url = "casos/unirse/eliminar/" + Id_caso;
         $(bt).attr("disabled", true);
         $(bt).html('Enviando <i class="fa fa-spinner fa-spin fa-fw"></i>');
         $.ajax({
             url: dominio + "" + url,
             type: 'GET',
             data: Data,
             success: function(data) {

                 $(bt).attr("disabled", false);
                 $(bt).html('Retirarse');
                 if (data.r) {
                     ultimoBT.classList.remove("BTUSIPOST");
                     ultimoBT.classList.add("BTUNIRSEMODAL");
                     ultimoBT.children[0].setAttribute("data-tooltip","Unirse al caso");
                     ultimoBT.children[0].classList.remove("fa-user-minus");
                     ultimoBT.children[0].classList.add("fa-user-plus");
                     ultimoBT.nextElementSibling.setAttribute("postulado", "");
                     ultimoBT.nextElementSibling.classList.add("hide");
                     $(".DivR_U").addClass("hide");
                     $("#modalUnirsecaso").modal('close');
                     M.toast({ html: data.msg }, 5000);
                     buscarUni_Pos(); 
                 } else {
                     M.toast({ html: data.msg }, 5000);
                 }

             },
             error: function(xhr, status, errorThrown) {
                 $(bt).attr("disabled", false);
                 $(bt).html('Retirarse');
                 console.log(xhr);
             }
         });
     });
     $(document).on('click', ".BTRETIRARSECASO", function() {
         //modalUnirsecaso
         var bt = this;
         var Data = {};
         Data.id_usuario = user.id;
         Data.token = user.token;
         Data.id_caso = Id_caso;

         url = "casos/postularse/eliminar/" + Id_caso;
         $(bt).attr("disabled", true);
         $(bt).html('Enviando <i class="fa fa-spinner fa-spin fa-fw"></i>');
         $.ajax({
             url: dominio + "" + url,
             type: 'GET',
             data: Data,
             success: function(data) {

                 $(bt).attr("disabled", false);
                 $(bt).html('Retirarse');
                 if (data.r) {
                     ultimoBT.classList.remove("BTSIPOST");
                     ultimoBT.classList.add("BTNOPOST");
                     ultimoBT.children[0].setAttribute("data-tooltip","Postularse al caso");
                     ultimoBT.children[0].classList.remove("fa-user-minus");
                     ultimoBT.children[0].classList.add("fa-user-plus");
                     $(".div_p_n").addClass("hide");
                     $("#modalPostularcaso").modal('close');
                     M.toast({ html: data.msg }, 5000);
                     buscarUni_Pos();
                     
                 } else {
                     M.toast({ html: data.msg }, 5000);
                 }

             },
             error: function(xhr, status, errorThrown) {
                 $(bt).attr("disabled", false);
                 $(bt).html('Retirarse');
                 console.log(xhr);
             }
         });
     });
  $(document).on('click', ".save_fondo", function() {
    var monto = parseFloat(Quitarcoma($("#monto_fon").val().trim()));
    if (!monto || monto<=0) {
         M.toast({ html: "Es necesario que ingreses un monto." }, 5000);
        return;
    }
    if (monto<MontoMin) {
         M.toast({ html: "El monto debe ser mayor o igual a: " + formato.precio(MontoMin) }, 5000);
        return;
    }
    if(!id_postulado) return;
     //modalUnirsecaso
         var bt = this;
         var formData = new FormData();
         formData.append("id_usuario", user.id);
         formData.append("token", user.token);
         formData.append("id_caso", id_caso_pago);
         formData.append("monto", monto);

         url = "transacciones/unirse/pagar";
         $(bt).attr("disabled", true);
         $(bt).html('Enviando <i class="fa fa-spinner fa-spin fa-fw"></i>');
         $.ajax({
             url: dominio + "" + url,
             type: 'POST',
             cache: false,
             dataType: 'json',
             contentType: false,
             processData: false,
             data: formData,
             success: function(data) {

                 $(bt).attr("disabled", false);
                 $(bt).html('Procesar');
                 if (data.r) {
                     paypalForm = {
                        cmd:'_xclick',
                        page_style:'primary',
                        no_shipping:'1',
                        business: "merlyprincipal-facilitator@gmail.com",
                        amount: monto,
                        item_name: "Recaudo para alacarcel.com - " + obtenerCaso(id_caso_pago).nom_caso + " - " + user.nom_usu,
                        item_number: "ID PAGO: " + data.id,
                        return: dominioweb+"?op=pagado",
                        cancel_return: dominioweb+"?op=cancelado",
                        notify_url:dominio+"transacciones/process_pay?id="+id_postulado,
                        rm:'2',
                        no_note:'1',
                        currency_code:'USD',
                        cn:'PP-BuyNowBF',
                        custom:'',
                        first_name:'',
                        last_name:'',
                        address1:'',
                        city:'N/A',
                        zip:'',
                        night_phone_a:'',
                        night_phone_b:'',
                        night_phone_c:'',
                        lc:'es',
                        country:'ES'
                    }
                     
                            var h = ((document.getElementById("publicados")) ? document.getElementById("publicados").offsetHeight : document.body.clientHeight);
                            if(h<200) {
                                h = 780;
                            }
                            URL_PAYPAL="https://www.sandbox.paypal.com/cgi-bin/webscr";
                            var ventana = open(URL_PAYPAL+"/?"+jQuery.param( paypalForm ), '_blank', 'toolbar=yes,scrollbars=yes,resizable=yes,left=60,location=no,toolbar=no,zoom=no,width=' + (document.body.clientWidth - 150) + ',height=' + (h-245));
                    $("#modalfondos").modal("close");
                    $("#monto_fon").val("");    
                 } else {
                    
                     M.toast({ html: data.msg }, 5000);
                 }

             },
             error: function(xhr, status, errorThrown) {
                 $(bt).attr("disabled", false);
                 $(bt).html('Procesar');
                 console.log(xhr);
             }
         });
    

  });
    
    $(document).on('click', '.btnComentarios', function() {
        idcaso = this.id;
        data = {
             ida: user.id,
             token: user.token
         }
         $("#divComentarios-"+idcaso).html(comencargando());
         $("#divComentarios-"+idcaso).removeClass("hide");
         $.ajax({
             url: dominio + 'casos/comentarios/'+idcaso,
             type: 'GET',
             data: data,
             success: function(data) {
                 if(data.length==0){
                    if(paneluser=='usuario')
                        M.toast({ html: "No hay comentarios registrados." }, 5000);
                    $("#divComentarios-"+idcaso).addClass("hide");
                    $("#divComentarios-"+idcaso).html('');
                    return;
                 }
                 comhtml = '';
                 for (var i = 0; i < data.length; i++) {
                     if(i>0)
                        comhtml += '<hr>';
                     comhtml += '<div class="row m-0">';
                        comhtml += '<div class="col s1">';
                            if(data[i].usuario.imagen!=null)
                                imgcomen=data[i].usuario.imagen;
                            else
                                imgcomen=imguserperfil;
                            comhtml += '<img src="' + imgcomen + '" class="img-user img-cover img-comen">';
                        comhtml += '</div>';
                        comhtml += '<div class="col s11"><h6 class="mt-1"><b>' + data[i].usuario.nom_usu + ' ' + data[i].usuario.ape_usu + '</b></h6><h6 class="m-0" style="word-break: break-word;width: 95% !important;">' + data[i].des_pos + '</h6></div>';
                     comhtml += '</div>';
                  }
                 $("#divComentarios-"+idcaso).html(comhtml);
                 $('.img-comen').on('error', function() {
                    $(this).attr('src', imguserperfil);
                 });
                 
             }

         })
    });

    //FUNCIONES PARA CREAR FUNCIONARIOS E INSTITUCIONES
    
    function listintituciones() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'instituciones',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_instituciones(data)
            }

        })
    }

    function actualizar_tabla_instituciones(data) {
        veri = 0;
        for (var i = 0; i < data.length; i++) {
            veri++;
            datos = data[i];
            $("#institucion").append('<option value="' + datos.id + '">' + datos.nom_ins + '</option>');
            if (veri == data.length) {
                $('#institucion').select2({
                    language: {
                        noResults: function() {
                            return "No hay resultado";
                        },
                        searching: function() {
                            return "Buscando..";
                        }
                    },
                     dropdownParent: $('#modalCrearFuncionario') 
                });
                $('.select2').css("width", "100%");
            }
        }

    }
    $(document).on('click', ".save-fun", function() {
        var formData = new FormData();
        formData.append("nom", $("#nom_fun").val());
        formData.append("ape", $("#ape_fun").val());
        formData.append("ced", $("#ced_fun").val());
        formData.append("car", $("#car_fun").val());
        formData.append("ins", $("#institucion").val());
        formData.append("id", '');
        formData.append("ida", user.id);
        formData.append("token", user.token);
        url = "funcionario";
        $(".save-fun").attr("disabled", true);
        $(".save-fun").html('Guardando <i class="fa fa-spinner fa-spin fa-fw"></i>');
        $.ajax({
            url: dominio + "" + url,
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                $(".save-fun").attr("disabled", false);
                $(".save-fun").html('Guardar');
                if (data.r) {
                    listfuncionarios(data.id);
                    $("#modalCrearFuncionario").modal('close');
                    //$('#list_fun').append('<option value="'+data.id+'" selected>'+data.nom_fun+' '+data.ape_fun+'</option>').trigger("change");
                    $("#nom_fun , #ape_fun , #ced_fun , #car_fun").val('');
                    $("#institucion").val('').trigger("change");
                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                $(".save-fun").attr("disabled", false);
                $(".save-fun").html('Guardar');
                console.log(xhr);
            }
        });
    });

    ajaxEstados2(2);
    function ajaxEstados2(id) {

        $.ajax({
            url: dominio + 'paises/estado/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected>Selecciona el estado</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }
                $("#est_i").html(html);
                $("#est_i").formSelect();
            }

        })
    }

    function ajaxCiudades2(id) {

        $.ajax({
            url: dominio + 'paises/ciudad/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected>Selecciona la ciudad</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }
                $("#ciu_i").html(html);
                $("#ciu_i").formSelect();
            }

        })
    }

    $("#est_i").on('change', function() {
        ajaxCiudades2(this.value);
    });

    $(document).on('click', ".save-inst", function() {
        var formData = new FormData();
        formData.append("nom_ins", $("#nom_ins").val());
        formData.append("des_ins", $("#des_ins").val());
        formData.append("id_pais", 2);
        formData.append("id_estado", $("#est_i").val());
        formData.append("id_ciudad", $("#ciu_i").val());
        formData.append("id", '');
        formData.append("ida", user.id);
        formData.append("token", user.token);
        url = "instituciones";
        $(".save-inst").attr("disabled", true);
        $(".save-inst").html('Agregando <i class="fa fa-spinner fa-spin fa-fw"></i>');
        $.ajax({
            url: dominio + "" + url,
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                $(".save-inst").attr("disabled", false);
                $(".save-inst").html('Agregar');
                if (data.r) {
                    $("#formInstituto").hide();
                    $('#institucion').append('<option value="'+data.id+'" selected>'+data.nom_ins+'</option>').trigger("change");
                    $("#nom_ins , #des_ins").val('');
                    $("#est_i").val('').trigger("change");
                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                $(".save-inst").attr("disabled", false);
                $(".save-inst").html('Agregar');
                console.log(xhr);
            }
        });
    });

    $(document).on('click', ".compart-link", function() {
        idcs = this.id;
        titcs = $(this).attr("tit");
        linkcompartir = dominioweb+"?op=listadodecasos&viewcs="+idcs;
        linkcompartirf = dominioweb+"?op=listadodecasos%26viewcs="+idcs;
        
        $("#copyurl").val(linkcompartir);
        $("#compartir-social-f").attr("onclick","popUp=window.open('https://www.facebook.com/sharer/sharer.php?u="+linkcompartirf+" ', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;");
        $("#compartir-social-t").attr("onclick","popUp=window.open('http://twitter.com/intent/tweet?text="+titcs+" en "+linkcompartirf+" ', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;");
        $("#compartir-social-l").attr("onclick","popUp=window.open('https://www.linkedin.com/sharing/share-offsite/?url="+linkcompartir+" ', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;");
        $("#modalCompartir").modal("open");
    });
    $(document).on('click', ".copy-text", function() {
        $("#copyurl").select();
        document.execCommand("copy");
        M.toast({ html: "Url copiada" }, 5000);
    });

    function listcasos_sinloguearse(op) {
         $("#publicados").html(divcargando());
         
         data = {
             op: op,
             id_caso: getParameterByName('viewcs')
         }

         $.ajax({
             url: dominio + "casos",
             type: 'GET',
             data: data,
             success: function(data) {
                //console.log(data);
                actualizar_lista_casos_sinloguearse(data, op)
             }

         });
    }
    function actualizar_lista_casos_sinloguearse(data, op) {
         lista = '';
         datac = data.casos;
         if (datac.length == 0) {
             
         }

         for (var i = 0; i < datac.length; i++) {

             datos = datac[i];
             if(datos.usuario.imagen!=null)
                imgcaso=datos.usuario.imagen;
             else
                imgcaso=imguserperfil;
            txtsts='';
             if(datos.sts_caso==1)
                txtsts='Caso en propuesta';
             else if(datos.sts_caso==2)
                txtsts='Caso en curso';
             else if(datos.sts_caso==3)
                txtsts='Caso terminado';
             else if(datos.sts_caso==4)
                txtsts='Caso cancelado';
             else if(datos.sts_caso==5)
                txtsts='En votación';

             lista += `<ul class="collection" style="background: transparent;">
                <li class="collection-item clr_black">
                    <div class="row mb-0">
                        <div class="col s4 center">
                            <a href="#!"><img src="${imgcaso}" class="img-user boxPerfil--img img-cover imgcasos"></a>
                            <h6 style="font-size:14px;"><b>${datos.usuario.nom_usu} ${datos.usuario.ape_usu}</b></h6>
                        </div>
                        <div class="col s8">
                            <h5><b>#${datos.id}</b> Caso: ${datos.nom_caso}</h5>
                        </div>
                    </div>
                </li>

                <li class="collection-item clr_black">
                    <div class="row mb-0">
                        <div class="col s4"><b>Descripción</b></div>
                        <div class="col s8 border-left">${datos.des_caso}</div>
                    </div>
                </li>

                <li class="collection-item clr_black">
                    <div class="row mb-0">
                        <div class="col s4"><b>Estatus</b></div>
                        <div class="col s8 border-left">${txtsts}</div>
                    </div>
                </li>

                <li class="collection-item clr_black">
                    <div class="row mb-0">
                        <div class="col s4"><b>Monto recaudado</b></div>
                        <div class="col s8 border-left MontoRe" id="${datos.id}">0 $</div>
                    </div>
                </li>

                <li class="collection-item clr_black">
                    <div class="row mb-0">
                        <div class="col s4"><b>Fecha de publicación</b></div>
                        <div class="col s8 border-left">${moment(datos.fec_reg_caso).format('DD-MM-YYYY')}</div>
                    </div>
                </li>

                <li class="collection-item text-center">
                    <a href="#!" class="btn btn-blue clr_white unirse-sin-loguearse">Unirse</a>
                </li>
            </ul>`;
         }

         $("#publicados").html('');
         $("#publicados-sinloguearse").html('');
         $("#publicados-sinloguearse").append(lista);
         $('.imgcasos').on('error', function() {
            $(this).attr('src', imguserperfil);
         });
         buscarmonto_sinloguearse();
         
         $("#modalCompartircaso").modal({dismissible: false});
         $('#modalCompartircaso').modal('open');
         
    }
    function buscarmonto_sinloguearse() {

         
         data = {
            casos: getParameterByName('viewcs')
         }
         $.ajax({
             url: dominio + 'casos/postulaciones-y-unidos/0',
             type: 'GET',
             data: data,
             success: function(data) {
                
                 var MontoRe = $(".MontoRe");

                 for (var i = 0; i < MontoRe.length; i++) {
                     var mon = MontoRe[i];
                    for (var j = 0; j < data[0].length; j++) {
                         var caso = data[0][j];
                         
                         if(mon) {
                            if (caso.id_caso == mon.id) {
                             mon.innerText = formato.precio(caso.recaudo) + " $";
                            }
                         }
                     };
                 };
                 
             },
             error: function(xhr, status, errorThrown) {
                console.log('error');
             }

         });
    }
    $(document).on('click', ".unirse-sin-loguearse", function() {
        window.sessionStorage.setItem("casocompartirview", getParameterByName('viewcs'));
        window.location = "?op=iniciarsesion";
    });

    $(document).on('click', ".abrir-pdf", function() {
        var idcp = this.id;
        M.toast({ html: "Abriendo pdf..." }, 2000);
        $(".abrir-pdf-"+idcp).find("i").attr('data-tooltip', 'Abriendo pdf...');
        $('.tooltipped').tooltip();
        $(".abrir-pdf-"+idcp).find("i").removeClass('fa-file-pdf');
        $(".abrir-pdf-"+idcp).find("i").addClass('fa-spinner fa-spin fa-fw');
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
             url: dominio + 'casos/buscar-caso-pdf/'+idcp,
             type: 'GET',
             data: data,
             success: function(data) {
                $(".abrir-pdf-"+idcp).find("i").removeClass('fa-spinner fa-spin fa-fw');
                $(".abrir-pdf-"+idcp).find("i").addClass('fa-file-pdf');
                $(".abrir-pdf-"+idcp).find("i").attr('data-tooltip', 'PDF con información del caso');
                $('.tooltipped').tooltip();
                //console.log(data);
                $("#casopdf").val(JSON.stringify(data));
                if(data.length>0) {
                    $("#form_pdf").submit();
                }
                
             },
             error: function(xhr, status, errorThrown) {
                console.log('error');
             }

         });
    });
 });