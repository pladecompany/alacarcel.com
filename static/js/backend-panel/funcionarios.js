$(document).ready(function() {
    listfuncionarios();
    listintituciones();

    institu = [];
    //institu[1]="111";
    //institu[2]="222";

    //funcion para quitar tabindex del modal y funcione el select2
    setTimeout(function() { document.getElementById("instituto").removeAttribute("tabindex"); }, 2000);

    function listintituciones() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'instituciones',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_instituciones(data)
            }

        })
    }

    function actualizar_tabla_instituciones(data) {
        veri = 0;
        for (var i = 0; i < data.length; i++) {
            veri++;
            datos = data[i];
            institu[datos.id] = datos.nom_ins;
            $("#institucion").append('<option value="' + datos.id + '">' + datos.nom_ins + '</option>');
            if (veri == data.length) {
                $('#institucion').select2({
                    language: {
                        noResults: function() {
                            return "No hay resultado";
                        },
                        searching: function() {
                            return "Buscando..";
                        }
                    },
                     dropdownParent: $('#instituto')
                });
                $('.select2').css("width", "100%");
            }
        }

    }

    function listfuncionarios() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'funcionario',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_funcionario(data)
            }

        })
    }

    function actualizar_tabla_funcionario(data) {
        var table = $('.funt').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            datos = data[i];

            var option = '<a class="btn btn-blue ver-funcionario" id="' + datos.id + '"><i class="fa fa-eye"></i></a> <a class="btn btn-blue edit-funcionario" id="' + datos.id + '"><i class="fa fa-edit"></i></a> <a class="btn btn-blue delete_f" id="' + datos.id + '"><i class="fa fa-trash"></i></a>';
            var row = [count += 1, "<textarea id='fun_s_" + datos.id + "' style='display:none;'>" + JSON.stringify(datos) + "</textarea>" + datos.ced_fun, datos.nom_fun, datos.ape_fun, datos.institucion.nom_ins, datos.car_fun, option]
            table.row.add(row).draw().node();
        }
    }


    $(document).on('click', ".edit-funcionario", function() {
        idf = $(this).attr("id");
        var obj = JSON.parse($("#fun_s_" + idf).val());
        $("#id_fun").val(obj.id);
        $("#ced_fun").val(obj.ced_fun);
        $("#nom_fun").val(obj.nom_fun);
        $("#ape_fun").val(obj.ape_fun);
        $("#cargo").val(obj.car_fun);
        $("#institucion").val(datos.id_institucion).trigger("change");
        $("#instituto").modal("open");
        //setTimeout(function() { document.getElementById("instituto").removeAttribute("tabindex"); }, 1000);
    });

    $(document).on('click', ".add_funcionario", function() {
        $(".limpiar_form").val("");
        $(".limpiar_form_2").val("").trigger("change");
        $("#instituto").modal("open");
        //setTimeout(function() { document.getElementById("instituto").removeAttribute("tabindex"); }, 1000);
    });
    $(document).on('click', ".ver-funcionario", function() {
        idf = $(this).attr("id");
        var obj = JSON.parse($("#fun_s_" + idf).val());
        $(".ced_fun").html(obj.ced_fun);
        $(".nom_fun").html(obj.nom_fun);
        $(".ape_fun").html(obj.ape_fun);
        $(".car_fun").html(obj.car_fun);
        $(".ins_fun").html(obj.institucion.nom_ins);
        $("#modalVer").modal("open");
    });

    $(document).on('click', ".save", function() {
        idf = $("#id_fun").val();
        var formData = new FormData();
        formData.append("nom", $("#nom_fun").val());
        formData.append("ape", $("#ape_fun").val());
        formData.append("ced", $("#ced_fun").val());
        formData.append("car", $("#cargo").val());
        formData.append("ins", $("#institucion").val());
        formData.append("id", idf);
        formData.append("ida", user.id);
        formData.append("token", user.token);
        if (idf != '')
            url = "funcionario/editar";
        else
            url = "funcionario";
        $.ajax({
            url: dominio + "" + url,
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                if (data.r) {
                    listfuncionarios();
                    $("#instituto").modal('close');
                    $(".limpiar_form").val('');
                    $(".limpiar_form_2").val('').trigger("change");
                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                console.log(xhr);
            }
        });
    });


    //-----------------ELIMINAR DATOS----------------------------//
    $(document).on('click', '.delete_f', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste registro?</span><br><button class="btn-flat toast-action conf_si" id="' + idv + '">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

        M.toast({ html: toastContent }, 4000);
    });
    $(document).on('click', '.conf_si', function() {
        id = this.id
        var data = {
            ida: user.id,
            token: user.token
        };
        M.Toast.dismissAll();
        $.ajax({
            url: dominio + 'funcionario/delete/' + id,
            type: 'GET',
            data: data,
            success: function(data) {
                if (data.msj) {
                    M.toast({ html: data.msj }, 10000);
                    listfuncionarios();
                } else {
                    M.toast({ html: "Error al eliminar." }, 10000);

                }
            }
        })

    })
});