$(document).ready(function() {
    $("#login_entrar").on('submit', function(e) {
        $('#btn_entrar').prop('disabled', true);
        $('#login1').html("<img src='../static/img/cargando.gif' style='vertical-align:middle;' width='15px'>");
        var data = {
            correo: $("#correo").val(),
            pass: $("#pass").val()
        };


        $.ajax({
            url: dominio + 'login/admin',
            type: 'POST',
            data: data,
            success: function(data) {
                console.log(data)
                if (data.auth == true) {
                    window.sessionStorage.setItem("user", JSON.stringify(data.user));
                    data = data.user
                    window.location = "../panel/";

                } else {
                    $('#login1').html("");
                    $('#btn_entrar').prop('disabled', false);
                }
            },
            error: function(err) {
                console.log(err)
                M.toast({ html: err.responseJSON.msg }, 10000);
                $('#login1').html("");
                $('#btn_entrar').prop('disabled', false);
            }
        })
        return false;
    });
});