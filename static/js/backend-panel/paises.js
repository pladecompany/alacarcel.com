var ida = false;
$(document).ready(function(){

    console.log("Modulo paises");
    ajaxPaises();
    var table = $('.paisess').DataTable({
        "language": {
            "url": "../static/lib/JTables/Spanish.json"
            }
        });

    function ajaxPaises(){
        /*data = {
            ida:user.id,
            token:user.token
        }*/
        $.ajax({
            url : dominio+'paises',
            type: 'GET',
            success:function(data){
                actualizar_tabla_paises(data)
            }

        })
    }

    function actualizar_tabla_paises(data){
        
        count = 0
        table.clear().draw();
        for(var i=0;i<data.length;i++){
            datos = data[i]
            estatus = `
            <div class="switch">
                <label>
                Inactivo
                <input ${datos.estatus==1 ? 'checked ' :''} type="checkbox" class="estatus" id="${datos.id}">
                <span class="lever"></span>
                Activo
                </label>
            </div>
            `
            var option = `
            <a href="" onclick="return false"class="btn btn-red delete" id="${datos.id}"><img src="../static/img/icons/trash.png"></a>
            `
            var row = [count+=1,datos.nombre,`<img src="../${datos.imagen}" width="80">`,estatus]
            table.row.add(row).draw().node();
        }
    }

    $('#img_pais').on('change',function(event){
        var output = document.getElementById('imagen');
        output.src = URL.createObjectURL(event.target.files[0]);
    })

    $('.save').on('click',function(){
        var nombre = $("#nom_pais").val()
        var imagen = document.getElementById('img_pais')
        var err = false;
        if(nombre.trim().length <3){
            err = "Nombre: Mínimo 3 carácteres."
            $('#nom_pais').focus()
        }
        else if(imagen.length<1){
            err = "Por favor seleccione una imagen."
        }
        if(err){
            M.toast({html:err},2000)
            return false;
        }
        var formData = new FormData()
        formData.append('nombre',nombre)
        if(imagen.files[0] != undefined){
            formData.append('imagen',imagen.files[0],imagen.files[0].name)
        }
        if(ida){
            url = dominio+'paises/edit'
            formData.append('id',ida)

        }else{
            url = dominio+'paises'
        }
        $.ajax({
            url : url,
            type: 'POST',
            data:formData,
            contentType: false,
            processData: false,
            success:function(data){
                if(!data.r){
                    M.toast({html:'País registrado con éxito.'},2000)
                    $("#nom_pais").val('')
                    $("#img_pais").val('')
                    $('#imagen').attr('src','')
                    $('#modalPais').modal('close')
                    ajaxPaises()
                }else{
                    M.toast({html:data.msg},2000)
                }
            }
        })
    })
    $(document).on('click','.ver',function(){
        var id = this.id;
        $.ajax({
            url : dominio+'paises/'+id,
            type: 'get',
            success:function(data){
                if(!data.r){
                   $('#nombre').html(data.nombre)
                   $('#img').attr('src',data.imagen)
                    $('#VerPais').modal('open');
                }else{
                    M.toast({html:data.msg},2000)
                }
            }
        })
    })
    $(document).on('click','.edit',function(){
        var id = this.id
        ida = id
        $('#title').html('EDITAR PAÍS')
        $('.save').html('Actualizar')
        $.ajax({
            url : dominio+'paises/'+id,
            type: 'get',
            success:function(data){
                if(!data.r){
                   $('#nom_pais').val(data.nombre)
                   $('#imagen').attr('src',data.imagen)
                    $('#modalPais').modal('open');
                }else{
                    M.toast({html:data.msg},2000)
                }
            }
        })
    })
    $('.new').on('click',function(){
        $('#modalPais').modal('open');
        $('#title').html('NUEVO PAÍS')
        $('.save').html('Guardar')
        $("#nom_pais").val('')
        $("#img_pais").val('')
        $('#imagen').attr('src','')
        ida = false;
    })
    $(document).on('click','.delete',function(){
        var idv = this.id;
        var toastContent = '<span>¿Está seguro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';
  
        M.toast({html:toastContent}, 4000);
    });
    $(document).on('click','.conf_si',function(){
        id = this.id
        M.Toast.dismissAll();
        
        $.ajax({
            url:dominio+'paises/delete/'+id,
            type:'GET',
            success:function(data){
                M.toast({html:data}, 2000);
                ajaxPaises();
            }
        })
    })
    $(document).on('change','.estatus',function(){
        val = $(this).prop('checked')
        if(val == true){
            estatus = 1;
        }else{
            estatus = 0;
        }
        data = {
            id:this.id,
            estatus:estatus,
        }
        $.ajax({
            url : dominio+'paises/edit',
            type: 'post',
            data:data,
            success:function(data){
                if(data.r ==  false){
                    M.toast({html:data.msg},2000)
                    setTimeout(function(){ 
                        location.href="?op=paises" 
                
                    }, 2000);
                }else{
                    M.toast({html:data.msj},2000)
                }
            }
        })
    })
});

