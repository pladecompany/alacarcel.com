$(document).ready(function() {

	$(document).on('click', ".buscar-caso", function() {
    idc = $("#id_del_caso").val();
    if(idc.trim()=='')
      M.toast({ html: "Ingresa la ID del caso." }, 5000);
    else{
      $(".buscar-caso").attr("disabled", true);
      $(".buscar-caso").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
      data = {
        ida: user.id,
        token: user.token,
        idc: idc
      }
      if(op=='vercasos'){
        history.pushState(null, null, window.location.search.split("&")[0]+"&idc="+idc); 
        idcaso=idc;
        $("#sts_caso_b").attr("disabled",true);
        $("#sts_caso_b").val('').trigger("change");
      }
      $.ajax({
        url: dominio + 'casos/buscar-caso',
        type: 'GET',
        data: data,
        success: function(data) {
          $(".buscar-caso").attr("disabled", false);
          $(".buscar-caso").html('<i class="fa fa-search"></i>');
          if(data.length==0){
            if(op=='inicio')
              M.toast({ html: "No existe un caso con esta ID" }, 5000);
            else if(op=='vercasos')
              $("#publicados").html(divnocaso());
          }else{
            if(op=='inicio' || !op)
              window.location = "?op=vercasos&idc="+idc;
            else if(op=='vercasos'){
              buscarlistcasos(idc);
            }
          }
        }

      });
    }
    
  });

  idcaso = getParameterByName('idc');
  if(idcaso)
    buscarlistcasos(idcaso);
  else if(op == 'vercasos')
    window.location = "?op=inicio";
  function divnocaso() {
    htmlnocaso = '<div class="row mt-2 text-center msj-casos">';
    htmlnocaso += '<div class="col s12">';
    htmlnocaso += '<div class="row m-0">';
    htmlnocaso += '<div class="col s12 p-2">';
    htmlnocaso += '<i class="fa fa-times-circle fa-4x clr_blue"></i>';
    htmlnocaso += '<h5>No se ha encontrado un caso con esta ID</h5>';
    htmlnocaso += '</div>';
    htmlnocaso += '</div>';
    htmlnocaso += '</div>';
    htmlnocaso += '</div>';
    return htmlnocaso;
  }
  function divcargando() {
         htmlcar = '';
         htmlcar += '<div class="row mt-3 divCaso">';
         htmlcar += '<div class="col s12">';
         htmlcar += '<div class="row">';
         htmlcar += '<div class="col s2 center p-2">';
         htmlcar += '<span class="cspan cspan1"></span>';
         htmlcar += '<span class="cspan cspan2"></span>';
         htmlcar += '<span class="cspan cspan2-2"></span>';
         htmlcar += '</div>';
         htmlcar += '<div class="col s10">';
         htmlcar += '<span class="cspan cspan3"></span>';
         htmlcar += '<span class="cspan cspan4"></span>';
         htmlcar += '<br>';
         htmlcar += '<span class="cspan cspan4"></span>';
         htmlcar += '<span class="cspan cspan5"></span>';
         htmlcar += '</div>';
         htmlcar += '</div>';
         htmlcar += '</div>';
         htmlcar += '</div>';
         return htmlcar;
     }

  function buscarlistcasos(op) {
    $("#publicados").html(divcargando());
    $("#id_del_caso").val(op);     
    data = {
      ida: user.id,
      token: user.token,
      op: 1,
      id_ciudad: 0,
      buscar: '',
      id_estado: 0,
      id_caso: op
    }
    url = "casos";
         
    $.ajax({
        url: dominio + url,
        type: 'GET',
        data: data,
        success: function(data) {
          //console.log(data);
          actualizar_lista_casos_buscar(data, op);
          Casos = data.casos;
        }

    });
  }

     function actualizar_lista_casos_buscar(data, op) {
         //console.log(data.casos)
         lista = '';
         
          IDCASOS = [];
          IDCASOSDOS = [];
         datac = data.casos;
         if (datac.length == 0) {
            $("#publicados").html(divnocaso());
            return;
         }

         for (var i = 0; i < datac.length; i++) {

             datos = datac[i];
             $("#sts_caso_b").attr("disabled",false);
             $("#sts_caso_b").val(datos.sts_caso).trigger("change");
             IDCASOSDOS.push(datos.id);
             lista += '<div class="row mt-3 divCaso">';
             lista += '<div class="col s12">';
             lista += '<div class="row mb-0">';
             lista += '<div class="col s3 m2 center p-2">';
             if(datos.usuario.imagen!=null)
                imgcaso=datos.usuario.imagen;
             else
                imgcaso='../static/img/user.png';
             lista += '<a href="?op=listadodecasos&list=publicados&usuario='+datos.id_usuario+'"><img src="' + imgcaso + '" class="img-user boxPerfil--img img-cover imgcasos"></a>';
             lista += '<h6 style="font-size:14px;"><b>' + datos.usuario.nom_usu + ' ' + datos.usuario.ape_usu + '</b></h6>';
             lista += '</div>';

             lista += '<div class="col s9 m10 p-1">';

             lista += '<span class="right opc_ciu pl-2 pr-2 text-uppercase"><i class="fa fa-flag"></i> ' + datos.funcionario.institucion.nombre_estado + ' - ' + datos.funcionario.institucion.nombre + ' </span>';
             txtsts='';
             if(datos.sts_caso==1)
                txtsts='Caso en propuesta';
             else if(datos.sts_caso==2)
                txtsts='Caso en curso';
             else if(datos.sts_caso==3)
                txtsts='Caso terminado';
             else if(datos.sts_caso==4)
                txtsts='Caso cancelado';
            else if(datos.sts_caso==5)
                txtsts='En votación';
             if(txtsts!='')
                lista += '<span class="right opc_sts pl-2 pr-2 text-uppercase"><i class="fa fa-certificate"></i> '+txtsts+' </span>';
             lista += '<div class="row mb-0"><div class="col s12 m9"><h5 class="m-1"><b class="tooltipped" data-position="bottom" data-tooltip="ID del Caso">#' + datos.id + ' </b>Caso: ' + datos.nom_caso + '</h5></div><div class="col s12 m3"><h6>Monto recaudado: <b id="' + datos.id + '" class="clr_blue MontoRe">0.00$</b></h6></div></div>';
             lista += '<p class="m-0 text-justify pr-2">' + datos.des_caso + '</p>';
             lista += '<h6> '+((datos.abogado)?' <i class="fa fa-user icon"></i> Representante: ' + datos.abogado.nom_usu + ' ' + datos.abogado.ape_usu + ' ':'')+' <i class="fa fa-user icon"></i> ' + datos.funcionario.nom_fun + ' ' + datos.funcionario.ape_fun + ' &nbsp;&nbsp;&nbsp;<i class="fa fa-building icon"></i> ' + datos.funcionario.institucion.nom_ins + ' &nbsp;&nbsp;&nbsp;';
             if (datos.archivos.length > 0) {
                lista += '<a style="cursor:pointer;" id="' + datos.id + '" class="clr_black ver_archivos ver_archivo_' + datos.id + '"><i class="fa fa-folder-open icon"></i> Archivos adjuntos&nbsp;&nbsp;</a>';
             } 

             lista += '<a style="cursor:pointer;" id="' + datos.id + '" class="clr_black btnComentarios"><i class="fa fa-comments icon"></i> '+datos.comentarios+' comentarios &nbsp;&nbsp;</a></h6>';   
             
             lista += '<button type="button" id="' + datos.id + '"  href="#!" class="btn-blue btn-flat btn-floating  waves-effect waves-light BTUNIDOS">';
             lista += '<i class="fa fa-eye tooltipped total_unidos" style="font-size:16px;" id="' + datos.id + '" data-position="bottom" data-tooltip="Unidos al caso <b>(0)</b>"></i>';
             lista += '</button> &nbsp;';

             lista += '<button type="button" id="' + datos.id + '" class="btn-blue btn-flat btn-floating  waves-effect waves-light BTPOSTULADOS">';
             lista += '<i class="fa fa-users tooltipped total_postulados" style="font-size:16px;" id="' + datos.id + '" data-position="bottom" data-tooltip="Postulados al caso <b>(0)</b>"></i>';
             lista += '</button> &nbsp;';

             lista += '<button type="button" id="' + datos.id + '"  href="#!" class="btn-blue btn-flat btn-floating  waves-effect waves-light DEVOLVER hide">';
             lista += '<i class="fab fa-paypal tooltipped" style="font-size:16px;" id="' + datos.id + '" data-position="bottom" data-tooltip="Devolver dinero."></i>';
             lista += '</button> &nbsp;';
             
             
            lista += '<button type="button" id="' + datos.id + '" class="btn-blue btn-flat btn-floating waves-effect waves-light abrir-pdf abrir-pdf-' + datos.id + '">';
            lista += '<i class="fa fa-file-pdf tooltipped" style="font-size:16px;" data-position="bottom" data-tooltip="PDF con información del caso"></i>';
            lista += '</button> &nbsp;';

             lista += '<br><small class="right"><i class="fa fa-clock"></i> ' + moment(datos.fec_reg_caso).toNow() + '</small>';
             lista += '<div id="divComentarios-' + datos.id + '" class="mt-3 divComentarios hide"></div>';
             lista += '</div>';
             lista += '</div>';
             lista += '</div>';
             lista += '</div>';
         }

         $("#publicados").html('');
         $("#publicados").append(lista);
         $(".btnComentarios").trigger("click");
         $('.tooltipped').tooltip();
         $('.imgcasos').on('error', function() {
             $(this).attr('src', '../static/img/user.png');
         });
         
         buscarUni_Pos();
     }
     function buscarUni_Pos() {

         $(".total_unidos").attr('data-tooltip', 'Unidos al caso <b>(cargando...)</b>');
         $(".total_postulados").attr('data-tooltip', 'Postulados al caso <b>(cargando...)</b>');
         $('.tooltipped').tooltip();
         data = {
             id_usuario: user.id,
             token: user.token,
             casos: IDCASOSDOS
         }
         $.ajax({
             url: dominio + 'casos/postulaciones-y-unidos/0',
             type: 'GET',
             data: data,
             success: function(data) {
                
                 $(".total_unidos").attr('data-tooltip', 'Unidos al caso <b>(0)</b>');
                 $(".total_postulados").attr('data-tooltip', 'Postulados al caso <b>(0)</b>');
                 var MontoRe = $(".MontoRe");
                 var buttons = $(".total_unidos");
                 var buttonsp = $(".total_postulados");
                 var buttonspay = $(".DEVOLVER");
                 for (var i = 0; i < buttons.length; i++) {
                     var b = buttons[i];
                     for (var j = 0; j < data[0].length; j++) {
                         var caso = data[0][j];
                         if (caso.id_caso == b.id) {
                             MontoRe[i].innerText = formato.precio(caso.recaudo) + " $";
                             b.setAttribute("data-tooltip", "Unidos al caso <b>(" + caso.todos + ")</b>");
                             if((parseFloat(caso.pagado)==parseFloat(caso.a_pagar) && parseFloat(caso.recaudo)-parseFloat(caso.pagado)>0) || parseFloat(caso.a_pagar)<=0){
                                buttonspay[i].classList.remove("hide");
                             }
                         }
                     };
                 };
                 for (var i = 0; i < buttonsp.length; i++) {
                     var b = buttonsp[i];
                     for (var j = 0; j < data[1].length; j++) {
                         var caso = data[1][j];
                         if (caso.id_caso == b.id) {
                             b.setAttribute("data-tooltip", "Postulados al caso <b>(" + caso.todos + ")</b>");
                         }
                     };
                 };
             },
             error: function(xhr, status, errorThrown) {
                 $(".total_unidos").attr('data-tooltip', 'Unidos al caso <b>(0)</b>');
                 $(".total_postulados").attr('data-tooltip', 'Postulados al caso <b>(0)</b>');
                 $('.tooltipped').tooltip();
             }

         });
     }

     $(document).on('click', '#save_sts_caso', function() {
         valor = $("#sts_caso_b").val();
         var data = {
            ida: user.id,
            token: user.token,
            sts: valor
         };
         $("#save_sts_caso").attr("disabled", true);
         $("#save_sts_caso").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
         $.ajax({
             url: dominio + 'casos/actualizar-sts/'+idcaso,
             type: 'GET',
             data: data,
             success: function(data) {
                 $("#save_sts_caso").attr("disabled", false);
                 $("#save_sts_caso").html('<i class="fa fa-check fa-fw"></i>');
                 if (data.r) {
                     M.toast({ html: data.msg }, 5000);
                     buscarlistcasos(idcaso);
                 } else {
                     M.toast({ html: "Error al actualizar estatus del caso." }, 5000);
                 }
             }
         })

     });
     var Upagar = [];
     var lastBT = null;
     $(document).on('click', ".DEVOLVER", function() {
         //modalUnirsecaso
         lastBT = this;
         var ul = $(".UnidosList");
         var bt = this;
         var Data = {};
         Data.id_usuario = user.id;
         Data.token = user.token;
         Data.id_caso = this.id;
         Upagar = [];
         url = "casos/unirse/listado/" + Data.id_caso;
         $(bt).attr("disabled", true);
         $(bt).find("i").removeClass('fab fa-paypal');
         $(bt).find("i").addClass('fa fa-spinner fa-spin fa-fw');
         ul.html("");
         $.ajax({
             url: dominio + "" + url,
             type: 'GET',
             data: Data,
             success: function(data) {

                 $(bt).attr("disabled", false);
                 $(bt).find("i").addClass('fab fa-paypal');
                 $(bt).find("i").removeClass('fa fa-spinner fa-spin fa-fw');
                 var UL = `
                            <li class="collection-item collection-casos avatar clr_black text-center">
                                Pagado al representante: <b>${formato.precio(data[0].pagado_abogado)} $</b>
                                | Sobrante: <b>${formato.precio((data[0].recaudo-data[0].pagado_abogado))} $</b> <br>
                                ${((data[0].a_pagar<=0)?"<span style='font-size:10px;color: #ce0c0cd4;'>Nota: el abogado no cobrará por representar el caso...</span>":"")}
                            </li>
                        `;
                 for (var i = 0; i < data.length; i++) {
                     var datos = data[i];
                     var porcentaje = parseFloat(datos.aporte/datos.recaudo);
                     var pago = parseFloat(porcentaje*(datos.recaudo-datos.pagado_abogado));
                     datos.pago = parseFloat(pago.toFixed(2));
                     
                     UL += `<li class="collection-item collection-casos avatar clr_black">
                                <div class="row">
                                    <div class="col s4 m4">
                                        <a href="?op=listadodecasos&list=publicados&usuario=${datos.id_usuario}">
                                            <img src="${datos.imagen}" alt="" class="circle Imgunidos" width="80%">
                                            <h6 class="title clr_blue text-bold">${datos.nom_usu} ${datos.ape_usu}</h6>
                                        </a>
                                    </div>
                                    <div class="col s3 m3">
                                        <h6 class="title clr_blue text-bold">${formato.precio(datos.aporte)} $ / ${formato.precio(datos.recaudo)} $</h6>
                                    </div>
                                    <div class="col s2 m2">
                                        <h5 class="title clr_blue text-bold">${formato.precio(datos.pago)} $</h5>
                                    </div>
                                    <div class="col s3 m3 ${((datos.correo_paypal && datos.pagado!=datos.pago)?'':'hide')}">
                                        <button type="button" class="btn btn-blue Enviarpago" id="${i}">Transferir</button> 
                                    </div>
                                    <div class="col s3 m3 ${((datos.pagado==datos.pago)?'':'hide')}">
                                        <h6 class="title clr_blue text-bold">${datos.estatus_pago}</h6>
                                    </div>
                                    <div class="col s3 m3 ${((!datos.correo_paypal && datos.pagado!=datos.pago)?'':'hide')}">
                                        No posee cuenta paypal.
                                    </div>
                                </div>
                            </li>`;
                 };
                  Upagar = data;
                 if (data.length >= 1) {
                     ul.html(UL);
                     $('.Imgunidos').on('error', function() {
                         $(this).attr('src', imguserperfil);
                     });
                     $("#modalUsuariosunidos").modal('open');

                 } else {
                     M.toast({ html: "Ningún usuario se ha unido a este caso." }, 5000);
                 }

             },
             error: function(xhr, status, errorThrown) {
                 $(bt).attr("disabled", false);
                 $(bt).find("i").addClass('fab fa-paypal');
                 $(bt).find("i").removeClass('fa fa-spinner fa-spin fa-fw');
                 console.log(xhr);
             }
         });
     });
    $(document).on('click', ".Enviarpago", function() {
        var bt = this;
        var Pago = Upagar[this.id];

         if(!Pago.pago || parseFloat(Pago.pago) <= parseFloat(0)){
            M.toast({ html: "El monto a pagar es necesario." }, 5000);
        }else{
           var formData = new FormData();
            formData.append("id_usuario", Pago.id_usuario);
            formData.append("id_caso", Pago.id_caso);
            formData.append("monto", Pago.pago);
            formData.append("ref", "");
            formData.append("ida", user.id);
            formData.append("token", user.token);
            url = "pago-pendiente";
            $(bt).attr("disabled", true); $(bt).html("Enviando..."); 
            $.ajax({
                url: dominio + "transacciones/" + url,
                type: 'POST',
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                data: formData,
                success: function(data) {
                    
                    if (data.r) {
                        $(lastBT).click();
                        M.toast({ html: data.msg }, 5000);
                    } else {
                        $(bt).attr("disabled", false); $(bt).html("Transferir"); 
                        M.toast({ html: data.msg }, 5000);
                    }

                },
                error: function(xhr, status, errorThrown) {
                    console.log(xhr);
                     $(bt).attr("disabled", false); $(bt).html("Transferir");
                }
            }); 
        }
        
    });
    $(document).on('click', ".generar-pdf-admin", function() {
        var idcp = $("#id_del_caso").val();
        if($("#id_del_caso").val()==''){
            M.toast({ html: "Ingresa la ID del caso." }, 5000);
            return;
        }
        $(".generar-pdf-admin").find("i").attr('data-tooltip', 'Buscando caso para generar pdf...');
        $('.tooltipped').tooltip();
        $(".generar-pdf-admin").find("i").removeClass('fa-file-pdf');
        $(".generar-pdf-admin").find("i").addClass('fa-spinner fa-spin fa-fw');
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
             url: dominio + 'casos/buscar-caso-pdf/'+idcp,
             type: 'GET',
             data: data,
             success: function(data) {
                $(".generar-pdf-admin").find("i").removeClass('fa-spinner fa-spin fa-fw');
                $(".generar-pdf-admin").find("i").addClass('fa-file-pdf');
                $(".generar-pdf-admin").find("i").attr('data-tooltip', 'PDF con información del caso');
                $('.tooltipped').tooltip();
                //console.log(data);
                if(data.length>0) {
                    $("#casopdf").val(JSON.stringify(data));
                    $("#form_pdf").submit();
                }else{
                    M.toast({ html: "No existe un caso con esta ID" }, 5000);
                }
                
             },
             error: function(xhr, status, errorThrown) {
                console.log('error');
             }

         });
    });
});
