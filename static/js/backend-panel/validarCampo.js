(function($) {
	$.fn.validCampo = function(cadena) {
		$(this).on({
			keypress : function(e){
				var key = e.which,
					keye = e.keyCode,
					tecla = String.fromCharCode(key).toLowerCase(),
					letras = cadena;
				if(letras.indexOf(tecla)==-1 && keye!=9&& (key==37 || keye!=37)&& (keye!=39 || key==39) && keye!=8 && (keye!=46 || key==46) || key==161){
					e.preventDefault();
				}
			}
		});
	};
	$(".text").validCampo("abcdefghijklmnñopqrstuvwxyz ");
	$(".text-space").validCampo("abcdefghijklmnñopqrstuvwxyz");
	$(".number").validCampo("1234567890");
	$(".tel").validCampo("1234567890+");
	$(".number-2").validCampo("1234567890.");
	$(".number-1").validCampo("1234567890-");
  	$(".text-number-").validCampo("1234567890abcdefghijklmnñopqrstuvwxyz");
  	$(".text-number-1").validCampo("1234567890abcdefghijklmnñopqrstuvwxyz- ");

})( jQuery );

formato = {

    formatear: function(num, op) {
        if (op == null || op == "undefined") {
            separador = ","; // separador para los miles
            sepDecimal = '.'; // separador para los decimales
        } else {
            separador = "-"; // separador para los miles
            sepDecimal = '.'; // separador para los decimales
        }
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + separador + '$2');
        }
        return this.simbol + splitLeft + splitRight;
    },
    precio: function(num, op, simbol) {
        this.simbol = simbol || '';

        if (num == null || !num) {
            num = 0;
        }
        var num = num.toString();
        num = num.replace(/,/g, ".");

        num = Math.round(num * 100) / 100;


        return this.formatear(num.toFixed(2), op).replace(/-/g, "");

    }
}