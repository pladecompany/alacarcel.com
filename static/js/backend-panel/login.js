$(document).ready(function() {
    function getSearchParameters() {
        var prmstr = window.location.search.substr(1);
        return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
    }

    function transformToAssocArray(prmstr) {
        var params = {};
        var prmarr = prmstr.split("&");
        for (var i = 0; i < prmarr.length; i++) {
            var tmparr = prmarr[i].split("=");
            params[tmparr[0]] = tmparr[1];
        }
        return params;
    }

    var params = getSearchParameters()
    if (window.localStorage.getItem('recuerdame')) {
        recuerdame = JSON.parse(atob(window.localStorage.getItem('recuerdame')));
        if (recuerdame != null) {
            $('#correo').val(recuerdame.correo)
            $('#pass').val(recuerdame.pass)
        }

    }
    $("#login_entrar").on('submit', function(e) {
        $('#btn_entrar').prop('disabled', true);
        $('#login1').html("<img src='static/img/cargando.gif' style='vertical-align:middle;' width='15px'>");
        var data = {
            correo: $("#correo").val(),
            pass: $("#pass").val(),
        };

        var recuerdame = $("input[name='recuerdame']:checked").val();
        if (recuerdame) {
            window.localStorage.setItem("recuerdame", btoa(JSON.stringify(data)));
        }


        $.ajax({
            url: dominio + 'login/',
            type: 'POST',
            data: data,
            success: function(data) {
                if (data.auth == true) {
                    window.sessionStorage.setItem("user", JSON.stringify(data.user));
                    data = data.user;
                    casocompartir = window.sessionStorage.getItem('casocompartirview');
                    if(!casocompartir){
                        window.location = "?op=listadodecasos";
                    }else{
                        if(data.est_usu!=1)
                            window.sessionStorage.setItem("casocompartirview", null);
                        window.location = "?op=listadodecasos&viewcs="+casocompartir;
                    }
                } else {
                    $('#login1').html("");
                    $('#btn_entrar').prop('disabled', false);
                }
            },
            error: function(err) {
                console.log(err)
                M.toast({ html: err.responseJSON.msg }, 10000);
                $('#login1').html("");
                $('#btn_entrar').prop('disabled', false);
            }
        })
        return false;
    });
});