$(document).ready(function(){
    $(".telefono").mask("+99 99999999999999999");
    //$(".boxPerfil").find("input, select, .saveB").attr("disabled",true)
    perfil()
    function perfil(){
        var user = JSON.parse(window.sessionStorage.getItem('user'));
        $('#nombre').val(user.nombre)
        $('#apellido').val(user.apellido)
        $('#correo').val(user.correo)
        $('#telefono').val(user.telefono)
        $('#sexo').val(user.sexo)
        if(user.imagen != null){
            $('#imagen').attr('src',user.imagen)
            $('#img-perfil').attr('src',user.imagen)
        }
        $('#imagen').on('error',function(){
            $(this).attr('src','../static/img/user.png')
        });
        $('#img-perfil').on('error',function(){
            $(this).attr('src','../static/img/user.png')
        });
    }
    /*$('.BTRESET').on('click',function(event){
        $(".boxPerfil").find("input, select, .saveB").attr("disabled",false)
    })*/
    $('#img_usu').on('change',function(event){
        var output = document.getElementById('imagen');
        output.src = URL.createObjectURL(event.target.files[0]);
    })

    $('#update_perfil').on('submit',function(e){
        e.preventDefault();
        nombre = $('#nombre').val()
        apellido = $('#apellido').val()
        correo = $('#correo').val()
        telefono = $('#telefono').val()
        sexo = $('#sexo').val()
        if (nombre.length<3){
            M.toast({html:'Por favor ingresa un nombre.'})
            return false;
        }
        else if(apellido.length<3){
            M.toast({html:'Por favor ingresa un apellido.'})
            return false;
        }
        else if(telefono.replace(/ /g, "").length < 8 /*|| telefono.replace(/ /g, "").length >15*/){
            M.toast({html:'Télefono: Fórmato inválido.'})
            return false;
        }
        var array = nombre.split("");
        nombre = array[0].toLocaleUpperCase()+nombre.substr(1).toLowerCase();
        var data = new FormData(this);
        data.append("nombre", nombre);
        data.append("id", user.id);
        $.ajax({
          url: dominio + "admin/edit/",
          type: "POST",
          data: data,
          contentType: false,
          processData: false,
        success: function(data){
          M.toast({html:data.msj}, 10000);
          if (data.msj == "Perfil actualizado correctamente") {
              window.sessionStorage.setItem("user", JSON.stringify(data));
              perfil()
              setTimeout(function(){

                location.href = "?op=inicio";

              },2500);
          }
        },
        error: function(xhr, status, errorThrown){
            //M.toast({html:err.data.msj}, 10000);
          console.log("Errr : ");
          
        }
        });
    })
});
