var limit = 5;
$(document).ready(function() {
    var CookieArray = [];
    obtener_mis_notis = function obtener_mis_notis() {
        var html = '';
        //OBTENER LOS MODULOS QUE PERTENECE EL USUARIO
        console.log(user.rol)
        if (user.rol == 'Abogado' || user.rol == 'Usuario') {
            var url = dominio + 'notificaciones/';
            var tipe = 'POST'
        } else {
            var url = dominio + 'notificaciones/admin';
            var tipe = 'GET'
        }
        $.ajax({
            url: url,
            type: tipe,
            data: {
                id: user.id,
                limit: limit
            },
            success: function(data) {
                $(".view_more").html('<div onclick="$(this).html(\'<center>Cargando...</center>\'); obtener_mis_notis(); " class="view_more"><center>Cargar más...</center></div>');

                if (data.length > 0 && data.length < 5 || data.length == 0)
                    $(".view_more").addClass("hide");

                for (i = data.length - 1; i >= 0; i--) {
                    CargarNoti(data[i]);
                }

                if (limit == 5)
                    $(".misNotificaciones").scrollTop($('.misNotificaciones')[0].scrollHeight);

                if (data.length == 0)
                    $(".misNotificaciones").html(sin_resultados());

                limit = (limit * 2);
            },
            error: function(err) {
                $(".view_more").addClass("hide");
                $(".misNotificaciones").html(sin_resultados());
            }
        })
    }



    obtener_mis_notis();

    buscarID = function buscarID(id) {

        for (var i = 0; i < CookieArray.length; i++) {
            if (CookieArray[i].id == id)
                return true;
        }
        return false;
    }
    CargarNoti = function CargarNoti(data) {
        moment.locale("es");
        if (buscarID(data.id)) {
            return false;
        }
        CookieArray.push(data);
        var html = '';
        viewsts = 'no-vist';
        if (data.est_noti == 1)
            viewsts = '';
        html += ' <a class="anclaCard vernotificacion" noti="' + data.id + '" usable="' + data.id_usable + '" tipo="' + data.tipo_noti + '" href="#!">';
        html += '     <div class="card-notifi card noti-' + data.id + ' ' + viewsts + '">';
        html += '<i style="margin:0.5rem;" class="fa right"></i>';
        html += '         <div class="row m-0">';
        html += '             <div class="col s12">';
        html += '                 <h6 class="text-bold m-0">' + data.descripcion + '</h6>';
        
        html += '                 <p class="m-0">';
        html += data.contenido;

        html += '                 </p>';

        if(data.tipo_noti==0) html += " <b style='color:#039be5;'>Ver el perfil del usuario</b>";
        if(data.tipo_noti==1) html += " <b style='color:#039be5;'>Ver el perfil del abogado</b>";
        if(data.tipo_noti==2) html += " <b style='color:#039be5;'>Ver detalle del caso</b>";
         

        
        html += '                 <div class="text-right mb-2">';
        
        html += '                   <i class="fa fa-clock"></i> ';
       
        html += '                   <span class="timeago" > ' + moment(data.fecha).toNow() + '</span>';
        html += '                 </div>';
        html += '             </div>';
        html += '         </div>';
        html += '     </div>';
        html += ' </a>';

        $(".misNotificaciones").prepend(html);
    }

    function sin_resultados() {
        var html = '';
        html += '     <div class="card card-notifi">';
        html += '         <div class="row m-0">';
        html += '             <div class="col s12">';
        html += '                 <p class="center">No se encontraron resultados</p>';
        html += '             </div>';
        html += '         </div>';
        html += '     </div>';
        return html;
    }
    var notificacionId = null;
    
    $(document).on('click', '.BTvistanoti', function() { 
        var div = $(this); 
        var id = $(this).attr("noti");
        var i = $(this).find('i');
        if (user.rol == 'Abogado' || user.rol == 'Usuario') {
            $(i).html("<img src='static/img/cargando.gif' style='vertical-align:middle;' width='15px'>");
        } else {
            $(i).html("<img src='../static/img/cargando.gif' style='vertical-align:middle;' width='15px'>");
        }
        $.ajax({
            url: dominio + 'notificaciones/vista/' + id,
            type: 'post',
            success: function(data) {
                $(".noti-" + id).parent("a").remove();
                $(i).html('');
                div.remove();
                CargarNotiAcoun();
                
            },
            error: function(err) {
                $(i).removeClass('');
            }
        })
    });
    $(document).on('click', '.vernotificacion', function() {
        var div = $(this);
        var tipo = $(this).attr("tipo");
        var usable = $(this).attr("usable");
        var id = $(this).attr("noti");
        notificacionId = id;
        var i = $(this).find('i');

        if(tipo==4){
             buscarCaso(usable, tipo);
            return;
        }

        if(tipo==5){
             buscarCaso(usable, tipo);
            return;
        }

        if (user.rol == 'Abogado' || user.rol == 'Usuario') {
            $(i).html("<img src='static/img/cargando.gif' style='vertical-align:middle;' width='15px'>");
        } else {
            $(i).html("<img src='../static/img/cargando.gif' style='vertical-align:middle;' width='15px'>");
        }

        $.ajax({
            url: dominio + 'notificaciones/vista/' + id,
            type: 'post',
            success: function(data) {
                $(".noti-" + id).removeClass('no-vist');
                $(i).html('');
                div.remove();
                CargarNotiAcoun();
                if(tipo==0){
                    location.href = "?op=listadodecasos&list=publicados&usuario="+usable;
                }else if(tipo==1){
                    location.href = "?op=listadodecasos&list=postulados&usuario="+usable;
                }else if(tipo==2){
                    buscarCaso(usable);
                }else if(tipo==3){
                    location.href = "?op=pagosrealizados&pago="+usable;
                }
                
            },
            error: function(err) {
                $(i).removeClass('');
            }
        })

    });
    function divcargando() {
         htmlcar = '';
         htmlcar += '<div class="row mt-3" style="background: #fff;border: 1px solid #ccc;border-radius: .5rem;">';
         htmlcar += '<div class="col s12">';
         htmlcar += '<div class="row">';
         htmlcar += '<div class="col s2 center p-2">';
         htmlcar += '<span class="cspan cspan1"></span>';
         htmlcar += '<span class="cspan cspan2"></span>';
         htmlcar += '<span class="cspan cspan2-2"></span>';
         htmlcar += '</div>';
         htmlcar += '<div class="col s10">';
         htmlcar += '<span class="cspan cspan3"></span>';
         htmlcar += '<span class="cspan cspan4"></span>';
         htmlcar += '<br>';
         htmlcar += '<span class="cspan cspan4"></span>';
         htmlcar += '<span class="cspan cspan5"></span>';
         htmlcar += '</div>';
         htmlcar += '</div>';
         htmlcar += '</div>';
         htmlcar += '</div>';
         return htmlcar;
     }
     function divcargandod() {
         htmlcar = '<li class="collection-item collection-casos avatar clr_black">';
        htmlcar += '<div class="row m-0">';
            htmlcar += '<div class="col s1">';
                htmlcar += '<span class="cspan cspan1-c"></span>';
            htmlcar += '</div>';
            htmlcar += '<div class="col s11"><span class="cspan cspan4"></span><span class="cspan cspan5"></span></div>';
        htmlcar += '</div>';
        htmlcar += '</li>';
        return htmlcar;
     }
    function buscarCaso(usable, tipo){
        $(".DivCuerpo").html(divcargando());
        $("#modalCaso").removeClass("modalCasoP");
        $("#modalCaso").modal("open");
        $(".PostuladosList").html("");
        $(".PostuladosList").addClass("hide");

        
         data = {
             ida: user.id,
             token: user.token,
             usable: usable 
         }

         url = "casos/caso-obtener";
        
         $.ajax({
             url: dominio + url,
             type: 'GET',
             data: data,
             success: function(data) {
                datac = data;
                lista = '';
                 
                 for (var i = 0; i < datac.length; i++) {
                             datos = datac[i];
                             lista += '<div class="row mt-3" style="background: #fff;border: 1px solid #ccc;border-radius: .5rem;">';
                             lista += '<div class="col s12">';
                             lista += '<div class="row mb-0">';
                             lista += '<div class="col s2 center p-2">';
                             lista += '<a href="?op=listadodecasos&list=publicados&usuario='+datos.id_usuario+'"><img src="' + datos.usuario.imagen + '" class="img-user boxPerfil--img img-cover imgcasos"></a>';
                             lista += '<h6><b>' + datos.usuario.nom_usu + ' ' + datos.usuario.ape_usu + '</b></h6>';
                             lista += '</div>';

                             lista += '<div class="col s10 p-1">';
                             lista += ' <span class="right opc_ciu pl-2 pr-2 text-uppercase"><i class="fa fa-flag"></i> ' + datos.funcionario.institucion.nombre_estado + ' - ' + datos.funcionario.institucion.nombre + ' </span>';
                             
                              var txtsts='';
                             if(datos.sts_caso==1)
                                txtsts='Caso en propuesta';
                             else if(datos.sts_caso==2)
                                txtsts='Caso en curso';
                             else if(datos.sts_caso==3)
                                txtsts='Caso terminado';
                             else if(datos.sts_caso==4)
                                txtsts='Caso cancelado';
                            else if(datos.sts_caso==5)
                             txtsts='En votación';
                             if(txtsts!='')
                             lista += '<span class="right opc_sts pl-2 pr-2 text-uppercase"><i class="fa fa-certificate"></i> '+txtsts+' </span>';
                             lista += '<span style="background:#ccc !important; cursor:pointer;" noti="'+notificacionId+'" class="hide right opc_sts pl-2 pr-2 text-uppercase BTvistanoti"><i class="fa fa-clock"></i> Marcar como vista</span>';
                             lista += '<h5>Caso: ' + datos.nom_caso + '</h5>';
                             lista += '<p>' + datos.des_caso + '</p>';
                             lista += '<h6> '+((datos.abogado)?' <i class="fa fa-user icon"></i> Representante: ' + datos.abogado.nom_usu + ' ' + datos.abogado.ape_usu + ' ':'')+' <i class="fa fa-user"></i> ' + datos.funcionario.nom_fun + ' ' + datos.funcionario.ape_fun + ' &nbsp;&nbsp;&nbsp;<i class="fa fa-building"></i> ' + datos.funcionario.institucion.nom_ins + ' &nbsp;&nbsp;&nbsp;';
                             if (datos.archivos.length > 0) {
                                 lista += '<a href="#" id="' + datos.id + '" class="clr_black ver_archivos ver_archivo_' + datos.id + '"><i class="fa fa-folder-open"></i> Archivos adjuntos</a></h6>';
                             } else
                                 lista += '</h6>';

                             lista += '<br><small class="right"><i class="fa fa-clock"></i> ' + moment(datos.fec_reg_caso).toNow() + '</small>';
                             lista += '</div>';
                             lista += '</div>';
                             lista += '</div>';
                             lista += '</div>';
                         }

         
                 $(".DivCuerpo").html('');
                 $(".DivCuerpo").append(lista);
                 $('.tooltipped').tooltip();
                 $('.imgcasos').on('error', function() {
                     $(this).attr('src', 'static/img/user.png');
                 });
                 if(tipo==4 && datac.length>0){
                    if(datac[0].sts_caso==5){
                        ModalPostulado(datac[0]);
                    } else {
                        $(".BTvistanoti").removeClass("hide");
                    }
                 }else if(tipo==5 && datac.length>0){
                    if(datac[0].sts_caso==5 && !datac[0].id_abogado){
                        FormularioPregunta(datac[0]);
                    } else {
                        $(".BTvistanoti").removeClass("hide");
                    }
                 } else {
                        $(".BTvistanoti").removeClass("hide");
                 }
             },
             error: function(xhr, status, errorThrown){
               $(".DivCuerpo").html('');
               $("#modalCaso").modal("close");
             }

         });
    }
     if(paneluser=='usuario')
        imguserperfil='static/img/user.png';
     else
        imguserperfil='../static/img/user.png';

    var votado = null;
    var caso_votado = null;
    var Casoobj = null;
    function FormularioPregunta(caso){
           Casoobj = caso;
           var ul = $(".PostuladosList");
            ul.removeClass("hide");
            
                         
         var Data = {};
         Data.id_usuario = user.id;
         Data.token = user.token;
         Data.id_caso = caso.id;

         url = "casos/postularse/listado-votacion/" + Data.id_caso;
          
         ul.html(divcargandod());
         ul.removeClass("hide");
          
         $.ajax({
             url: dominio + "" + url,
             type: 'GET',
             data: Data,
             success: function(data) {
                      //console.log(data);
                      var UL = `
                        <li class="collection-item collection-casos avatar clr_black Divradios">
                                <div class="row">
                                <h4 class="text-center clr_blue text-bold">Quieres representar este caso?</h4>
                                    <div class="col s12">
                                        <button type="button" id="${caso.id}" style="margin:0.5rem;" class="right btn btn-red BTcancel ">Rechazar</button>
                                        <button type="button" id="${caso.id}" style="margin:0.5rem;" class="right btn btn-blue BTaccept">Aceptar</button>
                                    </div>
                                </div>
                            </li>
                         `;
                         for (var i = 0; i < data.length; i++) {
                          var datos = data[i];
                          
                              if(datos.id_usuario==user.id) {
                                ul.html(UL);
                                ul.removeClass("hide"); break;
                              }else {
                                 ul.addClass("hide");
                              }
                         }
                         if(data.length<=0){
                            ul.addClass("hide");
                         }

             },
             error: function(xhr, status, errorThrown) {
                  ul.html("");
                 console.log(xhr);
             }
             });  

    }
    function ModalPostulado(caso) {
        Casoobj = caso;
        $("#modalCaso").addClass("modalCasoP");
        var ul = $(".PostuladosList");
         votado = null;
         caso_votado = null;
         var Data = {};
         Data.id_usuario = user.id;
         Data.token = user.token;
         Data.id_caso = caso.id;

         url = "casos/postularse/listado-votacion/" + Data.id_caso;
          
         ul.html(divcargandod()+divcargandod()+divcargandod()+divcargandod());
         ul.removeClass("hide");
          
         $.ajax({
             url: dominio + "" + url,
             type: 'GET',
             data: Data,
             success: function(data) {

                  
                    var UL = `
                            <li class="collection-item collection-casos avatar clr_black">
                                <div class="row">
                                <h4 class="text-center clr_blue text-bold">Postulados</h4>
                                    <div class="col s12 ${((caso.id_abogado)?'hide':'')} Divradios">
                                        <button type="button" class="right btn btn-blue BTvotar">Votar</button>
                                    </div>
                                </div>
                            </li>
                         `;
                         ul.html(UL);
                    UL = "";
                 for (var i = 0; i < data.length; i++) {

                     var datos = data[i];
                     if(i==0 && (caso.id_abogado || datos.yavote>0)){
                            $(".Divradios").remove();
                     }
                     UL += `<li class="collection-item collection-casos avatar clr_black" style=" ${((caso.id_abogado==datos.id_usuario)?'border: 0.1rem solid':'')}">
                                <div class="row">
                                    <div class="col s9">
                                        <a href="?op=listadodecasos&list=postulados&usuario=${datos.id_usuario}">
                                            <img src="${datos.imagen}" alt="" class="circle ImgunidosP" width="80%">
                                            <h6 class="title clr_blue text-bold">${datos.nom_usu} ${datos.ape_usu}</h6>
                                        </a>
                                    </div>
                                    <div class="col s3 ${((caso.id_abogado || datos.yavote>0)?'hide':'')} Divradios">
                                        <label title="Elegir">
                                            <input name="group1" type="radio" id="${caso.id}" value="${datos.id_usuario}" class="BTRadio" />
                                            <span style="position: relative;top: 0.7rem;"></span>
                                        </label>
                                    </div>
                                    <div class="col s3 ${((datos.mielegido==datos.id_usuario && datos.yavote>0)?'':'hide')}">
                                           <b class="right opc_sts pl-2 pr-2 text-uppercase">Mi elegido</b>
                                    </div>
                                    
                                </div>
                            </li>`;
                 };

                 if (data.length >= 1) {
                     ul.append(UL);
                     ul.removeClass("hide");
                     $('.ImgunidosP').on('error', function() {
                         $(this).attr('src', imguserperfil);
                     });
                      

                 } else {
                     ul.html("");
                     M.toast({ html: "Ningún abogado se ha postulado a este caso." }, 5000);
                 }

             },
             error: function(xhr, status, errorThrown) {
                  ul.html("");
                 console.log(xhr);
             }
         });
    }

    $(document).on('change', ".BTRadio", function() { 
        votado = this.value;
        caso_votado = this.id;
    });
    
    $(document).on('click', ".BTvotar", function() {
        
        if(!votado) {
             M.toast({ html: "No olvide elegir un abogado..." }, 5000);
             return;
        }
        if(!caso_votado) {
            return;
        }
        var formData = new FormData();
        formData.append("id_abogado", votado);
        formData.append("id_usuario", user.id);
        formData.append("token", user.token);
        formData.append("id_caso", caso_votado);
        

        url = "casos/votar";
        $(".BTvotar").attr("disabled", true);
        $(".BTvotar").html('Enviando <i class="fa fa-spinner fa-spin fa-fw"></i>');
        $.ajax({
            url: dominio + "" + url,
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                $(".BTvotar").attr("disabled", true);
                $(".BTvotar").html('Votar');
                if (data.r) {
                    ModalPostulado(Casoobj);
                    votado = null;
                    $(".Divradios").remove();
                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                }

            },
            error: function(xhr, status, errorThrown) {
                $(".BTvotar").attr("disabled", false);
                $(".BTvotar").html('Votar');
                console.log(xhr);
            }
        });
    });

    $(document).on('click', ".BTaccept", function() {
        
        if(!this.id) {
             M.toast({ html: "No olvide elegir un caso..." }, 5000);
             return;
        }
         
        var formData = new FormData();
        formData.append("id_usuario", user.id);
        formData.append("token", user.token);
        formData.append("id_caso", this.id);
        var cas = this.id;

        url = "casos/abogado-aceptar-caso";
        $(".BTaccept").attr("disabled", true);
        $(".BTaccept").html('Enviando <i class="fa fa-spinner fa-spin fa-fw"></i>');
        $.ajax({
            url: dominio + "" + url,
            type: 'POST',
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                 
                
                if (data.r) {
                    
                    buscarCaso(cas, 5);
                     
                    $(".Divradios").remove();
                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                    $(".BTaccept").attr("disabled", false);
                    $(".BTaccept").html('Aceptar');
                }

            },
            error: function(xhr, status, errorThrown) {
                $(".BTaccept").attr("disabled", false);
                $(".BTaccept").html('Aceptar');
                console.log(xhr);
            }
        });
    });

    $(document).on('click', ".BTcancel", function() {
        
        if(!this.id) {
             M.toast({ html: "No olvide elegir un caso..." }, 5000);
             return;
        }
         var cas = this.id;
        var formData = new FormData();
        formData.append("id_usuario", user.id);
        formData.append("token", user.token);
        formData.append("id_caso", cas);
        

        url = "casos/abogado-rechazar-caso";
        $(".BTcancel").attr("disabled", true);
        $(".BTcancel").html('Enviando <i class="fa fa-spinner fa-spin fa-fw"></i>');
        $.ajax({
             url: dominio + "" + url,
             type: 'POST',
             cache: false,
             dataType: 'json',
             contentType: false,
             processData: false,
             data: formData,
             success: function(data) {
                 
                
                if (data.r) {
                     
                    buscarCaso(cas, 5);
                     
                    $(".Divradios").remove();
                    M.toast({ html: data.msg }, 5000);
                } else {
                    M.toast({ html: data.msg }, 5000);
                    $(".BTcancel").attr("disabled", false);
                    $(".BTcancel").html('Rechazar');
                }

            },
            error: function(xhr, status, errorThrown) {
                $(".BTcancel").attr("disabled", false);
                $(".BTcancel").html('Rechazar');
                console.log(xhr);
            }
        });
    });

    $(document).on('click', ".ver_archivos", function() {
         idc = $(this).attr("id");
         data = {
             ida: user.id,
             token: user.token,
             idc: idc
         }
         $.ajax({
             url: dominio + 'casos/archivos/',
             type: 'GET',
             data: data,
             success: function(data) {
                 //console.log(data);
                 $("#ul_arc").html('');
                 lihtml = '';
                 for (var i = 0; i < data.length; i++) {
                     lihtml += '<li class="collection-item collection-casos avatar clr_black li_arc_' + data[i].id + '">';
                     if (data[i].tipo == 'image/png' || data[i].tipo == 'image/gif' || data[i].tipo == 'image/jpeg' || data[i].tipo == 'image/jpg')
                         lihtml += '<i class="fa fa-image circle green"></i>';
                     else if (data[i].tipo == 'application/pdf')
                         lihtml += '<i class="fa fa-file-pdf circle red"></i>';
                     else
                         lihtml += '<i class="fa fa-file circle"></i>';
                     lihtml += '<p>' + data[i].nom_archivo + '</p>';
                     lihtml += '<a href="#!" class="secondary-content descargar-arc" ruta="' + data[i].archivo + '" name="' + data[i].nom_archivo + '"><i class="fa fa-download"></i></a>';
                     lihtml += '</li>';
                 }
                 $("#ul_arc").html(lihtml);
                 $("#modalArchivosadjuntados").modal("open");
             }

         });
     $(document).on('click', ".descargar-arc", function() {
         ruta = $(this).attr("ruta").split("\\");
         name = $(this).attr("name");
         window.open('descargar.php?url=' + ruta[3] + "&name=" + name, '_blank');
     });
     });
   
}); 