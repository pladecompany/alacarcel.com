$(document).ready(function(){
    listconfig();
   
    function listconfig(){
        data = {
            ida:user.id,
            token:user.token
        }
        $.ajax({
            url : dominio+'configuracion',
            type: 'GET',
            data:data,
            success:function(data){
                //console.log(data);
                actualizar_config(data)
            },
            error: function(xhr, status, errorThrown){
                console.log(xhr);
            }
        });
    }
    function actualizar_config(data){
        $("#mon_con").val(data.monto);
        $("#can_con").val(data.demandantes);
        $("#votos").val(data.voto).trigger("change");
    }  

    $(document).on('click', "#bt_save", function(){
            var formData = new FormData(document.getElementById("form_con"));
            formData.append("ida" ,user.id);
            formData.append("token" ,user.token);
            $.ajax({
                url: dominio + "configuracion/", 
                type: 'POST',
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                data: formData,
                success: function(data){
                    setTimeout(function() {
                        location.href = "?op=inicio";
                    }, 1500);
                    M.toast({html:data.msg}, 5000);
                    
                },
                error: function(xhr, status, errorThrown){
                  console.log(xhr);
                }
            });
    });    
    
    
    
});

