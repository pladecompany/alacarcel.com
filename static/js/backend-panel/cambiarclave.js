document.write("<script src='static/js/backend-panel/general.js'></script>");


$(document).ready(function() {
    $('#perfil').attr('src', dominio + user.img_usu)

    //submit al cambiar clave
    $("#cambiar_clave").on('submit', function(e) {
        e.preventDefault();


        var pass = $('#nue_cla').val()
        if (pass.length < 4) {
            M.toast({ html: "Contraseña: Mínimo 4 caracteres." }, 5000);
            $("#nue_cla").focus()
            return false;
        }
        var data = new FormData(this);
        data.append('rol', user.rol);
        data.append("id", user.id);
        $('#bt_save').attr('disabled', true);
        $.ajax({
            url: dominio + "usuarios/cambiar-clave",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                M.toast({ html: data.msg }, 10000);
                if (data.msg == "Ha cambiado su contraseña satisfactoriamente.") {
                    $('#cambiar_clave').trigger("reset");
                }
                $('#bt_save').attr('disabled', false);
            },
            error: function(xhr, status, errorThrown) {
                //M.toast({html:err.data.msj}, 10000);
                console.log("Errr : ");
                $('#bt_save').attr('disabled', false);
            }
        });

    });

});