var dominio = "https://inthecompanies.com:8200/";
//var dominio = "http://localhost:8200/";


if(location.host=="localhost") {
   var dominioweb = "http://localhost/denunciapp/";
} else {
   var dominioweb = "https://pladecompany.com/demos/denunciapp/";
}
var user = JSON.parse(window.sessionStorage.getItem('user'));

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
op = getParameterByName('op');



//habilitar opciones segun el tipo de usuario
if (user) {
    if (user.rol == 'Abogado')
        $(".hab_niv_abo").removeClass('hide');
    else
        $(".hab_niv_usu").removeClass('hide');
    //if (user.est_usu == 1 && op != 'perfil')
    //    window.location = "?op=perfil";
} else
    $(".logo-principal").attr("href", "?op=iniciarsesion");

//console.log(paneluser);//paneluser esta en los index de cada panel
//crear usuario denuncia
if (op == 'inicio' && paneluser == 'usuario') {
    document.write("<script src='static/js/backend-panel/registro.js'></script>");
}
if ((op == 'inicio' || op == 'vercasos' || !op) && paneluser == 'admin') {
    document.write("<script src='../static/js/backend-panel/casos.js'></script>");
    document.write("<script src='../static/js/axios.min.js'></script>");
    document.write("<script src='../static/js/backend-panel/buscarpanel.js'></script>");

}

if (op == 'usuarios' || op == 'abogados') {
    document.write("<script src='../static/js/backend-panel/usuarios.js'></script>");
}

//funcionarios
if (op == 'funcionarios') {
    document.write("<script src='../static/js/backend-panel/funcionarios.js'></script>");
}
//instituciones
if (op == 'instituciones') {
    document.write("<script src='../static/js/backend-panel/instituciones.js'></script>");
}
//configuracion
if (op == 'configuracion') {
    document.write("<script src='../static/js/backend-panel/configuracion.js'></script>");
}
//transacciones
if (op == 'transacciones') {
    document.write("<script src='../static/js/backend-panel/transacciones.js'></script>");
}
//pagos_pendientes
if (op == 'pagos_pendientes') {
    document.write("<script src='../static/js/backend-panel/pagos_pendientes.js'></script>");
}
//casos
if (op == 'listadodecasos') {
    document.write("<script src='static/js/backend-panel/casos.js'></script>");
    document.write("<script src='static/js/axios.min.js'></script>");
}

if (op == 'mensajes') {
    document.write("<script src='../static/js/backend-panel/chats.js'></script>");
}


//paises
if (op == 'ciudades') {
    document.write("<script src='../static/js/backend-panel/ciudades.js'></script>");
}
//paises
if (op == 'paises') {
    document.write("<script src='../static/js/backend-panel/paises.js'></script>");
}



//puesto de trabajo
if (op == 'perfil') {
    document.write("<script src='static/js/backend-panel/perfil.js'></script>");
}

if (op == 'pagosrecibidos') {
    document.write("<script src='static/js/backend-panel/pagosrecibidos.js'></script>");
}

if (op == 'pagosrealizados') {
    document.write("<script src='static/js/backend-panel/pagosrealizados.js'></script>");
}
//crearcuenta
if (op == 'crearcuenta') {
    document.write("<script src='static/js/backend-panel/usuario_rec.js'></script>");
}

//paises
if (op == 'recuperar') {
    document.write("<script src='static/js/backend-panel/recuperar.js'></script>");
}

if (op == 'recuperar-clave') {
    document.write("<script src='static/js/backend-panel/recuperar_clave.js'></script>");
}
if (op == 'recuperar') {
    document.write("<script src='../static/js/backend-panel/recuperar_admin.js'></script>");
}

if (op == 'recuperar-clave') {
    document.write("<script src='../static/js/backend-panel/recuperar_clave_admin.js'></script>");
}

//notificaciones
if (op == 'notificaciones') {
    if (user.rol == 'Abogado' || user.rol == 'Usuario') {
        document.write("<script src='static/js/backend-panel/notificaciones.js'></script>");
    } else {
        document.write("<script src='../static/js/backend-panel/notificaciones.js'></script>");

    }
}