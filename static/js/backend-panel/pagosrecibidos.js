$(document).ready(function() {
        var table = $('.pagos').DataTable({
            "language": {
                "url": "static/lib/JTables/Spanish.json"
            }
        });
    listpagos();


    pagos = [];


    function listpagos() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'transacciones/recibidos',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                pagos = data;
                actualizar_tabla_pagos(data)
            }

        })
    }

    function actualizar_tabla_pagos(data) {
        var table = $('.pagos').DataTable({
            "language": {
                "url": "static/lib/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            datos = data[i];

            var option = '<a class="btn btn-blue ver-PAG" i="' + i + '"><i class="fa fa-eye"></i></a>';
            var row = [(i+1),"#" + datos.id, datos.nom_caso, "$" + formato.precio(datos.monto), moment(datos.fec_reg_pago).format("DD-MM-YYYY HH:mm"), datos.estatus_pago, option]
            table.row.add(row).draw().node();
        }
    }


   
    $(document).on('click', ".ver-PAG", function() {

        var obj = pagos[$(this).attr("i")];
        $(".ID").html("#" + obj.id);
        $(".CASO").html(obj.nom_caso);
        $(".MONTO").html("$" + formato.precio(obj.monto));
        $(".FECHA").html(moment(obj.fec_reg_pago).format("DD-MM-YYYY HH:mm"));
        $(".ESTATUS").html(obj.estatus_pago);
        $(".IDPAGO").html(obj.id_pago);
        $("#modalVer").modal("open");
    });


});