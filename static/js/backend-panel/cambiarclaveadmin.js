document.write("<script src='../static/js/backend-panel/general.js'></script>");


$(document).ready(function() {
    //submit al cambiar clave

    $("#cambiar_clave").on('submit', function(e) {
        e.preventDefault();
        $('#bt_save').attr('disabled', true);


        var data = new FormData(this);
        data.append("id", user.id);
        $.ajax({
            url: dominio + "admin/cambiar-clave",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                M.toast({ html: data.msg }, 10000);
                if (data.msg == "Ha cambiado su contraseña satisfactoriamente.") {
                    $('#cambiar_clave').trigger("reset");
                }
                $('#bt_save').attr('disabled', false);
            },
            error: function(xhr, status, errorThrown) {
                //M.toast({html:err.data.msj}, 10000);
                console.log("Errr : ");
                $('#bt_save').attr('disabled', false);
            }
        });

    });

});