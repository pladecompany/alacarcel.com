function getParameterByName(name) {
         name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
         var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
             results = regex.exec(location.search);
         return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var socket = io(dominio);

if (user != null) {
    socket.emit("conectar", user);
}
//recibir notificaciones
socket.on('RECIBIRNOTIFICACION', function(obj) {
    M.toast({ html: obj.descripcion }, 5000);
    //console.log(obj);
    if($(".count_bell").length>0){
    	 var count = $(".count_bell").text();
	     count = parseInt(count)+1;
	     $(".count_bell").text(count);
    } else {
    	var html = `
					<li>
						<a href="?op=notificaciones" class="dropdown-button" id="icon_noti" href="javascript:;"  margin-top: -.4rem;>
							<i class="fa fa-bell" style="color:#2196F3;"></i><span class="count_bell" style="border-radius:50%;color:#2196F3;padding:4px;">1</span>
						</a>
					</li>`; 
		$('#noti').html(html);
    }
    if(getParameterByName("op") == "listadodecasos" && obj.tipo_noti ==3) {
    	QuitarBotones(obj);
    }
});