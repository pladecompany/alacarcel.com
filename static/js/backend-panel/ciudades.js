$(document).ready(function() {
    var arrayData = [];
    var ciudades = [];
    let edit = false;
    var countt = 0;
    /*
    var table = $('.ciudades').DataTable({
        dom: "Bfrtip",
        searching: true,
        processing: true,
        paging: true,
        filter: true,
        orderable: false,
        order: [[ 1, 'asc' ]],
        ajax: function ( data, callback, settings ) {

            $.ajax({
                url: dominio+'paises/ciudades/1',
                // dataType: 'text',
                type: 'get',
                contentType: 'application/x-www-form-urlencoded',
                data: {
                    min: data.start,
                    max: 10
                },
                success: function( data, textStatus, jQxhr ){
                    countt=0;
                    callback({
                        draw:data.draw,
                        data: data.ciudades,
                        recordsTotal:  data.count,
                        recordsFiltered: data.count,
                    });
                    arrayData=data.ciudades
                },
                error: function( jqXhr, textStatus, errorThrown ){
                }
            });
        },
        serverSide: true,
        columns: [
            {data: null,
                render: function ( data, type, row ) {
                  return `
                    ${data.id}
                  `;
                }},
            { data: "pais" },
            { data: "estado" },
            { data: "nombre" },
            {data: null,
                render: function ( data, type, row ) {
                    countt +=1;
                    return `
                    <a href="" onclick="return false;" class="btn btn-blue edit" id="${countt-1}"><img src="../static/img/icons/edit.png"></a>
                    <a href="" onclick="return false;" class="btn btn-blue delete" id="${data.id}"><img src="../static/img/icons/trash.png"></a>
                    `;
                }}
        ]

    });*/



    ajaxPaises();
    ajaxCiudades();

    var table = $('.ciudades').DataTable({
        "language": {
            "url": "../static/lib/JTables/Spanish.json"
        }
    });

    function ajaxCiudades() {

        $.ajax({
            url: dominio + 'paises/ciudades/2',
            type: 'GET',
            success: function(data) {
                console.log(data)
                actualizar_tabla_puesto(data.ciudades)
            }

        })
    }

    function actualizar_tabla_puesto(data) {
        count = 0
        arrayData = data;
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            datos = data[i]
            var option = `
                <a href="" onclick="return false;" class="btn btn-blue edit" id="${i}"><img src="../static/img/icons/edit.png"></a>
                <a href="" onclick="return false;" class="btn btn-blue delete" id="${datos.id}"><img src="../static/img/icons/trash.png"></a>
                `
            var row = [count += 1, datos.pais, datos.estado, datos.nombre, option]
            table.row.add(row).draw().node();
        }
    }


    function ajaxPaises() {
        $.ajax({
            url: dominio + 'paises',
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected>Seleccione</option>';

                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#pais").html(html);
                $("#pais").formSelect();
            }

        })
    }

    var contador = 0;
    $('.add').on('click', function() {
        var ciudad = $('#ciudades').val()
        var pais = $("#pais").val()
        var estado = $("#estado").val()

        if (!pais || !estado) {
            M.toast({ html: 'Debe selecciona el país y estado antes de añadir la ciudad.' }, 2000)
            return false;
        } else {
            if (ciudad.trim() < 3) {
                M.toast({ html: 'Ciudad:Mínimo 3 caracteres.' }, 2000)
                $('#ciudades').focus()
                return false;
            }
            ciudades.push({ ciudad: ciudad, index: contador })
            $('.ciudadess').append(`
                <div class="chip" id="${contador}">
                    ${ciudad}
                    <img style="padding-left:0.2rem;" src="../static/img/icons/times.png" class="close">
                </div>
            `)
            contador += 1;
            $('#ciudades').val('')

        }

    })
    $(document).on('click', '.close', function() {
        var deletee = $(this).parent().attr('id');
        var indexx = null;
        ciudades.find(function(value, index) {
            if (value.index == deletee) {
                indexx = index
            }
        });
        ciudades.splice(indexx, 1)
    })

    function ajaxEstados(id, id_estado = 0) {
        $.ajax({
            url: dominio + 'paises/estado/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected>Seleccione</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }
                $("#estado").html(html);
                if (id_estado != 0) {
                    $('#estado').val(id_estado)
                }
                $("#estado").formSelect();
            }
        })
    }

    $('#pais').on('change', function() {
        ajaxEstados(this.value)

    })
    $('.save').on('click', function() {

        if (!edit) {
            if (ciudades.length < 1) {
                M.toast({ html: 'No ha añadido ninguna ciudad.' }, 2000)
                return false;
            }
        }
        var estado = $("#estado").val()

        if (edit) {
            url = dominio + 'paises/ciudad/edit'
            msg = 'Ciudad actualizada'
            data = {
                estado: estado,
                ciudades: $('#ciudades_edit').val(),
                id: edit
            }
        } else {
            url = dominio + 'paises/ciudad'
            msg = ciudades.length > 1 ? 'Ciudades registradas' : 'Ciudad registrada'
            data = {
                estado: estado,
                ciudades: ciudades,
            }
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function(data) {
                if (data.r == false) {
                    M.toast({ html: data.msg }, 2000)
                } else {
                    M.toast({ html: `${msg} con éxito.` }, 2000)
                    $('#modalCiudades').modal('close')
                    ciudades = [];
                    $('.ciudadess').html('')
                    edit = false;
                    ajaxCiudades();
                }
            }
        })

    })

    $(document).on('click', '.edit', function() {
        var data = arrayData[this.id];
        edit = data.id
        $('.no_edit').addClass('hide')
        $('.edite').removeClass('hide')
        $("#pais").val(data.id_pais);
        ajaxEstados(data.id_pais, data.id_estado)
        $('#ciudades_edit').val(data.nombre)
        $('#modalCiudades').modal('open')
    })

    $('.open').on('click', function() {
        edit = false;
        $('.edite').addClass('hide');
        $('.no_edit').removeClass('hide');
        $('.ciudadess').html("");
        $('#modalCiudades').modal('open');
        $('#pais').val('');
        $('#estado').val('');
        $('#ciudades_edit').val('');
        $('#ciudades').val('');
        ciudades = [];

    })
    $(document).on('click', '.delete', function() {
        var idv = this.id;
        var toastContent = '<span>¿Está seguro?</span><br><button class="btn-flat toast-action conf_si" id="' + idv + '">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

        M.toast({ html: toastContent }, 4000);
    });
    $(document).on('click', '.conf_si', function() {
        id = this.id
        M.Toast.dismissAll();

        $.ajax({
            url: dominio + 'paises/ciudades/delete/' + id,
            type: 'GET',
            success: function(data) {
                M.toast({ html: data }, 2000);
                ajaxCiudades();
            }
        })
    })
});