$(document).ready(function() {
      

    newcuenta=getParameterByName('New');
    if(newcuenta==1 || user.est_usu==1)
       M.toast({ html: "Debes completar tu perfil para tener acceso completo al sistema" }, 6000); 
    
    $(".telefono").mask("+99 99999999999999999");
    $(".boxPerfilBloquear").find("input, select, .saveB").attr("disabled", true);
    $(".boxPerfild").find("input, select, textarea, .saveBD").attr("disabled", true);
    $("#cv_file").attr("disabled", false);
   


    perfil()
    //ajaxPuestos();

    function paises() {
        $('#pais').html('');
        $('#pais').html('<option value="" selected disabled>Seleccione un país</option>');
        $.ajax({
            url: dominio + "paises/activos",
            type: 'get',
            success: function(data) {
                for (var i = 0; i < data.length; i++) {
                    datos = data[i]
                    $('#pais').append(`<option value="${datos.id}">${datos.nombre}</option>`)
                }
                $('#pais').val(user.id_pais)
            }
        })
    }

    $('#pais').on('change', function() {
        if (this.value) {
            ajaxEstados(this.value);
        } else {
            $('#estado').html('');
        }
    });


    function ajaxEstados(id, id_estado = 0) {
        $('#estado').html('<option value="" selected disabled>Cargando...</option>');
        $.ajax({
            url: dominio + 'paises/estado/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected disabled>Seleccione un estado</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#estado").html(html);
                if (user.id_estados) {
                    $("#estado").val(user.id_estados);
                }
                $("#estado").formSelect();
            }

        })
    }

    $("#estado").on('change', function() {

        ajaxCiudades(this.value);

    });

    function ajaxCiudades(id, id_ciudad = 0) {
        if (!id) {
            return;
        }
        $.ajax({
            url: dominio + 'paises/ciudad/' + id,
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected disabled>Selecciona una ciudad</option>';
                TiposVeh = data;
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#ciudad").html(html);
                if (user.id_ciudad != 0) {
                    $("#ciudad").val(user.id_ciudad);
                }
                $("#ciudad").formSelect();
            }

        })
    }

    function ajaxPuestos() {
        $.ajax({
            url: dominio + 'puestos',
            type: 'GET',
            success: function(data) {

                html = '<option value="" selected disabled>Selecciona el cargo</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#id_cargo").html(html);
                $("#id_cargo").formSelect();
            }

        })
    }
    

    $('#img_usu').on('change', function(event) {
        if(event.target.files[0]){
            var output = document.getElementById('imagen');
            output.src = URL.createObjectURL(event.target.files[0]);
        }
    });
    $('#img_usu_1').on('change', function(event) {
        if(event.target.files[0]){
            var output = document.getElementById('imagen_1');
            output.src = URL.createObjectURL(event.target.files[0]);
        }
    });
    $('#img_usu_2').on('change', function(event) {
        if(event.target.files[0]){
            var output = document.getElementById('imagen_2');
            output.src = URL.createObjectURL(event.target.files[0]);
        }
    });

    function perfil() {
        var user = JSON.parse(window.sessionStorage.getItem('user'));
        ajaxEstados(2);
        //paises()
        $('#nombree').val(user.nom_usu)
        $('#apellido').val(user.ape_usu)
        $('#correo').val(user.cor_usu)
        $('#telefono').val(user.tlf_usu)
        $('#sexo').val(user.sex_usu)
        $('#tipo_doc').val(user.tip_doc_usu)
        $('#num').val(user.num_doc_usu)    
        $('#correo_paypal').val(user.correo_paypal)
        if(user.rol=='Abogado'){
               
            $(".info-tipo").html(" / ABOGADO / MATRICULA # "+user.mat_usu);
        }
            
        else
            $(".info-tipo").html(" / USUARIO / NICKNAME: "+user.nic_usu);
        /*if (user.id_pais) {
            ajaxEstados(user.id_pais);
        }*/
        if (user.id_estados) {
            ajaxCiudades(user.id_estados);
        }

        if (user.imagen != null) {
            $('#imagen').attr('src', user.imagen)
            $('#img-perfil').attr('src', user.imagen)
        }
        if (user.imagen_1 != null) {
            $('#imagen_1').attr('src', user.imagen_1)
        }
        $('#imagen_1').on('error', function() {
            $(this).attr('src', 'static/img/user.png')
        })
        if (user.imagen_2 != null) {
            $('#imagen_2').attr('src', user.imagen_2)
        }
        $('#imagen_2').on('error', function() {
            $(this).attr('src', 'static/img/user.png')
        })
        if (user.imagen_firma != null) {
            $('#imagen_3').attr('src', user.imagen_firma)
        }
        $('#imagen_3').on('error', function() {
            $(this).attr('src', 'static/img/logo.png')
        })

        $('#imagen').on('error', function() {
            $(this).attr('src', 'static/img/user.png')
        })
        $('#img-perfil').on('error', function() {
            $(this).attr('src', 'static/img/user.png')
        })
    }
    

    $(document).on('click', '.saveperfil', function() {
        nombre = $('#nombree').val()
        apellido = $('#apellido').val()
        telefono = $('#telefono').val()
        tipo = $('#tipo_doc').val()
        num = $('#num').val()
        sexo = $('#sexo').val()
        correo_paypal = $('#correo_paypal').val().trim()
        //var pais = $("#pais").val()
        var estado = $("#estado").val()
        var ciudad = $("#ciudad").val()
        if (nombre.length < 3) {
            M.toast({ html: 'Por favor ingresa un nombre.' })
            return false;
        } else if (apellido.length < 3) {
            M.toast({ html: 'Por favor ingresa un apellido.' })
            return false;
        } else if (telefono.replace(/ /g, "").length < 8 /*|| telefono.replace(/ /g, "").length >15*/ ) {
            M.toast({ html: 'Télefono: Fórmato inválido.' })
            return false;
        } else if (!tipo) {
            M.toast({ html: 'Debe Seleccionar un tipo de documento.' })
            return false;
        } else if (num.length < 3) {
            M.toast({ html: 'Por favor ingresa el número de documento.' })
            return false;
        } else if (!sexo) {
            M.toast({ html: 'Debe Seleccionar un sexo.' })
            return false;
        } else if (!estado) {
            M.toast({ html: 'Debe Seleccionar un estado.' })
            return false;
        } else if (!ciudad) {
            M.toast({ html: 'Debe Seleccionar un ciudad.' })
            return false;
        }
        else if ((!validar_mail(correo_paypal) && correo_paypal) || (getParameterByName('viewcs') && !validar_mail(correo_paypal))) {
            M.toast({ html: 'Ingrese un correo paypal válido.' })
            return false;
        }
        
        url = dominio + "usuarios/edit/";

        var array = nombre.split("");
        nombre = array[0].toLocaleUpperCase() + nombre.substr(1).toLowerCase();
        var data = new FormData(document.getElementById("update_perfil"));
        data.append("id", user.id);
        data.append("nombre", nombre);
        data.append("status_usu", user.est_usu);
        $('.saveperfil').attr("disabled", true);
        $('.saveperfil').html("Enviando...");
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.saveperfil').attr("disabled", false);
                $('.saveperfil').html("Guardar");
                M.toast({ html: data.msj }, 10000);
                if (data.msj == "Se editó correctamente") {
                    window.sessionStorage.setItem("user", JSON.stringify(data));
                    if (data.imagen != null) {
                        $('#imagen').attr('src', data.imagen)
                        $('#img-perfil').attr('src', data.imagen)
                    }
                    if (data.imagen_1 != null) {
                        $('#imagen_1').attr('src', data.imagen_1)
                    }
                    if (data.imagen_2 != null) {
                        $('#imagen_2').attr('src', data.imagen_2)
                    }
                    if (data.imagen_firma != null) {
                        $('#imagen_3').attr('src', data.imagen_firma)
                    }
                    if(user.est_usu==1)
                        user.est_usu=2;
                    setTimeout(function() {
                        casocompartir = window.sessionStorage.getItem('casocompartirview');
                        
                        if((!casocompartir || casocompartir=="null") && getParameterByName('viewcs')) {
                            casocompartir = getParameterByName('viewcs');
                        }

                        if(!casocompartir || casocompartir=="null")
                            location.href = "?op=listadodecasos";
                        else{
                            window.sessionStorage.setItem("casocompartirview", null);
                            window.location = "?op=listadodecasos&viewcs="+casocompartir;
                        }
                    }, 2500);
                }
            },
            error: function(xhr, status, err) {
                console.log("Errr : " + err);
                $('.saveperfil').attr("disabled", false);
                $('.saveperfil').html("Guardar");

            }
        });
    });




//FIRMA DIGITAL
var limpiar = document.getElementById("limpiar");
var imagen_firma = document.getElementById("imagen_firma");
var guardarFirma = document.getElementById("guardarFirma");
var canvas = document.getElementById("canvas");
var dataURL = "";
var ctx = canvas.getContext("2d");
var cw = canvas.width = 300,
  cx = cw / 2;
var ch = canvas.height = 300,
  cy = ch / 2;

var dibujar = false;
var factorDeAlisamiento = 5;
var Trazados = [];
var puntos = [];
ctx.lineJoin = "round";

limpiar.addEventListener('click', function(evt) {
  dibujar = false;
  ctx.clearRect(0, 0, cw, ch);
  Trazados.length = 0;
  puntos.length = 0;
  guardarFirma.classList.add("hide");
  limpiar.classList.add("hide");
  imagen_firma.value = "";
}, false);
guardarFirma.addEventListener('click', function(evt) {
    dataURL = canvas.toDataURL();
    imagen_firma.value = dataURL;
    guardarFirma.classList.add("hide");
    limpiar.classList.add("hide");
}, false);



canvas.addEventListener('mousedown', function(evt) {
  dibujar = true;
  //ctx.clearRect(0, 0, cw, ch);
  puntos.length = 0;
  ctx.beginPath();
  guardarFirma.classList.remove("hide");
  limpiar.classList.remove("hide");
}, false);

canvas.addEventListener('mouseup', function(evt) {
  redibujarTrazados();
}, false);

canvas.addEventListener("mouseout", function(evt) {
  redibujarTrazados();
}, false);

canvas.addEventListener("mousemove", function(evt) {
  if (dibujar) {
    var m = oMousePos(canvas, evt);
    puntos.push(m);
    ctx.lineTo(m.x, m.y);
    ctx.stroke();
    guardarFirma.classList.remove("hide");
    limpiar.classList.remove("hide");
  }
}, false);





function reducirArray(n,elArray) {
  var nuevoArray = [];
  nuevoArray[0] = elArray[0];
  for (var i = 0; i < elArray.length; i++) {
    if (i % n == 0) {
      nuevoArray[nuevoArray.length] = elArray[i];
    }
  }
  nuevoArray[nuevoArray.length - 1] = elArray[elArray.length - 1];
  Trazados.push(nuevoArray);
}

function calcularPuntoDeControl(ry, a, b) {
  var pc = {}
  pc.x = (ry[a].x + ry[b].x) / 2;
  pc.y = (ry[a].y + ry[b].y) / 2;
  return pc;
}

function alisarTrazado(ry) {
  if (ry.length > 1) {
    var ultimoPunto = ry.length - 1;
    ctx.beginPath();
    ctx.moveTo(ry[0].x, ry[0].y);
    for (i = 1; i < ry.length - 2; i++) {
      var pc = calcularPuntoDeControl(ry, i, i + 1);
      ctx.quadraticCurveTo(ry[i].x, ry[i].y, pc.x, pc.y);
    }
    ctx.quadraticCurveTo(ry[ultimoPunto - 1].x, ry[ultimoPunto - 1].y, ry[ultimoPunto].x, ry[ultimoPunto].y);
    ctx.stroke();
  }
}


function redibujarTrazados(){
  dibujar = false;
  ctx.clearRect(0, 0, cw, ch);
  reducirArray(factorDeAlisamiento,puntos);
  for(var i = 0; i < Trazados.length; i++) {
    alisarTrazado(Trazados[i]);
  }
  

}

function oMousePos(canvas, evt) {
  var ClientRect = canvas.getBoundingClientRect();
  return { //objeto
    x: Math.round(evt.clientX - ClientRect.left),
    y: Math.round(evt.clientY - ClientRect.top)
  }
}

});