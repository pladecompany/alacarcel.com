$(document).ready(function() {
    listabogados();
    listpendiente();

    limiteapagar = 0;
    limiteapagard = 0;
    limiteapagart = 0;
    limitpide= 0;
    montoapagar=[];
    montoapagard=[];
    montoapagart=[];
    montopide=[];
    function listpendiente() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'transacciones/pagos-pendientes',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_pendiente(data);
            }

        })
    }

    function actualizar_tabla_pendiente(data) {
        var table = $('.funt').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            datos = data[i];
            if(datos.usuario.imagen!=null)
                imgusuariot=datos.usuario.imagen;
             else
                imgusuariot='../static/img/user.png';
            var fr = moment(datos.fec_reg_pago).format('YYYY-MM-DD, HH:mm:ss');
            var option = '<a class="btn btn-blue ver-funcionario" href="?op=vercasos&idc=' + datos.id_caso + '"><i class="fa fa-eye"></i></a> <a class="btn btn-blue delete_f '+((datos.estatus_pago =='Denied')? '' : 'hide')+'" id="' + datos.id + '"><i class="fa fa-trash"></i></a>';
            var row = [count += 1, '<img src="' + imgusuariot + '" onerror="this.src =\'../static/img/user.png\'" class="img-user boxPerfil--img img-cover" style="width:35px;height:35px;"> ', datos.usuario.nom_usu+" "+datos.usuario.ape_usu, "#"+datos.id_caso+" "+datos.caso.nom_caso, formato.precio(datos.monto) + " $", fr,datos.estatus_pago,datos.id_pago, option]
            table.row.add(row).draw().node();
        }
        
    }

    function listabogados() {
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'usuarios/abogado',
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_abogados(data)
            }

        })
    }

    function actualizar_abogados(data) {
        veri = 0;
        for (var i = 0; i < data.length; i++) {
            veri++;
            datos = data[i];
            $("#abo_pago").append('<option value="' + datos.id + '">' + datos.nom_usu + ' '+datos.ape_usu+'</option>');
            if (veri == data.length) {
                $('#abo_pago').select2({
                    language: {
                        noResults: function() {
                            return "No hay resultado";
                        },
                        searching: function() {
                            return "Buscando..";
                        }
                    },
                    dropdownParent: $('#modalTransaccion')
                });
                $('.select2').css("width", "100%");
            }
        }

    }

    $('#abo_pago').on('change', function() {
        if (this.value) {
            buscarcarsos(this.value);
        } else {
            $('.limite-pago').html('');
            $('#cas_pago').html('<option value="" selected>Selecciona el caso</option>');
        }
    });

    function buscarcarsos(ida) {
        $('#cas_pago').html('<option value="" selected>Buscando caso</option>');
        $('#cas_pago').attr("disabled",true);
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'transacciones/buscar-casos-completados-abogado/'+ida,
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_casos(data);
            }

        })
    }

    function actualizar_casos(data) {
        veri = 0;
        $('#cas_pago').html('<option value="" selected>Selecciona el caso</option>');
        if(data.length==0){
            M.toast({ html: "Este abogado no tiene casos en cursos." }, 5000);
            return;
        }
        for (var i = 0; i < data.length; i++) {
            veri++;
            datos = data[i];
             
            montoapagar[datos.id] = parseFloat(datos.recaudo) - parseFloat(datos.pagado);
            montoapagard[datos.id] = datos.recaudo;
            var sobra = (parseFloat(datos.pagado) - parseFloat(datos.recaudo));
            if(sobra<=0){
                sobra = 0;
            }
            montoapagart[datos.id] =  sobra;
            montopide[datos.id] = datos.pre_pos_2;
            $("#cas_pago").append('<option value="' + datos.id + '">#'+datos.id+' '+ datos.nom_caso +'</option>');
            if (veri == data.length) {
                $('#cas_pago').select2({
                    language: {
                        noResults: function() {
                            return "No hay resultado";
                        },
                        searching: function() {
                            return "Buscando..";
                        }
                    },
                    dropdownParent: $('#modalTransaccion')
                });
                $('.select2').css("width", "100%");
            }
        }
        $('#cas_pago').attr("disabled",false);

    }

    $('#cas_pago').on('change', function() {
        if (this.value) {
            limiteapagar = montoapagar[this.value];
            limiteapagard = montoapagard[this.value];
            limiteapagart = montoapagart[this.value];
            limitpide = montopide[this.value];
            var etiqueta = "";
            if(limitpide<=0){
                etiqueta = "<br> <span style='font-size:10px;color: #ce0c0cd4;'>Éste abogado no desea cobrar por representar éste caso...</span>";
            }
            $('.limite-pago').html(" (Pendiente a pagar "+formato.precio(limiteapagar)+" $) (Recaudo "+formato.precio(limiteapagard)+" $) (Sobrante "+formato.precio(limiteapagart)+" $)" + etiqueta);
        } else {
            $('.limite-pago').html('');
        }
    });

    $(document).on('click', ".add_pago", function() {
        $(".limpiar_form").val("");
        $(".limpiar_form_2").val("").trigger("change");
        $('#cas_pago').html('<option value="" selected>Buscando caso</option>');
        $('#cas_pago').attr("disabled",true);
        
        data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + 'transacciones/cantidad-casos-en-curso/',
            type: 'GET',
            data: data,
            success: function(data) {
                if(data[0]['count(*)']>0)
                    $("#modalTransaccion").modal("open");
                else
                    M.toast({ html: "Actualmente no hay casos en cursos." }, 5000);
            },
            error: function(xhr, status, errorThrown) {
                console.log(xhr);
            }

        })
        
    });

    $(document).on('click', ".save", function() {
        if(parseFloat(Quitarcoma($("#mon_pago").val())) > parseFloat(limiteapagar)){
            M.toast({ html: "El monto a pagar no debe ser mayor a: " + formato.precio(limiteapagar) }, 5000);
        } else if(!$("#mon_pago").val() || parseFloat($("#mon_pago").val()) <= parseFloat(0)){
            M.toast({ html: "El monto a pagar es necesario." }, 5000);
        }else{
           var formData = new FormData();
            formData.append("id_abogado", $("#abo_pago").val());
            formData.append("id_caso", $("#cas_pago").val());
            formData.append("monto", Quitarcoma($("#mon_pago").val()));
            formData.append("ref", $("#ref_pago").val());
            formData.append("ida", user.id);
            formData.append("token", user.token);
            url = "pago-pendiente";
            $(".save").attr("disabled", true); $(".save").html("Enviando..."); 
            $.ajax({
                url: dominio + "transacciones/" + url,
                type: 'POST',
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                data: formData,
                success: function(data) {
                    $(".save").attr("disabled", false); $(".save").html("Transferir monto"); 
                    if (data.r) {
                        listpendiente();
                        $("#modalTransaccion").modal('close');
                        $(".limpiar_form").val('');
                        $(".limpiar_form_2").val('').trigger("change");
                        M.toast({ html: data.msg }, 5000);
                    } else {
                        M.toast({ html: data.msg }, 5000);
                    }

                },
                error: function(xhr, status, errorThrown) {
                    console.log(xhr);
                     $(".save").attr("disabled", false); $(".save").html("Transferir monto");
                }
            }); 
        }
        
    });

    //-----------------ELIMINAR DATOS----------------------------//
    $(document).on('click', '.delete_f', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste pago?</span><br><button class="btn-flat toast-action conf_si" id="' + idv + '">Si</button><button class="btn-flat toast-action" onclick="M.Toast.dismissAll();">No</button>';

        M.toast({ html: toastContent }, 4000);
    });
    $(document).on('click', '.conf_si', function() {
        id = this.id
        var data = {
            ida: user.id,
            token: user.token
        };
        M.Toast.dismissAll();
        $.ajax({
            url: dominio + 'transacciones/delete/' + id,
            type: 'GET',
            data: data,
            success: function(data) {
                if (data.msj) {
                    M.toast({ html: data.msj }, 10000);
                    listpendiente();
                } else {
                    M.toast({ html: "Error al eliminar." }, 10000);

                }
            }
        })

    });

    function Quitarcoma(value, op) {
         if (value == "") {
             value = "0";
         }
         if (value) {
             for (var i = 0; i < value.split(",").length + 10; i++) {
                 value = value.replace(",", "");
             }
             //value = value.replace(",", ".");
         }
         if (value.length == "3" || value.length == "2") {
             if (value.split(".").length == 0)
                 value = "0." + "" + value;
             else
                 value = "0" + "" + value;
         }
         return parseFloat(value);
    }
});