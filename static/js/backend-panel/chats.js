$(document).on('ready', function() {
    console.log("Modulo chats");
    axiosChats();



    //declaro la tabla Datatable
    var tabla = $('#tabla_todos').DataTable({
        "language": {
            "url": "../static/lib/JTables/Spanish.json"
        }
    });

    //Cargar Listado de usuario al chat
    function axiosChats() {

        axios.get(dominio + 'chats/admin/obtener-usuarios?id=' + user.id + '&token=' + user.token)
            .then(function(response) {
                data = response.data
                for (var i = 0; i < data.length; i++) {
                    datos = data[i]
                    if (datos.pendientes > 0) {
                        var badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">' + datos.pendientes + '</span>'
                    } else {
                        var badge = ''
                    }
                    if (datos.envio == 1) {
                        usu = 'cliente'
                        nombre = `<a href="" onclick="return false;" class="link"><p class="boxChat--name nombre">${datos.nombre} (${usu})</p></a>`
                        img = '../static/img/user.png'
                    } else {
                        usu = 'Profesional'
                        nombre = `<a href="" onclick="return false;" class="link"><p class="boxChat--name nombre">${datos.nombre} (${usu})</p></a>`
                        img = datos.img
                    }
                    $('.listado').append(` 
                <div class="row boxList" id="${datos.envio}-${datos.id_usuario}">
                    <div class="col s2">
                        <img src="${img}" width="100%" class="boxList--img circle" >
                    </div>
                    <div class="col s10 chat" id="${datos.id}" >
                        ${nombre}
                        <p id="msj">${datos.mensaje}</p>
                        <span class="timeago" title="${datos.hora}"></span>
                        <input type="number"  value="${datos.envio}" class="envio" style="display:none">
                        <input type="number"  value="${datos.id_usuario}" class="id_usuario" style="display:none">
                        ${badge}
                    </div>
                </div>
                    `);

                }


            })
            .catch(function(error) {
                console.log(error);

            });
    }
    /*
    content = ''
    mensaje = document.getElementById('enviar')
    mensaje.addEventListener('paste', function (evt) {
        $('#enviar').html('')
      content = evt.clipboardData.getData('text/plain');
      $('#enviar').html(content)
       

    });
    */
    $("#enviar").bind('paste', function(e) {
        //$(this).html(content)
        $(e.target).keyup(getInput);
        //console.log($(e.target).context)
    });

    function getInput(e) {
        //content = e.originalEvent.clipboardData.getData('text/plain');
        var inputText = $(e.target).html();
        $('#enviar').html(inputText.replace(/<[^>]*>?/g, ''))
        $(e.target).unbind('keyup');
    }

    $(document).on('click', '.boxList', function() {

        men = $('#men').css('display', 'block')
        $('#mensajes').html('');
        $('#mensajes').append("<center><img src='static/img/cargando.gif' style='vertical-align:middle;' width='15px'></center>");
        $('#init').remove()
        envio = $(`#${this.id} > .chat > .envio`).val()

        nombre = $(`#${this.id} > .chat > .link > .boxChat--name`).text()
        id_usuario = $(`#${this.id} >.chat > .id_usuario`).val()


        pen1 = $('#num').html()
        pen2 = $(`#${this.id} > .chat > .badge`).html()
        if (parseInt(pen1) > 0) {
            resta = parseInt(pen1) - parseInt(pen2)
            if (resta == 0) {
                $('#msj1').css('color', '#fff')
            }
            $('#num').html(resta)
        }
        $(`#${this.id} > .chat > .badge`).remove()
        if (envio == 2) {
            $('#nombre').html('<a href="?op=perfil&id=' + id_usuario + '" style="color:gray;">' + nombre + '</a>')
        } else {
            $('#nombre').html(nombre)
        }
        $('#id_usuario').val(id_usuario)
        $('#envio').val(envio)


        axios.get(dominio + 'chats/admin/obtener-chats?envio=' + envio + '&id_usuario=' + id_usuario + '&token=' + user.token)
            .then(res => {
                datos = res.data.reverse()
                $('#mensajes').html('');
                coun1 = 0
                for (var i = 0; i < datos.length; i++) {
                    data = datos[i]
                    if (data.tabla_usuario == 'admin') {
                        tipo = 'other'
                        if (envio == 2) {
                            img = data.img
                        } else {
                            img = '../static/img/user.png'
                        }
                        if (coun1 == 0) {
                            $('#msj-chat').attr('src', img)
                            coun1 += 1;
                        }
                    } else {
                        tipo = 'self'
                        img = '../static/img/logo.png'
                    }
                    $('#mensajes').append(`
                <li class="row ${tipo}">
                    <div class="avatar">
                        <img src="${img}" draggable="false"/>
                    </div>
                    <div class="msg">
                        <p>${data.mensaje}</p>
                        
                        <span class="timeago" title="${data.hora}"></span>
                    </div>
                </li>
                `)

                }
                scrollToBottom()
            })
        input = $('#bus_men').val()
        if (input.length > 0) {
            $('#bus_men').val('')
            $('#bus_men').keyup()
        }
    })

    function scrollToBottom() {
        $("#content_messages").scrollTop($("#content_messages")[0].scrollHeight);
    }
    usuarios = []
    var searchArray = function(arr, regex) {
        var matches = [],
            i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i].nombre.match(regex)) matches.push(arr[i]);
        }
        if (usuarios.length < 1) {
            usuarios.push(arr)
        }
        return matches;
    };
    $("#bus_men").keyup(function() {
        if ($(".listado > div").length > 0) {

            nombre = $('.nombre')
            var ss = [];
            for (var i = 0; i < nombre.length; i++) {
                nombres = $(nombre[i]).html()
                html = $(nombre[i]).parent().parent().parent()
                filter1 = $(nombre[i]).parent().parent()
                id_usuario1 = $(`#${filter1[0].id} > .id_usuario`).val()
                envio1 = $(`#${filter1[0].id} > .envio`).val()
                pendientes = $(`#${filter1[0].id} > .badge`).html()
                if (pendientes == undefined) {
                    pendientes = 0;
                }
                ss.push({ pendientes: parseInt(pendientes), id_usuario: parseInt(id_usuario1), envio: parseInt(envio1), nombre: nombres.toLowerCase(), html: html })


            }
            filtro = $(this).val().toLowerCase()
            if (filtro != '') {
                result = searchArray(ss, filtro); // => ['german shepard', 'chihuahua']


                socket.emit('ENCONTRAR-USUARIO', filtro)
                socket.on('USUARIO-ENCONTRADO', function(data) {
                    if (data.length > 0) {
                        $('.listado').html('')
                        for (var i = 0; i < data.length; i++) {
                            datos = data[i]
                            if (datos.pendientes > 0) {
                                var badge = '<span class="badge" style="font-weight: 300;font-size: 0.9rem;color: #fff;background-color: #e51c23;border-radius: 2px;">' + datos.pendientes + '</span>'

                            } else {
                                var badge = ''
                            }
                            if (datos.envio == 1) {
                                usu = 'cliente'
                                nombre = `<a href="" onclick="return false;" class="link"><p class="boxChat--name nombre">${datos.nombre} (${usu})</p></a>`
                                img = '../static/img/user.png'
                                if (datos.mensaje != null) {
                                    mensaje = datos.mensaje
                                } else {
                                    mensaje = 'No hay mensajes con este usuario'
                                }
                            } else {
                                usu = 'Profesional'
                                nombre = `<a href="" onclick="return false;" class="link"><p class="boxChat--name nombre">${datos.nombre} (${usu})</p></a>`
                                img = datos.img
                                if (datos.mensaje != null) {
                                    mensaje = datos.mensaje
                                } else {
                                    mensaje = 'No hay mensajes con este usuario'
                                }
                            }

                            $('.listado').append(` 
                        <div class="row boxList" id="${datos.envio}-${datos.id_usuario}">
                            <div class="col s2">
                                <img src="${img}" width="100%" class="boxList--img circle" >
                            </div>
                            <div class="col s10 chat" id="${datos.envio}" >
                                ${nombre}
                                <p id="msj">${mensaje}</p>
                                <span class="timeago" title="${datos.hora}"></span>
                                <input type="number"  value="${datos.envio}" class="envio" style="display:none">
                                <input type="number"  value="${datos.id_usuario}" class="id_usuario" style="display:none">
                                ${badge}
                            </div>
                        </div>
                            `);
                            $('img').on('error', function() {
                                    $(this).attr('src', '../static/img/user.png')
                                })
                                //ss.push({pendientes:parseInt(pendientes),id_usuario:parseInt(id_usuario1),envio:parseInt(envio1),nombre:nombres.toLowerCase(),html:html})
                        }
                    } else {
                        $('.listado').html('<div><center><p style="color:#fff;font-weight:bold;">NO SE ENCONTRÓ "' + filtro + '"</p></center></div>')

                    }
                })
            } else {

                $('.listado').html('')
                for (var i = 0; i < usuarios[0].length; i++) {
                    //console.log(usuarios[i].html)
                    $('.listado').append(usuarios[0][i].html)
                }

            }
            /*if(filtro != ''){
                if(ss.length == 0){
                    result = searchArray(usuarios[0], filtro); // => ['german shepard', 'chihuahua']
                }else{
                    result = searchArray(ss, filtro); // => ['german shepard', 'chihuahua']
                }
                if(result == ''){
                    $('.listado').html('<div><center><p style="color:#fff;font-weight:bold;">NO SE ENCONTRO "'+filtro+'"</p></center></div>')
                }
                else{
                    $('.listado').html('')
                }
                for(var i =0;i<result.length;i++){
                    $('.listado').append(result[i].html)
                }
            }*/
        }


    });
    $('.send').on('click', function() {
        $(this).attr('disabled', true);
        if ($("#enviar").html().trim() != "") {
            mensaje = $('#enviar').html()
            id_usuario = $('#id_usuario').val()
            envio = $('#envio').val()
            if (parseInt(envio) == 1) {
                tabla_usuario = "cliente"
                tablausuario = "clientes"
            } else {
                tabla_usuario = "profesional"
                tablausuario = "profesionales"

            }
            var data = {
                id_usuario: id_usuario,
                mensaje: mensaje,
                tabla_usuario: tabla_usuario,
                envio: envio,
                tablausuario: tablausuario,
            };
            hora = moment().format("YYYY-MM-DD HH:mm:ss")
            $('#mensajes').append(` 
            <div class="row self">
                <div class="col s2 avatar">
                    <img src="../static/img/logo.png" draggable="false"/>
                </div>
                <div class="col s10 msg">
                    <p>${mensaje}</p>
                    
                    <span class="timeago" title="${hora}"></span>
                </div>
            </div>
        `)
            scrollToBottom()
            socket.emit("ENVIARMENSAJE", data);
            $("#enviar").html("");
        }
        $(this).attr('disabled', false);

    })
});