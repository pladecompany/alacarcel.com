var ida = false;
$(document).ready(function() {
    $('.save_reg').on('click', function() {
        var tipo = $("#tip_reg").val();
        var matri = $("#mat_reg").val();
        var nick = $("#nic_reg").val();
        var correo = $("#cor_reg").val();

        var pass = $("#password_neg").val();
        var pass2 = $("#password_con").val();
        
        //var condiciones = $("input[name='condi']:checked").val();
        
        var err = false;
        if (tipo == '') {
            err = "Selecciona tu Rol";
            $("#password_neg").focus();
        } else if (tipo == 'Abogado' && matri.length < 2) {
            err = "Matricula: Mínimo 2 caracteres.";
            $("#password_neg").focus();
        } else if (tipo == 'Usuario' && nick.length < 2) {
            err = "Nickname: Mínimo 2 caracteres.";
            $("#password_neg").focus();
        } else if (!validar_mail(correo)) {
            err = "Correo: Fórmato inválido.";
            $("#cor_reg").focus();
        } else if (pass.length < 4) {
            err = "Contraseña: Mínimo 4 caracteres.";
            $("#password_neg").focus();
        } else if (pass2.length < 4) {
            err = "Confirmar Contraseña: Mínimo 4 caracteres.";
            $("#password_con").focus();
        } else if (pass2 != pass) {
            err = "Contraseñas: Las constraseñas deben coincidir.";
        }

        if (err) {
            M.toast({ html: err }, 2000);
            return false;
        }

        var data = new FormData(document.getElementById("login_entrar"));
        
        $('.save_reg').attr("disabled", true);
        $('.save_reg').html("Registrar <img src='static/img/cargando.gif' style='vertical-align:middle;' width='15px'>");
        
        $.ajax({
            url: dominio + 'usuarios/',
            type: 'POST',
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.save_reg').attr("disabled", false);
                $('.save_reg').html("Registrar");
                //console.log(data);
                if (!data.user) {
                    M.toast({ html: data.msg }, 8000);
                } else {
                    M.toast({ html: "Verifíca tu correo para activar tu cuenta." });
                    $("#cor_reg").val('');
                    $("#mat_reg").val('');
                    $("#nic_reg").val('');
                    $("#password_neg").val('');
                    $("#password_con").val('');
                }
            },
            error: function(xhr, status, errorThrown) {
                console.log("Errr : ");
                $('.save_reg').attr("disabled", false);
                $('.save_reg').html("Registrar");
            }
        });
    });
    

});