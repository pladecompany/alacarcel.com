$(document).ready(function() {


    //submit al cambiar clave
    $("#recuperar_clave").on('submit', function(e) {
        e.preventDefault();

        var pass = $('#nue_cla').val()
        var passr = $('#rep_cla').val()
        if (pass.length < 4) {
            M.toast({ html: "Contraseña: Mínimo 4 caracteres." }, 5000);
            $("#nue_cla").focus()
            return false;
        }
        if (passr.length < 4) {
            M.toast({ html: "Repetir Contraseña: Mínimo 4 caracteres." }, 5000);
            $("#nue_cla").focus()
            return false;
        }
        if (passr != pass) {
            M.toast({ html: "Las Contraseñas no coinciden." }, 5000);
            $("#nue_cla").focus()
            return false;
        }
        $('#btn_REC').prop('disabled', true);
        $('#RES1').html("<img src='static/img/cargando.gif' style='vertical-align:middle;' width='15px'>");
        var data = new FormData(this);
        $.ajax({
            url: dominio + "usuarios/recuperar-clave-token",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                
                if (data.auth == true) {
                    window.sessionStorage.setItem("user", JSON.stringify(data.user));
                    data = data.user
                       window.location = "?op=home";
                    
                } else {
                    $('#RES1').html("");
                    $('#btn_REC').prop('disabled', false);
                }

            },
            error: function(xhr, status, errorThrown) {
                $('#RES1').html("");
               $('#btn_REC').prop('disabled', false);
            }
        });

    });

});