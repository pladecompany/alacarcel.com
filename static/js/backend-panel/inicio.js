$(document).on('ready', function(){ 
	$.ajax({
          url: dominio + "notificaciones/limpieza_autos?token="+user.token,
          type: "GET",
          data: {},
          dataType: "json",
          success: function(data){
            $(".contador_limpieza").text(parseInt(data.length));
          },
          error: function(xhr, status, errorThrown){
            console.log("Errr : ");
          }
      });
  $.ajax({
          url: dominio + "notificaciones/cotizaciones-mantenimiento?token="+user.token,
          type: "GET",
          data: {},
          dataType: "json",
          success: function(data){
            $(".contador_mantenimiento").text(parseInt(data.length));
          },
          error: function(xhr, status, errorThrown){
            console.log("Errr : ");
          }
  });
  $.ajax({
          url: dominio + "notificaciones/limpieza?token="+user.token,
          type: "GET",
          data: {},
          dataType: "json",
          success: function(data){
            $(".contador_limpieza_do").text(parseInt(data.length));
          },
          error: function(xhr, status, errorThrown){
            console.log("Errr : ");
          }
  });
  $.ajax({
          url: dominio + "chats/admin/pendientes?token="+user.token,
          type: "GET",
          data: {},
          dataType: "json",
          success: function(data){
            $("#num").text(parseInt(data[0].pendientes));
            if(parseInt(data[0].pendientes) >0){
              $('#msj1').css('color','red')
            }
          },
          error: function(xhr, status, errorThrown){
            console.log("Errr : ");
          }
  });
  
  ultimas_notis = function ultimas_notis(){
    $.ajax({
            url: dominio + "notificaciones/administrador?token="+user.token,
            type: "GET",
            data: {limit:5},
            dataType: "json",
            success: function(data){
              var html="";
              for (var i = 0; i < data.length; i++) {
                html += '<li><a href="#!">'+data[i].tit_noti+'</a></li>';  
              }
              html += '<li><a href="?op=notificaciones"><b style="font-size:14px">Ver todas </b></a></li>';  
              
              $("#notificaciones").html(html);
              if(data.length > 0){
                document.querySelector(".campanaNoti").classList.add("text-rojo");
                document.querySelector(".campanaNoti").classList.add("animated");
                document.querySelector(".campanaNoti").classList.remove("text-moradoOscuro");
              }
            },
            error: function(xhr, status, errorThrown){
              console.log("Errr : ");
            }
    });
  }
  ultimas_notis();

});
