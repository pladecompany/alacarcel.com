(function($) {
    $.fn.validCampo = function(cadena) {
        $(this).on({
            keypress : function(e){
                var key = e.which,
                    keye = e.keyCode,
                    tecla = String.fromCharCode(key).toLowerCase(),
                    letras = cadena;
                if(letras.indexOf(tecla)==-1 && keye!=9&& (key==37 || keye!=37)&& (keye!=39 || key==39) && keye!=8 && (keye!=46 || key==46) || key==161){
                    e.preventDefault();
                }
            }
        });
    };
    $(document).on('blur keyup keydown', '.text', function(){
        $(this).validCampo("abcdefghijklmnñopqrstuvwxyz ");
    });
    $(document).on('blur keyup keydown', '.number', function(){
        $(this).validCampo("1234567890");
    });
    $(document).on('blur keyup keydown', '.number-2', function(){
        $(this).validCampo("1234567890.");
    });
    $(document).on('blur keyup keydown', '.number-1', function(){
        $(this).validCampo("1234567890-");
    });
    $(document).on('blur keyup keydown', '.text-number-', function(){
        $(this).validCampo("1234567890abcdefghijklmnñopqrstuvwxyz");
    });
    $(document).on('blur keyup keydown', '.text-number-1', function(){
        $(this).validCampo("1234567890abcdefghijklmnñopqrstuvwxyz- ");
    });
   
})( jQuery );

function mensajeChat(obj, tipo){
  var html = "";
  html+='<li class="'+tipo+'">';
  html+='	<div class="avatar">';
  html+='		<img src="'+ dominio + obj.usuario.img_usu +'" draggable="false" onerror="this.src=\'../static/img/user.png\'">';
  html+='	</div>';
  html+='	<div class="msg">';
  html+='		<p>'+ obj.mensaje +'</p>';
  html+='		<span class="timeago" title="'+ obj.fec_reg_chat +'"></span>';
  html+='	</div>';
  html+='</li>';
  return html;
}


function buscar_paso2(inputext,datos,op) {
    var obj = '';
    var texto = '';
    
        obj = JSON.stringify(datos);
        if(op==1)
        texto = 'nom_trans-ape_trans-cod_trans';
        else if(op==2)
        texto = 'nom_pro-cod_pro';
        else if(op==3)
        texto = 'nom_vend-ape_vend-cod_vend-id';
        else 
        texto = 'nom_com-nom_alma';


    var data = buscar_paso3(obj, inputext, texto);

    var html = "";
    for (i = 0; i < data.length; i++) {
        if (data.length == 1) {
            return data[i];
        }
    }
    return false;
}
function buscar_paso3(obj, cadena, texto) {

    //BUSCAR TODO EN MINUSCULA
    var cadena = new String(cadena);
    cadena = cadena.toLowerCase();
    //FIN BUSCAR TODO EN MINUSCULA

    var data = JSON.parse(obj);

    var response = new Array();
    var cont = 0;
    for (i = 0; i < data.length; i++) {


        var text = texto;
        var text = text.split("-");

        if (text.length >= 2) {

            if (text.length == 2) {
                var cadenaminu1 = new String(data[i][text[0]]);
                cadenaminu1 = cadenaminu1.toLowerCase();
                var cadenaminu2 = new String(data[i][text[1]]);
                cadenaminu2 = cadenaminu2.toLowerCase();


                if (cadenaminu1.match(cadena) || cadenaminu2.match(cadena)) {
                    //SI ENCUENTRA UNA CONCIDENCIA EN UNA FILA DEL OBJETO SE AGREGA AL ARREGLO PARA RETORNO
                    if (cont == 0 && i >= 1) cont = 0;
                    else cont = i;

                    response[cont] = data[i];

                    cont = cont + 1;
                }
            } else if (text.length == 3) {

                var cadenaminu1 = new String(data[i][text[0]]);
                cadenaminu1 = cadenaminu1.toLowerCase();
                var cadenaminu2 = new String(data[i][text[1]]);
                cadenaminu2 = cadenaminu2.toLowerCase();
                var cadenaminu3 = new String(data[i][text[2]]);
                cadenaminu3 = cadenaminu3.toLowerCase();

                if (cadenaminu1.match(cadena) || cadenaminu2.match(cadena) || cadenaminu3.match(cadena)) {
                    //SI ENCUENTRA UNA CONCIDENCIA EN UNA FILA DEL OBJETO SE AGREGA AL ARREGLO PARA RETORNO
                    if (cont == 0 && i >= 1) cont = 0;
                    else cont = i;

                    response[cont] = data[i];

                    cont = cont + 1;
                }

            } else if (text.length == 4) {
                var cadenaminu1 = new String(data[i][text[0]]);
                cadenaminu1 = cadenaminu1.toLowerCase();
                var cadenaminu2 = new String(data[i][text[1]]);
                cadenaminu2 = cadenaminu2.toLowerCase();
                var cadenaminu3 = new String(data[i][text[2]]);
                cadenaminu3 = cadenaminu3.toLowerCase();
                var cadenaminu4 = new String(data[i][text[3]]);
                cadenaminu4 = cadenaminu4.toLowerCase();

                if (cadenaminu1.match(cadena) || cadenaminu2.match(cadena) || cadenaminu3.match(cadena) || cadenaminu4.match(cadena)) {
                    //SI ENCUENTRA UNA CONCIDENCIA EN UNA FILA DEL OBJETO SE AGREGA AL ARREGLO PARA RETORNO
                    if (cont == 0 && i >= 1) cont = 0;
                    else cont = i;

                    response[cont] = data[i];

                    cont = cont + 1;
                }

            }




        } else {


            var cadenaminu = new String(data[i][texto]);
            cadenaminu = cadenaminu.toLowerCase();



            if (cadenaminu.match(cadena)) {
                //SI ENCUENTRA UNA CONCIDENCIA EN UNA FILA DEL OBJETO SE AGREGA AL ARREGLO PARA RETORNO
                if (cont == 0 && i >= 1) cont = 0;
                else cont = i;

                response[cont] = data[i];

                cont = cont + 1;


            }

        }
    }
    return response;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function validar_mail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
(function($) {
    $.fn.validCampo = function(cadena) {
        $(this).on({
            keypress : function(e){
                var key = e.which,
                    keye = e.keyCode,
                    tecla = String.fromCharCode(key).toLowerCase(),
                    letras = cadena;
                if(letras.indexOf(tecla)==-1 && keye!=9&& (key==37 || keye!=37)&& (keye!=39 || key==39) && keye!=8 && (keye!=46 || key==46) || key==161){
                    e.preventDefault();
                }
            }
        });
    };
    $(document).on('blur keyup keydown', '.text', function(){
        $(this).validCampo("abcdefghijklmnñopqrstuvwxyz ");
    });
    $(document).on('blur keyup keydown', '.number', function(){
        $(this).validCampo("1234567890");
    });
    $(document).on('blur keyup keydown', '.number-2', function(){
        $(this).validCampo("1234567890.");
    });
    $(document).on('blur keyup keydown', '.number-1', function(){
        $(this).validCampo("1234567890-");
    });
    $(document).on('blur keyup keydown', '.text-number-', function(){
        $(this).validCampo("1234567890abcdefghijklmnñopqrstuvwxyz");
    });
    $(document).on('blur keyup keydown', '.text-number-1', function(){
        $(this).validCampo("1234567890abcdefghijklmnñopqrstuvwxyz- ");
    });
   
})( jQuery );
$(document).on('click', ".btn_ver_pedido", function(){
        
        var Pedido = getDataPED($(this).attr("id_pedido"));
        
        $(".TITLEMODAL").text("PEDIDO");

        $(".cuerpo_modal_smpn").html('');
        
                var htmlsmpn ="";
                    htmlsmpn +='<div class="row">';
                        htmlsmpn +='<div class="col s12 m2">';
                            htmlsmpn +='<h6 class="text-bold">Pedido</h6>';
                            htmlsmpn +='<p class="">'+Pedido.idd+'</p>';
                        htmlsmpn +='</div>';
                        htmlsmpn +='<div class="col s12 m2">';
                            htmlsmpn +='<h6 class="text-bold">Fecha</h6>';
                            htmlsmpn +='<p class="">'+moment(Pedido.fecha_ped).format('DD/MM/YYYY')+'</p>';
                        htmlsmpn +='</div>';

                        htmlsmpn +='<div class="col s12 m3">';
                            htmlsmpn +='<h6 class="text-bold">Código de orden de compra</h6>';
                            htmlsmpn +='<p class="">'+Pedido.ord_comp_ped+'</p>';
                        htmlsmpn +='</div>';
                        htmlsmpn +='<div class="col s12 m2">';
                            htmlsmpn +='<h6 class="text-bold">Vigencia</h6> ';
                            htmlsmpn +='<p class="">'+Pedido.vig_ped+'</p>';
                        htmlsmpn +='</div>';
                        htmlsmpn +='<div class="col s12 m3 input-field mt-0">';
                            htmlsmpn +='<h6 class="text-bold">Cliente</h6>';
                            htmlsmpn +='<p class="">'+Pedido.cliente.nom_com+'</p>';
                        htmlsmpn +='</div>'; 
                    htmlsmpn +='</div>';
                    htmlsmpn +='<div class="row">'; 
                        htmlsmpn +='<div class="col s12 m2">';
                            htmlsmpn +='<h6 class="text-bold">Número de depósito</h6> ';  
                            htmlsmpn +='<p class="">'+Pedido.num_dep_ped+'</p>';
                        htmlsmpn +='</div>';            
                        htmlsmpn +='<div class="col s6 m2">';
                            htmlsmpn +='<h6 class="text-bold">Tipo de pedido</h6>';   
                            htmlsmpn +='<p class="">Nacional</p>';
                        htmlsmpn +='</div>';
                        htmlsmpn +='<div class="col s12 m2">';
                            htmlsmpn +='<h6 class="text-bold">Fecha de entrega</h6>';
                            htmlsmpn +='<p class="">'+moment(Pedido.fecha_ent).format('DD/MM/YYYY')+'</p>';
                        htmlsmpn +='</div>';
                        /*htmlsmpn +='<div class="col s12 m4">';
                            htmlsmpn +='<h6 class="text-bold">Este producto es de entrega parcial</h6>';
                            htmlsmpn +='<p class="">'+data[0].tip_ent_ped+'</p>';
                        htmlsmpn +='</div>';*/
                    htmlsmpn +='</div>';
                    htmlsmpn +='<div class="row">';
                        htmlsmpn +='<div class="col s12 m12">';
                            htmlsmpn +='<h6 class="text-bold">Observaciones</h6>';    
                            htmlsmpn +='<p class="">'+Pedido.obs_ped+'</p>';
                        htmlsmpn +='</div>';
                        
                    htmlsmpn +='</div>';

                    
                $(".cuerpo_modal_smpn").html(htmlsmpn);  
                
                obtenerDetallesPEDIDOS(Pedido);
                
           
    });
    function obtenerDetallesPEDIDOS(Pedido,tabla) {
    
        
        var data = {
            ida: user.id,
            token: user.token
        }
        $.ajax({
            url: dominio + "pedidos/"+Pedido.id+"/detalles/productos",
            type: "GET",
            data: data,
            dataType: "json",
            success: function(data) {
                 
                obtenerProductosP(data,tabla);
            },
            error: function(xhr, status, errorThrown) {
                console.log(xhr.responseJSON);
            }
        });
    }
    function obtenerProductosP(data,tabla){
        var table = tabla_pro;
        if(tabla){
            table = tabla
        }
        table.clear().draw();
        for (var i = 0; i < data.length; i++) {
            var obj = data[i];
            var row = "";
            var row = [obj.cant_ped_pro,'KG', obj.cod_pro, obj.nom_pro , formato.precio(obj.precio), formato.precio((obj.cant_ped_pro*obj.precio)), obj.und_desp_ped_pro, obj.und_falt_ped_pro];
            table.row.add(row).draw().node();
        }
    }

    var numeroALetras = (function() {

// Código basado en https://gist.github.com/alfchee/e563340276f89b22042a
    function Unidades(num){

        switch(num)
        {
            case 1: return 'UN';
            case 2: return 'DOS';
            case 3: return 'TRES';
            case 4: return 'CUATRO';
            case 5: return 'CINCO';
            case 6: return 'SEIS';
            case 7: return 'SIETE';
            case 8: return 'OCHO';
            case 9: return 'NUEVE';
        }

        return '';
    }//Unidades()

    function Decenas(num){

        let decena = Math.floor(num/10);
        let unidad = num - (decena * 10);

        switch(decena)
        {
            case 1:
                switch(unidad)
                {
                    case 0: return 'DIEZ';
                    case 1: return 'ONCE';
                    case 2: return 'DOCE';
                    case 3: return 'TRECE';
                    case 4: return 'CATORCE';
                    case 5: return 'QUINCE';
                    default: return 'DIECI' + Unidades(unidad);
                }
            case 2:
                switch(unidad)
                {
                    case 0: return 'VEINTE';
                    default: return 'VEINTI' + Unidades(unidad);
                }
            case 3: return DecenasY('TREINTA', unidad);
            case 4: return DecenasY('CUARENTA', unidad);
            case 5: return DecenasY('CINCUENTA', unidad);
            case 6: return DecenasY('SESENTA', unidad);
            case 7: return DecenasY('SETENTA', unidad);
            case 8: return DecenasY('OCHENTA', unidad);
            case 9: return DecenasY('NOVENTA', unidad);
            case 0: return Unidades(unidad);
        }
    }//Unidades()

    function DecenasY(strSin, numUnidades) {
        if (numUnidades > 0)
            return strSin + ' Y ' + Unidades(numUnidades)

        return strSin;
    }//DecenasY()

    function Centenas(num) {
        let centenas = Math.floor(num / 100);
        let decenas = num - (centenas * 100);

        switch(centenas)
        {
            case 1:
                if (decenas > 0)
                    return 'CIENTO ' + Decenas(decenas);
                return 'CIEN';
            case 2: return 'DOSCIENTOS ' + Decenas(decenas);
            case 3: return 'TRESCIENTOS ' + Decenas(decenas);
            case 4: return 'CUATROCIENTOS ' + Decenas(decenas);
            case 5: return 'QUINIENTOS ' + Decenas(decenas);
            case 6: return 'SEISCIENTOS ' + Decenas(decenas);
            case 7: return 'SETECIENTOS ' + Decenas(decenas);
            case 8: return 'OCHOCIENTOS ' + Decenas(decenas);
            case 9: return 'NOVECIENTOS ' + Decenas(decenas);
        }

        return Decenas(decenas);
    }//Centenas()

    function Seccion(num, divisor, strSingular, strPlural) {
        let cientos = Math.floor(num / divisor)
        let resto = num - (cientos * divisor)

        let letras = '';

        if (cientos > 0)
            if (cientos > 1)
                letras = Centenas(cientos) + ' ' + strPlural;
            else
                letras = strSingular;

        if (resto > 0)
            letras += '';

        return letras;
    }//Seccion()

    function Miles(num) {
        let divisor = 1000;
        let cientos = Math.floor(num / divisor)
        let resto = num - (cientos * divisor)

        let strMiles = Seccion(num, divisor, 'UN MIL', 'MIL');
        let strCentenas = Centenas(resto);

        if(strMiles == '')
            return strCentenas;

        return strMiles + ' ' + strCentenas;
    }//Miles()

    function Millones(num) {
        let divisor = 1000000;
        let cientos = Math.floor(num / divisor)
        let resto = num - (cientos * divisor)

        let strMillones = Seccion(num, divisor, 'UN MILLON DE', 'MILLONES DE');
        let strMiles = Miles(resto);

        if(strMillones == '')
            return strMiles;

        return strMillones + ' ' + strMiles;
    }//Millones()

    return function NumeroALetras(num, currency) {
        currency = currency || {};
        let data = {
            numero: num,
            enteros: Math.floor(num),
            centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
            letrasCentavos: '',
            letrasMonedaPlural: currency.plural || 'DÓLARES',//'PESOS', 'Dólares', 'Bolívares', 'etcs'
            letrasMonedaSingular: currency.singular || 'DÖLAR', //'PESO', 'Dólar', 'Bolivar', 'etc'
            letrasMonedaCentavoPlural: currency.centPlural || 'CENTAVOS',
            letrasMonedaCentavoSingular: currency.centSingular || 'CENTAVO'
        };

        if (data.centavos > 0) {
            data.letrasCentavos = 'CON ' + (function () {
                    if (data.centavos == 1)
                        return Millones(data.centavos) + ' ' + data.letrasMonedaCentavoSingular;
                    else
                        return Millones(data.centavos) + ' ' + data.letrasMonedaCentavoPlural;
                })();
        };

        if(data.enteros == 0)
            return 'CERO ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
        if (data.enteros == 1)
            return Millones(data.enteros) + ' ' + data.letrasMonedaSingular + ' ' + data.letrasCentavos;
        else
            return Millones(data.enteros) + ' ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
    };

})();

// Modo de uso: 500,34 USD
numeroALetras(500.34, {
  plural: 'dólares estadounidenses',
  singular: 'dólar estadounidense',
  centPlural: 'centavos',
  centSingular: 'centavo'
});

formato = {
            separador: ',', // separador para los miles
            sepDecimal: '.', // separador para los decimales
            formatear:function (num){
                num +='';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                }
                return this.simbol + splitLeft  +splitRight;
            },
            precio:function(num, simbol){
                this.simbol = simbol ||'';
                
                var num = num.toString();
                num=num.replace(/,/g, '.');
                num=Math.round(num * 100) / 100;
                
                
                return this.formatear(num.toFixed(2));
    
            }
}
