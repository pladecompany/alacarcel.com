<!--
Copyright 2020, Ivan Vanney. 


This file is part of alacarcel.com.

    Alacarcel.com is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License (GPL-3.0-or-later) as published by
    the Free Software Foundation, either version 3 of the License, or
   any later version.

    Alacarcel.com is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Alacarcel.com.  If not, see <https://www.gnu.org/licenses/>.
	--!>


<!DOCTYPE html>
<?php
	
	 //YA NO ES NECESARIO COMENTAR ESTO PORQUE VALIDA SI ESTA EN EL SERVER O EN LOCAL
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	if($_SERVER['SERVER_NAME']=="denunciapp.com"){	
	 if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
		 $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		 header('HTTP/1.1 301 Moved Permanently');
		 header('Location: ' . $redirect);
		 exit();
	 }
	}

include_once('ruta.php');
?>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- For iPhone -->
	<meta name="msapplication-TileColor" content="#00bcd4">
	<meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
	<?php 
		if(isset($_GET['viewcs'])){
			$dominioweb = "https://pladecompany.com/demos/denunciapp/";
			
			function llamarApi($url, $method){
				$dominio = "https://inthecompanies.com:8200/";
				//$dominio = "http://localhost:8200/";
				$url = $dominio.$url;

				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL => $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => $method,
					CURLOPT_POSTFIELDS => null
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);
				curl_close($curl);
				$data = json_decode($response, true);
				$response = $data;
				return $response;
			}
			$idc = $_GET['viewcs'];
			$url = "casos/buscar-caso-id/".$idc;     
			$caso = llamarApi($url, "GET")[0];
			echo '<meta property="og:url"           content="'.$dominioweb.'?op=listadodecasos&viewcs='.$caso['id'].'" />';
  			echo '<meta property="og:type"          content="website" />';
  			echo '<meta property="og:title"         content="'.$caso['nom_caso'].'" />';
  			echo '<meta property="og:description"   content="'.$caso['des_caso'].'" />';
  			echo '<meta property="og:image"         content="'.$dominioweb.'static/img/logo.png" />';
			 
		}
	?>
	<title>alacarcel</title>
	<link rel="shortcut icon" href="static/img/logo.png" />
	<link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">

	<!-- CORE CSS   --> 
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/materialize.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/style.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/scroll.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/flickity/css/flickity.css"> 
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/flickity/css/flickityStyle.css">

	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/jquery.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/dataTables.bootstrap.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/responsive.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/select2/select2.min.css"/>
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/font-awesome/css/all.min.css">
	<link href="static/js/quill/quill.css" rel="stylesheet">
	<script type="text/javascript" src="static/js/jquery.min.js"></script>
	<script type="text/javascript">paneluser = 'usuario';</script>

	<?php include_once("analytics.php");?>
</head>

<body>
	<?php 
		if ($_GET["op"] != "bienvenido") {
	?>

	<header>
		<nav class="red lighten-2" role="navigation">
			<div class="nav-wrapper">
				<a href="?op=listadodecasos" id="link_principal">
					<img src="static/img/logo.png" class="brand-logo">
				</a>
				
				<a class="button-collapse" data-activates="nav_mobile" href="javascript:;">
					<!--<i class="fa fa-bars"></i>-->
				</a>

				<ul class="right" style="margin-bottom: -1rem;" id="ulprincipal">
				
					<ul class="right hab_niv_usu hab_niv_abo hide" id="log">
						<!--<li class="pais">
							<a class="dropdown-trigger" data-target="configuracion1" style="margin-top: -.4rem;">
								<img src="" width="20px" style="vertical-align: middle;" id="img_pais" class="hide"> <b id="nombre">País</b> &nbsp; <i class="fa fa-caret-down"></i>
							</a>
							<ul id="configuracion1" class="dropdown-content dp3">
							</ul>
						</li>-->
						<!--OPCIONES COMO USUARIO-->
						<li>
							<a href="?op=listadodecasos&list=publicados" class="text-bold hab_niv_usu hide">Mis casos</a>
						</li>
						<li>
							<a href="?op=listadodecasos&list=unidos" class="text-bold hab_niv_usu hide">Me he unido</a>
						</li>

						<!--OPCIONES COMO ABOGADO-->
						<li>
							<a href="?op=pagosrecibidos" class="text-bold hab_niv_abo hab_niv_usu hide">Pagos recibidos</a>
						</li>
						<li>
							<a href="?op=pagosrealizados" class="text-bold hab_niv_usu hide">Pagos realizados</a>
						</li>
						<li>
							<a href="?op=listadodecasos&list=representados" class="text-bold hab_niv_abo hide">He representado</a>
						</li>
						<li>
							<a href="?op=listadodecasos&list=postulados" class="text-bold hab_niv_abo hide">Me he postulado</a>
						</li>
						<li id="noti">
						
						</li>

						<li>
							<a class="dropdown-trigger" data-target="configuracion">
								<img src="static/img/user.png"  width="50px" alt="" class="circle" id="img-perfil" style="vertical-align: middle; width:50px;height:50px;border:2px solid var(--blue);object-fit: cover;object-position: top center;" >
							</a>
							
							<ul id="configuracion" class="dropdown-content dp3">
								<li><a href="?op=perfil">Perfil</a></li>
								<li><a href="?op=cambiarclave">Cambiar contraseña</a></li>
								<li><a href="#modal-salir" class="waves-effect waves-block waves-light modal-trigger">Salir</a></li>
							</ul>
						</li>
					</ul>
					
					
					<li class="log hide">
						<a class="dropdown-trigger" data-target="configuracion">
							<img src="static/img/user.png" width="40px" alt="" class="circle img_nav hide" >
						</a>
						<div class="nom_log">
							<span class="nom_logueado"></span>
						</div>
						
						<ul id="configuracion" class="dropdown-content dp3 text-bold">
							<li><a href="?op=perfil">Perfil</a></li>
							<li><a href="?op=cambiarclave">Cambiar contraseña</a></li>
							<li><a href="#modal-salir" class="waves-effect waves-block waves-light modal-trigger">Salir</a></li>
						</ul>
					</li>
				
					<li class="no_log hide display-mv">
						<a href="?op=inicio" class="dropdown-button text-bold" href="javascript:;">
							Registro
						</a>
					</li>
 
					<li class="no_log hide display-mv">
						<a href="?op=iniciarsesion" class="dropdown-button text-bold" href="javascript:;">
							Ingresar
						</a>
					</li>
					
					<!-- mobile-->
					<li class="no_log hide display-lp">
						<a href="?op=inicio" class="dropdown-button text-bold" href="javascript:;">
							<i class="fa fa-address-card"></i>
						</a>
					</li>
 
					<li class="no_log hide display-lp">
						<a href="?op=iniciarsesion" class="dropdown-button text-bold" href="javascript:;">
							<i class="fa fa-user-circle"></i>
						</a>
					</li>
					<!--FIN DE OPCIONES NO LOGUEADO-->
				</ul>

				<ul class="right">
					 
				</ul>
			</div>
		</nav>
	</header>


	<div id="modal-salir" class="modal modal-salir">
		<div class="modal-content">
			<center><img src="static/img/key.png"></center>
			<h5 class="text-center"><i class="mdi-hardware-security"></i> ¿Desea cerrar sesión?</h5>
			<div class="text-center">
				<a href="vistas/salir.php" id="" class="btn btn-blue modal-action bt_salir">Sí <i class="fa fa-check-circle"></i></a>
				<button class="btn btn-blue modal-action modal-close">No <i class="fa fa-times-circle"></i></button>
			</div>
		</div>
	</div>

	<div id="modal-contacto" class="modal" style="width:50%;">
		<div class="modal-header">
			<div class="boxHead mt-0">
				<h6 id="title"><i class="fa fa-comments"></i> Contáctanos</h6>
				<a href="#" class="right modal-close white-text mr-4"><i class="fas fa-times fa-2x btnCerrar"></i></a>
			</div>
		</div>
		<div class="modal-content">
			<form action="" method="" id="formMail" onsubmit="return false;">
				<div class="row mb-0">
					<div class="col s12">
						<div class="input-field">
							<p>Correo</p>
							<input id="correo_contacto" name="cor_con" type="email" class="form-app" placeholder="Correo">
						</div>
					</div>
				</div>

				<div class="row mb-0">
					<div class="col s12">
						<div class="input-field">
							<p>Asunto</p>
							<input id="asunto" name="asu_con" type="text" class="form-app" placeholder="Asunto">
						</div>
					</div>
				</div>

				<div class="row mb-0">
					<div class="col s12">
						<div class="input-field">
							<p>Mensaje</p>
							<textarea id="mensaje" name="msj_con" type="text" class="form-app textarea" ></textarea>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col s12 center">
						<a href="#" onclick="return false" class="btn btn-blue savee">Enviar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

	<?php  } ?>
		<?php
		include_once($ruta);
		?>
	<?php 
		if ($_GET["op"] != "bienvenido") {
	?>

	<footer class="footer pb-1">
		<div class="container">
			<div class="row m-0 text-center">
				<div class="col s12 pt-4">
					<a href="" target="_blank" class="clr_black"><i class="fab fa-facebook fa-2x"></i></a>
					<a href="" target="_blank" class="clr_black"><i class="fab fa-instagram fa-2x"></i></a>
				</div>
			</div>

			<div class="row mb-0">
				<div class="col s12 text-center">
					<p class="text-bold m-2">© 2020 alacarcel.com</p>
				</div>
			</div>
		</div>
	</footer>

	<?php  } ?>


	<script type="text/javascript" src="static/js/materialize.min.js"></script>
	<script src="static/js/jquery.masknumber.min.js"></script>
	<script type="text/javascript" src="static/js/mask.js"></script>
	<script src="static/js/quill/highlight.min.js"></script>
	<script src="static/js/quill/quill.js"></script>
	<script src="static/js/quill/image-resize.min.js"></script>
	<script type="text/javascript" src="static/js/menu.js"></script>
	<script type="text/javascript" src="static/js/moment.min.js"></script>
	<script type="text/javascript" src="static/js/moment.es.js"></script>
	<script type="text/javascript" src="static/js/backend-panel/general.js"></script>
	<script type="text/javascript" src="static/js/socket/socket.js"></script>
	<script type="text/javascript" src="static/js/backend-panel/validarCampo.js"></script>
	<script type="text/javascript" src="static/js/main.js"></script>
	
	<script type="text/javascript" src="static/lib/JTables/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" src="static/lib/select2/select2.min.js"></script>
	<?php 
		if ($_GET["op"] != "pagado" && $_GET["op"] != "cancelado") {
	?>
		<script type="text/javascript" src="static/js/backend-panel/socket_events.js"></script>
	<?php  
		} 
	?>

<style type="text/css">
	.backdrop{
		background: #00b0f0;
	}
</style>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.Amount').maskNumber();
			$(document).on('blur', '.Amount', function(){
		        if(this.value.trim().length<=3){
		        	if(this.value.trim().length==3)
		        		this.value = formato.precio(this.value.split(",")[1]);
		        	else
		        		this.value = formato.precio(this.value);
		        } 
		    });
			$('.tooltipped').tooltip();
			$('.tooltipped').tooltip({delay: 50}).each(function () {
				var background = $(this).data('background-color');
				if (background) {
					$("#" + $(this).data('tooltip-id')).find(".backdrop").addClass(background);
				};
				var text = $(this).data('text-color');
				if (text) {
					$("#" + $(this).data('tooltip-id')).find("span").addClass(text);
				};
			});
			if($('.quill-edit').length>0){
						var container = $('.quill-edit').get(0);
						var toolbar_quill = ` 
						<!--<span class="ql-formats">
					      <select class="ql-font"></select>
					      <select class="ql-size"></select>
					    </span>-->
					    <span class="ql-formats">
					      <button class="ql-bold"></button>
					      <button class="ql-italic"></button>
					      <button class="ql-underline"></button>
					      <!--<button class="ql-strike"></button>-->
					    </span>
					    <!--<span class="ql-formats">
					      <select class="ql-color"></select>
					      <select class="ql-background"></select>
					    </span>
					    <span class="ql-formats">
					      <button class="ql-script" value="sub"></button>
					      <button class="ql-script" value="super"></button>
					    </span>
					    <span class="ql-formats">
					      <button class="ql-header" value="1"></button>
					      <button class="ql-header" value="2"></button>
					      <button class="ql-blockquote"></button>
					      <button class="ql-code-block"></button>
					    </span>
					    <span class="ql-formats">
					      <button class="ql-list" value="ordered"></button>
					      <button class="ql-list" value="bullet"></button>
					      <button class="ql-indent" value="-1"></button>
					      <button class="ql-indent" value="+1"></button>
					    </span>-->
					    <span class="ql-formats">
						<!--<button class="ql-direction" value="rtl"></button>-->
					      <select class="ql-align"></select>
					    </span>
					    <!--<span class="ql-formats">
					      <button class="ql-link"></button>
					      <button class="ql-image"></button>
					      <button class="ql-video"></button>
					      <button class="ql-formula"></button>
					      
					    </span>
					    <span class="ql-formats">
					      <button class="ql-clean"></button>
					      <button class="ql-showHtml"></button>
					    </span>-->`; 
						 
					    $(".toolbar-quill-edit").html(toolbar_quill);
						  quill = new Quill(container, {
						    modules: {
						      syntax: true,
						      toolbar: '.toolbar-quill-edit',
						      imageResize: {
						      		displaySize: true,
						            toolbarStyles: {
						            	
						                backgroundColor: 'black',
						                border: 'none',
						                color: 'white'
						            },
						            toolbarButtonStyles: {
						            },
						            toolbarButtonSvgStyles: {
						            },
					        	},
						    },
						    placeholder: "Ingrese su descripción aquí",
						    theme: 'snow'
						  });
						  var txtArea = document.createElement('textarea');
							txtArea.style.cssText = "min-height: 200px;z-index:100;width: 100%;margin-left:-0.1rem;background: rgb(29, 29, 29);box-sizing: border-box;color: rgb(204, 204, 204);font-size: 15px;outline: none;padding: 20px;line-height: 24px;font-family: Consolas, Menlo, Monaco, &quot;Courier New&quot;, monospace;position: absolute;top: 0;bottom: 0;border: none;display:none"

							var htmlEditor = quill.addContainer('ql-custom')
							htmlEditor.appendChild(txtArea)

							var myEditor = document.querySelector('.quill-edit');
							const characterLimit = 3501; 
							quill.on('text-change', (delta, oldDelta, source) => {
							  var html = myEditor.children[0].innerHTML;
							  txtArea.value = html;
								$('.character-count').text((characterLimit - quill.getLength()));
								if (quill.getLength() <= characterLimit) {
									return;
								}   
								
								const { ops } = delta;
								let updatedOps;
								if (ops.length === 1) {
									// text is inserted at the beginning
									updatedOps = [{ delete: ops[0].insert.length }]; 
								} else {
									// ops[0] == {retain: numberOfCharsBeforeInsertedText}
									// ops[1] == {insert: "example"}
									updatedOps = [ops[0], { delete: ops[1].insert.length }]; 
								}   
								quill.updateContents({ ops: updatedOps });
								
							})

							var customButton = document.querySelector('.ql-showHtml');
							if(customButton){
								customButton.addEventListener('click', function() {
									if (txtArea.style.display === '') {
										var html = txtArea.value
										quill.pasteHTML(html);
										
									}
									txtArea.style.display = txtArea.style.display === 'none' ? '' : 'none'
								});
							}
					}
		});

	</script>

	<script>
		 
		if(op != "bienvenido"){
			if(user){
				if(op == 'inicio' || op == 'iniciarsesion' || op == 'crearcuenta_vac' || op == 'crearcuenta'){
					location.href = '?op=listadodecasos';
				}
				if(user.imagen != null){
					$('#img-perfil').attr('src',user.imagen)
				}
				$('#img-perfil').on('error',function(){
					$(this).attr('src','static/img/user.png')
				})
				$('.log').removeClass('hide')
				
				if(user.rol == 'Usuario' || user.rol == 'Abogado'){
					 
					$("#correo_contacto").val(user.cor_usu);
					$('.postulado').removeClass('hide');
					if(user.nom_usu){
						var nom = user.nom_usu.split(" ");
						nom = nom[0];
						if(nom.length>6){
						  if(nom.length==8)
							nom = nom;
						  else
							nom = nom.substr(0,8)+"..";
						}
						$(".nom_logueado").text(nom);
					}
					
					CargarNotiAcoun = function CargarNotiAcoun(data) {
						var html = `
							<li>
								<a href="?op=notificaciones" class="dropdown-button" id="icon_noti" href="javascript:;" style="margin-top: -.2rem;">
									<i class="fa fa-bell fa-2x" style="color:#888;"></i>
								</a>
							</li>` 
						$('#noti').html(html)
						$.ajax({
							url: dominio + "notificaciones/count",
							type: "post",
							dataType: "json",
							data:{id:user.id},
							success: function(data){
								if(data[0].count >0){
									var html = `
									<li>
										<a href="?op=notificaciones" class="dropdown-button" id="icon_noti" href="javascript:;"  margin-top: -.4rem;>
											<i class="fa fa-bell" style="color:#2196F3;"></i><span class="count_bell" style="border-radius:50%;color:#2196F3;padding:4px;">${data[0].count}</span>
										</a>
									</li>` 
									$('#noti').html(html)
								}
								
							},
							error: function(xhr, status, errorThrown){
							console.log("Errr : "+status);
							}
						});
					}
					CargarNotiAcoun();
					
				}
				//$(".Footerdiv").addClass('down');
				//$('#footerlg').removeClass('hide');
				$('#footerr').removeClass('hide');
				$('#footerr').addClass('fotterLOG');
			 
			}else{
				$('#footerr').removeClass('hide');
				$('.no_log').removeClass('hide');
			}
			function paisget(id){
				for (var i = 0; i < TODOSPAIS.length; i++) {
					if(TODOSPAIS[i].id==id) return TODOSPAIS[i];
				}				
				return false;	
			}
			var TODOSPAIS = [];
				$.ajax({
					url: dominio+"paises/activos",
					type:'get',
					success:function(data){
						 
						TODOSPAIS = data;
						for(var i= 0;i<data.length;i++){
							datos = data[i]
							$('#configuracion1').append(`
								<li><a href="#!" 
									class="paiss"
									id="${datos.id}" 
									name="${datos.nombre}" 
									imagen="${datos.imagen}"><img src="${datos.imagen}" width="20px"> <b>${datos.nombre}</b></a></li>
							`)
							$('#paisss').append(`
								<option value="${datos.id}">${datos.nombre}</option>
							`)
						}
						if(user){
							if(user.id_pais){
								datap = {
									id:paisget(user.id_pais).id,
									nombre:paisget(user.id_pais).nombre,
									imagen:paisget(user.id_pais).imagen
								}
									window.localStorage.setItem("pais", JSON.stringify(datap));
							}
						}
						
						pais = JSON.parse(window.localStorage.getItem('pais'));
						if(pais){
							$('#nombre').html(pais.nombre)
							$('#img_pais').attr('src',pais.imagen);
							$('#img_pais').removeClass('hide');
						}else{
							//$('.paiss')[0].click()
						}
					}
				})
				getPais()
				function getPais(){
					pais = JSON.parse(window.localStorage.getItem('pais'));
					if(pais){
						$('#nombre').html(pais.nombre)
						$('#img_pais').attr('src',pais.imagen);
						$('#img_pais').removeClass('hide');
					}
				//}
					$(document).ready(function () {
						$(document).on('click','.paiss',function(){
							data = {
								id:this.id,
								nombre:this.name,
								imagen:$(this).attr('imagen')
							}
								window.localStorage.setItem("pais", JSON.stringify(data));
								<?php if($_GET['op']!='recuperar-clave' && $_GET['op']!='verificar-usuarios' && $_GET['op']!='verificar-reclutadores' && $_GET['op']!='PDF') { ?>
										location.href="?op=inicio";
								<?php } ?>	
						});
					});
					
					

			}
		}
		function validar_mail(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
		}
		$(document).on('click','.savee',function(){

			if(!validar_mail($("#correo_contacto").val())){
				M.toast({html:"Verifica tu correo e ingresalo de nuevo."})
				return;
			}
			if($("#asunto").val().trim()==""){
				M.toast({html:"Ingresa el asunto."})
				return;
			}

			if($("#mensaje").val().trim()==""){
				M.toast({html:"Ingresa el mensaje."})
				return;
			}
				$.ajax({
					url: dominio+"contactanos",
					type:'post',
					data:{correo:$("#correo_contacto").val(),asunto:$("#asunto").val(),mensaje:$("#mensaje").val()},
					success:function(data){
						if(data.r){
							$("#modal-contacto").modal("close");
							$('#formMail')[0].reset();
							M.toast({html:"Mensaje enviado, gracias por contactarnos"});
						}
					},
					error:function(err){

					}
				});
		})	
		


	  function mostrarContrasena(idname){
	  	  if(idname)
		  	var tipo = document.getElementById(idname);
		  else
		  	var tipo = document.getElementById("password_neg") || document.getElementById("password_vac");
		  if(tipo.type == "password"){
			  tipo.type = "text";
		  }else{
			  tipo.type = "password";
		  }
	  }

	  
	  if(window.localStorage.getItem('nueva_postulacion')){
		if(window.localStorage.getItem('nueva_postulacion')=="e30="){
		window.localStorage.setItem("nueva_postulacion", JSON.stringify({}));
		}
	  }
	</script>


</body>
</html>
